<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class PollutionModel extends CI_Model {


function view()
{
	$sql = "SELECT pol_title,pol_thumb_img,pol_status,pol_id FROM `tbl_pollutions`";
    // echo $sql;die;
	$query=$this->db->query($sql);
	return $query->result();
}

public function getstatusWhereLike($filterstatus) {
        $sql = "SELECT * FROM tbl_pollutions WHERE pol_status= '$filterstatus'";
        $query = $this->db->query($sql);
        return $query->result();
    }

function add($service_title,$service_desc,$status,$imageone,$imagetwo,$price,$selling_price){
	$u_createdby = $this->session->userdata('usersid');

	$sql = "INSERT INTO `tbl_pollutions`(`pol_title`, `pol_description`,`pol_thumb_img`, `pol_img`, `pol_status`, `price`, `selling_price`) VALUES (".$this->db->escape($service_title).",".$this->db->escape($service_desc).",".$this->db->escape($imageone).",".$this->db->escape($imagetwo).",".$this->db->escape($status).",".$this->db->escape($price).",".$this->db->escape($selling_price)."
	)";
	$query=$this->db->query($sql);
	if($query==1){
		return true;
	}
	else
	{
		return false;
	}
}

function update($service_title,$service_desc,$status,$imageone,$imagetwo,$pol_id,$price,$selling_price){
  
    $userid = $this->session->userdata('usersid');
$sql = "UPDATE `tbl_pollutions` SET `pol_title`=".$this->db->escape($service_title).",`pol_description`=".$this->db->escape($service_desc).",`pol_status`=".$this->db->escape($status)." 
,`pol_thumb_img`=".$this->db->escape($imageone).",`pol_img`=".$this->db->escape($imagetwo).",`price`=".$this->db->escape($price).",`selling_price`=".$this->db->escape($selling_price)."
WHERE pol_id=".$this->db->escape($pol_id)."";
// echo $sql;die;
if ($this->db->simple_query($sql))
{
	$respose = $this->db->affected_rows();
	return $respose;
}
else
{
       return false;
}
}

function loaddata($id){
	$sql = "SELECT * FROM `tbl_pollutions` where pol_id='$id'";

	$query = $this->db->query($sql);
  
	return $query->result();
}
function loaddata1($id){
	$sql = "SELECT * FROM `tbl_pollutions` where pol_id='$id'";
	$query = $this->db->query($sql);
  	return $query->result();
}

function getUserDetails(){
    $response = array();
    $sql = "SELECT `pol_id`, `pol_title`, `pol_description`, `pol_status`, `price`, `selling_price` FROM `tbl_pollutions`";
    // echo $sql;die;
  $query=$this->db->query($sql);
    $response = $query->result_array();
    return $response;
  }



}