<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class DashboardModel extends CI_Model {

function musers()
{
	$sql = "SELECT count(muser_id) as muser_id FROM `tbl_m_users`";
	$query=$this->db->query($sql);
	return $query->result_array();
}
function supplier()
{
	$sql = "SELECT count(puser_id) as puser_id FROM `tbl_p_users` where puser_type='Supplier'";
	$query=$this->db->query($sql);
	return $query->result_array();
}
function technician()
{
	$sql = "SELECT count(puser_id) as puser_id FROM `tbl_p_users` where puser_type='Technician'";
	$query=$this->db->query($sql);
	return $query->result_array();
}

function service()
{
	$sql = "SELECT count(ser_id) as service FROM `tbl_service`";
	$query=$this->db->query($sql);
	return $query->result_array();
}
function spare()
{
	$sql = "SELECT count(sp_id) as spare FROM `tbl_spare`";
	$query=$this->db->query($sql);
	return $query->result_array();
}

function users()
{
	$sql = "SELECT count(cust_id) as users FROM `tbl_cust`";
	$query=$this->db->query($sql);
	return $query->result_array();
}

function sellers()
{
	$sql = "SELECT count(u_id) as sellers FROM `tbl_user` where u_role='Seller'";
	$query=$this->db->query($sql);
	return $query->result_array();
}

function deliveryagents()
{
	$sql = "SELECT count(da_id) as deliveryagents FROM `tbl_deliveryagent`";
	$query=$this->db->query($sql);
	return $query->result_array();
}

function orders()
{
	$sql = "SELECT count(order_id) as orders FROM `tbl_orders`";
	$query=$this->db->query($sql);
	return $query->result_array();
}

function pending_orders()
{
	$sql = "SELECT count(order_id) as pending_orders FROM `tbl_orders` where order_status='Pending'";
	$query=$this->db->query($sql);
	return $query->result_array();
}

function intransit_orders()
{
	$sql = "SELECT count(order_id) as intransit_orders FROM `tbl_order` where order_status='Intransist'";
	$query=$this->db->query($sql);
	return $query->result_array();
}

function completed_orders()
{
	$sql = "SELECT count(order_id) as completed_orders FROM `tbl_orders` where order_status='Completed'";
	$query=$this->db->query($sql);
	return $query->result_array();
}
function completed_ongoing()
{
	$sql = "SELECT count(order_id) as completed_ongoing FROM `tbl_orders` where order_status='ongoing'";
	$query=$this->db->query($sql);
	return $query->result_array();
}

function total_revenue()
{
	$sql = "SELECT SUM(od_service_price) as total_revenue FROM tbl_orderdetails";
	$query=$this->db->query($sql);
	return $query->result_array();
}

function products_categories()
{
	$sql = "SELECT count(prod_id) as products,(SELECT count(procat_id) as categories FROM `tbl_productcategory`) as categories FROM `tbl_products`";
	$query=$this->db->query($sql);
	return $query->result_array();
}

}