<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class BannerModel extends CI_Model {


function view()
{
	$sql = "SELECT ban_type,ban_thumb_img,ban_title,ban_id,ban_status FROM `tbl_banners`";
    // echo $sql;die;
	$query=$this->db->query($sql);
	return $query->result();
}
public function getstatusWhereLike($filterstatus) {
        $sql = "SELECT * FROM tbl_banners WHERE ban_status= '$filterstatus'";
        $query = $this->db->query($sql);
        return $query->result();
    }

function add($bannertitle,$banner_desc,$bannertype,$status,$imageone,$imagetwo){
	$u_createdby = $this->session->userdata('usersid');

	$sql = "INSERT INTO `tbl_banners`(`ban_title`, `ban_description`,`ban_type`, `ban_img`, `ban_thumb_img`, `ban_status`) VALUES (".$this->db->escape($bannertitle).",".$this->db->escape($banner_desc).",".$this->db->escape($bannertype).",".$this->db->escape($imagetwo).",".$this->db->escape($imageone).",".$this->db->escape($status)."
	)";
	$query=$this->db->query($sql);
	if($query==1){
		return true;
	}
	else
	{
		return false;
	}
}

function update($bannertitle,$banner_desc,$bannertype,$status,$imageone,$imagetwo,$ban_id){
  
    $userid = $this->session->userdata('usersid');
$sql = "UPDATE `tbl_banners` SET `ban_title`=".$this->db->escape($bannertitle).",`ban_description`=".$this->db->escape($banner_desc).",`ban_type`=".$this->db->escape($bannertype)." ,`ban_status`=".$this->db->escape($status)." 
,`ban_thumb_img`=".$this->db->escape($imageone).",`ban_img`=".$this->db->escape($imagetwo)."
WHERE ban_id=".$this->db->escape($ban_id)."";
// echo $sql;die;
if ($this->db->simple_query($sql))
{
	$respose = $this->db->affected_rows();
	return $respose;
}
else
{
       return false;
}
}

function loaddata($id){
	$sql = "SELECT * FROM `tbl_banners` where ban_id='$id'";

	$query = $this->db->query($sql);
  
	return $query->result();
}
function loaddata1($id){
	$sql = "SELECT * FROM `tbl_banners` where ban_id='$id'";
	$query = $this->db->query($sql);
  	return $query->result();
}

function getUserDetails(){
    $response = array();
    $sql = "SELECT `ban_id`, `ban_title`, `ban_description`, `ban_type`, `ban_status` FROM `tbl_banners`";
    // echo $sql;die;
  $query=$this->db->query($sql);
    $response = $query->result_array();
    return $response;
  }


}