<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class QuickInspectionModel extends CI_Model {

function view()
{
	$sql = "SELECT
              tbl_quickinspection.id,tbl_quickinspection.userID,tbl_quickinspection.vehicleid,
              tbl_m_users.muser_name,remark,status,inspectiontype
            FROM tbl_m_users
            JOIN tbl_quickinspection
            ON tbl_m_users.muser_id = tbl_quickinspection.userID
            JOIN tbl_vehicle
            ON tbl_vehicle.vehb_id = tbl_quickinspection.vehicleid";
    // echo $sql;die;
	$query=$this->db->query($sql);
	return $query->result();
}

public function getstatusWhereLike($filterstatus) {
        $sql = "SELECT * FROM tbl_quickinspection WHERE status= '$filterstatus'";
        $query = $this->db->query($sql);
        return $query->result();
    }

function loaddata1($id,$userID){
	$sql = "SELECT muser_name,status,remark FROM `tbl_quickinspection` Right join tbl_m_users on tbl_quickinspection.userID=tbl_m_users.muser_id where tbl_quickinspection.userID='$userID'";

	$query = $this->db->query($sql);
  
	return $query->result();
}
function loaddata2($id,$vehicleid){
	$sql = "SELECT vehb_type,vehb_make,vehb_model,vehb_trim,vehb_fuel,vehb_image,vehb_status
	        FROM `tbl_quickinspection` Right join tbl_vehicle on 
	        tbl_quickinspection.vehicleid=tbl_vehicle.vehb_id 
	        where tbl_quickinspection.vehicleid='$vehicleid'";
	$query = $this->db->query($sql);
  	return $query->result();
}

function getUserDetails(){
    $response = array();
    //$this->db->select('id,userID,inspectiontype,vehicleid,status,remark,createdat');
    //$q = $this->db->get('tbl_quickinspection');
    $sql = "SELECT
              tbl_quickinspection.id,
              tbl_m_users.muser_name,inspectiontype,tbl_vehicle.vehb_make,status,remark,createdat
            FROM tbl_m_users
            JOIN tbl_quickinspection
            ON tbl_m_users.muser_id = tbl_quickinspection.userID
            JOIN tbl_vehicle
            ON tbl_vehicle.vehb_id = tbl_quickinspection.vehicleid";
    // echo $sql;die;
  $query=$this->db->query($sql);
    $response = $query->result_array();
    return $response;
  }


}