<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class InspectionModel extends CI_Model {


function view()
{
	$sql = "SELECT * FROM `tbl_inspections` order by id desc";
    // echo $sql;die;
	$query=$this->db->query($sql);
	return $query->result();
}

public function getstatusWhereLike($filterstatus) {
        $sql = "SELECT * FROM tbl_inspections WHERE status= '$filterstatus' order by id desc";
        $query = $this->db->query($sql);
        return $query->result();
    }

function add($title,$description,$imageone,$type,$veh_type,$price,$status,$gst_percentage,$pre_pur,$selling_price){
	$u_createdby = $this->session->userdata('usersid');

	$sql = "INSERT INTO `tbl_inspections`(`title`, `description`,`thumbnail`,`type`,`vehicle_type`, `price`, `status`,`expanded`, `selling_price`,`gst_percentage`,`createdby`,`created`) VALUES (".$this->db->escape($title).",".$this->db->escape($description).",".$this->db->escape($imageone).",".$this->db->escape($type).",".$this->db->escape($veh_type).",".$this->db->escape($price).",".$this->db->escape($status).",".$this->db->escape($pre_pur).",".$this->db->escape($selling_price).",".$this->db->escape($gst_percentage).",".$this->db->escape($u_createdby).",CURRENT_TIMESTAMP)";
	$query=$this->db->query($sql);
	if($query==1){
		return true;
	}
	else
	{
		return false;
	}
}

function update($title,$id,$description,$imageone,$type,$veh_type,$status,$price,$gst_percentage,$pre_pur,$selling_price){
  
    $userid = $this->session->userdata('usersid');
$sql = "UPDATE `tbl_inspections` SET `title`=".$this->db->escape($title).",`description`=".$this->db->escape($description).",`thumbnail`=".$this->db->escape($imageone).",`vehicle_type`=".$this->db->escape($veh_type).",`type`=".$this->db->escape($type).",`status`=".$this->db->escape($status).",`price`=".$this->db->escape($price).",`expanded`=".$this->db->escape($pre_pur).",`selling_price`=".$this->db->escape($selling_price).",`gst_percentage`=".$this->db->escape($gst_percentage).",`modifiedby`=".$this->db->escape($userid).",`modified`=CURRENT_TIMESTAMP WHERE id=".$this->db->escape($id)."";
// echo $sql;die;
if ($this->db->simple_query($sql))
{
	$respose = $this->db->affected_rows();
	return $respose;
}
else
{
       return false;
}
}

function loaddata($id){
	$sql = "SELECT * FROM `tbl_inspections` where id='$id'";

	$query = $this->db->query($sql);
  
	return $query->result();
}
function loaddata1($id){
	$sql = "SELECT * FROM `tbl_inspections` where id='$id'";
	$query = $this->db->query($sql);
  	return $query->result();
}


function getUserDetails(){
    $response = array();
    $sql = "SELECT `id`, `title`, `description`, `type`, `price`, `selling_price`, `status`, `created`, `modified`, `expanded` FROM `tbl_inspections` order by id desc";
    $query=$this->db->query($sql);
    $response = $query->result_array();
    return $response;
  }

}