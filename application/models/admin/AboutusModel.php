<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class AboutusModel extends CI_Model {


function view()
{
	$sql = "SELECT abt_id,abt_description FROM `tbl_aboutus`";
    // echo $sql;die;
	$query=$this->db->query($sql);
	return $query->result();
}

function add($abt_description){
	$u_createdby = $this->session->userdata('usersid');

	$sql = "INSERT INTO `tbl_aboutus`(`abt_description`,`abt_create_date`, `abt_createdby`) VALUES (".$this->db->escape($abt_description).",CURRENT_TIMESTAMP,".$this->db->escape($u_createdby).")";
	$query=$this->db->query($sql);
	if($query==1){
		return true;
	}
	else
	{
		return false;
	}
}


function loaddata1($id){
	$sql = "SELECT abt_id,abt_description FROM `tbl_aboutus` where abt_id='$id'";

	$query = $this->db->query($sql);
  
	return $query->result();
}

function loaddata($id){
	$sql = "SELECT abt_id,abt_description FROM `tbl_aboutus` where abt_id='$id'";

	$query = $this->db->query($sql);
  
	return $query->result();
}

function update($abt_id,$abt_description){
  
    $userid = $this->session->userdata('usersid');
$sql = "UPDATE `tbl_aboutus` SET `abt_description`=".$this->db->escape($abt_description).",`abt_modify_date`=CURRENT_TIMESTAMP,`abt_modifyby`='$userid' WHERE abt_id=".$this->db->escape($abt_id)."";
if ($this->db->simple_query($sql))
{
	$respose = $this->db->affected_rows();
	return $respose;
}
else
{
       return false;
}
}

function getUserDetails(){
    $response = array();
    $sql = "SELECT `abt_id`, `abt_description`, `abt_create_date`, `abt_modify_date` FROM `tbl_aboutus`";
    // echo $sql;die;
  $query=$this->db->query($sql);
    $response = $query->result_array();
    return $response;
  }

}