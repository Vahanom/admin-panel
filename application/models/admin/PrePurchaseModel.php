<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class PrePurchaseModel extends CI_Model {

function view()
{
	$sql = "SELECT tbl_prepurchaseinspection.id,inspectiontype,tbl_prepurchaseinspection.userID,status,remark,
	        tbl_m_users.muser_id,tbl_m_users.muser_name,
	        tbl_user_vehicles.vehb_id
            FROM tbl_m_users 
            JOIN tbl_prepurchaseinspection ON 
            tbl_m_users.muser_id = tbl_prepurchaseinspection.userID 
            JOIN tbl_user_vehicles ON 
            tbl_user_vehicles.vehb_id = tbl_prepurchaseinspection.vehicleid";
    // echo $sql;die;
	$query=$this->db->query($sql);
	return $query->result();
}

public function getstatusWhereLike($filterstatus) {
        $sql = "SELECT * FROM tbl_prepurchaseinspection WHERE status= '$filterstatus'";
        $query = $this->db->query($sql);
        return $query->result();
    }

function loaddata1($id,$muser_id){
	$sql = "SELECT muser_name,inspectiontype,status FROM `tbl_prepurchaseinspection` Right join tbl_m_users on tbl_prepurchaseinspection.userID=tbl_m_users.muser_id where tbl_prepurchaseinspection.userID='$muser_id'";

	$query = $this->db->query($sql);
  
	return $query->result();
}
function loaddata2($id,$vehb_id){
	$sql = "SELECT userv_fueltype,userv_vehicle_type,userv_manuf_year,userv_milage,vehicle_reg_no,vehicle_make,vehicle_model,vehicle_trim
	        FROM `tbl_prepurchaseinspection` Right join tbl_user_vehicles on 
	        tbl_prepurchaseinspection.vehicleid=tbl_user_vehicles.vehb_id 
	        where tbl_prepurchaseinspection.vehicleid='$vehb_id'";
	$query = $this->db->query($sql);
  	return $query->result();
}

function getUserDetails(){
    $response = array();
    $sql = "SELECT tbl_prepurchaseinspection.id,tbl_m_users.muser_name,inspectiontype,vehicle_make,status,remark,
	        createdat	        
            FROM tbl_m_users 
            JOIN tbl_prepurchaseinspection ON 
            tbl_m_users.muser_id = tbl_prepurchaseinspection.userID 
            JOIN tbl_user_vehicles ON 
            tbl_user_vehicles.vehb_id = tbl_prepurchaseinspection.vehicleid";
    // echo $sql;die;
  $query=$this->db->query($sql);
    $response = $query->result_array();
    return $response;
  }


}