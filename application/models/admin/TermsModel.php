<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class TermsModel extends CI_Model {


function view()
{
	$sql = "SELECT term_id,term_description FROM `tbl_terms`";
    // echo $sql;die;
	$query=$this->db->query($sql);
	return $query->result();
}

function add($term_description){
	$u_createdby = $this->session->userdata('usersid');

	$sql = "INSERT INTO `tbl_terms`(`term_description`,`term_create_date`, `term_createdby`) VALUES (".$this->db->escape($term_description).",CURRENT_TIMESTAMP,".$this->db->escape($u_createdby).")";
	$query=$this->db->query($sql);
	if($query==1){
		return true;
	}
	else
	{
		return false;
	}
}


function loaddata1($id){
	$sql = "SELECT term_id,term_description FROM `tbl_terms` where term_id='$id'";

	$query = $this->db->query($sql);
  
	return $query->result();
}

function loaddata($id){
	$sql = "SELECT term_id,term_description FROM `tbl_terms` where term_id='$id'";

	$query = $this->db->query($sql);
  
	return $query->result();
}

function update($term_id,$term_description){
  
       $userid = $this->session->userdata('usersid');
$sql = "UPDATE `tbl_terms` SET `term_description`=".$this->db->escape($term_description).",`term_modify_date`=CURRENT_TIMESTAMP,`term_modifyby`='$userid' WHERE term_id=".$this->db->escape($term_id)."";
if ($this->db->simple_query($sql))
{
	$respose = $this->db->affected_rows();
	return $respose;
}
else
{
       return false;
}
}

function getUserDetails(){
    $response = array();
    $sql = "SELECT `term_id`, `term_description`, `term_create_date`, `term_modify_date` FROM `tbl_terms`";
    // echo $sql;die;
  $query=$this->db->query($sql);
    $response = $query->result_array();
    return $response;
  }

}