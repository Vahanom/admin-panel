<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class OngoingModel extends CI_Model {


function view()
{
	$sql = "SELECT order_id,type_servicespare,order_details_id,order_type,order_date,order_schedule_date,order_address,order_pincode,technician_accept_status,supplier_order_status,order_status FROM `tbl_orders` WHERE order_status='ongoing'";
    // echo $sql;die;
	$query=$this->db->query($sql);
	return $query->result();
}
public function getstatusWhereLike($filterstatus) {
        $sql = "SELECT order_id,order_type,order_date,order_schedule_date,order_address,order_pincode,technician_accept_status,supplier_order_status,order_status FROM tbl_orders WHERE order_status= '$filterstatus'";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getstatusWhereLikeall($filterstatus) {
        $sql = "SELECT order_id,order_type,order_date,order_schedule_date,order_address,order_pincode,technician_accept_status,supplier_order_status,order_status FROM tbl_orders ";
        $query = $this->db->query($sql);
        return $query->result();
    }


function loaddata1($id){
	$sql = "SELECT `order_id`,`type_servicespare`, `puser_id`, `order_type`, `payment_id`, `order_date`, `order_schedule_date`, `order_schedule_timeslot`, `order_address`, `order_pincode`, `technician_id`, `technician_accept_status`, `supplier_id`, `supplier_order_status`, `technician_assignby`, `supplier_assignby`, `technician_otp`, `technician_otp_status`, `supplier_otp`, `supplier_otp_status`, `order_details_id`, `order_number`, `order_status`, `customer_otp`, `customer_otp_status`, `order_status_datetime`, `order_subtotal_amount`, `order_gst_amount`, `order_coupon_code`, `order_coupon_discount`, `order_total_amount` FROM `tbl_orders` where order_id='$id'";

	$query = $this->db->query($sql);
  
	return $query->result();
}

function loaddata2($order_details_id){
   // $sql = "SELECT `order_detail_id`,`od_order_id`,`id`,`image1`,`image2`,`image3`,`image4`,`image5`,`image6` FROM tbl_orderdetails LEFT JOIN tbl_order_images ON tbl_orderdetails.od_order_id = tbl_order_images.orderID WHERE order_details_id='$order_details_id'";
    $sql="SELECT `order_detail_id`,`od_order_id`,`id`,`image1`,`image2`,`image3`,`image4`,`image5`,`image6` FROM tbl_orderdetails LEFT JOIN tbl_order_images ON tbl_orderdetails.od_order_id = tbl_order_images.orderID WHERE tbl_orderdetails.order_detail_id='$order_details_id'";

    $query = $this->db->query($sql);
  
    return $query->result();
}
function loaddata3($id,$type_servicespare){
   /* $sql = "SELECT `ser_id`,`ser__title` FROM tbl_orderdetails RIGHT JOIN tbl_service ON tbl_orderdetails.od_service_id = tbl_service.ser_id WHERE od_order_id='$id'";

    $query = $this->db->query($sql);
  
    return $query->result();*/
    if($type_servicespare=='service'){
        $sql="SELECT `ser_id`,`ser__title`,`od_service_price`,`type_servicespare` FROM tbl_orders JOIN tbl_orderdetails ON tbl_orders.order_id = tbl_orderdetails.od_order_id JOIN tbl_service ON tbl_service.ser_id = tbl_orderdetails.od_service_id WHERE type_servicespare='service' AND od_order_id='$id'";
          $query = $this->db->query($sql);
          return $query->result();
    }
    else{
        $sql="SELECT `sp_id`,`sp_title`,`od_service_price`,`quantity`,`type_servicespare` FROM tbl_orders JOIN tbl_orderdetails ON tbl_orders.order_id = tbl_orderdetails.od_order_id JOIN tbl_spare ON tbl_spare.sp_id = tbl_orderdetails.od_service_id WHERE type_servicespare='spare' AND od_order_id='$id'";
          $query = $this->db->query($sql);
          return $query->result();
    }
}

}