<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class InsuranceModel extends CI_Model {


function view()
{
	$sql = "SELECT inusr_title,inusr_thumb_img,inusr_status,inusr_id FROM `tbl_insurance`";
    // echo $sql;die;
	$query=$this->db->query($sql);
	return $query->result();
}
public function getstatusWhereLike($filterstatus) {
        $sql = "SELECT * FROM tbl_insurance WHERE inusr_status= '$filterstatus'";
        $query = $this->db->query($sql);
        return $query->result();
    }

function add($service_title,$service_desc,$status,$imageone,$imagetwo,$instype,$price,$claim_per,$selling_price){
	$u_createdby = $this->session->userdata('usersid');

	$sql = "INSERT INTO `tbl_insurance`(`inusr_title`, `inusr_description`,`inusr_thumb_img`, `inusr_img`, `inusr_status`, `inusr_type`, `price`,`claim_percent`, `selling_price`) VALUES (".$this->db->escape($service_title).",".$this->db->escape($service_desc).",".$this->db->escape($imageone).",".$this->db->escape($imagetwo).",".$this->db->escape($status).",".$this->db->escape($instype).",".$this->db->escape($price).",".$this->db->escape($claim_per).",".$this->db->escape($selling_price).")";
	$query=$this->db->query($sql);
	if($query==1){
		return true;
	}
	else
	{
		return false;
	}
}

function update($service_title,$service_desc,$status,$imageone,$imagetwo,$inusr_id,$instype,$price,$claim_per,$selling_price){
  
    $userid = $this->session->userdata('usersid');
$sql = "UPDATE `tbl_insurance` SET `inusr_title`=".$this->db->escape($service_title).",`inusr_description`=".$this->db->escape($service_desc).",`inusr_status`=".$this->db->escape($status)." 
,`inusr_thumb_img`=".$this->db->escape($imageone).",`inusr_img`=".$this->db->escape($imagetwo).",`inusr_type`=".$this->db->escape($instype).",`price`=".$this->db->escape($price).",`claim_percent`=".$this->db->escape($claim_per).",`selling_price`=".$this->db->escape($selling_price)."
WHERE inusr_id=".$this->db->escape($inusr_id)."";
// echo $sql;die;
if ($this->db->simple_query($sql))
{
	$respose = $this->db->affected_rows();
	return $respose;
}
else
{
       return false;
}
}

function loaddata($id){
	$sql = "SELECT * FROM `tbl_insurance` where inusr_id='$id'";

	$query = $this->db->query($sql);
  
	return $query->result();
}
function loaddata1($id){
	$sql = "SELECT * FROM `tbl_insurance` where inusr_id='$id'";
	$query = $this->db->query($sql);
  	return $query->result();
}

function getUserDetails(){
    $response = array();
    $sql = "SELECT `inusr_id`, `inusr_title`, `inusr_description`, `inusr_status`, `inusr_type`, `price`, `selling_price` FROM `tbl_insurance`";
    $query=$this->db->query($sql);
    $response = $query->result_array();
    return $response;
  }


}