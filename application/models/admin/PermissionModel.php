<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class PermissionModel extends CI_Model {


function view()
{
	$sql = "SELECT coupon_title,cpn_code,cpn_status,cpn_id FROM `tbl_coupon`";
    // echo $sql;die;
	$query=$this->db->query($sql);
	return $query->result();
}
public function getstatusWhereLike($filterstatus) {
        $sql = "SELECT * FROM tbl_coupon WHERE cpn_status= '$filterstatus'";
        $query = $this->db->query($sql);
        return $query->result();
    }

function add($Coupon_title,$Coupon_desc,$coupontype,$Coupon_flat,$Coupon_per,$start_date,$end_date,$maximumbillamount,$maxuserlimit,$maxuseperuser,$minimumbillamount,$status,$couponcode){
	$u_createdby = $this->session->userdata('usersid');

	$sql = "INSERT INTO `tbl_coupon`(`coupon_title`, `cpn_code`,`cpn_description`, `cpn_type`, `cpn_flat_amount`,`cpn_percentage`, `cpn_startdate`,`cpn_enddate`, `cpn_status`, `cpn_min_bill_amt`, `cpn_max_discount`,`cpn_max_users`, `cpn_maxperusers`, `cpn_createdate`,`cpn_createby`) VALUES (".$this->db->escape($Coupon_title).",".$this->db->escape($couponcode).",".$this->db->escape($Coupon_desc).",".$this->db->escape($coupontype).",".$this->db->escape($Coupon_flat).",".$this->db->escape($Coupon_per).",".$this->db->escape($start_date).",".$this->db->escape($end_date).",".$this->db->escape($status).",".$this->db->escape($minimumbillamount).",".$this->db->escape($maximumbillamount).",".$this->db->escape($maxuserlimit).",".$this->db->escape($maxuseperuser).",CURRENT_TIMESTAMP,".$this->db->escape($u_createdby)."
	)";
	$query=$this->db->query($sql);
	if($query==1){
		return true;
	}
	else
	{
		return false;
	}
}

function updatepermission($permissionarrayencode,$column,$empid){
  
    $userid = $this->session->userdata('usersid');
$sql = "UPDATE `tbl_permission` SET `$column`=".$this->db->escape($permissionarrayencode)."
WHERE permission_emp_id=".$this->db->escape($empid)."  ";
// echo $sql;die;
if ($this->db->simple_query($sql))
{
	$respose = $this->db->affected_rows();
	return $respose;
}
else
{
       return false;
}
}

function addpermission($permissionarrayencode,$column,$empid){
  
    $userid = $this->session->userdata('usersid');
	$sql = "INSERT INTO `tbl_permission`(`$column`,`permission_emp_id`) VALUES (".$this->db->escape($permissionarrayencode).",".$this->db->escape($empid)."
	)";
// echo $sql;die;
if ($this->db->simple_query($sql))
{
	$respose = $this->db->affected_rows();
	return $respose;
}
else
{
       return false;
}
}

function getUserDetails(){
    $response = array();
    $sql = "SELECT `coupon_title`, `cpn_status` FROM `tbl_coupon";
    // echo $sql;die;
  $query=$this->db->query($sql);
    $response = $query->result_array();
    return $response;
  }


}