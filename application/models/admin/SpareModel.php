<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class SpareModel extends CI_Model {


function view()
{
	$sql = "SELECT *  FROM `tbl_spare`";
    // echo $sql;die;
	$query=$this->db->query($sql);
	return $query->result();
}

public function getstatusWhereLike($filterstatus) {
        $sql = "SELECT * FROM tbl_spare WHERE sp_status= '$filterstatus'";
        $query = $this->db->query($sql);
        return $query->result();
    }

function add($space_title,$spare_desc,$space_hsn,$tech_hours,$status,$imageone,$imagetwo,$imagethree,$imagefour,$vehiclename,$sparecategory,$veh_cost,$lb_rate,$lb_commission,$margin){
	$u_createdby = $this->session->userdata('usersid');

	$sql = "INSERT INTO `tbl_spare`(`sp_title`, `sp_description`,`sp_hsn_code`,`tech_hours`, `veh_id`,`sp_imageone`, `sp_imagetwo`,`sp_imagethree`, `sp_imagefour`, `sp_status`,`sp_createdate`, `sp_createby`,`sp_category_id`,`labour_rate`,`vh_labour_commission`,`vh_cost`,`margin`) VALUES (".$this->db->escape($space_title).",".$this->db->escape($spare_desc).",".$this->db->escape($space_hsn).",".$this->db->escape($tech_hours).",".$this->db->escape($vehiclename).",".$this->db->escape($imageone).",".$this->db->escape($imagetwo).",".$this->db->escape($imagethree).",".$this->db->escape($imagefour).",".$this->db->escape($status).",CURRENT_TIMESTAMP,".$this->db->escape($u_createdby).",".$this->db->escape($sparecategory).",".$this->db->escape($lb_rate).",".$this->db->escape($lb_commission).",".$this->db->escape($veh_cost).",".$this->db->escape($margin).")";
	//echo $sql;die;
	$query=$this->db->query($sql);
	if($query==1){
		return true;
	}
	else
	{
		return false;
	}
}

function update($space_title,$spare_desc,$space_hsn,$tech_hours,$status,$imageone,$imagetwo,$imagethree,$imagefour,$vehiclename,$sp_id,$sparecategory,$veh_cost,$lb_rate,$lb_commission,$margin){
  
    $userid = $this->session->userdata('usersid');
$sql = "UPDATE `tbl_spare` SET `sp_title`=".$this->db->escape($space_title).",`sp_description`=".$this->db->escape($spare_desc).",`sp_hsn_code`=".$this->db->escape($space_hsn).",`tech_hours`=".$this->db->escape($tech_hours).",`sp_status`=".$this->db->escape($status)."
,`sp_imageone`=".$this->db->escape($imageone).",`sp_imagetwo`=".$this->db->escape($imagetwo).",`sp_imagethree`=".$this->db->escape($imagethree).",`sp_imagefour`=".$this->db->escape($imagefour).",`veh_id`=".$this->db->escape($vehiclename).",`sp_category_id`=".$this->db->escape($sparecategory).",`sp_modifyby`=".$this->db->escape($userid).",`labour_rate`=".$this->db->escape($lb_rate).",`vh_labour_commission`=".$this->db->escape($lb_commission).",`vh_cost`=".$this->db->escape($veh_cost).",`margin`=".$this->db->escape($margin).",`sp_modifydate`=CURRENT_TIMESTAMP WHERE sp_id=".$this->db->escape($sp_id)."";
//  echo $sql;die;
if ($this->db->simple_query($sql))
{
	$respose = $this->db->affected_rows();
	return $respose;
}
else
{
       return false;
}
}

function loaddata($id){
	$sql = "SELECT * FROM `tbl_spare` left JOIN tbl_vehicle ON FIND_IN_SET(tbl_vehicle.vehb_id, veh_id) > 0 where sp_id='$id'";

	
	$query = $this->db->query($sql);
  
	return $query->result();
}
function loaddata1($id){
	$sql = "SELECT * FROM `tbl_spare` left JOIN tbl_vehicle ON FIND_IN_SET(tbl_vehicle.vehb_id, veh_id) > 0 where sp_id='$id'";

	$query = $this->db->query($sql);
  
	return $query->result();
}


function phonenocheckexist($username)
{
    $this->db->select('puser_id'); 
    $this->db->from('tbl_p_users');
    $this->db->where('puser_phno', $username);
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}

function emailcheckexist($username)
{
    $this->db->select('puser_id'); 
    $this->db->from('tbl_p_users');
    $this->db->where('puser_email', $username);
    $query = $this->db->get();
    $result = $query->result_array();

    return $result;
}
function namecheckexist1($username)
{
    $this->db->select('sp_id'); 
    $this->db->from('tbl_spare');
    $this->db->where('sp_title', rtrim($username));
    $query = $this->db->get();
    $result = $query->result_array();

    return $result;
}

function sparecat()
{
	$sql = "SELECT spcat_title,spcat_id  FROM `tbl_spare_catg` where `spcat_status`='Active'";
    // echo $sql;die;
	$query=$this->db->query($sql);
	return $query->result();
}

function getUserDetails(){
    $response = array();
    $sql = "SELECT sp_id,spcat_title, sp_title,sp_description, sp_hsn_code,veh_id,sp_imageone, sp_imagetwo, sp_imagethree, sp_imagefour, sp_status,sp_createdate,sp_modifydate,margin FROM `tbl_spare` INNER join tbl_spare_catg on tbl_spare.sp_category_id=tbl_spare_catg.spcat_id";
    // echo $sql;die;
  $query=$this->db->query($sql);
    $response = $query->result_array();
    return $response;
  }
}