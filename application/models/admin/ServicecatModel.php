<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class ServicecatModel extends CI_Model {


function view()
{
	$sql = "SELECT ser_cat_title,ser_thumbnail,ser_category_status,ser_catid FROM `tbl_service_cat`";
    // echo $sql;die;
	$query=$this->db->query($sql);
	return $query->result();
}
public function getstatusWhereLike($filterstatus) {
        $sql = "SELECT * FROM tbl_service_cat WHERE ser_category_status= '$filterstatus'";
        $query = $this->db->query($sql);
        return $query->result();
    }

function add($service_title,$service_desc,$type_vehicle,$servicecategory,$status,$imageone,$imagetwo){
	$u_createdby = $this->session->userdata('usersid');

	$sql = "INSERT INTO `tbl_service_cat`(`ser_cat_title`, `ser_cat_description`,`ser_thumbnail`, `ser_image`, `ser_category_type`,`ser_category_status`, `ser_category_createdby`, `ser_category_createddate`,`vehicle_type`) VALUES (".$this->db->escape($service_title).",".$this->db->escape($service_desc).",".$this->db->escape($imageone).",".$this->db->escape($imagetwo).",".$this->db->escape($servicecategory).",".$this->db->escape($status).",".$this->db->escape($u_createdby).",CURRENT_TIMESTAMP,".$this->db->escape($type_vehicle).")";
	// echo $sql;die;
	$query=$this->db->query($sql);
	if($query==1){
		return true;
	}
	else
	{
		return false;
	}
}

function update($service_title,$service_desc,$type_vehicle,$servicecategory,$status,$imageone,$imagetwo,$ser_catid){
  
    $userid = $this->session->userdata('usersid');
$sql = "UPDATE `tbl_service_cat` SET `ser_cat_title`=".$this->db->escape($service_title).",`ser_cat_description`=".$this->db->escape($service_desc).",`vehicle_type`=".$this->db->escape($type_vehicle).",`ser_category_status`=".$this->db->escape($status)." 
,`ser_thumbnail`=".$this->db->escape($imageone).",`ser_image`=".$this->db->escape($imagetwo).",`ser_category_type`=".$this->db->escape($servicecategory)." ,`ser_category_modifiedby`=".$this->db->escape($userid).",`ser_category_modifieddate`=CURRENT_TIMESTAMP

WHERE ser_catid=".$this->db->escape($ser_catid)."";
// echo $sql;die;
if ($this->db->simple_query($sql))
{
	$respose = $this->db->affected_rows();
	return $respose;
}
else
{
       return false;
}
}

function loaddata($id){
	$sql = "SELECT * FROM `tbl_service_cat` where ser_catid='$id'";

	$query = $this->db->query($sql);
  
	return $query->result();
}
function loaddata1($id){
	$sql = "SELECT * FROM `tbl_service_cat` where ser_catid='$id'";

	$query = $this->db->query($sql);
  
	return $query->result();
}


function phonenocheckexist($username)
{
    $this->db->select('puser_id'); 
    $this->db->from('tbl_p_users');
    $this->db->where('puser_phno', $username);
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}

function emailcheckexist($username)
{
    $this->db->select('puser_id'); 
    $this->db->from('tbl_p_users');
    $this->db->where('puser_email', $username);
    $query = $this->db->get();
    $result = $query->result_array();

    return $result;
}
function getUserDetails(){
    $response = array();
    $sql = "SELECT `ser_catid`, `ser_cat_title`, `ser_cat_description`, `vehicle_type`, `ser_category_type`, `ser_category_status`, `ser_category_createdby`, `ser_category_createddate`, `ser_category_modifiedby`, `ser_category_modifieddate` FROM `tbl_service_cat`";
    // echo $sql;die;
  $query=$this->db->query($sql);
    $response = $query->result_array();
    return $response;
}

//duplicate name check functions 
function namecheckexist1($username)
{
    $this->db->select('ser_catid'); 
    $this->db->from('tbl_service_cat');
    $this->db->where('ser_cat_title', rtrim($username));
    $query = $this->db->get();
    $result = $query->result_array();

    return $result;
}

}