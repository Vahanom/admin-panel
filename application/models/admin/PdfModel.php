<?php
class PdfModel extends CI_Model
{
	
	function show_single_details($order_id)
	{
		/*$this->db->where('order_id', $order_id);
		$data = $this->db->get('tbl_orders');*/
		$data = "SELECT order_id,puser_id,order_gst_amount,technician_id,supplier_id,order_number,order_coupon_code,order_subtotal_amount,order_coupon_discount,order_gst_amount,order_total_amount,order_type,payment_id,order_date,order_schedule_date,order_address,order_pincode,technician_accept_status,supplier_order_status,order_status FROM `tbl_orders` WHERE order_id='$order_id'";
		$data=$this->db->query($data);
		$output = '<html>';
		// technician id..
		foreach($data->result() as $roww){
            $t_id=$roww->technician_id;
            $s_id=$roww->supplier_id;
            $c_id=$roww->puser_id;
            $order_coupon_discount=$roww->order_coupon_discount;
             $order_total_amount=$roww->order_total_amount;
        }
        $number = $order_total_amount; 
       // print_r($number); 
    $no = floor($number);
   $point = round($number - $no, 2) * 100;
   $hundred = null;
   $digits_1 = strlen($no);
   $i = 0;
   $str = array();
   $words = array('0' => '', '1' => 'one', '2' => 'two',
    '3' => 'three', '4' => 'four', '5' => 'five', '6' => 'six',
    '7' => 'seven', '8' => 'eight', '9' => 'nine',
    '10' => 'ten', '11' => 'eleven', '12' => 'twelve',
    '13' => 'thirteen', '14' => 'fourteen',
    '15' => 'fifteen', '16' => 'sixteen', '17' => 'seventeen',
    '18' => 'eighteen', '19' =>'nineteen', '20' => 'twenty',
    '30' => 'thirty', '40' => 'forty', '50' => 'fifty',
    '60' => 'sixty', '70' => 'seventy',
    '80' => 'eighty', '90' => 'ninety');
   $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
   while ($i < $digits_1) {
     $divider = ($i == 2) ? 10 : 100;
     $number = floor($no % $divider);
     $no = floor($no / $divider);
     $i += ($divider == 10) ? 1 : 2;
     if ($number) {
        $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
        $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
        $str [] = ($number < 21) ? $words[$number] .
            " " . $digits[$counter] . $plural . " " . $hundred
            :
            $words[floor($number / 10) * 10]
            . " " . $words[$number % 10] . " "
            . $digits[$counter] . $plural . " " . $hundred;
     } else $str[] = null;
  }
  $str = array_reverse($str);
  $result = implode('', $str);
 
 //echo $result . "Rupees  ";die;
  // data for technical id name..
        $data1 = "SELECT webuser_name FROM `tbl_web_user` WHERE webuser_id='$t_id'";
        // echo $data1;exit;
		$data1=$this->db->query($data1);
		$data1 = $data1->result();
		if($data1){
		  foreach($data1 as $row1){ 
		  $tech_name=$row1->webuser_name;
		  
		}
		}else{
		      $tech_name="N/A";
		  }
		$data2 = "SELECT webuser_name FROM `tbl_web_user` WHERE webuser_id='$s_id'";
// 		echo $data2;exit;
		$data2=$this->db->query($data2); 
		$data2 = $data2->result();
		if($data2){
		foreach($data2 as $row2){ 
		  $supp_name=$row2->webuser_name;
		}
		}else{
		      $supp_name="N/A";
		  }
		$data3 = "SELECT muser_name,m_phno,muser_email,muser_address,muser_pincode FROM `tbl_m_users` WHERE muser_id='$s_id'";
		
		$data3=$this->db->query($data3);
		$data3 = $data3->result();
		if($data3){
		foreach($data3 as $row3){ 
		  $muser_name=$row3->muser_name;
		  $m_phno=$row3->m_phno;
		  $muser_email=$row3->muser_email;
		  $muser_address=$row3->muser_address;
		  $muser_pincode=$row3->muser_pincode;
		}
		}else{
		$muser_name="N/A";
		  $m_phno="N/A";
		  $muser_email="N/A";
		  $muser_address="N/A";
		  $muser_pincode="N/A";
		  }
		$data4 = "SELECT ser__title,ser_amt FROM `tbl_service` WHERE ser_type='Quick'";
		
		$data4=$this->db->query($data4); 
		$data4 = $data4->result();
		
		if($data4){
		foreach($data4 as $row4){ 
		  $ser__title=$row4->ser__title;
		   $ser_amt=$row4->ser_amt;		 
		}
		}else{
		  $ser__title="N/A";
		   $ser_amt=0;		 
		  }
		
$Subtotal =$ser_amt - $order_coupon_discount;

		foreach($data->result() as $row)
		{ 
			$output .= '
		    <table class="table table-bordered table-hover table-checkable" id="example" style="margin-top: 13px; width:900px;">
		    <tbody>
		      <tr>
			   <td><p><b>Address:</b>'.$row->order_address.'</p></</td>
			   <td><p><b>Phone Number : </b>'.$row->order_pincode.'</p></</td>
			  </tr>
			  <tr>
			   <td><p><b>Email : </b>'.$row->order_type.'</p></td>
			   <td><p><b>GST Number : </b>'.$row->order_gst_amount.'</p></td>
			  </tr>
		    </tbody>
          </table>
          <hr>
           <table class="table table-bordered table-hover table-checkable" id="example" style="margin-top: 13px; width:725px;">
		    <tbody>
		      <tr>
			   <td><p><b>Order Number:</b>'.$row->order_number.'</p></td>
			   <td><p><b>Order Date : </b>'.$row->order_date.'</p></</td>
			   <td><p><b>Order Scehdule Date : </b>'.$row->order_schedule_date.'</p></td>
			  
			  </tr>
			   <td><p><b>Order Type : </b>'.$row->order_type.'</p></td>
			  <tr>
			  </tr>
		    </tbody>
          </table>
          <hr>
           <table class="table table-bordered table-hover table-checkable" id="example" style="margin-top: 13px; width:725px;">
		    <tbody>
		      <tr>
			   <td><p><b>Technician Name:</b>'.$tech_name.'</p></td>
			   <td><p><b>Supplier Name : </b>'.$supp_name.'</p></</td>
			   <td><p><b>Order Status : </b>'.$row->order_status.'</p></td>
			  </tr>
		    </tbody>
          </table>
          <hr>
          <table class="table table-bordered table-hover table-checkable" id="example" style="margin-top: 13px; width:800px;">
		    <tbody>
		      <tr>
			   <td><p><b>Customer Name:</b>'.$muser_name.'</p></</td>
			   <td><p><b>Customer Contact : </b>'.$m_phno.'</p></</td>
			   <td><p><b>Customer Email : </b>'.$muser_email.'</p></</td>
			  </tr>
			  <tr>
			   <td><p><b>Address : </b>'.$muser_address.'</p></td>
			   <td><p><b>Pincode : </b>'.$muser_pincode.'</p></td>
			   <td><p><b>Total Bill : </b>'.$row->order_type.'</p></td>
			  </tr>
		    </tbody>
          </table>
          <hr>
           <table class="table table-bordered table-hover table-checkable" id="example" style="margin-top: 13px; width:800px;">
		<thead>
			<tr>
			 <th>Sr no</th>
			 <th>Title</th>
			 <th>Quantity</th>
			 <th>Amount</th>
			</tr>
		</thead>
		<tbody>
			<td>1 </td>
			<td><p>'.$ser__title.'</p></td>
			<td>2</td>
			<td><p>'.$ser_amt.'</p></td>
		</tbody>
      </table>
      <hr>
      <table class="table table-bordered table-hover table-checkable" id="example1" style="margin-top: 13px; width:800px;">
  <caption>
   
  </caption>
  <col>
  <col>
  <colgroup span="3"></colgroup>
  <tbody>
    <tr>
      <th rowspan="3" scope="rowgroup">Coupon Code:'.$row->order_coupon_code.'</th>
      <th scope="row"> Subtotal:</th>
      <td><p>'.$ser_amt.'</p></td>
     
    </tr>
    <tr>
      <th scope="row"> Discount:</th>
      <td>'.$row->order_coupon_discount.'</td>
    </tr>
    <tr>
      <th scope="row"> Subtotal2:</th>
      <td><p>'.$Subtotal.'</p></td>
    </tr>
  <tbody>
    <tr>
      <th rowspan="2" scope="rowgroup">'.$result.'</th>
      <th scope="row"> GST:</th>
      <td>'.$row->order_gst_amount.'</td>
    </tr>
    <tr>
      <th scope="row"> Total:</th>
      <td>'.$row->order_total_amount.'</td>
    </tr>
  </tbody>
</table>

				
			';
		}
		$output .= '</html>';
		return $output;
	}
}
?>