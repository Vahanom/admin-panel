<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class RequestInsuranceModel extends CI_Model {


function view()
{
	$sql = "SELECT user_insuranceid,insurance_id,user_m_id,status,remark,inusr_id,inusr_title,muser_name,muser_email,m_phno
FROM tbl_insurance 
JOIN tbl_user_insurance ON 
tbl_insurance.inusr_id = tbl_user_insurance.insurance_id 
JOIN tbl_m_users ON 
tbl_m_users.muser_id = tbl_user_insurance.user_m_id";
    // echo $sql;die;
	$query=$this->db->query($sql);
	return $query->result();
}

public function getstatusWhereLike($filterstatus) {
        $sql = "SELECT * FROM tbl_insurance 
JOIN tbl_user_insurance ON 
tbl_insurance.inusr_id = tbl_user_insurance.insurance_id 
JOIN tbl_m_users ON 
tbl_m_users.muser_id = tbl_user_insurance.user_m_id WHERE status='$filterstatus'";
        $query = $this->db->query($sql);
        return $query->result();
    }

function update($user_insuranceid,$remark){
  
$sql = "UPDATE `tbl_user_insurance` SET `remark`=".$this->db->escape($remark)." WHERE user_insuranceid=".$this->db->escape($user_insuranceid)."";
// echo $sql;die;
if ($this->db->simple_query($sql))
{
	$respose = $this->db->affected_rows();
	return $respose;
}
else
{
       return false;
}
}
function loaddata1($id,$insurance_id){
	$sql = "SELECT user_insuranceid,inusr_title,inusr_id,status,remark FROM `tbl_user_insurance` left join tbl_insurance on tbl_user_insurance.user_insuranceid=tbl_insurance.inusr_id where tbl_insurance.inusr_id='$insurance_id'";
	$query = $this->db->query($sql);
  	return $query->result();
}
function loaddata2($id,$user_m_id){
	
	$sql = "SELECT user_insuranceid,muser_name,muser_id,m_phno,muser_email FROM `tbl_user_insurance` Right join tbl_m_users on tbl_user_insurance.user_m_id=tbl_m_users.muser_id where tbl_m_users.muser_id='$user_m_id'";
	$query = $this->db->query($sql);
  	return $query->result();
}

function loaddata3($value,$user_insuranceid){
	$sql = "UPDATE `tbl_user_insurance` SET `status`=".$this->db->escape($value)." WHERE user_insuranceid='$user_insuranceid'";
//echo $sql;die;
if ($this->db->simple_query($sql))
{
	$respose = $this->db->affected_rows();
	return $respose;
}
else
{
       return false;
}
   
}

function getUserDetails(){
 		$response = array();
		$sql="SELECT inusr_title,muser_name,m_phno,muser_email,remark,status
               FROM tbl_insurance 
               JOIN tbl_user_insurance ON 
               tbl_insurance.inusr_id = tbl_user_insurance.insurance_id 
               JOIN tbl_m_users ON 
               tbl_m_users.muser_id = tbl_user_insurance.user_m_id";
	    $q=$this->db->query($sql);
		$response = $q->result_array();
	 	return $response;
}


}