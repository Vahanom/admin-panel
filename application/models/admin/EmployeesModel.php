<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class EmployeesModel extends CI_Model {
 

function view()
{
	$sql = "SELECT webuser_phno,webuser_name,webuser_status,webuser_id FROM `tbl_web_user` where `webuser_type`!='admin'";
    // echo $sql;die;
	$query=$this->db->query($sql);
    // print_r($query->result());exit;
	return $query->result();
}

function getpermission($empid)
{
	$sql = "SELECT * FROM `tbl_permission` where `permission_emp_id`='$empid'";
    // echo $sql;die;
	$query=$this->db->query($sql);
	return $query->result();
}

function employeedtail($empid)
{
	$sql = "SELECT webuser_phno,webuser_name,webuser_status,webuser_id FROM `tbl_web_user`  where webuser_id='$empid'";
	$query=$this->db->query($sql);
	return $query->result();
}

function add($user_name,$user_email,$user_aadhar,$user_pan,$user_address,$blood_group,$user_type,$status,$phonenumber){
	$u_createdby = $this->session->userdata('usersid');

	$sql = "INSERT INTO `tbl_web_user`(`webuser_name`, `webuser_email`,`webuser_phno`, `webuser_address`, `webuser_pan_num`, `webuser_adhar_num`, `webuser_type`, `webuser_bloodgroup`, `webuser_status`, `webuser_createddate`, `webuser_createdby`) VALUES (".$this->db->escape($user_name).",".$this->db->escape($user_email).",".$this->db->escape($phonenumber).",".$this->db->escape($user_address).",".$this->db->escape($user_pan).",".$this->db->escape($user_aadhar).",".$this->db->escape($user_type).",".$this->db->escape($blood_group).",".$this->db->escape($status).",CURRENT_TIMESTAMP,".$this->db->escape($u_createdby).")";
    // print_r($sql);exit;
	$query=$this->db->query($sql);
	if($query==1){
		return true;
	}
	else
	{
		return false;
	}
}



function update($user_name,$user_email,$user_aadhar,$user_pan,$user_address,$blood_group,$user_type,$status,$phonenumber,$webuser_id){
  
    $userid = $this->session->userdata('usersid');
$sql = "UPDATE `tbl_web_user` SET `webuser_name`=".$this->db->escape($user_name).",`webuser_email`=".$this->db->escape($user_email).",`webuser_phno`=".$this->db->escape($phonenumber).",`webuser_address`=".$this->db->escape($user_address).",`webuser_pan_num`= ".$this->db->escape($user_pan).",`webuser_adhar_num`=".$this->db->escape($user_aadhar).",`webuser_type`=".$this->db->escape($user_type).",`webuser_bloodgroup`=".$this->db->escape($blood_group).",`webuser_status`= ".$this->db->escape($status).",`webuser_modifieddate`=CURRENT_TIMESTAMP,`webuser_modifiedby`='$userid' WHERE webuser_id=".$this->db->escape($webuser_id)."";
if ($this->db->simple_query($sql))
{
	$respose = $this->db->affected_rows();
	return $respose;
}
else
{
       return false;
}
}

function loaddata($id){
	$sql = "SELECT * FROM `tbl_web_user` where webuser_id='$id'";

	$query = $this->db->query($sql);
  
	return $query->result();
}
function loaddata1($id){
	$sql = "SELECT * FROM `tbl_web_user` where webuser_id='$id'";

	$query = $this->db->query($sql);
  
	return $query->result();
}


function phonenocheckexist($username)
{
    $this->db->select('webuser_id'); 
    $this->db->from('tbl_web_user');
    $this->db->where('webuser_phno', $username);
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}

function pancheckexist($username)
{
    $this->db->select('webuser_id'); 
    $this->db->from('tbl_web_user');
    $this->db->where('webuser_pan_num', $username);
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}
function adharcheckexist($username)
{
    $this->db->select('webuser_id'); 
    $this->db->from('tbl_web_user');
    $this->db->where('webuser_adhar_num', $username);
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}


function emailcheckexist($username)
{
    $this->db->select('webuser_id'); 
    $this->db->from('tbl_web_user');
    $this->db->where('webuser_email', $username);
    $query = $this->db->get();
    $result = $query->result_array();

    return $result;
}

public function getstatusWhereLike($filterstatus) {
        $sql = "SELECT * FROM tbl_web_user WHERE webuser_status= '$filterstatus' AND `webuser_type`!='admin'";
        $query = $this->db->query($sql);
        return $query->result();
    }

function getUserDetails(){
    $response = array();
    $sql = "SELECT `webuser_id`, `webuser_name`, `webuser_email`, `webuser_phno`, `webuser_address`, `webuser_pan_num`, `webuser_adhar_num`, `webuser_type`, `webuser_bloodgroup`, `webuser_status`, `webuser_createddate`, `webuser_modifieddate` FROM `tbl_web_user` where `webuser_type`!='admin'";
    // echo $sql;die;
  $query=$this->db->query($sql);
    $response = $query->result_array();
    return $response;
  }
}