<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class VehicleModel extends CI_Model {


function view()
{
	$sql = "SELECT vehb_model,vehb_make,vehb_trim,vehb_status,vehb_id FROM `tbl_vehicle`";
    // echo $sql;die;
	$query=$this->db->query($sql);
	return $query->result();
}
public function getstatusWhereLike($filterstatus) {
        $sql = "SELECT * FROM tbl_vehicle WHERE vehb_status= '$filterstatus'";
        $query = $this->db->query($sql);
        return $query->result();
    }

function add($make_name,$model_name,$trim_vehicle,$fuel,$type_vehicle,$status,$imageone){
	$u_createdby = $this->session->userdata('usersid');

	$sql = "INSERT INTO `tbl_vehicle`(`vehb_make`, `vehb_type`,`vehb_model`, `vehb_trim`, `vehb_fuel`, `vehb_image`, `vehb_status`, `vehb_createdate`, `vehb_createby`) VALUES (".$this->db->escape($make_name).",".$this->db->escape($type_vehicle).",".$this->db->escape($model_name).",".$this->db->escape($trim_vehicle).",".$this->db->escape($fuel).",".$this->db->escape($imageone).",".$this->db->escape($status).",CURRENT_TIMESTAMP,".$this->db->escape($u_createdby).")";
	//echo $sql;die;
	$query=$this->db->query($sql);
	if($query==1){
		return true;
	}
	else
	{
		return false;
	}
}

function update($make_name,$model_name,$trim_vehicle,$fuel,$type_vehicle,$status,$imageone,$vehb_id){
  
    $userid = $this->session->userdata('usersid');
$sql = "UPDATE `tbl_vehicle` SET `vehb_make`=".$this->db->escape($make_name).",`vehb_type`=".$this->db->escape($type_vehicle).",`vehb_model`=".$this->db->escape($model_name).",`vehb_trim`=".$this->db->escape($trim_vehicle).",`vehb_fuel`= ".$this->db->escape($fuel).",`vehb_image`=".$this->db->escape($imageone).",`vehb_status`=".$this->db->escape($status).",`vehb_modifydate`=CURRENT_TIMESTAMP,`vehb_modifyby`='$userid' WHERE vehb_id=".$this->db->escape($vehb_id)."";
// echo $sql;die;
if ($this->db->simple_query($sql))
{
	$respose = $this->db->affected_rows();
	return $respose;
}
else
{
       return false;
}
}

function loaddata($id){
	$sql = "SELECT * FROM `tbl_vehicle` where vehb_id='$id'";

	$query = $this->db->query($sql);
  
	return $query->result();
}
function loaddata1($id){
	$sql = "SELECT * FROM `tbl_vehicle` where vehb_id='$id'";

	$query = $this->db->query($sql);
  
	return $query->result();
}


function phonenocheckexist($username)
{
    $this->db->select('puser_id'); 
    $this->db->from('tbl_p_users');
    $this->db->where('puser_phno', $username);
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}

function emailcheckexist($username)
{
    $this->db->select('puser_id'); 
    $this->db->from('tbl_p_users');
    $this->db->where('puser_email', $username);
    $query = $this->db->get();
    $result = $query->result_array();

    return $result;
}

function getUserDetails(){
    $response = array();
    $sql = "SELECT `vehb_id`, `vehb_type`, `vehb_make`, `vehb_model`, `vehb_trim`, `vehb_fuel`, `vehb_image`, `vehb_status`, `vehb_createdate`, `vehb_modifydate` FROM `tbl_vehicle`";
    // echo $sql;die;
  $query=$this->db->query($sql);
    $response = $query->result_array();
    return $response;
  }
}