<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class RequestPollutionModel extends CI_Model {


function view()
{
	//$sql = "SELECT user_pollutionid,pollution_id,muser_id,createdbydate,status,remark FROM `tbl_user_pollution`";
	$sql="SELECT user_pollutionid, pollution_id, tbl_user_pollution.muser_id, status,remark,muser_name,muser_email,m_phno,pol_title FROM tbl_pollutions JOIN tbl_user_pollution ON tbl_pollutions.pol_id = tbl_user_pollution.pollution_id JOIN tbl_m_users ON tbl_m_users.muser_id = tbl_user_pollution.muser_id";
    // echo $sql;die;
	$query=$this->db->query($sql);
	return $query->result();
}
public function getstatusWhereLike($filterstatus) {
        $sql = "SELECT user_pollutionid, pollution_id, tbl_user_pollution.muser_id, status,remark,muser_name,muser_email,m_phno,pol_title FROM tbl_pollutions JOIN tbl_user_pollution ON tbl_pollutions.pol_id = tbl_user_pollution.pollution_id JOIN tbl_m_users ON tbl_m_users.muser_id = tbl_user_pollution.muser_id WHERE status= '$filterstatus'";
        $query = $this->db->query($sql);
        return $query->result();
    }
function view1()
{
	$sql = "SELECT pol_title,pol_id FROM `tbl_pollutions`";
    // echo $sql;die;
	$query=$this->db->query($sql);
	return $query->result();
}


function update($user_pollutionid,$remark){
  
$sql = "UPDATE `tbl_user_pollution` SET `remark`=".$this->db->escape($remark)." WHERE user_pollutionid=".$this->db->escape($user_pollutionid)."";
// echo $sql;die;
if ($this->db->simple_query($sql))
{
	$respose = $this->db->affected_rows();
	return $respose;
}
else
{
       return false;
}
}

function loaddata1($id,$pol_id){
	//$sql = "SELECT * FROM `tbl_user_pollution` where user_pollutionid='$id'";
	$sql = "SELECT user_pollutionid,pol_title,pollution_id,status,remark FROM `tbl_user_pollution` Right join tbl_pollutions on tbl_user_pollution.pollution_id=tbl_pollutions.pol_id where tbl_user_pollution.pollution_id='$pol_id'";
	$query = $this->db->query($sql);
  	return $query->result();
}
function loaddata2($id,$m_id){
	
	$sql = "SELECT user_pollutionid,muser_name,	m_phno,muser_email FROM `tbl_user_pollution` Right join tbl_m_users on tbl_user_pollution.muser_id=tbl_m_users.muser_id where tbl_user_pollution.muser_id='$m_id'";
	$query = $this->db->query($sql);
  	return $query->result();
}

function loaddata4($value,$user_pollutionid){
	$sql = "UPDATE `tbl_user_pollution` SET `status`=".$this->db->escape($value)." WHERE user_pollutionid='$user_pollutionid'";
// echo $sql;die;
// return $sql;
if ($this->db->simple_query($sql))
{
	$respose = $this->db->affected_rows();
	return $respose;
}
else
{
       return false;
}
   
}


function getUserDetails(){
 		$response = array();
		$sql="SELECT pol_title ,muser_name, m_phno,muser_email,remark,status FROM tbl_pollutions JOIN tbl_user_pollution ON tbl_pollutions.pol_id = tbl_user_pollution.pollution_id JOIN tbl_m_users ON tbl_m_users.muser_id = tbl_user_pollution.muser_id";
	    $q=$this->db->query($sql);
		$response = $q->result_array();
	 	return $response;
}


}