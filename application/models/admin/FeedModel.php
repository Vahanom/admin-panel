<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class FeedModel extends CI_Model {

function view() 
{
	$sql = "SELECT tbl_questions.*,`tbl_question_cat`.`cat_title`,`tbl_question_sub`.`sub_title` FROM `tbl_questions` INNER JOIN tbl_question_sub ON `tbl_question_sub`.`sub_id`=tbl_questions.`subid` INNER JOIN tbl_question_cat on `tbl_question_cat`.`cat_id`=`tbl_questions`.`catid`";
    // echo $sql;die;
	$query=$this->db->query($sql);
	return $query->result();
}
public function getstatusWhereLike($filterstatus) {
        $sql = "SELECT tbl_questions.*,`tbl_question_cat`.`cat_title`,`tbl_question_sub`.`sub_title` FROM `tbl_questions` INNER JOIN tbl_question_sub ON `tbl_question_sub`.`sub_id`=tbl_questions.`subid` INNER JOIN tbl_question_cat on `tbl_question_cat`.`cat_id`=`tbl_questions`.`catid` where `tbl_questions`.`status`= '$filterstatus'";
        $query = $this->db->query($sql);
        return $query->result();
    }
public function cat_view() {
        $sql = "SELECT * FROM tbl_question_cat";
        $query = $this->db->query($sql);
        return $query->result();
    }
public function sub_cat_view() {
        $sql = "SELECT * FROM tbl_question_sub";
        $query = $this->db->query($sql);
        return $query->result();
    }
public function subcat_view() {
        $sql = "SELECT * FROM tbl_question_sub";
        $query = $this->db->query($sql);
        return $query->result();
    }
public function deletesubmodel($que_sub_category_edit){
        $sql = "DELETE tbl_questions.* FROM tbl_questions WHERE tbl_questions.subid='$que_sub_category_edit'";
        $query = $this->db->query($sql);
        $sql = "DELETE tbl_question_sub.* FROM tbl_question_sub  WHERE sub_id='$que_sub_category_edit'";
        $query = $this->db->query($sql);
        // print_r($sql);exit;
        return $query;
}
public function deletemodel($que_category_del){
        $sql = "DELETE tbl_questions.* FROM tbl_questions WHERE tbl_questions.catid='$que_category_del'";
        $query = $this->db->query($sql);
        $sql = "DELETE tbl_question_sub.* FROM tbl_question_sub  WHERE catid='$que_category_del'";
        $query = $this->db->query($sql);
        $sql = "DELETE tbl_question_cat.* FROM tbl_question_cat  WHERE cat_id='$que_category_del'";
        $query = $this->db->query($sql);
        // print_r($sql);exit;
        return $query;
}
function add($que_category,$que_sub_category,$ser_type,$ques,$status){
	$u_createdby = $this->session->userdata('usersid');
        $cnt = 0;
        $i=0;
        $cnt=count($ques);
        foreach ($ques as $row) {
        $sql = "INSERT INTO `tbl_questions`(`subid`,`ser_type`, `question_name`,`selection_type`, `createdat`, `catid`, `status`) VALUES (".$this->db->escape($que_sub_category).",".$this->db->escape($ser_type).",".$this->db->escape($row).",2".",CURRENT_TIMESTAMP,".$this->db->escape($que_category).",".$this->db->escape($status).")";    
        $query=$this->db->query($sql);
    // echo $sql;
        }
    	// exit;
	//echo $sql;die;
	if($query==1){
		return true;
	}
	else
	{
		return false;
	}
}
function cat_match($username)
{
    $this->db->select('cat_id'); 
    $this->db->from('tbl_question_cat');
    $this->db->where('cat_title', $username);
    $query = $this->db->get();
    $result = $query->result_array();
    // print_r($query);exit;
    
    return $result;
}
function sub_cat_match($username,$username1)
{
    // $this->db->select('sub_id'); 
    // $this->db->from('tbl_question_sub');
    // $where=array('sub_title'=> $username,'catid'=>$username1);
    // $this->db->where($where);
    // $query = $this->db->get();
    // $result = $query->result_array();
        
        $sql = "SELECT sub_id FROM tbl_question_sub where catid='$username1' and sub_title='$username'";
        // echo $sql;exit;
        $query = $this->db->query($sql);
        return $query->result();
        // print_r($query);exit;
    
    // return $result;
}

function addmodel($cat_name,$status){
    $u_createdby = $this->session->userdata('usersid');

    $sql = "INSERT INTO `tbl_question_cat`(`cat_title`, `status`,`createdat`) VALUES (".$this->db->escape($cat_name).",".$this->db->escape($status).",CURRENT_TIMESTAMP".")";
    //echo $sql;die;
    $query=$this->db->query($sql);
    if($query==1){
        return true;
    }
    else
    {
        return false;
    }
}
function subaddmodel($cat_name,$sub_cat_name,$status){
    $u_createdby = $this->session->userdata('usersid');

    $sql = "INSERT INTO `tbl_question_sub`(`catid`,`sub_title`, `status`,`createdat`) VALUES (".$this->db->escape($cat_name).",".$this->db->escape($sub_cat_name).",".$this->db->escape($status).",CURRENT_TIMESTAMP".")";
    //echo $sql;die;
    $query=$this->db->query($sql);
    if($query==1){
        return true;
    }
    else
    {
        return false;
    }
}

function update($que_name,$ser_type,$status,$queid){
  
    $userid = $this->session->userdata('usersid');
$sql = "UPDATE `tbl_questions` SET `ser_type`=".$ser_type.",`question_name`=".$this->db->escape($que_name).",`status`=".$this->db->escape($status).",`modified_at`=CURRENT_TIMESTAMP WHERE queid=".$this->db->escape($queid)."";
// echo $sql;die;
if ($this->db->simple_query($sql))
{
	$respose = $this->db->affected_rows();
	return $respose;
}
else
{
       return false;
}
}

function loaddata($id){
	$sql = "SELECT * FROM `tbl_vehicle` where vehb_id='$id'";

	$query = $this->db->query($sql);
  
	return $query->result();
}
function loaddata1($id){
	$sql = "SELECT tbl_questions.*,tbl_question_sub.sub_title,tbl_question_cat.cat_title FROM `tbl_questions` left join tbl_question_sub on tbl_question_sub.sub_id=tbl_questions.subid left join tbl_question_cat on tbl_question_cat.cat_id=tbl_questions.subid where queid='$id'";

	$query = $this->db->query($sql);
  
	return $query->result();
}

function loadFeeCategory($que_category){
     $this->db->where('catid', $que_category);
     $query = $this->db->get('tbl_question_sub');
     $output = '<option value="">Select Sub Category</option>';
        foreach($query->result() as $row)
        {
          $output .= '<option value="'.$row->sub_id.'">'.$row->sub_title.'</option>';
        }
      return $output;
}

function phonenocheckexist($username)
{
    $this->db->select('puser_id'); 
    $this->db->from('tbl_p_users');
    $this->db->where('puser_phno', $username);
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}

function emailcheckexist($username)
{
    $this->db->select('puser_id'); 
    $this->db->from('tbl_p_users');
    $this->db->where('puser_email', $username);
    $query = $this->db->get();
    $result = $query->result_array();

    return $result;
}

function getUserDetails(){
    $response = array();
    $sql = "SELECT `vehb_id`, `vehb_type`, `vehb_make`, `vehb_model`, `vehb_trim`, `vehb_fuel`, `vehb_image`, `vehb_status`, `vehb_createdate`, `vehb_modifydate` FROM `tbl_vehicle`";
    // echo $sql;die;
  $query=$this->db->query($sql);
    $response = $query->result_array();
    return $response;
  }

  

}