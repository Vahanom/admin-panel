<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class OrderModel extends CI_Model {

function view($order_value)
{
   if($order_value=='all')
   {
       $sql = "SELECT order_id,tbl_maddress.address,order_details_id,type_servicespare,order_type,order_date,order_schedule_date,order_address,order_pincode,technician_accept_status,supplier_order_status,order_status FROM `tbl_orders` INNER JOIN tbl_maddress on tbl_maddress.addressid=tbl_orders.order_address order by tbl_orders.order_id desc";
       // echo $sql;die;
      $query=$this->db->query($sql);
      return $query->result();
   }
   else
   {
       $sql = "SELECT order_id,type_servicespare,order_type,order_date,order_schedule_date,order_address,order_pincode,technician_accept_status,supplier_order_status,order_status FROM `tbl_orders` WHERE order_status='$order_value' order by tbl_orders.order_id desc";
           // echo $sql;die;
        $query=$this->db->query($sql);
        return $query->result();
   }
}

function loaddata1($id){
  $sql = "SELECT `order_id`,tbl_p_users.puser_name as tech_man,tbl_maddress.address,`type_servicespare`, tbl_orders.`puser_id`, `order_type`, `payment_id`, `order_date`, `order_schedule_date`, `order_schedule_timeslot`, `order_address`, `order_pincode`, `technician_id`, `technician_accept_status`, `supplier_id`, `supplier_order_status`, `technician_assignby`, `supplier_assignby`, `technician_otp`, `technician_otp_status`, `supplier_otp`, `supplier_otp_status`, `order_details_id`, `order_number`, `order_status`, `customer_otp`, `customer_otp_status`, `order_status_datetime`, `order_subtotal_amount`, `order_gst_amount`, `order_coupon_code`, `order_coupon_discount`, `order_total_amount` FROM `tbl_orders` Left join tbl_p_users on tbl_p_users.puser_id=tbl_orders.technician_id Left JOIN tbl_maddress on tbl_maddress.addressid=tbl_orders.order_address where tbl_orders.order_id='$id' order by tbl_orders.order_id desc";
// print_r($sql);exit;
  $query = $this->db->query($sql);
  
  return $query->result();
}

function loaddata2($order_details_id){
    $sql="SELECT tbl_order_images.* FROM tbl_order_images  WHERE tbl_order_images.orderID='$order_details_id'";
    // print_r($sql);exit;
    $query = $this->db->query($sql);
  
    return $query->result();
}
function loaddata3($id,$type_servicespare){
    if($type_servicespare=='service'){
        $sql="SELECT `ser_id`,`ser__title`,`od_service_price`,`type_servicespare` FROM tbl_orders JOIN tbl_orderdetails ON tbl_orders.order_id = tbl_orderdetails.od_order_id JOIN tbl_service ON tbl_service.ser_id = tbl_orderdetails.od_service_id WHERE type_servicespare='service' AND od_order_id='$id'";
          $query = $this->db->query($sql);
          return $query->result();
    }
    else{
        $sql="SELECT `sp_id`,`sp_title`,`od_service_price`,`quantity`,`type_servicespare` FROM tbl_orders JOIN tbl_orderdetails ON tbl_orders.order_id = tbl_orderdetails.od_order_id JOIN tbl_spare ON tbl_spare.sp_id = tbl_orderdetails.od_service_id WHERE type_servicespare='spare' AND od_order_id='$id'";
          $query = $this->db->query($sql);
          return $query->result();
    }
}

function loaddata4($id){
    $sql="SELECT tbl_maddress.muserid,tbl_maddress.type,tbl_maddress.address,tbl_maddress.pincode,tbl_m_users.muser_name FROM tbl_orders JOIN tbl_maddress ON tbl_orders.order_address = tbl_maddress.addressid JOIN tbl_m_users ON tbl_m_users.muser_id = tbl_maddress.muserid WHERE tbl_orders.order_id='$id'";

    $query = $this->db->query($sql);
  
    return $query->result();
}

function loaddata5($id){
    $sql="SELECT question_answer FROM submit_question WHERE orderid='$id'";
    // print_r($sql);exit;
    $query = $this->db->query($sql);
  
    return $query->result();
}

//Parking model
function parkingmodel($id){
  $sql = "SELECT order_id,tbl_orders.puser_id,puser_phno,puser_name FROM `tbl_p_users` LEFT join tbl_orders on tbl_p_users.puser_id=tbl_orders.puser_id where tbl_p_users.puser_id=tbl_orders.puser_id AND order_status='parking' AND order_id='$id'";  
  $query = $this->db->query($sql);
  return $query->result();
}

function updatestatus($radioid,$id){
  $sql = "UPDATE `tbl_orders` SET `order_status`=".$this->db->escape($radioid)." WHERE order_id=".$this->db->escape($id)."";
  if ($this->db->simple_query($sql))
     {
      $respose = $this->db->affected_rows();
      return $respose;
     }
    else
      {
          return false;
      }

   }

function updateTechnicalid($radioid,$id,$techicianid,$userid){
  $status ="ongoing";
  $sql = "UPDATE `tbl_orders` SET `technician_id`=".$this->db->escape($techicianid).",`technician_assignby`=".$this->db->escape($userid).",`order_status`=".$this->db->escape($status)." WHERE order_id=".$this->db->escape($id)."";
  if ($this->db->simple_query($sql))
     {
      $respose = $this->db->affected_rows();
      return $respose;
     }
    else
      {
          return false;
      }

   }

function getdropdown($str)
    {
        $this->db->select('puser_id, puser_name as text,CONCAT(puser_name, " || ", puser_phno) AS text1');
        $this->db->like('puser_name', $str);
        $this->db->where('puser_type', 'Technician');
        $query = $this->db->get('tbl_p_users');
        return $query->result();
    }

//Pending Model
function pendingmodel($id){
  $sql = "SELECT order_id,tbl_orders.puser_id,puser_phno,puser_name FROM `tbl_p_users` LEFT join tbl_orders on tbl_p_users.puser_id=tbl_orders.puser_id where tbl_p_users.puser_id=tbl_orders.puser_id AND order_status='pending' AND order_id='$id' order by tbl_orders.order_id desc";
   // echo $sql; die;
  $query = $this->db->query($sql);
  return $query->result();

}

function updatependingstatus($radioid,$id){
  $sql = "UPDATE `tbl_orders` SET `order_status`=".$this->db->escape($radioid)." WHERE order_id=".$this->db->escape($id)."";
  if ($this->db->simple_query($sql))
     {
      $respose = $this->db->affected_rows();
      return $respose;
     }
    else
      {
          return false;
      }

   }

function updatependingTechnicalid($radioid,$id,$techicianid){
  $sql = "UPDATE `tbl_orders` SET `technician_id`=".$this->db->escape($techicianid)." WHERE order_id=".$this->db->escape($id)."";
  if ($this->db->simple_query($sql))
     {
      $respose = $this->db->affected_rows();
      return $respose;
     }
    else
      {
          return false;
      }

   }

function getpeningdropdown($str)
    {
      
        $this->db->select('puser_id, puser_name as text,CONCAT(puser_name, " | ", puser_phno) AS text1');
        $this->db->like('puser_name', $str);
        $this->db->where('puser_type', 'Technician');
        $query = $this->db->get('tbl_p_users');
        return $query->result();
    }

public function getstatusWhereLike($orderstatus) {
        $sql = "SELECT order_id,tbl_maddress.address,order_details_id,type_servicespare,order_type,order_date,order_schedule_date,order_address,order_pincode,technician_accept_status,supplier_order_status,order_status FROM `tbl_orders` INNER JOIN tbl_maddress on tbl_maddress.addressid=tbl_orders.order_address WHERE `tbl_orders`.order_status LIKE '%$orderstatus%' order by tbl_orders.order_id desc";
        // echo $sql;exit;
        $query = $this->db->query($sql);
        return $query->result();
    }
function getUserDetails($data){
    $response = array();
    $this->db->select('order_type,order_date,order_address,order_pincode,technician_accept_status,supplier_order_status,order_status');
    $this->db->where('order_status', "$data");
    $q = $this->db->get('tbl_orders');
    $response = $q->result_array();
    return $response;
  }
  
function getUserDetails1(){
    $response = array();
    $this->db->select('order_type,order_date,order_address,order_pincode,technician_accept_status,supplier_order_status,order_status');
    $q = $this->db->get('tbl_orders');
    $response = $q->result_array();
    return $response;
  }



}