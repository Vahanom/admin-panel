<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class NotificationModel extends CI_Model {


function view()
{
	$sql = "SELECT noti_id,noti_title,noti_description FROM `tbl_notifications`";
    // echo $sql;die;
	$query=$this->db->query($sql);
	return $query->result();
}

function add($noti_title,$noti_description,$noti_user){
	$u_createdby = $this->session->userdata('usersid');

	$sql = "INSERT INTO `tbl_notifications`(`noti_title`, `noti_description`,`noti_user`, `noti_create_date`, `noti_createdby`) VALUES (".$this->db->escape($noti_title).",".$this->db->escape($noti_description).",".$this->db->escape($noti_user).",CURRENT_TIMESTAMP,".$this->db->escape($u_createdby).")";
	$query=$this->db->query($sql);
	if($query==1){
		return true;
	}
	else
	{
		return false;
	}
}


function loaddata1($id){
	$sql = "SELECT noti_id,noti_title,noti_description,noti_user FROM `tbl_notifications` where noti_id='$id'";

	$query = $this->db->query($sql);
  
	return $query->result();
}

function getUserDetails(){
    $response = array();
    $sql = "SELECT `noti_id`, `noti_title`, `noti_description`, `noti_user`, `noti_status`, `noti_create_date` FROM `tbl_notifications";
    // echo $sql;die;
  $query=$this->db->query($sql);
    $response = $query->result_array();
    return $response;
  }
  
  function viewcustomer()
{
	$sql = "SELECT * FROM tbl_m_users where m_deviceid NOT in('','Null','NULL')";
	$query=$this->db->query($sql);
	return $query->result();	
}

function viewtech()
{
	$sql = "SELECT * FROM tbl_p_users where puser_type='Technician' and puser_deviceid NOT in('','Null','NULL')";
	$query=$this->db->query($sql);
	return $query->result();	
}
function viewsupp()
{
	$sql = "SELECT * FROM tbl_p_users where puser_type='Supplier' and puser_deviceid NOT in('','Null','NULL')";
	$query=$this->db->query($sql);
	return $query->result();	
}


}