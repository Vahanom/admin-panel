<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class UserPollutionModel extends CI_Model {

function view()
{
	$sql = "SELECT
              tbl_user_pollution.user_pollutionid,status,remark,
              tbl_m_users.muser_id,tbl_m_users.muser_name,
              tbl_pollutions.pol_id
              FROM tbl_m_users
              JOIN tbl_user_pollution
              ON tbl_m_users.muser_id = tbl_user_pollution.muser_id
              JOIN tbl_pollutions
              ON tbl_pollutions.pol_id = tbl_user_pollution.pollution_id";
    // echo $sql;die;
	$query=$this->db->query($sql);
	return $query->result();
}

public function getstatusWhereLike($filterstatus) {
        $sql = "SELECT * FROM tbl_user_pollution WHERE status= '$filterstatus'";
        $query = $this->db->query($sql);
        return $query->result();
    }

function loaddata1($id,$muser_id){
	$sql = "SELECT muser_name,remark,status FROM `tbl_user_pollution` Right join tbl_m_users on tbl_user_pollution.muser_id=tbl_m_users.muser_id where tbl_user_pollution.muser_id='$muser_id'";

	$query = $this->db->query($sql);
  
	return $query->result();
}
function loaddata2($id,$pol_id){
	$sql = "SELECT pol_title,pol_description,pol_img,pol_thumb_img,price,selling_price
            FROM `tbl_user_pollution` 
            Right join tbl_pollutions on 
            tbl_user_pollution.pollution_id=tbl_pollutions.pol_id 
            where tbl_user_pollution.pollution_id='$pol_id'";
	$query = $this->db->query($sql);
  	return $query->result();
}

function getUserDetails(){
    $response = array();
    $sql = "SELECT
              tbl_user_pollution.user_pollutionid,tbl_pollutions.pol_id,
              tbl_m_users.muser_name,status,remark,createdbydate
              FROM tbl_m_users
              JOIN tbl_user_pollution
              ON tbl_m_users.muser_id = tbl_user_pollution.muser_id
              JOIN tbl_pollutions
              ON tbl_pollutions.pol_id = tbl_user_pollution.pollution_id";
    // echo $sql;die;
  $query=$this->db->query($sql);
    $response = $query->result_array();
    return $response;
  }



}