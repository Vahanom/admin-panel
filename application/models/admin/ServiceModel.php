<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class ServiceModel extends CI_Model {


function view()
{
	$sql = "SELECT ser__title,ser_thumbnail,ser_status,ser_id FROM `tbl_service`";
    // echo $sql;die;
	$query=$this->db->query($sql);
	return $query->result();
}
public function getstatusWhereLike($filterstatus) {
        $sql = "SELECT * FROM tbl_service WHERE ser_status= '$filterstatus' ";
        $query = $this->db->query($sql);
        return $query->result();
    }

function add($service_title,$service_desc,$type_vehicle,$servicecategory,$status,$imageone,$imagetwo,$service_point,$service_extra,$servicetype,$service_time,$service_amount3,$service_amount2,$service_amount1,$labour_rate,$service_amount,$price,$selling_price){
	$u_createdby = $this->session->userdata('usersid');
    $gstamount = $selling_price*($service_extra/100);
    $gstexcludedspprice = $selling_price-$gstamount;
	$sql = "INSERT INTO `tbl_service`(`ser__title`, `ser__description`,`service_vehicle_type`, `ser_thumbnail`, `ser_image`,`ser_type`, `ser_points`, `ser_extra_field`,`ser_status`, `ser_createddate`,`ser_createdby`,`ser_category`,`labour_rate`, `ser_amt`,`ser_amt_level_one`, `ser_amt_level_two`,`ser_amt_level_three`,`ser_hours`,`price`,`gstexcludedspprice`,`gstamount`,`sellingprice`) VALUES (".$this->db->escape($service_title).",".$this->db->escape($service_desc).",".$this->db->escape($type_vehicle).",".$this->db->escape($imageone).",".$this->db->escape($imagetwo).",".$this->db->escape($servicetype).",".$this->db->escape($service_point).",".$this->db->escape($service_extra).",".$this->db->escape($status).",CURRENT_TIMESTAMP,".$this->db->escape($u_createdby).",".$this->db->escape($servicecategory).",".$this->db->escape($labour_rate).",".$this->db->escape($service_amount).",".$this->db->escape($service_amount1).",".$this->db->escape($service_amount2).",".$this->db->escape($service_amount3).",".$this->db->escape($service_time).",".$this->db->escape($price).",".$this->db->escape($gstexcludedspprice).",".$this->db->escape($gstamount).",".$this->db->escape($selling_price)."
	)";
// 	echo $sql;exit;
	$query=$this->db->query($sql);
	if($query==1){
		return true;
	}
	else
	{
		return false;
	}
}

function update($service_title,$service_desc,$type_vehicle,$servicecategory,$status,$imageone,$imagetwo,$service_point,$service_extra,$servicetype,$service_time,$service_amount3,$service_amount2,$service_amount1,$labour_rate,$service_amount,$ser_id,$price,$selling_price){
  
    $userid = $this->session->userdata('usersid');
$gstamount = $selling_price*($service_extra/100);
$gstexcludedspprice = $selling_price-$gstamount;

$sql = "UPDATE `tbl_service` SET `ser__title`=".$this->db->escape($service_title).",`ser__description`=".$this->db->escape($service_desc).",`service_vehicle_type`=".$this->db->escape($type_vehicle).",`ser_status`=".$this->db->escape($status)." 
,`ser_thumbnail`=".$this->db->escape($imageone).",`ser_image`=".$this->db->escape($imagetwo).",`ser_type`=".$this->db->escape($servicetype)." ,`ser_modifiedby`=".$this->db->escape($userid).",`ser_modifieddate`=CURRENT_TIMESTAMP
,`ser_points`=".$this->db->escape($service_point).",`ser_extra_field`=".$this->db->escape($service_extra).",`ser_category`=".$this->db->escape($servicecategory).",`ser_amt_level_two`=".$this->db->escape($service_amount2).",`labour_rate`=".$this->db->escape($labour_rate).",`ser_amt_level_three`=".$this->db->escape($service_amount3).",`ser_hours`=".$this->db->escape($service_time).",`ser_amt_level_one`=".$this->db->escape($service_amount1).",`ser_amt`=".$this->db->escape($service_amount).",`price`=".$this->db->escape($price)
.",`gstamount`=".$this->db->escape($gstamount).",`gstexcludedspprice`=".$this->db->escape($gstexcludedspprice).",`sellingprice`=".$this->db->escape($selling_price)."
WHERE ser_id=".$this->db->escape($ser_id)."";
	// echo $sql;die;
if ($this->db->simple_query($sql))
{
	$respose = $this->db->affected_rows();
	return $respose;
}
else
{
       return false;
}
}

function loaddata($id){
	$sql = "SELECT * FROM `tbl_service` where ser_id='$id'";

	$query = $this->db->query($sql);
  
	return $query->result();
}
function loaddata1($id){
	$sql = "SELECT * FROM `tbl_service` where ser_id='$id'";
	$query = $this->db->query($sql);
  	return $query->result();
}

function loaddataservice($id){
	$sql = "SELECT ser_catid,ser_cat_title
           FROM `tbl_service` 
           RIGHT join tbl_service_cat on 
           tbl_service.ser_category=tbl_service_cat.ser_catid 
           where 
           tbl_service.ser_id='$id'";
	$query = $this->db->query($sql);
  	return $query->result();
}


function phonenocheckexist($username)
{
    $this->db->select('puser_id'); 
    $this->db->from('tbl_p_users');
    $this->db->where('puser_phno', $username);
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}

function emailcheckexist($username)
{
    $this->db->select('puser_id'); 
    $this->db->from('tbl_p_users');
    $this->db->where('puser_email', $username);
    $query = $this->db->get();
    $result = $query->result_array();

    return $result;
}

function servicecat(){
	$sql = "SELECT ser_catid,ser_cat_title  FROM `tbl_service_cat` where `ser_category_status`='Active'";
    // echo $sql;die;
	$query=$this->db->query($sql);
	return $query->result();
}

function getUserDetails(){
    $response = array();
    $sql = "SELECT `ser_id`, `ser__title`, `ser__description`, `service_vehicle_type`, `ser_type`, `ser_points`, `ser_extra_field`, `ser_status`, `ser_createddate`, `ser_modifieddate`, `ser_category`, `ser_amt`, `ser_amt_level_one`, `ser_amt_level_two`, `ser_amt_level_three`, `ser_hours`, `price`, `sellingprice` FROM `tbl_service`";
    // echo $sql;die;
  $query=$this->db->query($sql);
    $response = $query->result_array();
    return $response;
}
//duplicate name check functions 
function namecheckexist1($username)
{
    $this->db->select('ser_id'); 
    $this->db->from('tbl_service');
    $this->db->where('ser__title', rtrim($username));
    $query = $this->db->get();
    $result = $query->result_array();

    return $result;
}


}
