<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class SparecatModel extends CI_Model {


function view()
{
	$sql = "SELECT spcat_title,spcat_vehicle_type,spcat_status,spcat_id FROM `tbl_spare_catg`";
    // echo $sql;die;
	$query=$this->db->query($sql);
	return $query->result();
}

public function getstatusWhereLike($filterstatus) {
        $sql = "SELECT * FROM tbl_spare_catg WHERE spcat_status= '$filterstatus'";
        $query = $this->db->query($sql);
        return $query->result();
    }

function add($space_categorytitle,$spare_desc,$type_vehicle,$status){
	$u_createdby = $this->session->userdata('usersid');

	$sql = "INSERT INTO `tbl_spare_catg`(`spcat_title`, `spcat_description`,`spcat_vehicle_type`, `spcat_status`) VALUES (".$this->db->escape($space_categorytitle).",".$this->db->escape($spare_desc).",".$this->db->escape($type_vehicle).",".$this->db->escape($status).")";
	//echo $sql;die;
	$query=$this->db->query($sql);
	if($query==1){
		return true;
	}
	else
	{
		return false;
	}
}

function update($space_categorytitle,$spare_desc,$type_vehicle,$status,$spcat_id){
  
    $userid = $this->session->userdata('usersid');
$sql = "UPDATE `tbl_spare_catg` SET `spcat_title`=".$this->db->escape($space_categorytitle).",`spcat_description`=".$this->db->escape($spare_desc).",`spcat_vehicle_type`=".$this->db->escape($type_vehicle).",`spcat_status`=".$this->db->escape($status)." WHERE spcat_id=".$this->db->escape($spcat_id)."";
// echo $sql;die;
if ($this->db->simple_query($sql))
{
	$respose = $this->db->affected_rows();
	return $respose;
}
else
{
       return false;
}
}

function loaddata($id){
	$sql = "SELECT * FROM `tbl_spare_catg` where spcat_id='$id'";

	$query = $this->db->query($sql);
  
	return $query->result();
}
function loaddata1($id){
	$sql = "SELECT * FROM `tbl_spare_catg` where spcat_id='$id'";

	$query = $this->db->query($sql);
  
	return $query->result();
}
function namecheckexist1($username)
{
    $this->db->select('spcat_id'); 
    $this->db->from('tbl_spare_catg');
    $this->db->where('spcat_title', rtrim($username));
    $query = $this->db->get();
    $result = $query->result_array();

    return $result;
}

function phonenocheckexist($username)
{
    $this->db->select('puser_id'); 
    $this->db->from('tbl_p_users');
    $this->db->where('puser_phno', $username);
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}

function emailcheckexist($username)
{
    $this->db->select('puser_id'); 
    $this->db->from('tbl_p_users');
    $this->db->where('puser_email', $username);
    $query = $this->db->get();
    $result = $query->result_array();

    return $result;
}

function getUserDetails(){
    $response = array();
    $sql = "SELECT spcat_id,spcat_title, spcat_description,spcat_vehicle_type, spcat_status FROM `tbl_spare_catg`";
    // echo $sql;die;
  $query=$this->db->query($sql);
    $response = $query->result_array();
    return $response;
}

/*function getUserDetails(){
    $response = array();
    $sql = "SELECT `ser_id`, `ser__title`, `ser__description`, `service_vehicle_type`, `ser_type`, `ser_points`, `ser_extra_field`, `ser_status`, `ser_createddate`, `ser_modifieddate`, `ser_category`, `ser_amt`, `ser_amt_level_one`, `ser_amt_level_two`, `ser_amt_level_three`, `ser_hours`, `price`, `sellingprice` FROM `tbl_service`";
    // echo $sql;die;
  $query=$this->db->query($sql);
    $response = $query->result_array();
    return $response;
}
*/


}