<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class ReportModel extends CI_Model {


function get_order_type($order_type,$order_status,$startdate,$enddate)
{
//	$sql = "SELECT order_id FROM `tbl_orders` Where order_type='$order_type' AND order_status='$order_status'";
    // echo $sql;die;
 if($order_type=='All' && $order_status=='All')
  { 
     $sql="select tbl_orderdetails.od_order_id as `Id`, order_type, order_date, order_schedule_date, order_address, order_pincode, order_number, order_status, type_servicespare,
          tbl_service.ser_id as `Service Id`,
          GROUP_CONCAT(tbl_service.ser__title) as `Service`
        from
          tbl_orderdetails,
          tbl_service,
          tbl_orders
        where
          tbl_orderdetails.od_service_id=tbl_service.ser_id AND 
          tbl_orders.order_id=tbl_orderdetails.od_order_id
       
        AND
           order_date BETWEEN '$startdate' AND '$enddate'
          group by tbl_orderdetails.od_order_id";

     $query=$this->db->query($sql);
     return $query->result();

  }
  elseif($order_type=='All')
  { 
     $sql="select tbl_orderdetails.od_order_id as `Id`, order_type, order_date, order_schedule_date, order_address, order_pincode, order_number, order_status, type_servicespare,
          tbl_service.ser_id as `Service Id`,
          GROUP_CONCAT(tbl_service.ser__title) as `Service`
        from
          tbl_orderdetails,
          tbl_service,
          tbl_orders
        where
          tbl_orderdetails.od_service_id=tbl_service.ser_id AND 
          tbl_orders.order_id=tbl_orderdetails.od_order_id
           AND
           tbl_orders.order_status='$order_status' 
      
        AND
           order_date BETWEEN '$startdate' AND '$enddate'
          group by tbl_orderdetails.od_order_id";

     $query=$this->db->query($sql);
     return $query->result();

  }
  else
  {  //for Spare
     $sql="select tbl_orderdetails.od_order_id as `Id`, order_type, order_date, order_schedule_date, order_address, order_pincode, order_number, order_status, type_servicespare,type_servicespare,
          tbl_spare.sp_id as `Service Id`,
          GROUP_CONCAT(tbl_spare.sp_title) as `Service`
        from
          tbl_orderdetails,
          tbl_spare,
          tbl_orders
        where
          tbl_orderdetails.od_service_id=tbl_spare.sp_id AND 
          tbl_orders.order_id=tbl_orderdetails.od_order_id
        AND
          tbl_orders.order_type='$order_type' AND tbl_orders.order_status='$order_status' AND type_servicespare='Spare'
        AND
          order_date BETWEEN '$startdate' AND '$enddate'
          group by tbl_orderdetails.od_order_id";

     $query=$this->db->query($sql);
     $t= $query->result();

  // for services and combne two query..
     $sql2="select tbl_orderdetails.od_order_id as `Id`, order_type, order_date, order_schedule_date, order_address, order_pincode, order_number, order_status, type_servicespare,type_servicespare,
          tbl_service.ser_id as `Service Id`,
          GROUP_CONCAT(tbl_service.ser__title) as `Service`
        from
          tbl_orderdetails,
          tbl_service,
          tbl_orders
        where
          tbl_orderdetails.od_service_id=tbl_service.ser_id AND 
          tbl_orders.order_id=tbl_orderdetails.od_order_id
        AND
          tbl_orders.order_type='$order_type' AND tbl_orders.order_status='$order_status' AND type_servicespare='Service'
        AND
          order_date BETWEEN '$startdate' AND '$enddate'
          group by tbl_orderdetails.od_order_id";

     $query2=$this->db->query($sql2);
     $t2= $query2->result();
      $results = array_merge($t, $t2);
     return $results;

  }
}


function get_technician_order_report($startdate,$enddate){
   $sql="SELECT order_id,order_status,order_type, order_date,order_schedule_date, order_address,order_pincode,order_number,
         tbl_p_users.puser_name ,puser_type
         FROM `tbl_orders` left join tbl_p_users on tbl_orders.technician_id=tbl_p_users.puser_id 
         Where technician_id!='' AND tbl_p_users.puser_type='Technician' AND technician_id IS NOT NULL
         AND order_date BETWEEN '$startdate' AND '$enddate'";
  $query=$this->db->query($sql);
     return $query->result();
}

function get_supplier_order_report($startdate,$enddate){
   $sql="SELECT order_id,order_status,order_type, order_date,order_schedule_date, order_address,order_pincode,order_number,
         tbl_p_users.puser_name ,puser_type 
         FROM `tbl_orders` left join tbl_p_users on tbl_orders.supplier_id=tbl_p_users.puser_id 
         Where supplier_id!='' AND tbl_p_users.puser_type='Supplier' AND supplier_id IS NOT NULL AND order_date BETWEEN '$startdate' AND '$enddate'";
  $query=$this->db->query($sql);
     return $query->result();
}

function get_special_type($startdate,$enddate,$special_type){
  if($special_type=='Insurance'){
      $sql = "SELECT
              tbl_user_insurance.user_insuranceid,tbl_user_insurance.insurance_id,tbl_user_insurance.user_m_id,
              tbl_m_users.muser_name,tbl_insurance.inusr_title,remark,status,createdbydate
            FROM tbl_m_users
            JOIN tbl_user_insurance
            ON tbl_m_users.muser_id = tbl_user_insurance.user_m_id
            JOIN tbl_insurance
            ON tbl_insurance.inusr_id = tbl_user_insurance.insurance_id
            WHERE createdbydate BETWEEN '$startdate' AND '$enddate'";
       // echo $sql;die;
      $query=$this->db->query($sql);
      return $query->result();

  }
  elseif($special_type=='PrePurchase'){
       $sql = "SELECT tbl_prepurchaseinspection.id,tbl_user_vehicles.vehb_id,tbl_prepurchaseinspection.userID,
                tbl_m_users.muser_name,inspectiontype,remark,status,createdat
            FROM tbl_m_users 
            JOIN tbl_prepurchaseinspection ON 
            tbl_m_users.muser_id = tbl_prepurchaseinspection.userID 
            JOIN tbl_user_vehicles ON 
            tbl_user_vehicles.vehb_id = tbl_prepurchaseinspection.vehicleid
            WHERE createdat BETWEEN '$startdate' AND '$enddate'";
    // echo $sql;die;
     $query=$this->db->query($sql);
     return $query->result();

  }
  elseif($special_type=='Pollution'){
       $sql = "SELECT
              tbl_user_pollution.user_pollutionid,tbl_pollutions.pol_id,tbl_m_users.muser_id,
              tbl_m_users.muser_name,tbl_pollutions.pol_title,remark,status,createdbydate
              FROM tbl_m_users
              JOIN tbl_user_pollution
              ON tbl_m_users.muser_id = tbl_user_pollution.muser_id
              JOIN tbl_pollutions
              ON tbl_pollutions.pol_id = tbl_user_pollution.pollution_id
              WHERE createdbydate BETWEEN '$startdate' AND '$enddate'";
         // echo $sql;die;
        $query=$this->db->query($sql);
        return $query->result();

  }
  elseif($special_type=='QuickInspection'){
       $sql = "SELECT
              tbl_quickinspection.id,tbl_quickinspection.vehicleid,tbl_quickinspection.userID,
              tbl_m_users.muser_name,inspectiontype,remark,status,createdat
            FROM tbl_m_users
            JOIN tbl_quickinspection
            ON tbl_m_users.muser_id = tbl_quickinspection.userID
            JOIN tbl_vehicle
            ON tbl_vehicle.vehb_id = tbl_quickinspection.vehicleid
            WHERE createdat BETWEEN '$startdate' AND '$enddate'";
    // echo $sql;die;
       $query=$this->db->query($sql);
       return $query->result();
  }
  else{
    $sql = "SELECT
              tbl_user_insurance.user_insuranceid,tbl_user_insurance.insurance_id,tbl_user_insurance.user_m_id,
              tbl_m_users.muser_name,tbl_insurance.inusr_title,remark,status,createdbydate
            FROM tbl_m_users
            JOIN tbl_user_insurance
            ON tbl_m_users.muser_id = tbl_user_insurance.user_m_id
            JOIN tbl_insurance
            ON tbl_insurance.inusr_id = tbl_user_insurance.insurance_id
            WHERE createdbydate BETWEEN '$startdate' AND '$enddate'";
       // echo $sql;die;
      $query=$this->db->query($sql);
      $result=$query->result();

      $sql1 = "SELECT tbl_prepurchaseinspection.id,tbl_user_vehicles.vehb_id,tbl_prepurchaseinspection.userID,
                tbl_m_users.muser_name,inspectiontype,remark,status,createdat
            FROM tbl_m_users 
            JOIN tbl_prepurchaseinspection ON 
            tbl_m_users.muser_id = tbl_prepurchaseinspection.userID 
            JOIN tbl_user_vehicles ON 
            tbl_user_vehicles.vehb_id = tbl_prepurchaseinspection.vehicleid
            WHERE createdat BETWEEN '$startdate' AND '$enddate'";
    // echo $sql;die;
     $query1=$this->db->query($sql1);
     $result1= $query1->result();

     $sql2 = "SELECT
              tbl_user_pollution.user_pollutionid,tbl_pollutions.pol_id,tbl_m_users.muser_id,
              tbl_m_users.muser_name,tbl_pollutions.pol_title,remark,status,createdbydate
              FROM tbl_m_users
              JOIN tbl_user_pollution
              ON tbl_m_users.muser_id = tbl_user_pollution.muser_id
              JOIN tbl_pollutions
              ON tbl_pollutions.pol_id = tbl_user_pollution.pollution_id
              WHERE createdbydate BETWEEN '$startdate' AND '$enddate'";
         // echo $sql;die;
        $query2=$this->db->query($sql2);
        $result2= $query2->result();

     $sql3 = "SELECT
              tbl_quickinspection.id,tbl_quickinspection.vehicleid,tbl_quickinspection.userID,
              tbl_m_users.muser_name,inspectiontype,remark,status,createdat
            FROM tbl_m_users
            JOIN tbl_quickinspection
            ON tbl_m_users.muser_id = tbl_quickinspection.userID
            JOIN tbl_vehicle
            ON tbl_vehicle.vehb_id = tbl_quickinspection.vehicleid
            WHERE createdat BETWEEN '$startdate' AND '$enddate'";
    // echo $sql;die;
       $query3=$this->db->query($sql3);
       $result3= $query3->result();


     $results = array_merge($result, $result1,$result2,$result3);
     return $results;
  }
}

//Spare report
public function select_spare(){
  $sql = "SELECT `sp_id`,`sp_title` FROM `tbl_spare`";

  $query = $this->db->query($sql);
  
  return $query->result();
}

public function get_spare_order_report($spare,$startdate,$enddate){
  $sql = "SELECT `sp_id`,`order_id`, `order_type`, `order_date`, `order_schedule_date`, `order_address`, `order_pincode`, `order_details_id`, `order_number`, `order_status`, `type_servicespare`
           FROM tbl_orders
         JOIN tbl_orderdetails
           ON tbl_orders.order_id = tbl_orderdetails.od_order_id
         JOIN tbl_spare
           ON tbl_spare.sp_id = tbl_orderdetails.od_service_id
         WHERE type_servicespare='Spare' AND od_service_id ='$spare'
          AND
          order_date BETWEEN '$startdate' AND '$enddate'";

  $query = $this->db->query($sql);
  
  return $query->result();
}


}