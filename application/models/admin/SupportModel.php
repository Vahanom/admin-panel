<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class SupportModel extends CI_Model {


function view()
{
	$sql = "SELECT support_id,support_phno,support_email FROM `tbl_support`";
    // echo $sql;die;
	$query=$this->db->query($sql);
	return $query->result();
}

function add($support_email,$support_phno){
	$u_createdby = $this->session->userdata('usersid');

	$sql = "INSERT INTO `tbl_support`(`support_email`,`support_phno`,`support_create_date`, `support_createdby`) VALUES (".$this->db->escape($support_email).",".$this->db->escape($support_phno).",CURRENT_TIMESTAMP,".$this->db->escape($u_createdby).")";
	$query=$this->db->query($sql);
	if($query==1){
		return true;
	}
	else
	{
		return false;
	}
}


function loaddata1($id){
	$sql = "SELECT support_id,support_phno,support_email FROM `tbl_support` where support_id='$id'";

	$query = $this->db->query($sql);
  
	return $query->result();
}

function loaddata($id){
	$sql = "SELECT support_id,support_phno,support_email FROM `tbl_support` where support_id='$id'";

	$query = $this->db->query($sql);
  
	return $query->result();
}

function update($support_id,$support_phno,$support_email){
  
       $userid = $this->session->userdata('usersid');
$sql = "UPDATE `tbl_support` SET `support_phno`=".$this->db->escape($support_phno).",`support_email`=".$this->db->escape($support_email).",`support_modify_date`=CURRENT_TIMESTAMP,`support_modifyby`='$userid' WHERE support_id=".$this->db->escape($support_id)."";
if ($this->db->simple_query($sql))
{
	$respose = $this->db->affected_rows();
	return $respose;
}
else
{
       return false;
}
}

function emailcheckexist($username)
{
    $this->db->select('support_id'); 
    $this->db->from('tbl_support');
    $this->db->where('support_email', $username);
    $query = $this->db->get();
    $result = $query->result_array();

    return $result;
}

function getUserDetails(){
 		$response = array();
		$this->db->select('support_id,support_phno,support_email,support_create_date,support_modify_date');
		$q = $this->db->get('tbl_support');
		$response = $q->result_array();
	 	return $response;
}

}