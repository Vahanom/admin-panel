<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class UserInsuranceModel extends CI_Model {

function view()
{
	$sql = "SELECT
              tbl_user_insurance.user_insuranceid,tbl_user_insurance.insurance_id,tbl_user_insurance.user_m_id,
              tbl_m_users.muser_name,remark,status
            FROM tbl_m_users
            JOIN tbl_user_insurance
            ON tbl_m_users.muser_id = tbl_user_insurance.user_m_id
            JOIN tbl_insurance
            ON tbl_insurance.inusr_id = tbl_user_insurance.insurance_id";
    // echo $sql;die;
	$query=$this->db->query($sql);
	return $query->result();
}

public function getstatusWhereLike($filterstatus) {
        $sql = "SELECT * FROM tbl_user_insurance WHERE status= '$filterstatus'";
        $query = $this->db->query($sql);
        return $query->result();
    }

function loaddata1($id,$user_m_id){
	$sql = "SELECT muser_name,status,remark FROM `tbl_user_insurance` Right join tbl_m_users on tbl_user_insurance.user_m_id=tbl_m_users.muser_id where tbl_user_insurance.user_m_id='$user_m_id'";

	$query = $this->db->query($sql);
  
	return $query->result();
}
function loaddata2($id,$insurance_id){
	$sql = "SELECT inusr_title,inusr_description,inusr_img,inusr_thumb_img,inusr_type,price,selling_price
	        FROM `tbl_user_insurance` Right join tbl_insurance on 
	        tbl_user_insurance.insurance_id=tbl_insurance.inusr_id 
	        where tbl_insurance.inusr_id='$insurance_id'";
	$query = $this->db->query($sql);
  	return $query->result();
}

function getUserDetails(){
    $response = array();
    $sql = "SELECT
              tbl_user_insurance.user_insuranceid,tbl_user_insurance.insurance_id,
              tbl_m_users.muser_name,remark,status,createdbydate
            FROM tbl_m_users
            JOIN tbl_user_insurance
            ON tbl_m_users.muser_id = tbl_user_insurance.user_m_id
            JOIN tbl_insurance
            ON tbl_insurance.inusr_id = tbl_user_insurance.insurance_id";
    // echo $sql;die;
  $query=$this->db->query($sql);
    $response = $query->result_array();
    return $response;
  }



}