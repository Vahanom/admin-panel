<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class CouponModel extends CI_Model {


function view()
{
	$sql = "SELECT coupon_title,cpn_code,cpn_status,cpn_id FROM `tbl_coupon`";
    // echo $sql;die;
	$query=$this->db->query($sql);
	return $query->result();
}
public function getstatusWhereLike($filterstatus) {
        $sql = "SELECT * FROM tbl_coupon WHERE cpn_status= '$filterstatus'";
        $query = $this->db->query($sql);
        return $query->result();
    }

function add($Coupon_title,$Coupon_desc,$coupontype,$Coupon_flat,$Coupon_per,$start_date,$end_date,$maximumbillamount,$maxuserlimit,$maxuseperuser,$minimumbillamount,$status,$couponcode){
	$u_createdby = $this->session->userdata('usersid');

	$sql = "INSERT INTO `tbl_coupon`(`coupon_title`, `cpn_code`,`cpn_description`, `cpn_type`, `cpn_flat_amount`,`cpn_percentage`, `cpn_startdate`,`cpn_enddate`, `cpn_status`, `cpn_min_bill_amt`, `cpn_max_discount`,`cpn_max_users`, `cpn_maxperusers`, `cpn_createdate`,`cpn_createby`) VALUES (".$this->db->escape($Coupon_title).",".$this->db->escape($couponcode).",".$this->db->escape($Coupon_desc).",".$this->db->escape($coupontype).",".$this->db->escape($Coupon_flat).",".$this->db->escape($Coupon_per).",".$this->db->escape($start_date).",".$this->db->escape($end_date).",".$this->db->escape($status).",".$this->db->escape($minimumbillamount).",".$this->db->escape($maximumbillamount).",".$this->db->escape($maxuserlimit).",".$this->db->escape($maxuseperuser).",CURRENT_TIMESTAMP,".$this->db->escape($u_createdby)."
	)";
	$query=$this->db->query($sql);
	if($query==1){
		return true;
	}
	else
	{
		return false;
	}
}

function update($Coupon_title,$Coupon_desc,$coupontype,$Coupon_flat,$Coupon_per,$start_date,$end_date,$maximumbillamount,$maxuserlimit,$maxuseperuser,$minimumbillamount,$status,$couponcode,$cpn_id){
  
    $userid = $this->session->userdata('usersid');
$sql = "UPDATE `tbl_coupon` SET `coupon_title`=".$this->db->escape($Coupon_title).",`cpn_code`=".$this->db->escape($couponcode).",`cpn_status`=".$this->db->escape($status)." 
,`cpn_description`=".$this->db->escape($Coupon_desc).",`cpn_type`=".$this->db->escape($coupontype).",
`cpn_flat_amount`=".$this->db->escape($Coupon_flat).",`cpn_percentage`=".$this->db->escape($Coupon_per).",`cpn_startdate`=".$this->db->escape($start_date)." 
,`cpn_enddate`=".$this->db->escape($end_date).",`cpn_min_bill_amt`=".$this->db->escape($minimumbillamount)."

,`cpn_max_discount`=".$this->db->escape($maximumbillamount).",
`cpn_max_users`=".$this->db->escape($maxuserlimit).",`cpn_max_users`=".$this->db->escape($maxuseperuser).",`cpn_modifieddate`=".$this->db->escape($start_date)." 
,`cpn_modifiedby`=CURRENT_TIMESTAMP
WHERE cpn_id=".$this->db->escape($cpn_id)."";
// echo $sql;die;
if ($this->db->simple_query($sql))
{
	$respose = $this->db->affected_rows();
	return $respose;
}
else
{
       return false;
}
}

function loaddata($id){
	$sql = "SELECT * FROM `tbl_coupon` where cpn_id='$id'";

	$query = $this->db->query($sql);
  
	return $query->result();
}
function loaddata1($id){
	$sql = "SELECT * FROM `tbl_coupon` where cpn_id='$id'";
	$query = $this->db->query($sql);
  	return $query->result();
}

function getUserDetails(){
    $response = array();
    $sql = "SELECT `cpn_code`, `cpn_description`, `cpn_type`, `cpn_flat_amount`, `cpn_percentage`, `cpn_startdate`, `cpn_enddate`, `cpn_status`, `cpn_min_bill_amt`, `cpn_max_discount`, `cpn_max_users`, `cpn_maxperusers`, `cpn_createdate`, `cpn_modifieddate`, `coupon_title`, `cpn_id` FROM `tbl_coupon` ";
    $query=$this->db->query($sql);
    $response = $query->result_array();
    return $response;
  }

//duplicate name check functions 
function namecheckexist1($username)
{
    $this->db->select('cpn_id'); 
    $this->db->from('tbl_coupon');
    $this->db->where('cpn_code', rtrim($username));
    $query = $this->db->get();
    $result = $query->result_array();

    return $result;
}
}