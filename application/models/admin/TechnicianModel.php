<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class TechnicianModel extends CI_Model {


function view()
{
	$sql = "SELECT puser_phno,puser_pincode,puser_name,puser_account_status,puser_id FROM `tbl_p_users` where `puser_type`='Technician'";
    // echo $sql;die;
	$query=$this->db->query($sql);
	return $query->result();
}
 
public function getstatusWhereLike($filterstatus) {
        $sql = "SELECT * FROM tbl_p_users WHERE puser_account_status= '$filterstatus' AND `puser_type`='Technician'";
        $query = $this->db->query($sql);
        return $query->result();
    }

function add($technician_name,$technician_email,$technician_aadhar,$technician_pan,$technician_address,$technician_pincode,$technician_phonenumber,$latlong,$technician_bankacc,$ifscnumber,$gstnumber,$technician_type,$vehiclename,$expertise,$status,$imageone,$imagetwo,$technician_alternatephonenumber,$long,$working_type,$technician_order_distance,$data,$weekly_available_hours){
	$u_createdby = $this->session->userdata('usersid');

	$sql = "INSERT INTO `tbl_p_users`(`puser_name`, `puser_address`,`puser_pincode`, `puser_lat_long`, `puser_phno`, `puser_email`, `puser_adhar_number`, `puser_pan_number`, `puser_GST_number`, `puser_bankacc_number`, `puser_bankacc_ifsc`, `puser_profile_image`,`puser_technician_type`,`puser_veh_id`,`expertise`, `puser_account_status`, `puser_document_url`, `puser_created_date`, `puser_created_by`, `puser_type`,`technician_alternatephonenumber`,`puser_long`, `working_type`, `technician_order_distance`, `technician_certificate`, `weekly_available_hours`) VALUES (".$this->db->escape($technician_name).",".$this->db->escape($technician_address).",".$this->db->escape($technician_pincode).",".$this->db->escape($latlong).",".$this->db->escape($technician_phonenumber).",".$this->db->escape($technician_email).",".$this->db->escape($technician_aadhar).",".$this->db->escape($technician_pan).",".$this->db->escape($gstnumber).",".$this->db->escape($technician_bankacc).",".$this->db->escape($ifscnumber).",".$this->db->escape($imageone).",".$this->db->escape($technician_type).",".$this->db->escape($vehiclename).",".$this->db->escape($expertise).",".$this->db->escape($status).",".$this->db->escape($imagetwo).",CURRENT_TIMESTAMP,".$this->db->escape($u_createdby).",".$this->db->escape('Technician').",".$this->db->escape($technician_alternatephonenumber).",".$this->db->escape($long).",".$this->db->escape($working_type).",".$this->db->escape($technician_order_distance).",".$this->db->escape($data).",".$this->db->escape($weekly_available_hours).")";
	// echo $sql;die;
	$query=$this->db->query($sql);
	if($query==1){
		return true;
	}
	else
	{
		return false;
	}
}

function update($technician_name,$technician_email,$technician_aadhar,$technician_pan,$technician_address,$technician_pincode,$technician_phonenumber,$latlong,$technician_bankacc,$ifscnumber,$gstnumber,$technician_type,$expertise,$vehiclename,$status,$imageone,$imagetwo,$puser_id,$technician_alternatephonenumber,$long,$working_type,$technician_order_distance,$weekly_available_hours,$data){
  
    $userid = $this->session->userdata('usersid');
$sql = "UPDATE `tbl_p_users` SET `puser_technician_type`=".$this->db->escape($technician_type).",`puser_veh_id`=".$this->db->escape($vehiclename).",`expertise`=".$this->db->escape($expertise).",`puser_name`=".$this->db->escape($technician_name).",`puser_address`=".$this->db->escape($technician_address).",`puser_pincode`=".$this->db->escape($technician_pincode).",`puser_lat_long`=".$this->db->escape($latlong).",`puser_phno`= ".$this->db->escape($technician_phonenumber).",`puser_email`=".$this->db->escape($technician_email).",`puser_adhar_number`=".$this->db->escape($technician_aadhar).",`puser_pan_number`=".$this->db->escape($technician_pan).",`puser_GST_number`= ".$this->db->escape($gstnumber).",`puser_bankacc_number`= ".$this->db->escape($technician_bankacc).",`puser_bankacc_ifsc`= ".$this->db->escape($ifscnumber).",`puser_profile_image`= ".$this->db->escape($imageone).",`puser_account_status`= ".$this->db->escape($status).",`puser_document_url`= ".$this->db->escape($imagetwo).",`technician_alternatephonenumber`= ".$this->db->escape($technician_alternatephonenumber).",`puser_modify_date`=CURRENT_TIMESTAMP,`puser_modify_by`='$userid' ,`puser_long`='$long',`working_type`='$working_type',`technician_order_distance`='$technician_order_distance',`weekly_available_hours`='$weekly_available_hours',`technician_certificate`='$data' WHERE puser_id=".$this->db->escape($puser_id)."";
//  echo $sql;die;
if ($this->db->simple_query($sql))
{
	$respose = $this->db->affected_rows();
	return $respose;
}
else
{
       return false;
}
}

function loaddata($id){
	$sql = "SELECT * FROM `tbl_p_users` left JOIN tbl_vehicle ON FIND_IN_SET(tbl_vehicle.vehb_id, puser_veh_id) > 0 where puser_id='$id'";

	$query = $this->db->query($sql);
  
	return $query->result();
}
function loaddata1($id){
	$sql = "SELECT * FROM `tbl_p_users` left JOIN tbl_vehicle ON FIND_IN_SET(tbl_vehicle.vehb_id, puser_veh_id) > 0 where puser_id='$id'";

	$query = $this->db->query($sql);
  
	return $query->result();
}


function phonenocheckexist($username)
{
    $this->db->select('puser_id'); 
    $this->db->from('tbl_p_users');
    $this->db->where('puser_phno', $username);
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}

function emailcheckexist($username)
{
    $this->db->select('puser_id'); 
    $this->db->from('tbl_p_users');
    $this->db->where('puser_email', $username);
    $query = $this->db->get();
    $result = $query->result_array();

    return $result; 
}
//..
function tool($id){
	$sql ="SELECT tbl_tools.tool_id,tbl_tools.tool_title,tbl_tools.tool_thumbnail,tbl_puser_tools.p_usertool_status ,tbl_puser_tools.p_usertool_create_date FROM `tbl_tools` RIGHT join tbl_puser_tools on tbl_tools.tool_id=tbl_puser_tools.tools_id where tbl_puser_tools.technician_id='$id'";
	$query = $this->db->query($sql);
  
	return $query->result();

}

function loadtechnician($id){
	$sql = "SELECT puser_phno,puser_pincode,puser_name,puser_account_status,puser_id,puser_phno FROM `tbl_p_users` where `puser_id`='$id'";
     //echo $sql;die;
	$query=$this->db->query($sql);
	return $query->result();

}

function loaddata2($id){
	$sql = "SELECT puser_id FROM `tbl_p_users` where `puser_id`='$id'";
	$query=$this->db->query($sql);
	return $query->result();
}

function loaddata3(){
	$sql = "SELECT tool_id,tool_title FROM `tbl_tools`";
	$query=$this->db->query($sql);
	return $query->result();
}
function updatemodeldata($tooloption,$technician_id){
	$status = "Active";
	$sql = "INSERT INTO `tbl_puser_tools`(`tools_id`,`p_usertool_status`, `p_usertool_create_date`, `technician_id`) VALUES (".$this->db->escape($tooloption).",".$this->db->escape($status).",CURRENT_TIMESTAMP,".$this->db->escape($technician_id).")";
	//echo $sql;die;
	$query=$this->db->query($sql);

	if($query==1){
		return true;
	}
	else
	{
		return false;
	}

}

function deletetechnicaltool($id){
 	$sql = "DELETE FROM tbl_puser_tools WHERE tools_id = '$id'";
 	$query=$this->db->query($sql);
 	if($query==1){
		return true;
	}
	else
	{
		return false;
	}

}

// service data..
function service($id){
	//SELECT ser_id,tbl_service.ser__title,ser_thumbnail,tbl_pservice.createdat,tbl_pservice.tool_status FROM `tbl_service` RIGHT join tbl_pservice on tbl_service.ser_id=tbl_pservice.pservice_id where tbl_pservice.ptechnician_id='25'
	$sql ="SELECT ser_id,tbl_service.ser__title,ser_thumbnail,tbl_pservice.createdat,tbl_pservice.tool_status FROM `tbl_service` RIGHT join tbl_pservice on tbl_service.ser_id=tbl_pservice.pservice_id where tbl_pservice.ptechnician_id='$id'";
	$query = $this->db->query($sql);
  
	return $query->result();

}
function loaddata4(){
	$sql = "SELECT ser_id,ser__title FROM `tbl_service`";
	$query=$this->db->query($sql);
	return $query->result();
}

function updateservicemodeldata($serviceoption,$technician_id){
	$status = "Active";
	$sql = "INSERT INTO `tbl_pservice`(`ptechnician_id`,`pservice_id`, `createdat`, `tool_status`) VALUES (".$this->db->escape($technician_id).",".$this->db->escape($serviceoption).",CURRENT_TIMESTAMP,".$this->db->escape($status).")";
	
	$query=$this->db->query($sql);

	if($query==1){
		return true;
	}
	else
	{
		return false;
	}

}

function deletetechnicalservice($id){
 	$sql = "DELETE FROM tbl_pservice WHERE pservice_id = '$id'";
 	$query=$this->db->query($sql);
 	if($query==1){
		return true;
	}
	else
	{
		return false;
	}

}

function getUserDetails(){
    $response = array();
    $sql = "SELECT puser_id,puser_name,puser_address,puser_pincode,puser_phno,puser_email,puser_adhar_number,puser_pan_number,puser_GST_number,puser_bankacc_number,puser_bankacc_ifsc,puser_availability_status,puser_account_status,puser_type FROM `tbl_p_users` 
	where `puser_type`='Technician'";
    // echo $sql;die;
  $query=$this->db->query($sql);
    $response = $query->result_array();
    return $response;
  }

function getUserDetails1($id){
    $response = array();
    $sql = "SELECT tbl_tools.tool_id,tbl_tools.tool_title,tbl_tools.tool_thumbnail,tbl_puser_tools.p_usertool_status ,tbl_puser_tools.p_usertool_create_date FROM `tbl_tools` RIGHT join tbl_puser_tools on tbl_tools.tool_id=tbl_puser_tools.tools_id where tbl_puser_tools.technician_id='$id'";
    // echo $sql;die;
  $query=$this->db->query($sql);
    $response = $query->result_array();
    return $response;
  }

function getUserDetails2($id){
    $response = array();
    $sql = "SELECT ser_id,tbl_service.ser__title,ser_thumbnail,tbl_pservice.createdat,tbl_pservice.tool_status FROM `tbl_service` RIGHT join tbl_pservice on tbl_service.ser_id=tbl_pservice.pservice_id where tbl_pservice.ptechnician_id='$id'";
    // echo $sql;die;
  $query=$this->db->query($sql);
    $response = $query->result_array();
    return $response;
  }



}