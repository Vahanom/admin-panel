<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class SupplierModel extends CI_Model {


function view()
{
	$sql = "SELECT puser_phno,puser_pincode,puser_name,puser_account_status,puser_id FROM `tbl_p_users` 
	where `puser_type`='Supplier' order by puser_id desc";
	$query=$this->db->query($sql);
	return $query->result();
}

function add($supplier_name,$supplier_email,$supplier_aadhar,$supplier_pan,$supplier_address,$supplier_pincode,$supllier_phonenumber,$status,$latlong,$supllier_bankacc,$ifscnumber,$companyregnumber,$gstnumber,$imageone,$mondaystarttime,$mondayendtime,$Tuesdaystarttime,$Tuesdayendtime,$Wednesdaystarttime,$Wednesdayendtime,$Thursdaystarttime,$Thursdayendtime,$Fridaystarttime,$Fridayendtime,$Saturdaystarttime,$Saturdayendtime,$Sundaystarttime,$Sundayendtime,$long,$imagetwo){
	$u_createdby = $this->session->userdata('usersid');

	$sql = "INSERT INTO `tbl_p_users`(`puser_name`, `puser_address`,`puser_pincode`, `puser_lat_long`, `puser_phno`, `puser_email`, `puser_adhar_number`, `puser_pan_number`, `puser_GST_number`, `puser_bankacc_number`, `puser_bankacc_ifsc`, `puser_profile_image`, `puser_account_status`, `puser_company_reg_number`, `puser_created_date`, `puser_created_by`, `puser_type`,`puser_long`,`idproof`) VALUES (".$this->db->escape($supplier_name).",".$this->db->escape($supplier_address).",".$this->db->escape($supplier_pincode).",".$this->db->escape($latlong).",".$this->db->escape($supllier_phonenumber).",".$this->db->escape($supplier_email).",".$this->db->escape($supplier_aadhar).",".$this->db->escape($supplier_pan).",".$this->db->escape($gstnumber).",".$this->db->escape($supllier_bankacc).",".$this->db->escape($ifscnumber).",".$this->db->escape($imageone).",".$this->db->escape($status).",".$this->db->escape($companyregnumber).",CURRENT_TIMESTAMP,".$this->db->escape($u_createdby).",".$this->db->escape('Supplier').",".$this->db->escape($long).",".$this->db->escape($imagetwo).")";
	// echo $sql;die;
	$query=$this->db->query($sql);
	$insert_id = $this->db->insert_id();
	// var_dump($insert_id);die;
	$sql1 = "INSERT INTO `tbl_puser_working_hours`(`puser_id`, `puserw_mo_open`,`puserw_mo_close`, `puserw_tu_open`, `puserw_tu_close`, `puserw_we_open`, `puserw_we_close`, `puserw_th_open`, `puserw_th_close`, `puserw_fr_open`, `puserw_fr_close`, `puserw_sa_open`, `puserw_sa_close`, `puserw_su_open`, `puserw_su_close`, `puserw_create`, `puserw_createby`) VALUES (".$this->db->escape($insert_id).",".$this->db->escape($mondaystarttime).",".$this->db->escape($mondayendtime).",".$this->db->escape($Tuesdaystarttime).",".$this->db->escape($Tuesdayendtime).",".$this->db->escape($Wednesdaystarttime).",".$this->db->escape($Wednesdayendtime).",".$this->db->escape($Thursdaystarttime).",".$this->db->escape($Thursdayendtime).",".$this->db->escape($Fridaystarttime).",".$this->db->escape($Fridayendtime).",".$this->db->escape($Saturdaystarttime).",".$this->db->escape($Saturdayendtime).",".$this->db->escape($Sundaystarttime).",".$this->db->escape($Sundayendtime).",CURRENT_TIMESTAMP,".$this->db->escape($u_createdby).")";
	//echo $sql;die;
	$query1=$this->db->query($sql1);




	if($query==1){
		return true;
	}
	else
	{
		return false;
	}
}



function update($supplier_name,$supplier_email,$supplier_aadhar,$supplier_pan,$supplier_address,$supplier_pincode,$supllier_phonenumber,$status,$latlong,$supllier_bankacc,$ifscnumber,$companyregnumber,$gstnumber,$imageone,$puser_id,$mondaystarttime,$mondayendtime,$Tuesdaystarttime,$Tuesdayendtime,$Wednesdaystarttime,$Wednesdayendtime,$Thursdaystarttime,$Thursdayendtime,$Fridaystarttime,$Fridayendtime,$Saturdaystarttime,$Saturdayendtime,$Sundaystarttime,$Sundayendtime,$long,$imagetwo){
  
    $userid = $this->session->userdata('usersid');
$sql = "UPDATE `tbl_p_users` SET `puser_name`=".$this->db->escape($supplier_name).",`puser_address`=".$this->db->escape($supplier_address).",`puser_pincode`=".$this->db->escape($supplier_pincode).",`puser_lat_long`=".$this->db->escape($latlong).",`puser_phno`= ".$this->db->escape($supllier_phonenumber).",`puser_email`=".$this->db->escape($supplier_email).",`puser_adhar_number`=".$this->db->escape($supplier_aadhar).",`puser_pan_number`=".$this->db->escape($supplier_pan).",`puser_GST_number`= ".$this->db->escape($gstnumber).",`puser_bankacc_number`= ".$this->db->escape($supllier_bankacc).",`puser_bankacc_ifsc`= ".$this->db->escape($ifscnumber).",`puser_profile_image`= ".$this->db->escape($imageone).",`puser_account_status`= ".$this->db->escape($status).",`puser_company_reg_number`= ".$this->db->escape($companyregnumber).",`puser_modify_date`=CURRENT_TIMESTAMP,`puser_modify_by`='$userid',`puser_long`='$long' ,`idproof`='$imagetwo' WHERE puser_id=".$this->db->escape($puser_id)."";


$this->db->select('puser_id'); 
$this->db->from('tbl_puser_working_hours');
$this->db->where('puser_id', $puser_id);
$query2 = $this->db->get();
$result2 = $query2->result_array();
if(count($result2)>0)
{
	$sql3 = "UPDATE `tbl_puser_working_hours` SET `puserw_mo_open`=".$this->db->escape($mondaystarttime).",`puserw_mo_close`=".$this->db->escape($mondayendtime).",`puserw_tu_open`=".$this->db->escape($Tuesdaystarttime).",`puserw_tu_close`=".$this->db->escape($Tuesdayendtime).",`puserw_we_open`= ".$this->db->escape($Wednesdaystarttime).",`puserw_we_close`=".$this->db->escape($Wednesdayendtime).",`puserw_th_open`=".$this->db->escape($Thursdaystarttime).",`puserw_th_close`=".$this->db->escape($Thursdayendtime).",`puserw_fr_open`= ".$this->db->escape($Fridaystarttime).",`puserw_fr_close`= ".$this->db->escape($Fridayendtime).",`puserw_sa_open`= ".$this->db->escape($Saturdaystarttime).",`puserw_sa_close`= ".$this->db->escape($Saturdayendtime).",`puserw_su_open`= ".$this->db->escape($Sundaystarttime).",`puserw_su_close`= ".$this->db->escape($Sundayendtime).",`puserw_modify`=CURRENT_TIMESTAMP,`puserw_modifyby`='$userid' WHERE puser_id=".$this->db->escape($puser_id)."";
	
	$query3=$this->db->query($sql3);

}else{
	$sql1 = "INSERT INTO `tbl_puser_working_hours`(`puser_id`, `puserw_mo_open`,`puserw_mo_close`, `puserw_tu_open`, `puserw_tu_close`, `puserw_we_open`, `puserw_we_close`, `puserw_th_open`, `puserw_th_close`, `puserw_fr_open`, `puserw_fr_close`, `puserw_sa_open`, `puserw_sa_close`, `puserw_su_open`, `puserw_su_close`, `puserw_create`, `puserw_createby`) VALUES (".$this->db->escape($puser_id).",".$this->db->escape($mondaystarttime).",".$this->db->escape($mondayendtime).",".$this->db->escape($Tuesdaystarttime).",".$this->db->escape($Tuesdayendtime).",".$this->db->escape($Wednesdaystarttime).",".$this->db->escape($Wednesdayendtime).",".$this->db->escape($Thursdaystarttime).",".$this->db->escape($Thursdayendtime).",".$this->db->escape($Fridaystarttime).",".$this->db->escape($Fridayendtime).",".$this->db->escape($Saturdaystarttime).",".$this->db->escape($Saturdayendtime).",".$this->db->escape($Sundaystarttime).",".$this->db->escape($Sundayendtime).",CURRENT_TIMESTAMP,".$this->db->escape($userid).")";
	//echo $sql;die;
	$query1=$this->db->query($sql1);

}

if ($this->db->simple_query($sql))
{
	$respose = $this->db->affected_rows();
	return $respose;
}
else
{
       return false;
}
}

function loaddata($id){
	$sql = "SELECT *,tbl_p_users.puser_id as puser_id FROM `tbl_p_users` 
	left join tbl_puser_working_hours on tbl_p_users.puser_id=tbl_puser_working_hours.puser_id
	where tbl_p_users.puser_id='$id'";
// echo $sql;die;
	$query = $this->db->query($sql);
  
	return $query->result();
}
function loaddata1($id){
	$sql = "SELECT * FROM `tbl_p_users` 
	left join tbl_puser_working_hours on tbl_p_users.puser_id=tbl_puser_working_hours.puser_id
	where tbl_p_users.puser_id='$id'";

	$query = $this->db->query($sql);
  
	return $query->result();
}


function phonenocheckexist($username)
{
    $this->db->select('puser_id'); 
    $this->db->from('tbl_p_users');
    $this->db->where('puser_phno', $username);
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}

function emailcheckexist($username)
{
    $this->db->select('puser_id'); 
    $this->db->from('tbl_p_users');
    $this->db->where('puser_email', $username);
    $query = $this->db->get();
    $result = $query->result_array();

    return $result;
}
// ..
function spare($id){
	$sql ="SELECT p_spare_id,tbl_spare.sp_id,spare_id,tbl_spare.sp_title,tbl_p_spares.p_spare_mrp,tbl_p_spares.p_spare_selling_price, tbl_p_spares.p_spare_gst_percentage FROM `tbl_spare` RIGHT join tbl_p_spares on tbl_spare.sp_id=tbl_p_spares.spare_id where tbl_p_spares.p_spare_create_by='$id'";$query = $this->db->query($sql);
  
	return $query->result();

}

function loadsupplier($id){
	$sql = "SELECT puser_phno,puser_pincode,puser_name,puser_address,puser_id,puser_phno FROM `tbl_p_users` where `puser_id`='$id'";
     //echo $sql;die;
	$query=$this->db->query($sql);
	return $query->result();

}

function loaddata2($id){
	$sql = "SELECT puser_id FROM `tbl_p_users` where `puser_id`='$id'";
	$query=$this->db->query($sql);
	return $query->result();
}
function loaddata3(){
	$sql = "SELECT spcat_id,spcat_title FROM `tbl_spare_catg`";
	$query=$this->db->query($sql);
	return $query->result();
}
function loaddata4(){
	$sql = "SELECT spcat_id,spcat_title FROM `tbl_spare_catg`";
	$query=$this->db->query($sql);
	return $query->result();
}
function loadSpareCategory($spare_cat){
     $this->db->where('sp_category_id', $spare_cat);
     $query = $this->db->get('tbl_spare');
     $output = '<option value="">Select spare</option>';
        foreach($query->result() as $row)
        {
          $output .= '<option value="'.$row->sp_id.'">'.$row->sp_title.'</option>';
        }
      return $output;
}

function updatemodeldata($spare_cat,$spare,$p_spare_mrp,$p_spare_selling_price,$p_spare_gst_percentage,$gst_amount,$gstspprice,$supplier_id){
	$status = "Active";
	$sql = "INSERT INTO `tbl_p_spares`(`spare_cat_id`,`p_spare_create_by`,`p_spare_mrp`,`p_spare_selling_price`,`p_spare_gst_percentage`,`gst_amount`,`gstspprice`,`spare_id`,`p_spare_status`,`p_spare_create_date`) VALUES (".$this->db->escape($spare_cat).",".$this->db->escape($supplier_id).",".$this->db->escape($p_spare_mrp).",".$this->db->escape($p_spare_selling_price).",".$this->db->escape($p_spare_gst_percentage).",".$this->db->escape($gst_amount).",".$this->db->escape($gstspprice).",".$this->db->escape($spare).",".$this->db->escape($status).",CURRENT_TIMESTAMP)";
	//echo $sql;die;
	$query=$this->db->query($sql);

	if($query==1){
		return true;
	}
	else
	{
		return false;
	}

}

function deleteSupplierspare($p_spare_id){
 	$sql = "DELETE FROM tbl_p_spares WHERE p_spare_id = '$p_spare_id'";
 	$query=$this->db->query($sql);
 	if($query==1){
		return true;
	}
	else
	{
		return false;
	}

}
public function getstatusWhereLike($filterstatus) {
        $sql = "SELECT * FROM tbl_p_users WHERE puser_account_status= '$filterstatus' AND `puser_type`='Supplier' order by puser_id desc";
        $query = $this->db->query($sql);
        return $query->result();
    }

public function getUserDetails(){
    $response = array();
    $sql = "SELECT puser_id,puser_name,puser_address,puser_pincode,puser_phno,puser_email,puser_adhar_number,puser_pan_number,puser_GST_number,puser_bankacc_number,puser_bankacc_ifsc,puser_availability_status,puser_account_status,puser_type FROM `tbl_p_users` 
	where `puser_type`='Supplier' order by puser_id desc";
    $query=$this->db->query($sql);
    $response = $query->result_array();
    return $response;
  }
public function getUserDetails1($id){
    $response = array();
    $sql = "SELECT p_spare_id,tbl_spare.sp_title,tbl_p_spares.p_spare_mrp,tbl_p_spares.p_spare_selling_price, tbl_p_spares.p_spare_gst_percentage FROM `tbl_spare` RIGHT join tbl_p_spares on tbl_spare.sp_id=tbl_p_spares.p_spare_create_by where tbl_p_spares.spare_id='$id'";
    $query=$this->db->query($sql);
    $response = $query->result_array();
    return $response;
  }

}