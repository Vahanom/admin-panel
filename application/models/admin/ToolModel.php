<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class ToolModel extends CI_Model {


function view()
{
	$sql = "SELECT tool_title,tool_thumbnail,tool_status,tool_id FROM `tbl_tools`";
    // echo $sql;die;
	$query=$this->db->query($sql);
	return $query->result();
}
public function getstatusWhereLike($filterstatus) {
        $sql = "SELECT * FROM tbl_tools WHERE tool_status= '$filterstatus'";
        $query = $this->db->query($sql);
        return $query->result();
    }

function add($service_title,$service_desc,$status,$imageone,$imagetwo){
	$u_createdby = $this->session->userdata('usersid');

	$sql = "INSERT INTO `tbl_tools`(`tool_title`, `tool_description`,`tool_thumbnail`, `tool_image`, `tool_status`,`tool_createdby`, `tool_createddate`) VALUES (".$this->db->escape($service_title).",".$this->db->escape($service_desc).",".$this->db->escape($imageone).",".$this->db->escape($imagetwo).",".$this->db->escape($status).",CURRENT_TIMESTAMP,".$this->db->escape($u_createdby)."
	)";
	$query=$this->db->query($sql);
	if($query==1){
		return true;
	}
	else
	{
		return false;
	}
}

function update($service_title,$service_desc,$status,$imageone,$imagetwo,$tool_id){
  
    $userid = $this->session->userdata('usersid');
$sql = "UPDATE `tbl_tools` SET `tool_title`=".$this->db->escape($service_title).",`tool_description`=".$this->db->escape($service_desc).",`tool_status`=".$this->db->escape($status)." 
,`tool_thumbnail`=".$this->db->escape($imageone).",`tool_image`=".$this->db->escape($imagetwo).",`tool_modifiedby`=".$this->db->escape($userid).",`tool_modifieddate`=CURRENT_TIMESTAMP
WHERE tool_id=".$this->db->escape($tool_id)."";
// echo $sql;die;
if ($this->db->simple_query($sql))
{
	$respose = $this->db->affected_rows();
	return $respose;
}
else
{
       return false;
}
}

function loaddata($id){
	$sql = "SELECT * FROM `tbl_tools` where tool_id='$id'";

	$query = $this->db->query($sql);
  
	return $query->result();
}
function loaddata1($id){
	$sql = "SELECT * FROM `tbl_tools` where tool_id='$id'";
	$query = $this->db->query($sql);
  	return $query->result();
}

function getUserDetails(){
    $response = array();
    $sql = "SELECT tool_id,tool_title,tool_description,tool_status,tool_createddate,tool_modifieddate FROM `tbl_tools`";
    // echo $sql;die;
  $query=$this->db->query($sql);
    $response = $query->result_array();
    return $response;
  }


}