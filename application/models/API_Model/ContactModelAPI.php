<?php
	
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class ContactModelAPI extends CI_Model
	{
		public function __construct(){
			$this->load->database();
		}

		 public function get_contact(){
        	$sql = "SELECT id,name ,email,contact_number,message FROM tbl_contact_details";
            // echo $sql;die;
			$query = $this->db->query($sql);
			if($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else
			{
				return 0;
			}
        }


         public function add_contact($name,$email,$contact_number,$message){
        	$sql = "INSERT INTO `tbl_contact_details`(`name`,`email`,`contact_number`,`message`) VALUES (".$this->db->escape($name).",".$this->db->escape($email).",".$this->db->escape($contact_number).",".$this->db->escape($message).")";
            // echo $sql;die;
			$query = $this->db->query($sql);
			if($query==1)
			{
				return true;
			}
			else
			{
				return false;
			}
        }

}