<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Insurance extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/InsuranceModel');
            $this->load->helper('form'); 
	  }

     public function index()
     {
   
      if($this->session->userdata('usersid') != null){
        foreach($this->session->userdata('permission') as $columnvalue){
        }
        if(!empty($columnvalue)){ $insurance =  json_decode($columnvalue->permission_insurance); }

        
if((!empty($insurance) && $insurance->view == "true") || $this->session->userdata('user_role') == "admin"){
        $title['title']="All Insurance";
        $data['insurance'] = $this->InsuranceModel->view();
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        $this->load->view('admin/insurance/allinsurance',$data);
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }
        }else{
             redirect('Logout');
        }
    }

    function searchvalue(){

     $filterstatus  = $this->input->post('filterstatus'); 
     if($filterstatus === 'all'){
       return redirect('admin/Insurance');
     }else{
     $data['insurance']= $this->InsuranceModel->getstatusWhereLike($filterstatus); 
            if($data){ 
              $title['title']="All Insurance";
              $this->load->view('admin/layouts/header',$title);
              $this->load->view('admin/layouts/navbar');
              $this->load->view('admin/insurance/allinsurance',$data);
              $this->load->view('admin/layouts/footer');
            }else{
               echo "filter data cant be show on view";
            }
     }
    }

    function add(){
      foreach($this->session->userdata('permission') as $columnvalue){
      }
      if(!empty($columnvalue)){ $insurance =  json_decode($columnvalue->permission_insurance); }

      
if((!empty($insurance) && $insurance->add == "true") || $this->session->userdata('user_role') == "admin"){
        $service_title = $this->input->post('service_title');
        $service_desc = $this->input->post('service_desc');
        $instype = $this->input->post('instype');
        $status = $this->input->post('status');
        $claim_per = $this->input->post('claim_per');
         $price = $this->input->post('price');
          $selling_price = $this->input->post('selling_price');
        
        if(!empty($_FILES['imageone']['name'])){
         
          $config['upload_path'] = './assets/images/insurance';
          $config['allowed_types'] = 'jpg|jpeg|png|gif';
          $config['file_name'] = $_FILES['imageone']['name'];
          
          //Load upload library and initialize configuration
          $this->load->library('upload',$config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('imageone')){
              $uploadData = $this->upload->data();
              $imageone = $uploadData['file_name'];
          }else{
           
              $imageone = '';
            
          }
      }else{
          $imageone = '';
      }

      if(!empty($_FILES['imagetwo']['name'])){
        $config['upload_path'] = './assets/images/insurance';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['file_name'] = $_FILES['imagetwo']['name'];
        
        //Load upload library and initialize configuration
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('imagetwo')){
            $uploadData = $this->upload->data();
            $imagetwo = $uploadData['file_name'];
        }else{
            $imagetwo = '';
        }
      }else{
        $imagetwo = '';
      }

        
     
       
 $result = $this->InsuranceModel->add($service_title,$service_desc,$status,$imageone,$imagetwo,$instype,$price,$claim_per,$selling_price);
//  die;
        if ($result == 'true') {
          echo "<script>alert('Insurance Added Successfully');
            window.location.href='../Insurance';
            </script>";
          }else{
            redirect("admin/Insurance");
        }
      }else{
        redirect("admin/Access");
      }
    }

 

    public function create()
    {
      if($this->session->userdata('usersid') != null){
        foreach($this->session->userdata('permission') as $columnvalue){
        }
        if(!empty($columnvalue)){ $insurance =  json_decode($columnvalue->permission_insurance); }
  
        
  if((!empty($insurance) && $insurance->add == "true") || $this->session->userdata('user_role') == "admin"){
        $title['title']="Add Insurance";
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        $this->load->view('admin/insurance/create');
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }  }else{
             redirect('Logout');
        }
   }

   function update(){
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $insurance =  json_decode($columnvalue->permission_insurance); }

  if((!empty($insurance) && $insurance->update == "true") || $this->session->userdata('user_role') == "admin"){
    $instype = $this->input->post('instype');
    $inusr_id = $this->input->post('inusr_id');
    $service_title = $this->input->post('service_title');
    $service_desc = $this->input->post('service_desc');
        $claim_per = $this->input->post('claim_per');
   
    $status = $this->input->post('status');
    $price = $this->input->post('price');
          $selling_price = $this->input->post('selling_price');

    if(!empty($_FILES['imageone']['name'])){
      $config['upload_path'] = './assets/images/insurance';
      $config['allowed_types'] = 'jpg|jpeg|png|gif';
      $config['file_name'] = $_FILES['imageone']['name'];
      
      //Load upload library and initialize configuration
      $this->load->library('upload',$config);
      $this->upload->initialize($config);
      if($this->upload->do_upload('imageone')){
          $uploadData = $this->upload->data();
          $imageone = $uploadData['file_name'];
      }else{
          $imageone = '';
      }
  }else{
    $imageone = $this->input->post('hiddenimageone');
  }

  if(!empty($_FILES['imagetwo']['name'])){
    $config['upload_path'] = './assets/images/insurance';
    $config['allowed_types'] = 'jpg|jpeg|png|gif';
    $config['file_name'] = $_FILES['imagetwo']['name'];
    
    //Load upload library and initialize configuration
    $this->load->library('upload',$config);
    $this->upload->initialize($config);
    if($this->upload->do_upload('imagetwo')){
        $uploadData = $this->upload->data();
        $imagetwo = $uploadData['file_name'];
    }else{
        $imagetwo = '';
    }
  }else{
    $imagetwo = $this->input->post('hiddenimagetwo');
  }
$result = $this->InsuranceModel->update($service_title,$service_desc,$status,$imageone,$imagetwo,$inusr_id,$instype,$price,$claim_per,$selling_price);

        if ($result == 1) {
          echo "<script>alert('Insurance Updated Successfully');
             window.location.href='../Insurance';
            </script>";
          }else{
        redirect("admin/Insurance");
        }
      }else{
        redirect("admin/Access");
      }
    }



    function loaddata(){
   
      $id = $this->input->post("id");
      $data['insurance'] = $this->InsuranceModel->loaddata($id);
      $this->load->view('admin/insurance/update',$data);
      
    }

    function loaddataview(){
   
      $id = $this->input->post("id");
      $data['insurance'] = $this->InsuranceModel->loaddata1($id);
     
      $this->load->view('admin/insurance/view',$data);
      
    }
    
     function exportcsv(){
     // file name 
    $filename = 'insurance_'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
     // get data 
    $usersData = $this->InsuranceModel->getUserDetails();
    // file creation 
    $file = fopen('php://output','w');
    $header = array("Id","inusr_title","inusr_description","inusr_status","inusr_type","price","selling_price"); 
    fputcsv($file, $header);
    foreach ($usersData as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
    }
   
    
    }

    