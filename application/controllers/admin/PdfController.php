<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PdfController extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/PdfModel');
		$this->load->library('pdf');
	}

	public function pdfdetails()
	{
		if($this->uri->segment(3))
		{
			$order_id = $this->uri->segment(4);
			$html_content = '<h3 align="center">Vahanom Completed Order Detail</h3>';
			$html_content .= $this->PdfModel->show_single_details($order_id);
			$this->pdf->loadHtml($html_content);
			$this->pdf->render();
			$this->pdf->stream("".$order_id.".pdf", array("Attachment"=>1));
		}
	}
}
?>