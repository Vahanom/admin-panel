<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class Dashboard extends CI_Controller {

    function __construct() { 
        parent::__construct(); 
        $this->load->library('session'); 
        $this->load->helper('form'); 
        $this->load->model('admin/DashboardModel');
     }

     public function index()
     {
        if($this->session->userdata('usersid') != null){
            $title['title']="Dashboard";
           
            $this->load->view('admin/layouts/header',$title);
            $this->load->view('admin/layouts/navbar');
            $data=$this->DashboardModel->musers();
            $supplier=$this->DashboardModel->supplier();
            $orders=$this->DashboardModel->orders();
            $completed_orders=$this->DashboardModel->completed_orders();
            $pending_orders=$this->DashboardModel->pending_orders();
            $technician=$this->DashboardModel->technician();
            $completed_ongoing=$this->DashboardModel->completed_ongoing();
             $total_revenue=$this->DashboardModel->total_revenue();
              $service=$this->DashboardModel->service();
               $spare=$this->DashboardModel->spare();

            $this->load->view('admin/dashboard',['data'=>$data,'supplier' => $supplier,'orders'=>$orders,'completed_orders'=>$completed_orders,'pending_orders'=>$pending_orders,'technician'=>$technician,'completed_ongoing'=>$completed_ongoing,'total_revenue'=>$total_revenue,'service'=>$service,'spare'=>$spare]);
            $this->load->view('admin/layouts/footer');
            }else{
                 return redirect('Logout');
            }
     }

}