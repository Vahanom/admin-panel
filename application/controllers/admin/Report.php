<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Report extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/AboutusModel');
             $this->load->model('admin/ReportModel');
            $this->load->helper('form'); 
	  }

     public function index()
     {
      if($this->session->userdata('usersid') != null){
      foreach($this->session->userdata('permission') as $columnvalue){}
if(!empty($columnvalue)){ $aboutus =  json_decode($columnvalue->permission_employee); }

if((!empty($aboutus) && $aboutus->view == "true") || $this->session->userdata('user_role') == "admin"){
          $title['title']="All Reports";
          $this->load->view('admin/layouts/header',$title);
          $this->load->view('admin/layouts/navbar');
          $data['spare'] = $this->ReportModel->select_spare();
          $this->load->view('admin/report/allreport',$data);
          $this->load->view('admin/layouts/footer');
        }else{
          redirect("admin/Access");
        }
    
        }else{
             redirect('Logout');
        }
    }

    function exportcsv(){
      $order_type = $this->input->post('order_type');
      $order_status = $this->input->post('order_status'); 
      $report_type = $this->input->post('report_type');
      $special_type = $this->input->post('special_type');
      $spare = $this->input->post('spare'); 
      $startdate = $this->input->post('startdate'); 
      $enddate = $this->input->post('enddate');
     // file name 
      if($report_type=='order_types'){
              if($order_type=='Special'){ 
                  $filename = 'special_report_'.date('Ymd').'.csv'; 
                   header("Content-Description: File Transfer"); 
                   header("Content-Disposition: attachment; filename=$filename"); 
                   header("Content-Type: application/csv; ");
                    // get data 
                   $usersData = $this->ReportModel->get_special_type($startdate,$enddate,$special_type); //print_r($usersData); die;
                   // file creation 
                   $file = fopen('php://output','w');
                   $header = array("Id","Insurance Id/Vehicle Id/Pollution Id","User Id","User Name","Insurance Title/Inspection Type/Pollution Title","Remark","status","Created Date"); 
                   fputcsv($file, $header);
                   foreach ($usersData as $key=>$line){ 
                     //fputcsv($file,$line); 
                     fputcsv($file, (array) $line);
                   }
                   fclose($file); 
                   exit; 

              }else{
                   $filename = 'order_report_'.date('Ymd').'.csv'; 
                   header("Content-Description: File Transfer"); 
                   header("Content-Disposition: attachment; filename=$filename"); 
                   header("Content-Type: application/csv; ");
                    // get data 
                   $usersData = $this->ReportModel->get_order_type($order_type,$order_status,$startdate,$enddate); //print_r($usersData); die;
                   // file creation 
                   $file = fopen('php://output','w');
                   $header = array("Order Id","Order Type","Order Date","order_schedule  Date","Order Address","Pincode","Order Number","Order Status","type_servicespare","Service Id","Service Title"); 
                   fputcsv($file, $header);
                   foreach ($usersData as $key=>$line){ 
                     //fputcsv($file,$line); 
                     fputcsv($file, (array) $line);
                   }
                   fclose($file); 
                   exit; 
              }
       } 
       elseif($report_type=='technician_order_report'){
                   $filename = 'Technician_order_report_'.date('Ymd').'.csv'; 
                   header("Content-Description: File Transfer"); 
                   header("Content-Disposition: attachment; filename=$filename"); 
                   header("Content-Type: application/csv; ");
                    // get data 
                   $usersData = $this->ReportModel->get_technician_order_report($startdate,$enddate); //print_r($usersData); die;
                   // file creation 
                   $file = fopen('php://output','w');
                   $header = array("Order Id","Order Status","Order Type","Order Date","order_schedule  Date","Order Address","Pincode","Order Number","User Name","User Type"); 
                   fputcsv($file, $header);
                   foreach ($usersData as $key=>$line){ 
                     //fputcsv($file,$line); 
                     fputcsv($file, (array) $line);
                   }
                   fclose($file); 
                   exit; 
       }
       elseif($report_type=='spare_order'){
                   $filename = 'spare_order_report_'.date('Ymd').'.csv'; 
                   header("Content-Description: File Transfer"); 
                   header("Content-Disposition: attachment; filename=$filename"); 
                   header("Content-Type: application/csv; ");
                    // get data 
                   $usersData = $this->ReportModel->get_spare_order_report($spare,$startdate,$enddate); //print_r($usersData); die;
                   // file creation 
                   $file = fopen('php://output','w');
                   $header = array("Spare id","Order Id","Order Type","Order Date","order_schedule  Date","Order Address","Pincode","Order Detail id","Order Number","Order Status","Spare"); 
                   fputcsv($file, $header);
                   foreach ($usersData as $key=>$line){ 
                     //fputcsv($file,$line); 
                     fputcsv($file, (array) $line);
                   }
                   fclose($file); 
                   exit; 
       }
       elseif($report_type=='supplier_order_report'){
                   $filename = 'Supplier_order_report_'.date('Ymd').'.csv'; 
                   header("Content-Description: File Transfer"); 
                   header("Content-Disposition: attachment; filename=$filename"); 
                   header("Content-Type: application/csv; ");
                    // get data 
                   $usersData = $this->ReportModel->get_supplier_order_report($startdate,$enddate); //print_r($usersData); die;
                   // file creation 
                   $file = fopen('php://output','w');
                   $header = array("Order Id","Order Status","Order Type","Order Date","order_schedule  Date","Order Address","Pincode","Order Number","User Name","User Type"); 
                   fputcsv($file, $header);
                   foreach ($usersData as $key=>$line){ 
                     //fputcsv($file,$line); 
                     fputcsv($file, (array) $line);
                   }
                   fclose($file); 
                   exit; 
       }
       else
        { 
          echo "noy";                

        }
}

}
    