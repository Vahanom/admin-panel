<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Permission extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/EmployeesModel');
            $this->load->model('admin/PermissionModel');
            $this->load->helper('form'); 
	  }

     public function index()
     {
      if($this->session->userdata('usersid') != null){
        foreach($this->session->userdata('permission') as $columnvalue){
        }
        if(!empty($columnvalue)){ $permission =  json_decode($columnvalue->permission_permission); }
  
if((!empty($permission) && $permission->view == "true") || $this->session->userdata('user_role') == "admin"){
        $title['title']="All Employees";
        $data['employees'] = $this->EmployeesModel->view();
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        $this->load->view('admin/permission/allemployees',$data);
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }
        }else{
             redirect('Logout');
        }
    }

    function searchvalue(){

     $filterstatus  = $this->input->post('filterstatus'); 
     if($filterstatus === 'all'){
       return redirect('admin/Permission');
     }else{
     $data['employees']= $this->EmployeesModel->getstatusWhereLike($filterstatus); 
            if($data){ 
              $title['title']="All Permission";
              $this->load->view('admin/layouts/header',$title);
              $this->load->view('admin/layouts/navbar');
              $this->load->view('admin/permission/allemployees',$data);
              $this->load->view('admin/layouts/footer');
            }else{
               echo "filter data cant be show on view";
            }
     }
    }

    public function create()
    {
      foreach($this->session->userdata('permission') as $columnvalue){
      }
      if(!empty($columnvalue)){ $permission =  json_decode($columnvalue->permission_permission); }


      $empid = $this->uri->segment(4);
      if($this->session->userdata('usersid') != null){
        if((!empty($permission) && $permission->add == "true") || $this->session->userdata('user_role') == "admin"){
        $title['title']="Add Permission";
        $data['defaultpermisions'] = $this->EmployeesModel->getpermission($empid);
        $data['empdetails'] = $this->EmployeesModel->employeedtail($empid);
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        $this->load->view('admin/permission/create',$data);
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }
        }else{
             redirect('Logout');
        }
   }
   public function permissionadd(){
 
    $empid = $this->input->post("emp_id");
    $defaultpermisions= $this->EmployeesModel->getpermission($empid);
  
$countofpermission=count($defaultpermisions);
if($countofpermission > 0){
  
$permissionarray = array("sidebar"=>$this->input->post("sidebar"),'add' => $this->input->post("add"),'view' =>$this->input->post("view"),'update' =>$this->input->post("update"));
$permissionarrayencode = json_encode($permissionarray);
// var_dump($permissionarrayencode);die;
$result = $this->PermissionModel->updatepermission($permissionarrayencode,$this->input->post("columnname"),$empid);
  
}
else{
  $permissionarray = array("sidebar"=>$this->input->post("sidebar"),'add' => $this->input->post("add"),'view' =>$this->input->post("view"),'update' =>$this->input->post("update"));
  $permissionarrayencode = json_encode($permissionarray);
  // var_dump($permissionarrayencode);die;
  $result = $this->PermissionModel->addpermission($permissionarrayencode,$this->input->post("columnname"),$empid);

}
}

   function exportcsv(){
     // file name 
    $filename = 'permission_'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
     // get data 
    $usersData = $this->PermissionModel->getUserDetails();
    // file creation 
    $file = fopen('php://output','w');
    $header = array("coupon_title","cpn_status"); 
    fputcsv($file, $header);
    foreach ($usersData as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
    }


   
    
    }

    