<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Getaquote extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/GetaModel');
            $this->load->helper('form'); 
	  }

     public function index() 
     {
      if($this->session->userdata('usersid') != null){

    
        foreach($this->session->userdata('permission') as $columnvalue){
        }
       
if(!empty($columnvalue)){ $emplyee =  json_decode($columnvalue->permission_employee); }


if((!empty($emplyee) && $emplyee->view == "true") || $this->session->userdata('user_role') == "admin"){
     
          $title['title']="All Quotation Requests";
          $this->load->view('admin/layouts/header',$title);
          $this->load->view('admin/layouts/navbar');
          $data['support'] = $this->GetaModel->view();
          $this->load->view('admin/Getaquote/Allquote',$data);
          $this->load->view('admin/layouts/footer');
        }else{
          redirect("admin/Access");
        }
    
        }else{
             redirect('Logout');
        }
    }

    public function create()
    {
      if($this->session->userdata('usersid') != null){

          foreach($this->session->userdata('permission') as $columnvalue){
           }
       
            if(!empty($columnvalue)){ $emplyee =  json_decode($columnvalue->permission_employee); }
  
  
             if((!empty($emplyee) && $emplyee->add == "true") || $this->session->userdata('user_role') == "admin"){
                 $title['title']="Add Support Details";
                 $this->load->view('admin/layouts/header',$title);
                 $this->load->view('admin/layouts/navbar');
                 $this->load->view('admin/Getaquote/create');
                 $this->load->view('admin/layouts/footer');
               }else{
                  redirect("admin/Access");
               }

       }else{
             redirect('Logout');
        }
     }    




    function loaddataview(){
      foreach($this->session->userdata('permission') as $columnvalue){
      }
     
         if(!empty($columnvalue)){ $emplyee =  json_decode($columnvalue->permission_employee); }
  
  
            if((!empty($emplyee) && $emplyee->view == "true") || $this->session->userdata('user_role') == "admin"){
             $id = $this->input->post("id");
             $data['support'] = $this->GetaModel->loaddata1($id);
     
             $this->load->view('admin/Getaquote/view',$data);
           }else{
              redirect("admin/Access");
        }
      
    }



    
    }

    