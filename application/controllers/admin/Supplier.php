<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Supplier extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/SupplierModel');
            $this->load->helper('form'); 
	  }

     public function index()
     {
      if($this->session->userdata('usersid') != null){
        
        
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $supplier =  json_decode($columnvalue->permission_supplier); }

if((!empty($supplier) && $supplier->view == "true") || $this->session->userdata('user_role') == "admin"){
        $title['title']="All Suppliers";
        
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        
        $data['supplier'] = $this->SupplierModel->view();
        $this->load->view('admin/supplier/allsupplier',$data);
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }  
        }else{
             redirect('Logout');
        }
    }

     

    function add(){
      
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $supplier =  json_decode($columnvalue->permission_supplier); }

if((!empty($supplier) && $supplier->view == "true") || $this->session->userdata('user_role') == "admin"){
        $supplier_name = $this->input->post('supplier_name');
        $supplier_email = $this->input->post('supplier_email');
        $supplier_aadhar = $this->input->post('supplier_aadhar');
        $supplier_pan = $this->input->post('supplier_pan');
        $supplier_address = $this->input->post('supplier_address');
        $supplier_pincode = $this->input->post('supplier_pincode');
        $supllier_phonenumber = $this->input->post('supllier_phonenumber');
        $status = $this->input->post('status');
        $latlong = $this->input->post('latlong');
        $long = $this->input->post('long');
        $supllier_bankacc = $this->input->post('supllier_bankacc');
        $ifscnumber = $this->input->post('ifscnumber');
        $companyregnumber = $this->input->post('companyregnumber');
        $gstnumber = $this->input->post('gstnumber');
        $mondaystarttime = $this->input->post('mondaystarttime');
        $mondayendtime = $this->input->post('mondayendtime');
        $Tuesdaystarttime = $this->input->post('Tuesdaystarttime');
        $Tuesdayendtime = $this->input->post('Tuesdayendtime');
        $Wednesdaystarttime = $this->input->post('Wednesdaystarttime');
        $Wednesdayendtime = $this->input->post('Wednesdayendtime');
        $Thursdaystarttime = $this->input->post('Thursdaystarttime');
        $Thursdayendtime = $this->input->post('Thursdayendtime');
        $Fridaystarttime = $this->input->post('Fridaystarttime');

        $Fridayendtime = $this->input->post('Fridayendtime');
        $Saturdaystarttime = $this->input->post('Saturdaystarttime');
        $Saturdayendtime = $this->input->post('Saturdayendtime');
        $Sundaystarttime = $this->input->post('Sundaystarttime');
        $Sundayendtime = $this->input->post('Sundayendtime');

        if(!empty($_FILES['imageone']['name'])){
          $config['upload_path'] = './assets/images/supplier';
          $config['allowed_types'] = 'jpg|jpeg|png|gif';
          $config['file_name'] = $_FILES['imageone']['name'];
          
          //Load upload library and initialize configuration
          $this->load->library('upload',$config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('imageone')){
              $uploadData = $this->upload->data();
              $imageone = $uploadData['file_name'];
          }else{
              $imageone = '';
          }
      }else{
          $imageone = '';
      }
      if(!empty($_FILES['imagetwo']['name'])){
        $config['upload_path'] = './assets/images/supplier';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['file_name'] = $_FILES['imagetwo']['name'];
        // var_dump($imagetwo);die;
        //Load upload library and initialize configuration
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('imagetwo')){
            $uploadData = $this->upload->data();
            $imagetwo = $uploadData['file_name'];
        }else{
            $imagetwo = '';
        }
    }else{
        $imagetwo = '';
    }
    // var_dump($imagetwo);die;
 $result = $this->SupplierModel->add($supplier_name,$supplier_email,$supplier_aadhar,$supplier_pan,$supplier_address,$supplier_pincode,$supllier_phonenumber,$status,$latlong,$supllier_bankacc,$ifscnumber,$companyregnumber,$gstnumber,$imageone,$mondaystarttime,$mondayendtime,$Tuesdaystarttime,$Tuesdayendtime,$Wednesdaystarttime,$Wednesdayendtime,$Thursdaystarttime,$Thursdayendtime,$Fridaystarttime,$Fridayendtime,$Saturdaystarttime,$Saturdayendtime,$Sundaystarttime,$Sundayendtime,$long,$imagetwo);
//  die;
        if ($result == 'true') {
          echo "<script>alert('Supplier Added Successfully');
            window.location.href='../Supplier';
            </script>";
          }else{
            redirect("admin/Supplier");
        }
      }else{
        redirect("admin/Access");
      }  
    }

 

    public function create()
    {
      if($this->session->userdata('usersid') != null){
        
      
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $supplier =  json_decode($columnvalue->permission_supplier); }

if((!empty($supplier) && $supplier->add == "true") || $this->session->userdata('user_role') == "admin"){
        $title['title']="Add Supplier";
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        $this->load->view('admin/supplier/create');
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }   
      }else{
             redirect('Logout');
        }
   }

   function update(){
      
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $supplier =  json_decode($columnvalue->permission_supplier); }

if((!empty($supplier) && $supplier->update == "true") || $this->session->userdata('user_role') == "admin"){
  $long = $this->input->post('long');
    $puser_id = $this->input->post('puser_id');
    $supplier_name = $this->input->post('supplier_name');
    $supplier_email = $this->input->post('supplier_email');
    $supplier_aadhar = $this->input->post('supplier_aadhar');
    $supplier_pan = $this->input->post('supplier_pan');
    $supplier_address = $this->input->post('supplier_address');
    $supplier_pincode = $this->input->post('supplier_pincode');
    $supllier_phonenumber = $this->input->post('supllier_phonenumber');
    $status = $this->input->post('status');
    $latlong = $this->input->post('latlong');

    $supllier_bankacc = $this->input->post('supllier_bankacc');
    $ifscnumber = $this->input->post('ifscnumber');
    $companyregnumber = $this->input->post('companyregnumber');
    $gstnumber = $this->input->post('gstnumber');
    $mondaystarttime = $this->input->post('mondaystarttime');
    $mondayendtime = $this->input->post('mondayendtime');
    $Tuesdaystarttime = $this->input->post('Tuesdaystarttime');
    $Tuesdayendtime = $this->input->post('Tuesdayendtime');
    $Wednesdaystarttime = $this->input->post('Wednesdaystarttime');
    $Wednesdayendtime = $this->input->post('Wednesdayendtime');
    $Thursdaystarttime = $this->input->post('Thursdaystarttime');
    $Thursdayendtime = $this->input->post('Thursdayendtime');
    $Fridaystarttime = $this->input->post('Fridaystarttime');

    $Fridayendtime = $this->input->post('Fridayendtime');
    $Saturdaystarttime = $this->input->post('Saturdaystarttime');
    $Saturdayendtime = $this->input->post('Saturdayendtime');
    $Sundaystarttime = $this->input->post('Sundaystarttime');
    $Sundayendtime = $this->input->post('Sundayendtime');

    if(!empty($_FILES['imageone']['name'])){
      $config['upload_path'] = './assets/images/supplier';
      $config['allowed_types'] = 'jpg|jpeg|png|gif';
      $config['file_name'] = $_FILES['imageone']['name'];
      
      //Load upload library and initialize configuration
      $this->load->library('upload',$config);
      $this->upload->initialize($config);
      if($this->upload->do_upload('imageone')){
          $uploadData = $this->upload->data();
          $imageone = $uploadData['file_name'];
      }else{
          $imageone = '';
      }
  }else{
    $imageone = $this->input->post('hiddenimageone');
  }

  if(!empty($_FILES['imagetwo']['name'])){
    $config['upload_path'] = './assets/images/supplier';
    $config['allowed_types'] = 'jpg|jpeg|png|gif';
    $config['file_name'] = $_FILES['imagetwo']['name'];
    
    //Load upload library and initialize configuration
    $this->load->library('upload',$config);
    $this->upload->initialize($config);
    if($this->upload->do_upload('imagetwo')){
        $uploadData = $this->upload->data();
        $imagetwo = $uploadData['file_name'];
    }else{
        $imagetwo = '';
    }
}else{
 
    $imagetwo = $this->input->post('hiddenimagetwo');
}
  // var_dump($Tuesdayendtime);die;
$result = $this->SupplierModel->update($supplier_name,$supplier_email,$supplier_aadhar,$supplier_pan,$supplier_address,$supplier_pincode,$supllier_phonenumber,$status,$latlong,$supllier_bankacc,$ifscnumber,$companyregnumber,$gstnumber,$imageone,$puser_id,$mondaystarttime,$mondayendtime,$Tuesdaystarttime,$Tuesdayendtime,$Wednesdaystarttime,$Wednesdayendtime,$Thursdaystarttime,$Thursdayendtime,$Fridaystarttime,$Fridayendtime,$Saturdaystarttime,$Saturdayendtime,$Sundaystarttime,$Sundayendtime,$long,$imagetwo);

        if ($result == 1) {
          echo "<script>alert('Supplier Updated Successfully');
            window.location.href='../Supplier';
            </script>";
          }else{
        redirect("admin/Supplier");
        }
      }else{
        redirect("admin/Access");
      }  
    }



    function loaddata(){
   
      $id = $this->input->post("id");
      
      $data['supplier'] = $this->SupplierModel->loaddata($id);
     
      $this->load->view('admin/supplier/update',$data);
      
    }

    function loaddataview(){
   
      $id = $this->input->post("id");
      
      $data['supplier'] = $this->SupplierModel->loaddata1($id);
     
      $this->load->view('admin/supplier/view',$data);
      
    }

    function phonenocheckexist(){
      $username = $_POST['key'];
        $exists = $this->SupplierModel->phonenocheckexist($username);
    
        $count = count($exists);
        
    
        if (empty($count)) {
            echo 1;
        } else {
            echo 2;
        }
    }
    
    function emailcheckexist(){
        $username = $_POST['key'];
          $exists = $this->SupplierModel->emailcheckexist($username);
      
          $count = count($exists);
          
      
          if (empty($count)) {
              echo 1;
          } else {
              echo 2;
          }
      }

      // ..

      function spare(){ 
        //SELECT spare_id,tbl_spare.sp_title,tbl_p_spares.p_spare_mrp,tbl_p_spares.p_spare_selling_price, tbl_p_spares. gstspprice FROM `tbl_spare` RIGHT join tbl_p_spares on tbl_spare.sp_id=tbl_p_spares.p_spare_id where tbl_p_spares.spare_id='1'
        if($this->session->userdata('usersid') != null){
         foreach($this->session->userdata('permission') as $columnvalue){
           }
           if(!empty($columnvalue)){ $supplier =  json_decode($columnvalue->permission_supplier);  }
             if((!empty($supplier) && $supplier->view == "true") || $this->session->userdata('user_role') == "admin"){
                 $title['title']="All Supplier";
                 $this->load->view('admin/layouts/header',$title);
                 $this->load->view('admin/layouts/navbar');
                 // Supplier id from puser_tbl..
                 $id = $this->uri->segment(4);
                 $data1 = $this->SupplierModel->loadsupplier($id);
                 $data = $this->SupplierModel->spare($id);// echo "<pre>"; print_r($data); echo "</pre>";die; 
                 $this->load->view('admin/supplier/allspare',['data1'=>$data1,'data'=>$data]);
                 $this->load->view('admin/layouts/footer');
               }else{
                 redirect("admin/Access");
               }  
             }else{
                  edirect('Logout');
             }
      }  

      function getModel(){
         $id = $this->input->post("id");
        $data = $this->SupplierModel->loaddata2($id);
        $spare_category = $this->SupplierModel->loaddata3();
        //$spare = $this->SupplierModel->loaddata4();
        $this->load->view('admin/supplier/modalview',['data' => $data,'spare_category' => $spare_category]);
      }

      function loadSpareCategory(){
        if($this->input->post('spare_cat'))
            {
               echo $this->SupplierModel->loadSpareCategory($this->input->post('spare_cat'));
            }
      }

       function Updatemodel(){
         $supplier_id =$this->input->post("supplier_id");
       $spare_id =$this->input->post("spare_id");
        $spare_cat =$this->input->post("spare_cat");
        $spare =$this->input->post("spare");
        $p_spare_mrp =$this->input->post("p_spare_mrp");
        $p_spare_selling_price =$this->input->post("p_spare_selling_price");
        $p_spare_gst_percentage =$this->input->post("p_spare_gst_percentage");
        $gst_amount =$this->input->post("gst_amount");
        $gstspprice =$this->input->post("gstspprice");
         $result=$this->SupplierModel->updatemodeldata($spare_cat,$spare,$p_spare_mrp,$p_spare_selling_price,$p_spare_gst_percentage,$gst_amount,$gstspprice,$supplier_id);
            if ($result == 'true') {
                         echo "<script>alert('Spare Added Successfully');
                                window.location.href='spare/$spare_id';
                                </script>";
                               // redirect('admin/Supplier/spare/'.$spare_id);
                 }else{
                          redirect("admin/Technician");
                 }
      }

      function deletedata(){
         $p_spare_id =$this->input->get("p_spare_id");
          $id = $this->uri->segment(4); 
         $result=$this->SupplierModel->deleteSupplierspare($p_spare_id);
          if ($result == 'true') {
                         echo "<script>alert('Spare Deleted Successfully');
                          window.location.href='../spare/$id';
                                </script>";
                              //  redirect('admin/Supplier/spare/'.$id);
                 }else{
                          redirect("admin/Technician");
                 }
       
      }

    function searchvalue(){

     $filterstatus  = $this->input->post('filterstatus'); 
     if($filterstatus === 'all'){
       return redirect('admin/Supplier');
     }else{
     $data['supplier']= $this->SupplierModel->getstatusWhereLike($filterstatus); 
            if($data){ 
              $title['title']="All Supplier";
              $this->load->view('admin/layouts/header',$title);
              $this->load->view('admin/layouts/navbar');
              $this->load->view('admin/supplier/allsupplier',$data);
              $this->load->view('admin/layouts/footer');
            }else{
               echo "filter data cant be show on view";
            }
     }
    }

    function exportcsv(){
     // file name 
    $filename = 'supplier_'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
     // get data 
    $usersData = $this->SupplierModel->getUserDetails();
    // file creation 
    $file = fopen('php://output','w');
    $header = array("User Id","Support Name","Support Address","Support Pincode","Support Phone Number","Support Email","Support Aadhar Number","Support Pan Number"," GST Number","Bank Number","Support IFSC","Support Availability Status","Support Status","User Type"); 
    fputcsv($file, $header);
    foreach ($usersData as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
    }

    function exportcsv1(){
       $id = $this->uri->segment(4);
     // file name 
    $filename = 'supplier_spare_'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
     // get data 
    $usersData = $this->SupplierModel->getUserDetails1($id);
    // file creation 
    $file = fopen('php://output','w');
    $header = array("spare Id","Spare Title","Spare MRP","Spare Selling Price","Spare GST Percentage"); 
    fputcsv($file, $header);
    foreach ($usersData as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
    }
  
}

    