<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Openings extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/OpeningsModel');
            $this->load->helper('form'); 
	  }
 
     public function index()
     {
      if($this->session->userdata('usersid') != null){
        
     
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $openings =  json_decode($columnvalue->permission_openings); }

if((!empty($openings) && $openings->view == "true") || $this->session->userdata('user_role') == "admin"){
        $title['title']="All Openings";
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        
        $data['openings'] = $this->OpeningsModel->view();
        $this->load->view('admin/career/allopenings',$data);
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }
        }else{
             redirect('Logout');
        }
    }

    function searchvalue(){

     $filterstatus  = $this->input->post('filterstatus'); 
     if($filterstatus === 'all'){
       return redirect('admin/Openings');
     }else{
     $data['openings']= $this->OpeningsModel->getstatusWhereLike($filterstatus); 
            if($data){ 
              $title['title']="All Openings";
              $this->load->view('admin/layouts/header',$title);
              $this->load->view('admin/layouts/navbar');
              $this->load->view('admin/openings/allopenings',$data);
              $this->load->view('admin/layouts/footer');
            }else{
               echo "filter data cant be show on view";
            }
     }
    }

    function add(){
      
     
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $openings =  json_decode($columnvalue->permission_openings); }

if((!empty($openings) && $openings->add == "true") || $this->session->userdata('user_role') == "admin"){
        
        // print_r($this->input->post());exit;
        $job_title = $this->input->post('job_title');
        $position = $this->input->post('position');
        $qualification = $this->input->post('qualification');
        $description = $this->input->post('description');
        $status = $this->input->post('status');
        
        if(!empty($_FILES['imageone']['name'])){
          $config['upload_path'] = './assets/images/openings';
          $config['allowed_types'] = 'jpg|jpeg|png|gif';
          $config['file_name'] = $_FILES['imageone']['name'];
          
          //Load upload library and initialize configuration
          $this->load->library('upload',$config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('imageone')){
              $uploadData = $this->upload->data();
              $imageone = $uploadData['file_name'];
          }else{
              $imageone = '';
          }
      }else{
          $imageone = '';
      }
     
        
 $result = $this->OpeningsModel->add($job_title,$position,$qualification,$description,$status,$imageone);
//  die;
        if ($result == 'true') {
          echo "<script>alert('Openings Added Successfully');
            window.location.href='../Openings';
            </script>";
          }else{
            redirect("admin/Openings");
        }
      }else{
        redirect("admin/Access");
      }
    }

 

    public function create()
    {
      if($this->session->userdata('usersid') != null){
        
     
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $openings =  json_decode($columnvalue->permission_openings); }

if((!empty($openings) && $openings->add == "true") || $this->session->userdata('user_role') == "admin"){
        $title['title']="Add Openings";
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        $this->load->view('admin/career/create');
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }
        }else{
             redirect('Logout');
        }
   }

   function update(){
     
     
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $openings =  json_decode($columnvalue->permission_openings); }

if((!empty($openings) && $openings->update == "true") || $this->session->userdata('user_role') == "admin"){
    
    // print_r($this->input->post());exit;
    $job_id = $this->input->post('job_id');
   
    $job_title = $this->input->post('job_title');
    $description = $this->input->post('description');
    $openings = $this->input->post('openings');
    $qualification = $this->input->post('qualification');
    $status = $this->input->post('status');
        if(!empty($_FILES['imageone']['name'])){
          $config['upload_path'] = './assets/images/openings';
          $config['allowed_types'] = 'jpg|jpeg|png|gif';
          $config['file_name'] = $_FILES['imageone']['name'];
          
          //Load upload library and initialize configuration
          $this->load->library('upload',$config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('imageone')){
              $uploadData = $this->upload->data();
              $imageone = $uploadData['file_name'];
          }else{
              $imageone = '';
          }
      }else{
        $imageone = $this->input->post('hiddenimageone');
      }
    
   
$result = $this->OpeningsModel->update($job_title,$description,$openings,$qualification,$status,$imageone,$job_id);

        if ($result == 1) {
          echo "<script>alert('Openings Updated Successfully');
            window.location.href='../Openings';
            </script>";
          }else{
        redirect("admin/Openings");
        }
      }else{
        redirect("admin/Access");
      }
    }



    function loaddata(){
   
      $id = $this->input->post("id");
      
      $data['openings'] = $this->OpeningsModel->loaddata($id);
     
      $this->load->view('admin/career/update',$data);
      
    }

    function loaddataview(){
   
      $id = $this->input->post("id");
      
      $data['openings'] = $this->OpeningsModel->loaddata1($id);
     
      $this->load->view('admin/career/view',$data); 
      
    }
    function openingslist()
    {
        $this->load->database();
        if(!empty($this->input->get("q"))){
			$q = $this->input->get("q");
            //$sql = "select vehb_id,vehb_make,vehb_model,vehb_trim from tbl_openings  where vehb_make LIKE '%$q%' and `vehb_status`='Active' ";
       $sql = "select vehb_id,vehb_make,vehb_model,vehb_trim from tbl_openings  where vehb_make LIKE '%$q%'";
            // echo $sql;die;
			$query = $this->db->query($sql);
        	$json = $query->result();
        }else{
            //$sql = "select vehb_id,vehb_make,vehb_model,vehb_trim from tbl_openings where `vehb_status`='Active'";
          $sql = "select vehb_id,vehb_make,vehb_model,vehb_trim from tbl_openings";
            // echo $sql;die;
			$query = $this->db->query($sql);
      	    $json = $query->result(); 
        }
      echo json_encode($json);
    }
   
    
    }

    