<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class WebTechnician extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/WebTechnicianModel');
            $this->load->helper('form'); 
	  }

     public function index()
     {
      if($this->session->userdata('usersid') != null){
      if(!empty($columnvalue)){ $webtechnician =  json_decode($columnvalue->permission_webtechnician); }
if((!empty($webtechnician) && $webtechnician->view == "true") || $this->session->userdata('user_role') == "admin"){
        $title['title']="All Web Technicians";
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
         $data['technician'] = $this->WebTechnicianModel->view();
        $this->load->view('admin/webtechnician/allwebtechnician',$data);
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }  
        }else{
             redirect('Logout');
        }
    }


    function loaddataview(){
   
      $id = $this->input->post("id");
      
      $data['technician'] = $this->WebTechnicianModel->loaddata1($id);
     
      $this->load->view('admin/webtechnician/view',$data);
      
    }

 function exportcsv(){
     // file name 
    $filename = 'WebTechnician_'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
     // get data 
    $usersData = $this->WebTechnicianModel->getUserDetails();
    // file creation 
    $file = fopen('php://output','w');
    $header = array("Id","Name","Email","Phone Number","Address","Pincode","Reference"); 
    fputcsv($file, $header);
    foreach ($usersData as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
    }
 }

    