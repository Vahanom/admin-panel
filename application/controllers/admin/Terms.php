<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Terms extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/TermsModel');
            $this->load->helper('form'); 
	  }

     public function index()
     {
      if($this->session->userdata('usersid') != null){

    
        foreach($this->session->userdata('permission') as $columnvalue){
        }
       
if(!empty($columnvalue)){ $terms =  json_decode($columnvalue->permission_terms); }


if((!empty($terms) && $terms->view == "true") || $this->session->userdata('user_role') == "admin"){
     
          $title['title']="All Terms And Conditions";
          $this->load->view('admin/layouts/header',$title);
          $this->load->view('admin/layouts/navbar');
          $data['terms'] = $this->TermsModel->view();
          $this->load->view('admin/Terms/allterms',$data);
          $this->load->view('admin/layouts/footer');
        }else{
          redirect("admin/Access");
        }
    
        }else{
             redirect('Logout');
        }
    }

    public function create()
    {
      if($this->session->userdata('usersid') != null){

          foreach($this->session->userdata('permission') as $columnvalue){
           }
       
            if(!empty($columnvalue)){ $terms =  json_decode($columnvalue->permission_terms); }
  
  
             if((!empty($terms) && $terms->add == "true") || $this->session->userdata('user_role') == "admin"){
                 $title['title']="Add Terms And Conditions";
                 $this->load->view('admin/layouts/header',$title);
                 $this->load->view('admin/layouts/navbar');
                 $this->load->view('admin/Terms/create');
                 $this->load->view('admin/layouts/footer');
               }else{
                  redirect("admin/Access");
               }

       }else{
             redirect('Logout');
        }
     }    


    function add(){
      
      foreach($this->session->userdata('permission') as $columnvalue){
      }
     
          if(!empty($columnvalue)){ $terms =  json_decode($columnvalue->permission_terms); }


          if((!empty($terms) && $terms->add == "true") || $this->session->userdata('user_role') == "admin"){
            $term_description = $this->input->post('term_description');

             $result = $this->TermsModel->add($term_description);
            //  die;
             if ($result == 'true') {
                    echo "<script>alert('Terms And Conditions Added Successfully');
                           window.location.href = '../Terms';
                         </script>";
            }else{
                    redirect("Terms/index");
            }
        }else{
            redirect("admin/Access");
      }
    }

    function loaddata(){
      foreach($this->session->userdata('permission') as $columnvalue){
      }
     
       if(!empty($columnvalue)){ $terms =  json_decode($columnvalue->permission_terms); }
  
  
        if((!empty($terms) && $terms->view == "true") || $this->session->userdata('user_role') == "admin"){
           $id = $this->input->post("id");
      
           $data['terms'] = $this->TermsModel->loaddata($id);
     
          $this->load->view('admin/Terms/update',$data);
         }else{
             redirect("admin/Access");
         }
      
    }

    function loaddataview(){
      foreach($this->session->userdata('permission') as $columnvalue){
      }
     
         if(!empty($columnvalue)){ $terms =  json_decode($columnvalue->permission_terms); }
  
  
            if((!empty($terms) && $terms->view == "true") || $this->session->userdata('user_role') == "admin"){
             $id = $this->input->post("id");
      
             $data['terms'] = $this->TermsModel->loaddata1($id);
     
             $this->load->view('admin/Terms/view',$data);
           }else{
              redirect("admin/Access");
        }
      
    }

     function update(){
     
    foreach($this->session->userdata('permission') as $columnvalue){
    }
   
      if(!empty($columnvalue)){ $terms =  json_decode($columnvalue->permission_terms); }


        if((!empty($terms) && $terms->update == "true") || $this->session->userdata('user_role') == "admin"){
            $term_id = $this->input->post('term_id');
            $term_description = $this->input->post('term_description');
           $result = $this->TermsModel->update($term_id,$term_description);


        if ($result == 1) {
          echo "<script>alert('Terms Updated Successfully');
            window.location.href = 'index';
            </script>";
          }else{
        redirect("Terms/index");
        }

      }else{
        redirect("admin/Access");
    }
    }

     function exportcsv(){
     // file name 
    $filename = 'Terms'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
     // get data 
    $usersData = $this->TermsModel->getUserDetails();
    // file creation 
    $file = fopen('php://output','w');
    $header = array("term_id","term_description","term_create_date","term_modify_date"); 
    fputcsv($file, $header);
    foreach ($usersData as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
    }
   
    
    }

    