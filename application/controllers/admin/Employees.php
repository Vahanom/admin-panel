<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Employees extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/EmployeesModel');
            $this->load->helper('form'); 
	  }

     public function index()
     {
      if($this->session->userdata('usersid') != null){

    
        foreach($this->session->userdata('permission') as $columnvalue){
        }
       
if(!empty($columnvalue)){ $emplyee =  json_decode($columnvalue->permission_employee); }

 
if((!empty($emplyee) && $emplyee->view == "true") || $this->session->userdata('user_role') == "admin"){
     
          $title['title']="All Employees";
          $this->load->view('admin/layouts/header',$title);
          $this->load->view('admin/layouts/navbar');
          $data['employees'] = $this->EmployeesModel->view();
          $this->load->view('admin/employees/allemployees',$data);
          $this->load->view('admin/layouts/footer');
        }else{
          redirect("admin/Access");
        }
    
        }else{
             redirect('Logout');
        }
    }

     

    function add()
    {
      
      foreach($this->session->userdata('permission') as $columnvalue)
      {
      }
     
      if(!empty($columnvalue)){ $emplyee =  json_decode($columnvalue->permission_employee); }
      if((!empty($emplyee) && $emplyee->add == "true") || $this->session->userdata('user_role') == "admin")
      {
        // print_r($this->input->post());exit;
        $user_name = $this->input->post('user_name');
        $user_email = $this->input->post('user_email');
        $user_aadhar = $this->input->post('user_aadhar');
        $user_pan = $this->input->post('user_pan');
        $user_address = $this->input->post('user_address');
        $blood_group = $this->input->post('blood_group');
        $user_type = $this->input->post('user_type');
        $status = $this->input->post('status');
        $phonenumber = $this->input->post('phonenumber');
        
        $result = $this->EmployeesModel->add($user_name,$user_email,$user_aadhar,$user_pan,$user_address,$blood_group,$user_type,$status,$phonenumber);
        //  die;
        if ($result == 'true') 
        {
          echo "<script>alert('Employee Added Successfully');
             window.location.href='../Employees';
            </script>";
          }else{
            redirect("admin/Employees");
          }
        }else{
          redirect("admin/Access");
        }
      }

 

    public function create()
    {
      if($this->session->userdata('usersid') != null){

        foreach($this->session->userdata('permission') as $columnvalue){
        }
       
  if(!empty($columnvalue)){ $emplyee =  json_decode($columnvalue->permission_employee); }
  
  
  if((!empty($emplyee) && $emplyee->add == "true") || $this->session->userdata('user_role') == "admin"){
        $title['title']="Add Employee";
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        $this->load->view('admin/employees/create');
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
    }

        }else{
             redirect('Logout');
        }
   }

   function update(){
     
    foreach($this->session->userdata('permission') as $columnvalue){
    }
   
if(!empty($columnvalue)){ $emplyee =  json_decode($columnvalue->permission_employee); }


if((!empty($emplyee) && $emplyee->update == "true") || $this->session->userdata('user_role') == "admin"){
    $webuser_id = $this->input->post('webuser_id');
    $user_name = $this->input->post('user_name');
    $user_email = $this->input->post('user_email');
    $user_aadhar = $this->input->post('user_aadhar');
    $user_pan = $this->input->post('user_pan');
    $user_address = $this->input->post('user_address');
    $blood_group = $this->input->post('blood_group');
    $user_type = $this->input->post('user_type');
    $status = $this->input->post('status');
    $phonenumber = $this->input->post('phonenumber');
    
$result = $this->EmployeesModel->update($user_name,$user_email,$user_aadhar,$user_pan,$user_address,$blood_group,$user_type,$status,$phonenumber,$webuser_id);

        if ($result == 1) {
          echo "<script>alert('Employee Updated Successfully');
            window.location.href='../Employees';
            </script>";
          }else{
        redirect("admin/Employees");
        }

      }else{
        redirect("admin/Access");
    }
    }



    function loaddata(){
      foreach($this->session->userdata('permission') as $columnvalue){
      }
     
  if(!empty($columnvalue)){ $emplyee =  json_decode($columnvalue->permission_employee); }
  
  
  if((!empty($emplyee) && $emplyee->view == "true") || $this->session->userdata('user_role') == "admin"){
      $id = $this->input->post("id");
      
      $data['employees'] = $this->EmployeesModel->loaddata($id);
     
      $this->load->view('admin/employees/update',$data);
    }else{
      redirect("admin/Access");
  }
      
    }

    function loaddataview(){
      foreach($this->session->userdata('permission') as $columnvalue){
      }
     
  if(!empty($columnvalue)){ $emplyee =  json_decode($columnvalue->permission_employee); }
  
  
  if((!empty($emplyee) && $emplyee->view == "true") || $this->session->userdata('user_role') == "admin"){
      $id = $this->input->post("id");
      
      $data['employees'] = $this->EmployeesModel->loaddata1($id);
     
      $this->load->view('admin/employees/view',$data);
    }else{
      redirect("admin/Access");
  }
      
    }

    function phonenocheckexist(){
      $username = $_POST['key'];
        $exists = $this->EmployeesModel->phonenocheckexist($username);
    
        $count = count($exists);
        
    
        if (empty($count)) {
            echo 1;
        } else {
            echo 2;
        }
    }
     function pancheckexist(){
      $username = $_POST['key'];
        $exists = $this->EmployeesModel->pancheckexist($username);
    
        $count = count($exists);
        
    
        if (empty($count)) {
            echo 1;
        } else {
            echo 2;
        }
    }
    
    function adharcheckexist(){
      $username = $_POST['key'];
        $exists = $this->EmployeesModel->adharcheckexist($username);
    
        $count = count($exists);
        
    
        if (empty($count)) {
            echo 1;
        } else {
            echo 2;
        }
    }


    function emailcheckexist(){
        $username = $_POST['key'];
          $exists = $this->EmployeesModel->emailcheckexist($username);
      
          $count = count($exists);
          
      
          if (empty($count)) {
              echo 1;
          } else {
              echo 2;
          }
      }

     function searchvalue(){

     $filterstatus  = $this->input->post('filterstatus'); 
     if($filterstatus === 'all'){
       return redirect('admin/Employees');
     }else{
     $data['employees']= $this->EmployeesModel->getstatusWhereLike($filterstatus); 
            if($data){ 
              $title['title']="All Employee";
              $this->load->view('admin/layouts/header',$title);
              $this->load->view('admin/layouts/navbar');
              $this->load->view('admin/employees/allemployees',$data);
              $this->load->view('admin/layouts/footer');
            }else{
               echo "filter data cant be show on view";
            }
     }
    }

    function exportcsv(){
     // file name 
    $filename = 'employee_'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
     // get data 
    $usersData = $this->EmployeesModel->getUserDetails();
    // file creation 
    $file = fopen('php://output','w');
    $header = array("webuser_id", "webuser_name", "webuser_email", "webuser_phno", "webuser_address", "webuser_pan_num", "webuser_adhar_num", "webuser_type", "webuser_bloodgroup", "webuser_status", "webuser_createddate", "webuser_modifieddate"); 
    fputcsv($file, $header);
    foreach ($usersData as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
    }
    
    }

    