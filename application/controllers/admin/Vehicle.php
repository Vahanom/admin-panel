<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Vehicle extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/VehicleModel');
            $this->load->helper('form'); 
	  }

     public function index()
     {
      if($this->session->userdata('usersid') != null){
        
     
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $vehicle =  json_decode($columnvalue->permission_vehicle); }

if((!empty($vehicle) && $vehicle->view == "true") || $this->session->userdata('user_role') == "admin"){
        $title['title']="All Vehicle";
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        
        $data['vehicle'] = $this->VehicleModel->view();
        $this->load->view('admin/vehicle/allvehicle',$data);
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }
        }else{
             redirect('Logout');
        }
    }

    function searchvalue(){

     $filterstatus  = $this->input->post('filterstatus'); 
     if($filterstatus === 'all'){
       return redirect('admin/Vehicle');
     }else{
     $data['vehicle']= $this->VehicleModel->getstatusWhereLike($filterstatus); 
            if($data){ 
              $title['title']="All Vehicle";
              $this->load->view('admin/layouts/header',$title);
              $this->load->view('admin/layouts/navbar');
              $this->load->view('admin/vehicle/allvehicle',$data);
              $this->load->view('admin/layouts/footer');
            }else{
               echo "filter data cant be show on view";
            }
     }
    }

    function add(){
      
     
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $vehicle =  json_decode($columnvalue->permission_vehicle); }

if((!empty($vehicle) && $vehicle->add == "true") || $this->session->userdata('user_role') == "admin"){
        $make_name = $this->input->post('make_name');
        $model_name = $this->input->post('model_name');
        $trim_vehicle = $this->input->post('trim_vehicle');
        $fuel = $this->input->post('fuel');
        $type_vehicle = $this->input->post('type_vehicle');
        $status = $this->input->post('status');

        if(!empty($_FILES['imageone']['name'])){
          $config['upload_path'] = './assets/images/vehicle';
          $config['allowed_types'] = 'jpg|jpeg|png|gif';
          $config['file_name'] = $_FILES['imageone']['name'];
          
          //Load upload library and initialize configuration
          $this->load->library('upload',$config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('imageone')){
              $uploadData = $this->upload->data();
              $imageone = $uploadData['file_name'];
          }else{
              $imageone = '';
          }
      }else{
          $imageone = '';
      }
     
        
 $result = $this->VehicleModel->add($make_name,$model_name,$trim_vehicle,$fuel,$type_vehicle,$status,$imageone);
//  die;
        if ($result == 'true') {
          echo "<script>alert('Vehicle Added Successfully');
            window.location.href='../Vehicle';
            </script>";
          }else{
            redirect("admin/Vehicle");
        }
      }else{
        redirect("admin/Access");
      }
    }

 

    public function create()
    {
      if($this->session->userdata('usersid') != null){
        
     
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $vehicle =  json_decode($columnvalue->permission_vehicle); }

if((!empty($vehicle) && $vehicle->add == "true") || $this->session->userdata('user_role') == "admin"){
        $title['title']="Add Vehicle";
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        $this->load->view('admin/vehicle/create');
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }
        }else{
             redirect('Logout');
        }
   }

   function update(){
     
     
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $vehicle =  json_decode($columnvalue->permission_vehicle); }

if((!empty($vehicle) && $vehicle->update == "true") || $this->session->userdata('user_role') == "admin"){
    $vehb_id = $this->input->post('vehb_id');
   
    $make_name = $this->input->post('make_name');
    $model_name = $this->input->post('model_name');
    $trim_vehicle = $this->input->post('trim_vehicle');
    $fuel = $this->input->post('fuel');
    $type_vehicle = $this->input->post('type_vehicle');
    $status = $this->input->post('status');
        if(!empty($_FILES['imageone']['name'])){
          $config['upload_path'] = './assets/images/technician';
          $config['allowed_types'] = 'jpg|jpeg|png|gif';
          $config['file_name'] = $_FILES['imageone']['name'];
          
          //Load upload library and initialize configuration
          $this->load->library('upload',$config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('imageone')){
              $uploadData = $this->upload->data();
              $imageone = $uploadData['file_name'];
          }else{
              $imageone = '';
          }
      }else{
        $imageone = $this->input->post('hiddenimageone');
      }
    
   
$result = $this->VehicleModel->update($make_name,$model_name,$trim_vehicle,$fuel,$type_vehicle,$status,$imageone,$vehb_id);

        if ($result == 1) {
          echo "<script>alert('Vehicle Updated Successfully');
            window.location.href='../Vehicle';
            </script>";
          }else{
        redirect("admin/Vehicle");
        }
      }else{
        redirect("admin/Access");
      }
    }



    function loaddata(){
   
      $id = $this->input->post("id");
      
      $data['vehicle'] = $this->VehicleModel->loaddata($id);
     
      $this->load->view('admin/vehicle/update',$data);
      
    }

    function loaddataview(){
   
      $id = $this->input->post("id");
      
      $data['vehicle'] = $this->VehicleModel->loaddata1($id);
     
      $this->load->view('admin/vehicle/view',$data);
      
    }
    function vehiclelist()
    {
        $this->load->database();
        if(!empty($this->input->get("q")))
        {
		  	  $q = $this->input->get("q");
            //$sql = "select vehb_id,vehb_make,vehb_model,vehb_trim from tbl_vehicle  where vehb_make LIKE '%$q%' and "vehb_status"='Active' ";
          $sql = "select vehb_id,vehb_make,vehb_model,vehb_trim from tbl_vehicle  where vehb_make LIKE '%$q%'";
            // echo $sql;die;
			    $query = $this->db->query($sql);
        	$json = $query->result();
        }else
        {
            //$sql = "select vehb_id,vehb_make,vehb_model,vehb_trim from tbl_vehicle where "vehb_status"='Active'";
          $sql = "select vehb_id,vehb_make,vehb_model,vehb_trim from tbl_vehicle";
            // echo $sql;die;
		      $query = $this->db->query($sql);
      	  $json = $query->result(); 
        }
      echo json_encode($json);
    }

     function exportcsv(){
     // file name 
    $filename = 'vehicle_'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
     // get data 
    $usersData = $this->VehicleModel->getUserDetails();
    // file creation 
    $file = fopen('php://output','w');
    $header = array("vehb_id", "vehb_type", "vehb_make", "vehb_model", "vehb_trim", "vehb_fuel", "vehb_image", "vehb_status", "vehb_createdate", "vehb_modifydate"); 
    fputcsv($file, $header);
    foreach ($usersData as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
    }
   
    
    }

    