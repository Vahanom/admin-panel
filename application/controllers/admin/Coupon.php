<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Coupon extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/CouponModel');
            $this->load->helper('form'); 
	  }

     public function index()
     {
      if($this->session->userdata('usersid') != null){

        foreach($this->session->userdata('permission') as $columnvalue){
        }
       
        if(!empty($columnvalue)){ $coupon =  json_decode($columnvalue->permission_coupon); }


if((!empty($coupon) && $coupon->view == "true") || $this->session->userdata('user_role') == "admin"){

        $title['title']="All Copoun";
        $data['coupon'] = $this->CouponModel->view();
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        $this->load->view('admin/coupon/allcoupon',$data);
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }   }else{
             redirect('Logout');
        }
    }
function searchvalue(){

     $filterstatus  = $this->input->post('filterstatus'); 
     if($filterstatus === 'all'){
       return redirect('admin/Coupon');
     }else{
     $data['coupon']= $this->CouponModel->getstatusWhereLike($filterstatus); 
            if($data){ 
              $title['title']="All Copoun";
              $this->load->view('admin/layouts/header',$title);
              $this->load->view('admin/layouts/navbar');
              $this->load->view('admin/coupon/allcoupon',$data);
              $this->load->view('admin/layouts/footer');
            }else{
               echo "filter data cant be show on view";
            }
     }
    }
    function add(){

      foreach($this->session->userdata('permission') as $columnvalue){
      }
     
      if(!empty($columnvalue)){ $coupon =  json_decode($columnvalue->permission_coupon); }


if((!empty($coupon) && $coupon->add == "true") || $this->session->userdata('user_role') == "admin"){
        $Coupon_title = $this->input->post('Coupon_title');
        $Coupon_desc = $this->input->post('Coupon_desc');
       $coupontype = $this->input->post('coupontype');

       $Coupon_flat = $this->input->post('Coupon_flat');
        $Coupon_per = $this->input->post('Coupon_per');
       $start_date = $this->input->post('start_date');
       $end_date = $this->input->post('end_date');
        $maximumbillamount = $this->input->post('maximumbillamount');
       $maxuserlimit = $this->input->post('maxuserlimit');
       $maxuseperuser = $this->input->post('maxuseperuser');
        $minimumbillamount = $this->input->post('minimumbillamount');
       $status = $this->input->post('status');
       $couponcode = $this->input->post('couponcode');
       
 $result = $this->CouponModel->add($Coupon_title,$Coupon_desc,$coupontype,$Coupon_flat,$Coupon_per,$start_date,$end_date,$maximumbillamount,$maxuserlimit,$maxuseperuser,$minimumbillamount,$status,$couponcode);
//  die;
        if ($result == 'true') {
          echo "<script>alert('Coupon Added Successfully');
            window.location.href = 'index';
            </script>";
          }else{
            redirect("admin/Access");
          } 
          }else{
            redirect("admin/Coupon");
				}
    }

 

    public function create()
    {
      if($this->session->userdata('usersid') != null){

        
      foreach($this->session->userdata('permission') as $columnvalue){
      }
     
      if(!empty($columnvalue)){ $coupon =  json_decode($columnvalue->permission_coupon); }


if((!empty($coupon) && $coupon->add == "true") || $this->session->userdata('user_role') == "admin"){
        $title['title']="Add Coupon";
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        $this->load->view('admin/coupon/create');
        $this->load->view('admin/layouts/footer');
        
      }else{
        redirect("admin/Access");
      } 
      }else{
             redirect('Logout');
        }
   }

   function update(){
     
    foreach($this->session->userdata('permission') as $columnvalue){
    }
   
    if(!empty($columnvalue)){ $coupon =  json_decode($columnvalue->permission_coupon); }


if((!empty($coupon) && $coupon->add == "true") || $this->session->userdata('user_role') == "admin"){

    $cpn_id = $this->input->post('cpn_id');
    $Coupon_title = $this->input->post('Coupon_title');
    $Coupon_desc = $this->input->post('Coupon_desc');
   $coupontype = $this->input->post('coupontype');

   $Coupon_flat = $this->input->post('Coupon_flat');
    $Coupon_per = $this->input->post('Coupon_per');
   $start_date = $this->input->post('start_date');
   $end_date = $this->input->post('end_date');
    $maximumbillamount = $this->input->post('maximumbillamount');
   $maxuserlimit = $this->input->post('maxuserlimit');
   $maxuseperuser = $this->input->post('maxuseperuser');
    $minimumbillamount = $this->input->post('minimumbillamount');
   $status = $this->input->post('status');
   $couponcode = $this->input->post('couponcode');
$result = $this->CouponModel->update($Coupon_title,$Coupon_desc,$coupontype,$Coupon_flat,$Coupon_per,$start_date,$end_date,$maximumbillamount,$maxuserlimit,$maxuseperuser,$minimumbillamount,$status,$couponcode,$cpn_id);

        if ($result == 1) {
          echo "<script>alert('Coupon Updated Successfully');
            window.location.href = 'index';
            </script>";
          }else{
        redirect("admin/Coupon");
        }
      }else{
        redirect("admin/Access");
      }
    }



    function loaddata(){
   
      $id = $this->input->post("id");
      $data['coupon'] = $this->CouponModel->loaddata($id);
      $this->load->view('admin/coupon/update',$data);
      
    }

    function loaddataview(){
   
      $id = $this->input->post("id");
      $data['coupon'] = $this->CouponModel->loaddata1($id);
     
      $this->load->view('admin/coupon/view',$data);
      
    }

     function exportcsv(){
     // file name 
    $filename = 'coupon_'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
     // get data 
    $usersData = $this->CouponModel->getUserDetails();
    // file creation 
    $file = fopen('php://output','w');
    $header = array("cpn_code", "cpn_description", "cpn_type", "cpn_flat_amount", "cpn_percentage", "cpn_startdate", "cpn_enddate", "cpn_status", "cpn_min_bill_amt", "cpn_max_discount", "cpn_max_users", "cpn_maxperusers", "cpn_createdate", "cpn_modifieddate", "coupon_title", "cpn_id"); 
    fputcsv($file, $header);
    foreach ($usersData as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
    }
   
    //Duplicate title check on blur 
    function namecheckexist()
    {
      $username = $_POST['key'];
      $exists = $this->CouponModel->namecheckexist1($username);
      $count = count($exists);
      
      if (empty($count)) {
        echo 1;
      } else {
              echo 2;
      }
    }
   
    
    }

    