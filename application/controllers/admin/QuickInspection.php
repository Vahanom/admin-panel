<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class QuickInspection extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/QuickInspectionModel');
            $this->load->helper('form'); 
	  }

     public function index()
     {
   
      if($this->session->userdata('usersid') != null){
        foreach($this->session->userdata('permission') as $columnvalue){
        }
        if(!empty($columnvalue)){ $insurance =  json_decode($columnvalue->permission_insurance); }

        
if((!empty($insurance) && $insurance->view == "true") || $this->session->userdata('user_role') == "admin"){
        $title['title']="All Quick Inspections";
        $data['quickinspection'] = $this->QuickInspectionModel->view();
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        $this->load->view('admin/quickinspection/allquickinspection',$data);
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }
        }else{
             redirect('Logout');
        }
    }

    function loaddataview(){
   
      $id = $this->input->post("id");
      $userID = $this->input->post("userID");
      $vehicleid = $this->input->post("vehicleid");
      $data = $this->QuickInspectionModel->loaddata1($id,$userID);
      $data2 = $this->QuickInspectionModel->loaddata2($id,$vehicleid);  
      $this->load->view('admin/quickinspection/view',['data'=>$data,'data2'=>$data2]);
      
    }

  function searchvalue(){

     $filterstatus  = $this->input->post('filterstatus'); 
     if($filterstatus === 'all'){
       return redirect('admin/QuickInspection');
     }else{
     $data['quickinspection']= $this->QuickInspectionModel->getstatusWhereLike($filterstatus); 
            if($data){ 
              $title['title']="All Quick Inspection";
              $this->load->view('admin/layouts/header',$title);
              $this->load->view('admin/layouts/navbar');
              $this->load->view('admin/quickinspection/allquickinspection',$data);
              $this->load->view('admin/layouts/footer');
            }else{
               echo "filter data cant be show on view";
            }
     }
    }
   
     function exportcsv(){
     // file name 
    $filename = 'quickinspection_'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
     // get data 
    $usersData = $this->QuickInspectionModel->getUserDetails();
    // file creation 
    $file = fopen('php://output','w');
    $header = array("Id","User Id","Inspection Type","Vehicle Id","Status","Remark","Created Date"); 
    fputcsv($file, $header);
    foreach ($usersData as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
    }
   
    
    }

