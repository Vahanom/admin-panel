<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Profile extends CI_Controller {
    
    function __construct()
    {
		    parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/ProfileModel');
            $this->load->helper('form'); 
    }

    public function index()
    {
     if($this->session->userdata('usersid') != null){
       $title['title']="My Profile";
       $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
       $seller_id = $this->uri->segment(4);	
       $data['myprofile'] = $this->ProfileModel->myprofile($this->session->userdata('usersid'));
      //  var_dump($data);die;
       $this->load->view('admin/profile/update',$data);
        $this->load->view('admin/layouts/footer');
       }else{
            redirect('Logout');
       }
    }

    function update(){
      $sa_name = $this->input->post('u_name');
      $sa_id = $this->session->userdata('usersid');
      $sa_email = $this->input->post('u_email');
      $sa_phno = $this->input->post('u_phno');
      $sa_address = $this->input->post('u_address');

        $result = $this->ProfileModel->update($sa_id,$sa_name,$sa_email,$sa_phno,$sa_address);
      if ($result == 'true') {
          echo "<script>alert('Profile Update Successfully');
            window.location.href = 'index';
            </script>";
          }else{
        redirect("admin/Profile");
        }
    }

}
