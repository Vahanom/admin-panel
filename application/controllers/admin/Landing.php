<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Landing extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/LandingModel');
            $this->load->helper('form'); 
	  }

     public function index()
     
     {
        //  print_r("test");exit;
      if($this->session->userdata('usersid') != null){

    
        foreach($this->session->userdata('permission') as $columnvalue){
        }
       
if(!empty($columnvalue)){ $landing =  json_decode($columnvalue->permission_employee); }


if((!empty($landing) && $landing->view == "true") || $this->session->userdata('user_role') == "admin"){
     
          $title['title']="All Landing Page";
          $this->load->view('admin/layouts/header',$title);
          $this->load->view('admin/layouts/navbar');
          $data['landing'] = $this->LandingModel->view();
          $this->load->view('admin/landing/alllanding',$data);
          $this->load->view('admin/layouts/footer');
        }else{
          redirect("admin/Access");
        }
    
        }else{
             redirect('Logout');
        }
    }

    public function create()
    {
      if($this->session->userdata('usersid') != null){

          foreach($this->session->userdata('permission') as $columnvalue){
           }
       
            if(!empty($columnvalue)){ $landing =  json_decode($columnvalue->permission_landing); }
  
  
             if((!empty($landing) && $landing->add == "true") || $this->session->userdata('user_role') == "admin"){
                 $title['title']="Add Landing Data";
                 $this->load->view('admin/layouts/header',$title);
                 $this->load->view('admin/layouts/navbar');
                 $this->load->view('admin/landing/create');
                 $this->load->view('admin/layouts/footer');
               }else{
                  redirect("admin/Access");
               }

       }else{
             redirect('Logout');
        }
     }    


    function add(){
      
      foreach($this->session->userdata('permission') as $columnvalue){
      }
     
          if(!empty($columnvalue)){ $landing =  json_decode($columnvalue->permission_landing); }


          if((!empty($landing) && $landing->add == "true") || $this->session->userdata('user_role') == "admin"){
            $abt_description = $this->input->post('abt_description');
            $status = $this->input->post('status');

             $result = $this->LandingModel->add($abt_description,$status);
            //  die;
             if ($result == 'true') {
                    echo "<script>alert('Landing Page Data Added Successfully');
                           window.location.href = 'index';
                         </script>";
            }else{
                    redirect("admin/Landing/create");
            }
        }else{
            redirect("admin/Access");
      }
    }

    function loaddata(){
      foreach($this->session->userdata('permission') as $columnvalue){
      }
     
       if(!empty($columnvalue)){ $aboutus =  json_decode($columnvalue->permission_aboutus); }
  
  
        if((!empty($aboutus) && $aboutus->view == "true") || $this->session->userdata('user_role') == "admin"){
           $id = $this->input->post("id");
      
           $data['landing'] = $this->LandingModel->loaddata($id);
     
          $this->load->view('admin/landing/update',$data);
         }else{
             redirect("admin/Access");
         }
      
    }

    function loaddataview(){
      foreach($this->session->userdata('permission') as $columnvalue){
      }
     
         if(!empty($columnvalue)){ $aboutus =  json_decode($columnvalue->permission_aboutus); }
  
  
            if((!empty($aboutus) && $aboutus->view == "true") || $this->session->userdata('user_role') == "admin"){
             $id = $this->input->post("id");
      
             $data['aboutus'] = $this->LandingModel->loaddata1($id);
     
             $this->load->view('admin/landing/view',$data);
           }else{
              redirect("admin/Access");
        }
      
    }

     function update(){
     
    foreach($this->session->userdata('permission') as $columnvalue){
    }
   
      if(!empty($columnvalue)){ $landing =  json_decode($columnvalue->permission_aboutus); }


        if((!empty($landing) && $landing->update == "true") || $this->session->userdata('user_role') == "admin"){
            $abt_id = $this->input->post('abt_id');
            $abt_description = $this->input->post('abt_description');
            $status = $this->input->post('status');
           $result = $this->LandingModel->update($abt_id,$abt_description,$status);

        if ($result == 1) {
          echo "<script>alert('Lading Page Data Updated Successfully');
            window.location.href = 'index';
            </script>";
          }else{
        redirect("landing/index");
        }

      }else{
        redirect("admin/Access");
    }
    }

    function exportcsv(){
     // file name 
    $filename = 'aboutus'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
     // get data 
    $usersData = $this->LandingModel->getUserDetails();
    // file creation 
    $file = fopen('php://output','w');
    $header = array("abt_id","abt_description","abt_create_date","abt_modify_date"); 
    fputcsv($file, $header);
    foreach ($usersData as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
    }
   
    
    }

    