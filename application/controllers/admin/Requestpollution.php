<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Requestpollution extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/RequestPollutionModel');
            $this->load->helper('form'); 
	  }

     public function index()
     {
      if($this->session->userdata('usersid') != null){
        foreach($this->session->userdata('permission') as $columnvalue){
        }
        if(!empty($columnvalue)){ $pollution =  json_decode($columnvalue->permission_pollution); }
  
if((!empty($pollution) && $pollution->view == "true") || $this->session->userdata('user_role') == "admin"){


        $title['title']="All Request Pollution";
        $data= $this->RequestPollutionModel->view();
         $data1= $this->RequestPollutionModel->view1();
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
         //$this->load->view('admin/requestpollution/allrequestpollution');
        $this->load->view('admin/requestpollution/allrequestpollution',['data'=>$data,'data1'=>$data1]);
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }
        }else{
             redirect('Logout');
        }
    }

    function searchvalue(){

     $filterstatus  = $this->input->post('filterstatus'); 
     if($filterstatus === 'all'){
       return redirect('admin/Requestpollution');
     }else{
     $data= $this->RequestPollutionModel->getstatusWhereLike($filterstatus); 
            if($data){ 
              $title['title']="All Pollution";
              $this->load->view('admin/layouts/header',$title);
              $this->load->view('admin/layouts/navbar');
              $this->load->view('admin/requestpollution/allrequestpollution',['data'=>$data]);
              $this->load->view('admin/layouts/footer');
            }else{
               echo "filter data cant be show on view";
            }
     }
    }

    public function update(){
      foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $pollution =  json_decode($columnvalue->permission_pollution); }

    if((!empty($pollution) && $pollution->update == "true") || $this->session->userdata('user_role') == "admin"){

      $user_pollutionid = $this->input->post('user_pollutionid');
      $remark = $this->input->post('remark');
      $result = $this->RequestPollutionModel->update($user_pollutionid,$remark);

        if ($result == 1) {
          echo "<script>alert('Pollution Updated Successfully');
            window.location.href = 'index';
            </script>";
          }else{
        redirect("admin/Pollution");
        }
      }else{
        redirect("admin/Access");
      }
    }


 function loaddata(){
      $id = $this->input->post("id");
      $pol_id = $this->input->post("pol_id");
      $data = $this->RequestPollutionModel->loaddata1($id,$pol_id);  
      $this->load->view('admin/requestpollution/update',['data'=>$data]);
      
    }

    function loaddataview(){
   
      $id = $this->input->post("id");
      $pol_id = $this->input->post("pol_id");
      $m_id = $this->input->post("m_id");
      $data = $this->RequestPollutionModel->loaddata1($id,$pol_id);
      $data2 = $this->RequestPollutionModel->loaddata2($id,$m_id);   
      $this->load->view('admin/requestpollution/view',['data'=>$data,'data2'=>$data2]);
      
    }

   function loaddata4(){ 
    //   print_r($this->input->post());exit;
     $value = $this->input->post("value"); 
      $user_pollutionid = $this->input->post("user_pollutionid"); 
     $data =$this->RequestPollutionModel->loaddata4($value,$user_pollutionid); 
     if($data== 1){
          echo "data";
     }else{
      echo "nto";
     }

      
    }

    function exportcsv(){
     // file name 
    $filename = 'requestpollution_'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
     // get data 
    $usersData = $this->RequestPollutionModel->getUserDetails();
    // file creation 
    $file = fopen('php://output','w');
    $header = array("Pollution Title","User Name","User Contact","Email id","Remark","Status"); 
    fputcsv($file, $header);
    foreach ($usersData as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
    }

   
    
    }

    