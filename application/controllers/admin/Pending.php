<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  
  class Pending extends CI_Controller {
    
    function __construct()
    {
            parent::__construct();        
            $this->load->library('session');
            $this->load->model('admin/PendingModel');
            $this->load->helper('form'); 
    }

     public function index()
     {
      if($this->session->userdata('usersid') != null){

    
        foreach($this->session->userdata('permission') as $columnvalue){
        }
       
        if(!empty($columnvalue)){ $emplyee =  json_decode($columnvalue->permission_employee); }


         if((!empty($emplyee) && $emplyee->view == "true") || $this->session->userdata('user_role') == "admin"){
     
          $title['title']="All Completed";
          $this->load->view('admin/layouts/header',$title);
          $this->load->view('admin/layouts/navbar');
          $data['pending'] = $this->PendingModel->view();
          $this->load->view('admin/Pending/allpending',$data);
          $this->load->view('admin/layouts/footer');
        }else{
          redirect("admin/Access");
        }
    
        }else{
             redirect('Logout');
        }
    }

    function searchvalue(){

     $filterstatus  = $this->input->post('filterstatus'); 
     if($filterstatus === 'all'){
       $title['title']="All Pending";
       $data['pending']= $this->PendingModel->getstatusWhereLikeall($filterstatus); 
              $this->load->view('admin/layouts/header',$title);
              $this->load->view('admin/layouts/navbar');
              $this->load->view('admin/Pending/allpending',$data);
              $this->load->view('admin/layouts/footer');
     }else{
     $data['pending']= $this->PendingModel->getstatusWhereLike($filterstatus); 
            if($data){ 
              $title['title']="All Pending";
              $this->load->view('admin/layouts/header',$title);
              $this->load->view('admin/layouts/navbar');
              $this->load->view('admin/Pending/allpending',$data);
              $this->load->view('admin/layouts/footer');
            }else{
               echo "filter data cant be show on view";
            }
     }
    }


    function loaddataview(){
      foreach($this->session->userdata('permission') as $columnvalue){
      }
     
         if(!empty($columnvalue)){ $emplyee =  json_decode($columnvalue->permission_employee); }
  
  
            if((!empty($emplyee) && $emplyee->view == "true") || $this->session->userdata('user_role') == "admin"){
             $id = $this->input->post("id");
             $type_servicespare = $this->input->post("type_servicespare");
             $order_details_id = $this->input->post("order_details_id");
             $completed= $this->PendingModel->loaddata1($id);
             $data1 = $this->PendingModel->loaddata4($order_details_id);
             $data2 = $this->PendingModel->loaddata5($id,$type_servicespare);
             $this->load->view('admin/Pending/view',['completed'=>$completed,'data1'=>$data1,'data2'=>$data2]);
           }else{
              redirect("admin/Access");
        }
      
    }
    //working code..
    function geteditModel() {
        $id = $this->input->post("id");
        $data['completed'] = $this->PendingModel->loaddata2($id);
        $this->load->view('admin/Pending/edit123',$data);

    }
    
    function Updatemodel(){
      $radioid = $this->input->post("roleSubjectRadio");
      if($radioid === "parking"){
      $id=$this->input->post("order_id");
      $result=$this->PendingModel->updatestatus($radioid,$id);
      if($result){
         echo "<script>alert('Order Status Updated Successfully');
            window.location.href = 'index';
            </script>";
      }
      else{
        echo "data not added";
      }
    }elseif($radioid === "manually"){
         // to get the user id from user table
         $techicianid= $this->input->post("techician");
         $id=$this->input->post("order_id");
         $result=$this->PendingModel->updateTechnicalid($radioid,$id,$techicianid);
         if($result){
            echo "<script>alert('technical id Updated Successfully');
            window.location.href = 'index';
            </script>";
          }
      else{
        echo "data not added";
      }
      }
    }

    function search()
    {
        $q = $this->input->get('q');
        $this->load->model('PendingModel');
        echo json_encode($this->PendingModel->getdropdown($q));
    }

 }

    