<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Pollution extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/PollutionModel');
            $this->load->helper('form'); 
	  }

     public function index()
     {
      if($this->session->userdata('usersid') != null){
        foreach($this->session->userdata('permission') as $columnvalue){
        }
        if(!empty($columnvalue)){ $pollution =  json_decode($columnvalue->permission_pollution); }
  
if((!empty($pollution) && $pollution->view == "true") || $this->session->userdata('user_role') == "admin"){


        $title['title']="All Pollution";
        $data['pollution'] = $this->PollutionModel->view();
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        $this->load->view('admin/pollution/allpollution',$data);
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }
        }else{
             redirect('Logout');
        }
    }

    function searchvalue(){

     $filterstatus  = $this->input->post('filterstatus'); 
     if($filterstatus === 'all'){
       return redirect('admin/Pollution');
     }else{
     $data['pollution']= $this->PollutionModel->getstatusWhereLike($filterstatus); 
            if($data){ 
              $title['title']="All Pollution";
              $this->load->view('admin/layouts/header',$title);
              $this->load->view('admin/layouts/navbar');
              $this->load->view('admin/pollution/allpollution',$data);
              $this->load->view('admin/layouts/footer');
            }else{
               echo "filter data cant be show on view";
            }
     }
    }

    function add(){
      foreach($this->session->userdata('permission') as $columnvalue){
      }
      if(!empty($columnvalue)){ $pollution =  json_decode($columnvalue->permission_pollution); }

if((!empty($pollution) && $pollution->add == "true") || $this->session->userdata('user_role') == "admin"){

        $service_title = $this->input->post('service_title');
        $service_desc = $this->input->post('service_desc');
       
        $status = $this->input->post('status');
        $price = $this->input->post('price');
        $selling_price = $this->input->post('selling_price');
        
        if(!empty($_FILES['imageone']['name'])){
         
          $config['upload_path'] = './assets/images/pollution';
          $config['allowed_types'] = 'jpg|jpeg|png|gif';
          $config['file_name'] = $_FILES['imageone']['name'];
          
          //Load upload library and initialize configuration
          $this->load->library('upload',$config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('imageone')){
              $uploadData = $this->upload->data();
              $imageone = $uploadData['file_name'];
          }else{
           
              $imageone = '';
            
          }
      }else{
          $imageone = '';
      }

      if(!empty($_FILES['imagetwo']['name'])){
        $config['upload_path'] = './assets/images/pollution';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['file_name'] = $_FILES['imagetwo']['name'];
        
        //Load upload library and initialize configuration
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('imagetwo')){
            $uploadData = $this->upload->data();
            $imagetwo = $uploadData['file_name'];
        }else{
            $imagetwo = '';
        }
      }else{
        $imagetwo = '';
      }

        
     
       
 $result = $this->PollutionModel->add($service_title,$service_desc,$status,$imageone,$imagetwo,$price,$selling_price);
//  die;
        if ($result == 'true') {
          echo "<script>alert('Pollution Added Successfully');
           window.location.href='../Pollution';
            </script>";
          }else{
            redirect("admin/Pollution");
        }
      }else{
        redirect("admin/Access");
      }
    }

 

    public function create()
    {
      if($this->session->userdata('usersid') != null){
        foreach($this->session->userdata('permission') as $columnvalue){
        }
        if(!empty($columnvalue)){ $pollution =  json_decode($columnvalue->permission_pollution); }
  
if((!empty($pollution) && $pollution->add == "true") || $this->session->userdata('user_role') == "admin"){

        $title['title']="Add Pollution";
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        $this->load->view('admin/pollution/create');
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      } }else{
             redirect('Logout');
        }
   }

   function update(){
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $pollution =  json_decode($columnvalue->permission_pollution); }

if((!empty($pollution) && $pollution->update == "true") || $this->session->userdata('user_role') == "admin"){

    $pol_id = $this->input->post('pol_id');
    $service_title = $this->input->post('service_title');
    $service_desc = $this->input->post('service_desc');
   
    $status = $this->input->post('status');
     $price = $this->input->post('price');
        $selling_price = $this->input->post('selling_price');

    if(!empty($_FILES['imageone']['name'])){
      $config['upload_path'] = './assets/images/pollution';
      $config['allowed_types'] = 'jpg|jpeg|png|gif';
      $config['file_name'] = $_FILES['imageone']['name'];
      
      //Load upload library and initialize configuration
      $this->load->library('upload',$config);
      $this->upload->initialize($config);
      if($this->upload->do_upload('imageone')){
          $uploadData = $this->upload->data();
          $imageone = $uploadData['file_name'];
      }else{
          $imageone = '';
      }
  }else{
    $imageone = $this->input->post('hiddenimageone');
  }

  if(!empty($_FILES['imagetwo']['name'])){
    $config['upload_path'] = './assets/images/pollution';
    $config['allowed_types'] = 'jpg|jpeg|png|gif';
    $config['file_name'] = $_FILES['imagetwo']['name'];
    
    //Load upload library and initialize configuration
    $this->load->library('upload',$config);
    $this->upload->initialize($config);
    if($this->upload->do_upload('imagetwo')){
        $uploadData = $this->upload->data();
        $imagetwo = $uploadData['file_name'];
    }else{
        $imagetwo = '';
    }
  }else{
    $imagetwo = $this->input->post('hiddenimagetwo');
  }
$result = $this->PollutionModel->update($service_title,$service_desc,$status,$imageone,$imagetwo,$pol_id,$price,$selling_price);

        if ($result == 1) {
          echo "<script>alert('Pollution Updated Successfully');
            window.location.href='../Pollution';
            </script>";
          }else{
        redirect("admin/Pollution");
        }
      }else{
        redirect("admin/Access");
      }
    }



    function loaddata(){
   
      $id = $this->input->post("id");
      $data['pollution'] = $this->PollutionModel->loaddata($id);
      $this->load->view('admin/pollution/update',$data);
      
    }

    function loaddataview(){
   
      $id = $this->input->post("id");
      $data['pollution'] = $this->PollutionModel->loaddata1($id);
     
      $this->load->view('admin/pollution/view',$data);
      
    }
     

    function exportcsv(){
     // file name 
    $filename = 'pollution_'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
     // get data 
    $usersData = $this->PollutionModel->getUserDetails();
    // file creation 
    $file = fopen('php://output','w');
    $header = array("pol_id", "pol_title", "pol_description", "pol_status", "price", "selling_price"); 
    fputcsv($file, $header);
    foreach ($usersData as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
    }

   
    
    }

    