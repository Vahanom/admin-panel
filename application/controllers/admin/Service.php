<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Service extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/ServiceModel');
            $this->load->model('admin/SpareModel');
            $this->load->helper('form'); 
	  }

     public function index()
     {
      if($this->session->userdata('usersid') != null){
        foreach($this->session->userdata('permission') as $columnvalue){
        }
        if(!empty($columnvalue)){ $service =  json_decode($columnvalue->permission_service); }
  
if((!empty($service) && $service->view == "true") || $this->session->userdata('user_role') == "admin"){



        $title['title']="All Service";
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        
        $data['service'] = $this->ServiceModel->view();
        $this->load->view('admin/service/allservice',$data);
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }  
      }else{
             redirect('Logout');
        }
    }

    function searchvalue(){

     $filterstatus  = $this->input->post('filterstatus'); 
     if($filterstatus === 'all'){
       return redirect('admin/Service');
     }else{
     $data['service']= $this->ServiceModel->getstatusWhereLike($filterstatus); 
            if($data){ 
              $title['title']="All service";
              $this->load->view('admin/layouts/header',$title);
              $this->load->view('admin/layouts/navbar');
              $this->load->view('admin/service/allservice',$data);
              $this->load->view('admin/layouts/footer');
            }else{
               echo "filter data cant be show on view";
            }
     }
    }

    function add(){
      foreach($this->session->userdata('permission') as $columnvalue){
      }
      if(!empty($columnvalue)){ $service =  json_decode($columnvalue->permission_service); }

if((!empty($service) && $service->add == "true") || $this->session->userdata('user_role') == "admin"){
        // print_r($this->input->post());exit;
        $service_title = $this->input->post('service_title');
        $service_desc = $this->input->post('service_desc');
        $type_vehicle = $this->input->post('type_vehicle');
        $servicecategory = $this->input->post('servicecategory');
        $status = $this->input->post('status');
         $service_point = $this->input->post('service_point');
        $service_extra = $this->input->post('service_extra');
        $labour_rate = $this->input->post('labour_rate');
        $servicetype = $this->input->post('servicetype');

        $service_amount = $this->input->post('service_amount');
        $service_amount1 = $this->input->post('service_amount1');
         $service_amount2 = $this->input->post('service_amount2');
        $service_amount3 = $this->input->post('service_amount3');
        $service_time = $this->input->post('service_time');
         $price = $this->input->post('price');
          $selling_price = $this->input->post('selling_price');
        if(!empty($_FILES['imageone']['name'])){
          $config['upload_path'] = './assets/images/service';
          $config['allowed_types'] = 'jpg|jpeg|png|gif';
          $config['file_name'] = $_FILES['imageone']['name'];
          
          //Load upload library and initialize configuration
          $this->load->library('upload',$config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('imageone')){
              $uploadData = $this->upload->data();
              $imageone = $uploadData['file_name'];
          }else{
              $imageone = '';
          }
      }else{
          $imageone = '';
      }

      if(!empty($_FILES['imagetwo']['name'])){
        $config['upload_path'] = './assets/images/service';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['file_name'] = $_FILES['imagetwo']['name'];
        
        //Load upload library and initialize configuration
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('imagetwo')){
            $uploadData = $this->upload->data();
            $imagetwo = $uploadData['file_name'];
        }else{
            $imagetwo = '';
        }
      }else{
        $imagetwo = '';
      }

        
     
        
 $result = $this->ServiceModel->add($service_title,$service_desc,$type_vehicle,$servicecategory,$status,$imageone,$imagetwo,$service_point,$service_extra,$servicetype,$service_time,$service_amount3,$service_amount2,$service_amount1,$labour_rate,$service_amount,$price,$selling_price);
//  die;
        if ($result == 'true') {
          echo "<script>alert('Service Added Successfully');
           window.location.href='../Service';
            </script>";
          }else{
            redirect("admin/Servicecat");
        }
      }else{
        redirect("admin/Access");
      }  
    }

 

    public function create()
    {
      if($this->session->userdata('usersid') != null){
        foreach($this->session->userdata('permission') as $columnvalue){
        }
        if(!empty($columnvalue)){ $service =  json_decode($columnvalue->permission_service); }
  
  if((!empty($service) && $service->add == "true") || $this->session->userdata('user_role') == "admin"){
        $title['title']="Add Service";
       // $data['sparecat'] = $this->SpareModel->sparecat();
        $data['sparecat'] = $this->ServiceModel->servicecat();
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        $this->load->view('admin/service/create',$data);
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }  
        }else{
             redirect('Logout');
        }
   }

   function update(){
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $service =  json_decode($columnvalue->permission_service); }

if((!empty($service) && $service->update == "true") || $this->session->userdata('user_role') == "admin"){
    $ser_id = $this->input->post('ser_id');
    $service_title = $this->input->post('service_title');
    $service_desc = $this->input->post('service_desc');
    $type_vehicle = $this->input->post('type_vehicle');
    //$servicecategory = $this->input->post('servicecategory');
    $servicecategory = $this->input->post('sparecategory');
    $status = $this->input->post('status');
     $service_point = $this->input->post('service_point');
        $labour_rate = $this->input->post('labour_rate');
    $service_extra = $this->input->post('service_extra');
    $servicetype = $this->input->post('servicetype');
    $service_amount = $this->input->post('service_amount');
    $service_amount1 = $this->input->post('service_amount1');
     $service_amount2 = $this->input->post('service_amount2');
    $service_amount3 = $this->input->post('service_amount3');
    $service_time = $this->input->post('service_time');
     $price = $this->input->post('price');
          $selling_price = $this->input->post('selling_price');

    if(!empty($_FILES['imageone']['name'])){
      $config['upload_path'] = './assets/images/service';
      $config['allowed_types'] = 'jpg|jpeg|png|gif';
      $config['file_name'] = $_FILES['imageone']['name'];
      
      //Load upload library and initialize configuration
      $this->load->library('upload',$config);
      $this->upload->initialize($config);
      if($this->upload->do_upload('imageone')){
          $uploadData = $this->upload->data();
          $imageone = $uploadData['file_name'];
      }else{
          $imageone = '';
      }
  }else{
    $imageone = $this->input->post('hiddenimageone');
  }

  if(!empty($_FILES['imagetwo']['name'])){
    $config['upload_path'] = './assets/images/service';
    $config['allowed_types'] = 'jpg|jpeg|png|gif';
    $config['file_name'] = $_FILES['imagetwo']['name'];
    
    //Load upload library and initialize configuration
    $this->load->library('upload',$config);
    $this->upload->initialize($config);
    if($this->upload->do_upload('imagetwo')){
        $uploadData = $this->upload->data();
        $imagetwo = $uploadData['file_name'];
    }else{
        $imagetwo = '';
    }
  }else{
    $imagetwo = $this->input->post('hiddenimagetwo');
  }
$result = $this->ServiceModel->update($service_title,$service_desc,$type_vehicle,$servicecategory,$status,$imageone,$imagetwo,$service_point,$service_extra,$servicetype,$service_time,$service_amount3,$service_amount2,$service_amount1,$labour_rate,$service_amount,$ser_id,$price,$selling_price);

        if ($result == 1) {
          echo "<script>alert('Service Updated Successfully');
            window.location.href='../Service';
            </script>";
          }else{
        redirect("admin/Service");
        } }else{
          redirect("admin/Access");
        }  
    }



    function loaddata(){
   
      $id = $this->input->post("id");
     // $data['sparecat'] = $this->SpareModel->sparecat();
      $data['sparecat'] = $this->ServiceModel->servicecat();
      $data['service'] = $this->ServiceModel->loaddata($id);
      $this->load->view('admin/service/update',$data);
      
    }

    function loaddataview(){
   
      $id = $this->input->post("id");
     // $data['sparecat'] = $this->SpareModel->sparecat();
      $data['sparecat'] = $this->ServiceModel->servicecat();
      $data['servicecat'] = $this->ServiceModel->loaddataservice($id);
      $data['service'] = $this->ServiceModel->loaddata1($id);
     
      $this->load->view('admin/service/view',$data);
      
    }


    function exportcsv(){
     // file name 
    $filename = 'service'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
     // get data 
    $usersData = $this->ServiceModel->getUserDetails();
    // file creation 
    $file = fopen('php://output','w');
    $header = array("ser_id", "ser__title", "ser__description", "service_vehicle_type", "ser_type", "ser_points", "ser_extra_field", "ser_status", "ser_createddate", "ser_modifieddate", "ser_category", "ser_amt", "ser_amt_level_one", "ser_amt_level_two", "ser_amt_level_three", "ser_hours", "price", "sellingprice"); 
    fputcsv($file, $header);
    foreach ($usersData as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
    }
   
    //Duplicate title check on blur 
    function namecheckexist()
    {
      $username = $_POST['key'];
      $exists = $this->ServiceModel->namecheckexist1($username);
      $count = count($exists);
      
      if (empty($count)) {
        echo 1;
      } else {
              echo 2;
      }
    }
    
    }

    