<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  
  class Logout extends CI_Controller {
    
        function __Construct(){
          parent::__Construct ();
          $this->load->database();
        }
    
        public function index()
        {
		
          $this->load->library('session');
		  
		  if($this->session->userdata('usersid') != null){
            $this->session->sess_destroy();
            redirect("admin/Login");
          }
        }
  }
?>
