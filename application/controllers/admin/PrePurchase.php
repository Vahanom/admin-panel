<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class PrePurchase extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/PrePurchaseModel');
            $this->load->model('admin/InsuranceModel');
            $this->load->helper('form'); 
	  }

     public function index()
     {
   
      if($this->session->userdata('usersid') != null){
        foreach($this->session->userdata('permission') as $columnvalue){
        }
        if(!empty($columnvalue)){ $insurance =  json_decode($columnvalue->permission_insurance); }

        
if((!empty($insurance) && $insurance->view == "true") || $this->session->userdata('user_role') == "admin"){
        $title['title']="All Pre-Purchase";
        $data['prepurchase'] = $this->PrePurchaseModel->view();
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        $this->load->view('admin/prepurchase/allprepurchase',$data);
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }
        }else{
             redirect('Logout');
        }
    }

    function loaddataview(){
   
      $id = $this->input->post("id");
      $muser_id = $this->input->post("muser_id");
      $vehb_id = $this->input->post("vehb_id");
      $data = $this->PrePurchaseModel->loaddata1($id,$muser_id);
      $data2 = $this->PrePurchaseModel->loaddata2($id,$vehb_id);  
      $this->load->view('admin/prepurchase/view',['data'=>$data,'data2'=>$data2]);
      
    }

function searchvalue(){

     $filterstatus  = $this->input->post('filterstatus'); 
     if($filterstatus === 'all'){
       return redirect('admin/PrePurchase');
     }else{
     $data['prepurchase']= $this->PrePurchaseModel->getstatusWhereLike($filterstatus); 
            if($data){ 
              $title['title']="All Pre Purchase";
              $this->load->view('admin/layouts/header',$title);
              $this->load->view('admin/layouts/navbar');
              $this->load->view('admin/prepurchase/allprepurchase',$data);
              $this->load->view('admin/layouts/footer');
            }else{
               echo "filter data cant be show on view";
            }
     }
    }
  

   function exportcsv(){
     // file name 
    $filename = 'prepurchase_'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
     // get data 
    $usersData = $this->PrePurchaseModel->getUserDetails();
    // file creation 
    $file = fopen('php://output','w');
    $header = array("Id","User Name","Inspection Type","Vehicle Name","Status","Remark","Created Date"); 
    fputcsv($file, $header);
    foreach ($usersData as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
    } 
    
    }

    