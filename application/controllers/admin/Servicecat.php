<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Servicecat extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/ServicecatModel');
            $this->load->helper('form'); 
	  }

     public function index()
     {
      if($this->session->userdata('usersid') != null){
        
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $servicecat =  json_decode($columnvalue->permission_servicecat); }

if((!empty($servicecat) && $servicecat->view == "true") || $this->session->userdata('user_role') == "admin"){
        $title['title']="All Service Category";
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        
        $data['servicecat'] = $this->ServicecatModel->view();
        $this->load->view('admin/servicecat/allservicecat',$data);
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }    }else{
             redirect('Logout');
        }
    }
function searchvalue(){

     $filterstatus  = $this->input->post('filterstatus'); 
     if($filterstatus === 'all'){
       return redirect('admin/Servicecat');
     }else{
     $data['servicecat']= $this->ServicecatModel->getstatusWhereLike($filterstatus); 
            if($data){ 
              $title['title']="All Service Category";
              $this->load->view('admin/layouts/header',$title);
              $this->load->view('admin/layouts/navbar');
              $this->load->view('admin/servicecat/allservicecat',$data);
              $this->load->view('admin/layouts/footer');
            }else{
               echo "filter data cant be show on view";
            }
     }
    }
    function add(){
          
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $servicecat =  json_decode($columnvalue->permission_servicecat); }

if((!empty($servicecat) && $servicecat->add == "true") || $this->session->userdata('user_role') == "admin"){
        $service_title = $this->input->post('service_title');
        $service_desc = $this->input->post('service_desc');
        $type_vehicle = $this->input->post('type_vehicle');
        $servicecategory = $this->input->post('servicecategory');
        $status = $this->input->post('status');

        if(!empty($_FILES['imageone']['name'])){
          $config['upload_path'] = './assets/images/service';
          $config['allowed_types'] = 'jpg|jpeg|png|gif';
          $config['file_name'] = $_FILES['imageone']['name'];
          
          //Load upload library and initialize configuration
          $this->load->library('upload',$config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('imageone')){
              $uploadData = $this->upload->data();
              $imageone = $uploadData['file_name'];
          }else{
              $imageone = '';
          }
      }else{
          $imageone = '';
      }

      if(!empty($_FILES['imagetwo']['name'])){
        $config['upload_path'] = './assets/images/service';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['file_name'] = $_FILES['imagetwo']['name'];
        
        //Load upload library and initialize configuration
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('imagetwo')){
            $uploadData = $this->upload->data();
            $imagetwo = $uploadData['file_name'];
        }else{
            $imagetwo = '';
        }
      }else{
        $imagetwo = '';
      }

        
     
        
 $result = $this->ServicecatModel->add($service_title,$service_desc,$type_vehicle,$servicecategory,$status,$imageone,$imagetwo);
//  die;
        if ($result == 'true') {
          echo "<script>alert('Service Category Added Successfully');
            window.location.href='../Servicecat';
            </script>";
          }else{
            redirect("admin/Servicecat");
        }
      }else{
        redirect("admin/Access");
      }  
    }

 

    public function create()
    {
      if($this->session->userdata('usersid') != null){
        foreach($this->session->userdata('permission') as $columnvalue){
        }
        if(!empty($columnvalue)){ $servicecat =  json_decode($columnvalue->permission_servicecat); }
    
    if((!empty($servicecat) && $servicecat->add == "true") || $this->session->userdata('user_role') == "admin"){
        $title['title']="Add Service Category";
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        $this->load->view('admin/servicecat/create');
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }   
      }else{
             redirect('Logout');
        }
   }

   function update(){
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $servicecat =  json_decode($columnvalue->permission_servicecat); }

if((!empty($servicecat) && $servicecat->update == "true") || $this->session->userdata('user_role') == "admin"){
    $ser_catid = $this->input->post('ser_catid');
    $service_title = $this->input->post('service_title');
    $service_desc = $this->input->post('service_desc');
    $type_vehicle = $this->input->post('type_vehicle');
    $servicecategory = $this->input->post('servicecategory');
    $status = $this->input->post('status');

    if(!empty($_FILES['imageone']['name'])){
      $config['upload_path'] = './assets/images/service';
      $config['allowed_types'] = 'jpg|jpeg|png|gif';
      $config['file_name'] = $_FILES['imageone']['name'];
      
      //Load upload library and initialize configuration
      $this->load->library('upload',$config);
      $this->upload->initialize($config);
      if($this->upload->do_upload('imageone')){
          $uploadData = $this->upload->data();
          $imageone = $uploadData['file_name'];
      }else{
          $imageone = '';
      }
  }else{
    $imageone = $this->input->post('hiddenimageone');
  }

  if(!empty($_FILES['imagetwo']['name'])){
    $config['upload_path'] = './assets/images/service';
    $config['allowed_types'] = 'jpg|jpeg|png|gif';
    $config['file_name'] = $_FILES['imagetwo']['name'];
    
    //Load upload library and initialize configuration
    $this->load->library('upload',$config);
    $this->upload->initialize($config);
    if($this->upload->do_upload('imagetwo')){
        $uploadData = $this->upload->data();
        $imagetwo = $uploadData['file_name'];
    }else{
        $imagetwo = '';
    }
  }else{
    $imagetwo = $this->input->post('hiddenimagetwo');
  }

    
        
    
   
$result = $this->ServicecatModel->update($service_title,$service_desc,$type_vehicle,$servicecategory,$status,$imageone,$imagetwo,$ser_catid);

        if ($result == 1) {
          echo "<script>alert('Service Category Updated Successfully');
            window.location.href='../Servicecat';
            </script>";
          }else{
        redirect("admin/Servicecat");
        }
      }else{
        redirect("admin/Access");
      }  
    }



    function loaddata(){
   
      $id = $this->input->post("id");
      
      $data['servicecat'] = $this->ServicecatModel->loaddata($id);
     
      $this->load->view('admin/servicecat/update',$data);
      
    }

    function loaddataview(){
   
      $id = $this->input->post("id");
      
      $data['servicecat'] = $this->ServicecatModel->loaddata1($id);
     
      $this->load->view('admin/servicecat/view',$data);
      
    }

    function exportcsv(){
     // file name 
    $filename = 'service_category_'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
     // get data 
    $usersData = $this->ServicecatModel->getUserDetails();
    // file creation 
    $file = fopen('php://output','w');
    $header = array("ser_catid", "ser_cat_title", "ser_cat_description", "vehicle_type", "ser_category_type", "ser_category_status", "ser_category_createdby", "ser_category_createddate", "ser_category_modifiedby", "ser_category_modifieddate"); 
    fputcsv($file, $header);
    foreach ($usersData as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
    }

    //Duplicate title check on blur 
    function namecheckexist()
    {
      $username = $_POST['key'];
      $exists = $this->ServicecatModel->namecheckexist1($username);
      $count = count($exists);
      
      if (empty($count)) {
        echo 1;
      } else {
              echo 2;
      }
    }
   
    
    }

    