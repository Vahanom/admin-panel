<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Jobs extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/JobsModel');
            $this->load->helper('form'); 
	  }

     public function index() 
     {
   
      if($this->session->userdata('usersid') != null){
        foreach($this->session->userdata('permission') as $columnvalue){
        }
        if(!empty($columnvalue)){ $Jobs =  json_decode($columnvalue->permission_Jobs); }

        
if((!empty($jobs) && $jobs->view == "true") || $this->session->userdata('user_role') == "admin"){
        $title['title']="All Job Requests";
        $data['jobs'] = $this->JobsModel->view();
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        $this->load->view('admin/Jobs/alljobs',$data);
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }
        }else{
             redirect('Logout');
        }
    } 

    function loaddataview(){
    $id=$this->input->post("id");
      $data['support'] = $this->JobsModel->loaddata1($id);  
      $this->load->view('admin/Jobs/view',$data);
      
    }

function searchvalue(){

     $filterstatus  = $this->input->post('filterstatus'); 
     if($filterstatus === 'all'){
       return redirect('admin/UserInsurance');
     }else{
     $data['userinsurance']= $this->UserInsuranceModel->getstatusWhereLike($filterstatus); 
            if($data){ 
              $title['title']="All User Insurance";
              $this->load->view('admin/layouts/header',$title);
              $this->load->view('admin/layouts/navbar');
              $this->load->view('admin/userinsurance/alluserinsurance',$data);
              $this->load->view('admin/layouts/footer');
            }else{
               echo "filter data cant be show on view";
            }
     }
    }


    function exportcsv(){
     // file name 
    $filename = 'Job_Request'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
     // get data 
    $usersData = $this->JobsModel->getUserDetails();
    // file creation 
    $file = fopen('php://output','w');
    $header = array("Job Title","User Name","User Contact","Email id","Message","Date Of Application"); 
    fputcsv($file, $header);
    foreach ($usersData as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
    }

   
    
    }

