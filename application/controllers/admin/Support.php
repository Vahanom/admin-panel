<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Support extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/SupportModel');
            $this->load->helper('form'); 
	  }

     public function index()
     {
      if($this->session->userdata('usersid') != null){

    
        foreach($this->session->userdata('permission') as $columnvalue){
        }
       
if(!empty($columnvalue)){ $emplyee =  json_decode($columnvalue->permission_employee); }


if((!empty($emplyee) && $emplyee->view == "true") || $this->session->userdata('user_role') == "admin"){
     
          $title['title']="All Support Conditions";
          $this->load->view('admin/layouts/header',$title);
          $this->load->view('admin/layouts/navbar');
          $data['support'] = $this->SupportModel->view();
          $this->load->view('admin/support/allsupport',$data);
          $this->load->view('admin/layouts/footer');
        }else{
          redirect("admin/Access");
        }
    
        }else{
             redirect('Logout');
        }
    }

    public function create()
    {
      if($this->session->userdata('usersid') != null){

          foreach($this->session->userdata('permission') as $columnvalue){
           }
       
            if(!empty($columnvalue)){ $emplyee =  json_decode($columnvalue->permission_employee); }
  
  
             if((!empty($emplyee) && $emplyee->add == "true") || $this->session->userdata('user_role') == "admin"){
                 $title['title']="Add Support Details";
                 $this->load->view('admin/layouts/header',$title);
                 $this->load->view('admin/layouts/navbar');
                 $this->load->view('admin/support/create');
                 $this->load->view('admin/layouts/footer');
               }else{
                  redirect("admin/Access");
               }

       }else{
             redirect('Logout');
        }
     }    


    function add(){
      
      foreach($this->session->userdata('permission') as $columnvalue){
      }
     
          if(!empty($columnvalue)){ $emplyee =  json_decode($columnvalue->permission_employee); }


          if((!empty($emplyee) && $emplyee->add == "true") || $this->session->userdata('user_role') == "admin"){
            $support_email = $this->input->post('support_email');
            $support_phno = $this->input->post('support_phno');

             $result = $this->SupportModel->add($support_email,$support_phno);
            //  die;
             if ($result == 'true') {
                    echo "<script>alert('Support Details Added Successfully');
                           window.location.href = 'index';
                         </script>";
            }else{
                    redirect("Support/index");
            }
        }else{
            redirect("admin/Access");
      }
    }

    function loaddata(){
      foreach($this->session->userdata('permission') as $columnvalue){
      }
     
       if(!empty($columnvalue)){ $emplyee =  json_decode($columnvalue->permission_employee); }
  
  
        if((!empty($emplyee) && $emplyee->view == "true") || $this->session->userdata('user_role') == "admin"){
           $id = $this->input->post("id");
      
           $data['support'] = $this->SupportModel->loaddata($id);
     
          $this->load->view('admin/support/update',$data);
         }else{
             redirect("admin/Access");
         }
      
    }

    function loaddataview(){
      foreach($this->session->userdata('permission') as $columnvalue){
      }
     
         if(!empty($columnvalue)){ $emplyee =  json_decode($columnvalue->permission_employee); }
  
  
            if((!empty($emplyee) && $emplyee->view == "true") || $this->session->userdata('user_role') == "admin"){
             $id = $this->input->post("id");
             $data['support'] = $this->SupportModel->loaddata1($id);
     
             $this->load->view('admin/support/view',$data);
           }else{
              redirect("admin/Access");
        }
      
    }

     function update(){
     
    foreach($this->session->userdata('permission') as $columnvalue){
    }
   
      if(!empty($columnvalue)){ $emplyee =  json_decode($columnvalue->permission_employee); }


        if((!empty($emplyee) && $emplyee->update == "true") || $this->session->userdata('user_role') == "admin"){
            $support_id = $this->input->post('support_id');
            $support_phno = $this->input->post('support_phno');
            $support_email = $this->input->post('support_email');
           $result = $this->SupportModel->update($support_id,$support_phno,$support_email);


        if ($result == 1) {
          echo "<script>alert('Support Conditions Updated Successfully');
            window.location.href = 'index';
            </script>";
          }else{
        redirect("Support/index");
        }

      }else{
        redirect("admin/Access");
    }
    }

    function emailcheckexist(){
        $username = $_POST['key'];
          $exists = $this->SupportModel->emailcheckexist($username);
      
          $count = count($exists);
          
      
          if (empty($count)) {
              echo 1;
          } else {
              echo 2;
          }
      }
     

     function exportcsv(){
     // file name 
    $filename = 'support_'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
     // get data 
    $usersData = $this->SupportModel->getUserDetails();
    // file creation 
    $file = fopen('php://output','w');
    $header = array("Support Id","Support Phone","Support Email","Support Created Date","Support Modified Date"); 
    fputcsv($file, $header);
    foreach ($usersData as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
    }
    
    }

    