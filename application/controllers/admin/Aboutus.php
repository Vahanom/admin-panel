<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Aboutus extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/AboutusModel');
            $this->load->helper('form'); 
	  }

     public function index()
     {
      if($this->session->userdata('usersid') != null){

    
        foreach($this->session->userdata('permission') as $columnvalue){
        }
       
if(!empty($columnvalue)){ $aboutus =  json_decode($columnvalue->permission_employee); }


if((!empty($aboutus) && $aboutus->view == "true") || $this->session->userdata('user_role') == "admin"){
     
          $title['title']="All About us";
          $this->load->view('admin/layouts/header',$title);
          $this->load->view('admin/layouts/navbar');
          $data['aboutus'] = $this->AboutusModel->view();
          $this->load->view('admin/Aboutus/allaboutus',$data);
          $this->load->view('admin/layouts/footer');
        }else{
          redirect("admin/Access");
        }
    
        }else{
             redirect('Logout');
        }
    }

    public function create()
    {
      if($this->session->userdata('usersid') != null){

          foreach($this->session->userdata('permission') as $columnvalue){
           }
       
            if(!empty($columnvalue)){ $aboutus =  json_decode($columnvalue->permission_aboutus); }
  
  
             if((!empty($aboutus) && $aboutus->add == "true") || $this->session->userdata('user_role') == "admin"){
                 $title['title']="Add About us";
                 $this->load->view('admin/layouts/header',$title);
                 $this->load->view('admin/layouts/navbar');
                 $this->load->view('admin/Aboutus/create');
                 $this->load->view('admin/layouts/footer');
               }else{
                  redirect("admin/Access");
               }

       }else{
             redirect('Logout');
        }
     }    


    function add(){
      
      foreach($this->session->userdata('permission') as $columnvalue){
      }
     
          if(!empty($columnvalue)){ $aboutus =  json_decode($columnvalue->permission_aboutus); }


          if((!empty($aboutus) && $aboutus->add == "true") || $this->session->userdata('user_role') == "admin"){
            $abt_description = $this->input->post('abt_description');

             $result = $this->AboutusModel->add($abt_description);
            //  die;
             if ($result == 'true') {
                    echo "<script>alert('About Us Added Successfully');
                           window.location.href = 'index';
                         </script>";
            }else{
                    redirect("admin/Employees");
            }
        }else{
            redirect("admin/Access");
      }
    }

    function loaddata(){
      foreach($this->session->userdata('permission') as $columnvalue){
      }
     
       if(!empty($columnvalue)){ $aboutus =  json_decode($columnvalue->permission_aboutus); }
  
  
        if((!empty($aboutus) && $aboutus->view == "true") || $this->session->userdata('user_role') == "admin"){
           $id = $this->input->post("id");
      
           $data['aboutus'] = $this->AboutusModel->loaddata($id);
     
          $this->load->view('admin/Aboutus/update',$data);
         }else{
             redirect("admin/Access");
         }
      
    }

    function loaddataview(){
      foreach($this->session->userdata('permission') as $columnvalue){
      }
     
         if(!empty($columnvalue)){ $aboutus =  json_decode($columnvalue->permission_aboutus); }
  
  
            if((!empty($aboutus) && $aboutus->view == "true") || $this->session->userdata('user_role') == "admin"){
             $id = $this->input->post("id");
      
             $data['aboutus'] = $this->AboutusModel->loaddata1($id);
     
             $this->load->view('admin/Aboutus/view',$data);
           }else{
              redirect("admin/Access");
        }
      
    }

     function update(){
     
    foreach($this->session->userdata('permission') as $columnvalue){
    }
   
      if(!empty($columnvalue)){ $aboutus =  json_decode($columnvalue->permission_aboutus); }


        if((!empty($aboutus) && $aboutus->update == "true") || $this->session->userdata('user_role') == "admin"){
            $abt_id = $this->input->post('abt_id');
            $abt_description = $this->input->post('abt_description');
           $result = $this->AboutusModel->update($abt_id,$abt_description);

        if ($result == 1) {
          echo "<script>alert('About Us Updated Successfully');
            window.location.href = 'index';
            </script>";
          }else{
        redirect("Aboutus/index");
        }

      }else{
        redirect("admin/Access");
    }
    }

    function exportcsv(){
     // file name 
    $filename = 'aboutus'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
     // get data 
    $usersData = $this->AboutusModel->getUserDetails();
    // file creation 
    $file = fopen('php://output','w');
    $header = array("abt_id","abt_description","abt_create_date","abt_modify_date"); 
    fputcsv($file, $header);
    foreach ($usersData as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
    }
   
    
    }

    