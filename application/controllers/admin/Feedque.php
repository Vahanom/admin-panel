<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  
  class Feedque extends CI_Controller {
    
 function __construct()
    {           
            parent::__construct();        
            $this->load->library('session');
            $this->load->model('admin/FeedModel');
            $this->load->helper('form'); 
    }
     public function index()
     {
      if($this->session->userdata('usersid') != null){
        
     
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $Feedque =  json_decode($columnvalue->permission_vehicle); }

if((!empty($Feedque) && $vehicle->view == "true") || $this->session->userdata('user_role') == "admin"){
        $title['title']="All Questions";
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        
        $data['questions'] = $this->FeedModel->view();
        $this->load->view('admin/feedbackquestions/allquestions',$data);
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }
        }else{
             redirect('Logout');
        }
    }
    function cat_match(){
      $username = $_POST['key'];
        $exists = $this->FeedModel->cat_match($username);
        $count = count($exists);
        // print_r($count);exit;
        
    
        if (empty($count)) {
            echo 1;
        } else {
            echo 2;
        }
    }
       function sub_cat_match(){
      $username = $_POST['key'];
      $username1 = $_POST['key1'];
        $exists = $this->FeedModel->sub_cat_match($username,$username1);
        $count = count($exists);
        // print_r($count);exit;
        
    
        if(!empty($count)) {
            echo 2;
        } else {
            echo 1;
        }
    }
    
 function getCatModel(){
        $this->load->view('admin/feedbackquestions/cat_modalview');
      }
      function getSubCatModel(){
        $data['cat'] = $this->FeedModel->cat_view();
        
        $this->load->view('admin/feedbackquestions/sub_cat_modalview',$data);
      }
      
 function loaddataview(){
      foreach($this->session->userdata('permission') as $columnvalue){
      }
     
         if(!empty($columnvalue)){ $Feedque =  json_decode($columnvalue->permission_feedque); }
  
  
            if((!empty($Feedque) && $Feedque->view == "true") || $this->session->userdata('user_role') == "admin"){
             $id = $this->input->post("id");
             $data['support'] = $this->FeedModel->loaddata1($id);
     
             $this->load->view('admin/feedbackquestions/view',$data);
           }else{
              redirect("admin/Access");
        }
      
    }
 function loaddata(){
      foreach($this->session->userdata('permission') as $columnvalue){
      }
     
         if(!empty($columnvalue)){ $Feedque =  json_decode($columnvalue->permission_feedque); }
  
  
            if((!empty($Feedque) && $Feedque->view == "true") || $this->session->userdata('user_role') == "admin"){
             $id = $this->input->post("id");
             $data['feedquestion'] = $this->FeedModel->loaddata1($id);
     
             $this->load->view('admin/feedbackquestions/update',$data);
           }else{
              redirect("admin/Access");
        }
      
    }
   function update(){
     
        foreach($this->session->userdata('permission') as $columnvalue){
        }
        if(!empty($columnvalue)){ $Feedque =  json_decode($columnvalue->permission_feedque); }
        if((!empty($Feedque) && $Feedque->update == "true") || $this->session->userdata('user_role') == "admin"){
            $queid = $this->input->post('queid');
            $que_name = $this->input->post('que_name');
            $ser_type = $this->input->post('ser_type');
            $status = $this->input->post('status');
            $result = $this->FeedModel->update($que_name,$ser_type,$status,$queid);
            if ($result == 1) {
                echo "<script>alert('Checkpoint Question Updated Successfully');
                    window.location.href='../Feedque';
                    </script>";
            }else{
                redirect("admin/Feedque");
            }
        }else{
        redirect("admin/Access");
      }  
    }

function getCatDeleteModel(){
        $data['cat'] = $this->FeedModel->cat_view();
        
        $this->load->view('admin/feedbackquestions/delete_cat_modal',$data);
    
}
function getsubCatDeleteModel(){
        $data['cat'] = $this->FeedModel->cat_view();
        // print_r($data['subcat']);exit;
        $this->load->view('admin/feedbackquestions/delete_subcat_modal',$data);
    
}
function loadfeedCategory(){
        if($this->input->post('que_category'))
            {
              echo $this->FeedModel->loadFeeCategory($this->input->post('que_category'));
         }
      }


function add(){
  // print_r($this->input->popen(command, mode)st());exit;
  $que_category = $this->input->post('que_category');
  $que_sub_category = $this->input->post('que_sub_category');
  $ques = $this->input->post('ques');
  $ser_type = $this->input->post('ser_type');
  $status = $this->input->post('status');
  $result = $this->FeedModel->add($que_category,$que_sub_category,$ser_type,$ques,$status);
  if ($result == 'true') {
          echo "<script>alert('Questions Added Successfully');
            window.location.href='../Feedque';
            </script>";
          }else{
            redirect("admin/Feedque");
        }
      
}

function addmodel(){
  $cat_name = $this->input->post('cat_name');
  $status = $this->input->post('status');
 $result = $this->FeedModel->addmodel($cat_name,$status);
  if ($result == 'true') {
          echo "<script>alert('Category Added Successfully');
            window.location.href='../Feedque/create';
            </script>";
          }else{
            redirect("admin/Feedque");
        }
      
}
function deletemodel(){
  $que_category_del = $this->input->post('que_category_del');
 $result = $this->FeedModel->deletemodel($que_category_del);
  if ($result == 'true') {
          echo "<script>alert('Checkpoint Category,Subcategory and There Questions Deleted Successfully');
            window.location.href='../Feedque/create';
            </script>";
          }else{
            redirect("admin/Feedque");
        }
      
}
function subaddmodel(){
  $que_category = $this->input->post('que_category');
  $sub_cat_name = $this->input->post('sub_cat_name');
  $status = $this->input->post('status');
 $result = $this->FeedModel->subaddmodel($que_category,$sub_cat_name,$status);
  if ($result == 'true') {
          echo "<script>alert('Sub Category Added Successfully');
            window.location.href='../Feedque/create';
            </script>";
          }else{
            redirect("admin/Feedque");
        }
      
}
function subdeletemodel(){
  $que_sub_category_edit = $this->input->post('que_sub_category_edit');
 $result = $this->FeedModel->deletesubmodel($que_sub_category_edit);
  if ($result == 1) {
          echo "<script>alert('Sub Category And There Questions Deleted Successfully');
            window.location.href='../Feedque/create';
            </script>";
          }else{
            redirect("admin/Feedque");
        }
}
function searchvalue(){

     $filterstatus  = $this->input->post('filterstatus'); 
     if($filterstatus === 'all'){
       return redirect('admin/Feedque');
     }else{
      $data['questions']= $this->FeedModel->getstatusWhereLike($filterstatus); 
            if($data){ 
             $title['title']="All Questions";
            $this->load->view('admin/layouts/header',$title);
            $this->load->view('admin/layouts/navbar');
            $this->load->view('admin/feedbackquestions/allquestions',$data);
            $this->load->view('admin/layouts/footer');
      }else{
               echo "filter data cant be show on view";
            }
     }
    }
public function create()
    {
      if($this->session->userdata('usersid') != null){
        
     
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $Feedque =  json_decode($columnvalue->permission_Feedque); }

if((!empty($Feedque) && $Feedque->add == "true") || $this->session->userdata('user_role') == "admin"){
        $title['title']="Create Questions";
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        $data['cat'] = $this->FeedModel->cat_view();
        $data['sub_cat'] = $this->FeedModel->subcat_view();
        $this->load->view('admin/feedbackquestions/create',$data);
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }
        }else{
             redirect('Logout');
        }
   }

    
    }

    