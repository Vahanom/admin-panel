<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Ongoing extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/OngoingModel');
            $this->load->helper('form'); 
	  }

     public function index()
     {
      if($this->session->userdata('usersid') != null){

    
        foreach($this->session->userdata('permission') as $columnvalue){
        }
       
        if(!empty($columnvalue)){ $emplyee =  json_decode($columnvalue->permission_employee); }


         if((!empty($emplyee) && $emplyee->view == "true") || $this->session->userdata('user_role') == "admin"){
     
          $title['title']="All On Going";
          $this->load->view('admin/layouts/header',$title);
          $this->load->view('admin/layouts/navbar');
          $data['aboutus'] = $this->OngoingModel->view();
          $this->load->view('admin/Ongoing/allongoing',$data);
          $this->load->view('admin/layouts/footer');
        }else{
          redirect("admin/Access");
        }
    
        }else{
             redirect('Logout');
        }
    }
    function searchvalue(){

     $filterstatus  = $this->input->post('filterstatus'); 
     if($filterstatus === 'all'){
       $title['title']="All Parking";
       $data['aboutus']= $this->OngoingModel->getstatusWhereLikeall($filterstatus); 
              $this->load->view('admin/layouts/header',$title);
              $this->load->view('admin/layouts/navbar');
              $this->load->view('admin/Ongoing/allongoing',$data);
              $this->load->view('admin/layouts/footer');
     }else{
     $data['aboutus']= $this->OngoingModel->getstatusWhereLike($filterstatus); 
            if($data){ 
              $title['title']="All Employee";
              $this->load->view('admin/layouts/header',$title);
              $this->load->view('admin/layouts/navbar');
              $this->load->view('admin/Ongoing/allongoing',$data);
              $this->load->view('admin/layouts/footer');
            }else{
               echo "filter data cant be show on view";
            }
     }
    }


    public function create()
    {
      if($this->session->userdata('usersid') != null){

          foreach($this->session->userdata('permission') as $columnvalue){
           }
       
            if(!empty($columnvalue)){ $emplyee =  json_decode($columnvalue->permission_employee); }
  
  
             if((!empty($emplyee) && $emplyee->add == "true") || $this->session->userdata('user_role') == "admin"){
                 $title['title']="Add About us";
                 $this->load->view('admin/layouts/header',$title);
                 $this->load->view('admin/layouts/navbar');
                 $this->load->view('admin/aboutus/create');
                 $this->load->view('admin/layouts/footer');
               }else{
                  redirect("admin/Access");
               }

       }else{
             redirect('Logout');
        }
     }    


    function loaddataview(){
      foreach($this->session->userdata('permission') as $columnvalue){
      }
     
         if(!empty($columnvalue)){ $emplyee =  json_decode($columnvalue->permission_employee); }
  
  
            if((!empty($emplyee) && $emplyee->view == "true") || $this->session->userdata('user_role') == "admin"){
             $id = $this->input->post("id");
              $type_servicespare = $this->input->post("type_servicespare");
             $order_details_id = $this->input->post("order_details_id");
             $ongoing = $this->OngoingModel->loaddata1($id);
             $data1 = $this->OngoingModel->loaddata2($order_details_id);
             $data2 = $this->OngoingModel->loaddata3($id,$type_servicespare);
     
             $this->load->view('admin/ongoing/view',['ongoing'=>$ongoing,'data1'=>$data1,'data2'=>$data2]);
           }else{
              redirect("admin/Access");
        }
      
    }

 }

    