<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Requestinsurance extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/RequestInsuranceModel');
            $this->load->helper('form'); 
	  }

     public function index()
     {
   
      if($this->session->userdata('usersid') != null){
        foreach($this->session->userdata('permission') as $columnvalue){
        }
        if(!empty($columnvalue)){ $insurance =  json_decode($columnvalue->permission_insurance); }

        
if((!empty($insurance) && $insurance->view == "true") || $this->session->userdata('user_role') == "admin"){
        $title['title']="All Insurance";
        $data = $this->RequestInsuranceModel->view();
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        $this->load->view('admin/requestinsurance/allrequestinsurance',['data'=>$data]);
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }
        }else{
             redirect('Logout');
        }
    }

    function searchvalue(){

     $filterstatus  = $this->input->post('filterstatus'); 
     if($filterstatus === 'all'){
       return redirect('admin/Requestinsurance');
     }else{
     $data= $this->RequestInsuranceModel->getstatusWhereLike($filterstatus); 
            if($data){ 
              $title['title']="All Employee";
              $this->load->view('admin/layouts/header',$title);
              $this->load->view('admin/layouts/navbar');
              $this->load->view('admin/requestinsurance/allrequestinsurance',['data'=>$data]);
              $this->load->view('admin/layouts/footer');
            }else{
               echo "filter data cant be show on view";
            }
     }
    }
   

    function loaddata(){
   
      $id = $this->input->post("id");
      $insurance_id = $this->input->post("insurance_id");
      $data = $this->RequestInsuranceModel->loaddata1($id,$insurance_id);  
      $this->load->view('admin/requestinsurance/update',['data'=>$data]);
      
    }

    function loaddataview(){
   
      $id = $this->input->post("id");
      $insurance_id = $this->input->post("insurance_id");
      $user_m_id = $this->input->post("user_m_id");
      $data = $this->RequestInsuranceModel->loaddata1($id,$insurance_id);
      $data2 = $this->RequestInsuranceModel->loaddata2($id,$user_m_id);   
      $this->load->view('admin/requestinsurance/view',['data'=>$data,'data2'=>$data2]);
      
    }
    
  public function update(){
     foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $pollution =  json_decode($columnvalue->permission_pollution); }

    if((!empty($pollution) && $pollution->update == "true") || $this->session->userdata('user_role') == "admin"){

      $user_insuranceid = $this->input->post('user_insuranceid');
      $remark = $this->input->post('remark');
      $result = $this->RequestInsuranceModel->update($user_insuranceid,$remark);

        if ($result == 1) {
          echo "<script>alert('Pollution Updated Successfully');
            window.location.href = 'index';
            </script>";
          }else{
        redirect("admin/Pollution");
        }
      }else{
        redirect("admin/Access");
      }
    }


    function loaddata3(){ 
     $value = $this->input->post("value"); 
      $user_insuranceid = $this->input->post("user_insuranceid"); 
     $data =$this->RequestInsuranceModel->loaddata3($value,$user_insuranceid); 
     if($data== 1){
           echo "data";
     }else{
      echo "nto";
     }

      
    }

    function exportcsv(){
     // file name 
    $filename = 'reqinsurance_'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
     // get data 
    $usersData = $this->RequestInsuranceModel->getUserDetails();
    // file creation 
    $file = fopen('php://output','w');
    $header = array("Insurance Title","User Name","User Contact","Email id","Remark","Status"); 
    fputcsv($file, $header);
    foreach ($usersData as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
    }


   
    
    }

    