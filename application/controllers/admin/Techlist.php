<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Techlist extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/TechlistModel');
            $this->load->helper('form'); 
	  }

     public function index() 
     {
      if($this->session->userdata('usersid') != null){

    
        foreach($this->session->userdata('permission') as $columnvalue){
        }
       
if(!empty($columnvalue)){ $techlist =  json_decode($columnvalue->permission_techlist); }


if((!empty($techlist) && $techlist->view == "true") || $this->session->userdata('user_role') == "admin"){
     
          $title['title']="Technician Show List";
          $this->load->view('admin/layouts/header',$title);
          $this->load->view('admin/layouts/navbar');
          $data['techlist'] = $this->TechlistModel->view();
          $this->load->view('admin/Show_Technician_List/show_list',$data);
          $this->load->view('admin/layouts/footer');
        }else{
          redirect("admin/Access");
        }
    
        }else{
             redirect('Logout');
        }
    }

   function update(){
     
        foreach($this->session->userdata('permission') as $columnvalue){
        }
        if(!empty($columnvalue)){ $techlist =  json_decode($columnvalue->permission_techlist); }
        if((!empty($techlist) && $techlist->update == "true") || $this->session->userdata('user_role') == "admin"){
            $status = $this->input->post('id');
            $result = $this->TechlistModel->update("1",$status);
            if ($result == 1) {
                return true;
            }else{
                return false;
                redirect("admin/Techlist");
            }
        }else{
        redirect("admin/Access");
      }  
    }




    
    }

    