<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class UserInsurance extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/UserInsuranceModel');
            $this->load->helper('form'); 
	  }

     public function index()
     {
   
      if($this->session->userdata('usersid') != null){
        foreach($this->session->userdata('permission') as $columnvalue){
        }
        if(!empty($columnvalue)){ $insurance =  json_decode($columnvalue->permission_insurance); }

        
if((!empty($insurance) && $insurance->view == "true") || $this->session->userdata('user_role') == "admin"){
        $title['title']="All User Insurance";
        $data['userinsurance'] = $this->UserInsuranceModel->view();
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        $this->load->view('admin/userinsurance/alluserinsurance',$data);
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }
        }else{
             redirect('Logout');
        }
    }

    function loaddataview(){
   
      $id = $this->input->post("id");
      $user_m_id = $this->input->post("user_m_id");
      $insurance_id = $this->input->post("insurance_id");
      $data = $this->UserInsuranceModel->loaddata1($id,$user_m_id);
      $data2 = $this->UserInsuranceModel->loaddata2($id,$insurance_id);  
      $this->load->view('admin/userinsurance/view',['data'=>$data,'data2'=>$data2]);
      
    }

function searchvalue(){

     $filterstatus  = $this->input->post('filterstatus'); 
     if($filterstatus === 'all'){
       return redirect('admin/UserInsurance');
     }else{
     $data['userinsurance']= $this->UserInsuranceModel->getstatusWhereLike($filterstatus); 
            if($data){ 
              $title['title']="All User Insurance";
              $this->load->view('admin/layouts/header',$title);
              $this->load->view('admin/layouts/navbar');
              $this->load->view('admin/userinsurance/alluserinsurance',$data);
              $this->load->view('admin/layouts/footer');
            }else{
               echo "filter data cant be show on view";
            }
     }
    }


     function exportcsv(){
     // file name 
    $filename = 'userinsurance_'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
     // get data 
    $usersData = $this->UserInsuranceModel->getUserDetails();
    // file creation 
    $file = fopen('php://output','w');
    $header = array("Id","Insurance Id","User Name","Status","Remark","Created Date"); 
    fputcsv($file, $header);
    foreach ($usersData as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
    }
   
    
    }

