<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Spare extends CI_Controller {
    
    function __construct()
    {
		parent::__construct();				
    $this->load->library('session');
    $this->load->model('admin/SpareModel');
    $this->load->helper('form'); 
	  }

     public function index()
     {
      if($this->session->userdata('usersid') != null){
        
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $spare =  json_decode($columnvalue->permission_spare); }

if((!empty($spare) && $spare->view == "true") || $this->session->userdata('user_role') == "admin"){
        $title['title']="All Spare";
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        $data['spare'] = $this->SpareModel->view();
        $this->load->view('admin/spare/allspare',$data);
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }  
        }else{
             redirect('Logout');
        }
    }

    function searchvalue(){

     $filterstatus  = $this->input->post('filterstatus'); 
     if($filterstatus === 'all'){
       return redirect('admin/Spare');
     }else{
     $data['spare']= $this->SpareModel->getstatusWhereLike($filterstatus); 
            if($data){ 
              $title['title']="All Spare";
              $this->load->view('admin/layouts/header',$title);
              $this->load->view('admin/layouts/navbar');
              $this->load->view('admin/spare/allspare',$data);
              $this->load->view('admin/layouts/footer');
            }else{
               echo "filter data cant be show on view";
            }
     }
    }

    function add(){
              
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $spare =  json_decode($columnvalue->permission_spare); }

if((!empty($spare) && $spare->add == "true") || $this->session->userdata('user_role') == "admin"){
    // print_r($this->input->post());exit;
      $sparecategory = $this->input->post('sparecategory');
        $space_hsn = $this->input->post('space_hsn');
        $tech_hours = $this->input->post('tech_hours');
        $spare_desc = $this->input->post('spare_desc');
        $space_title = $this->input->post('space_title');
        $status = $this->input->post('status');
        $vehiclename = $this->input->post('vehiclename');
        $margin = $this->input->post('margin');
        $veh_cost = $this->input->post('veh_cost');
        $lb_rate = $this->input->post('lb_rate');
        $lb_commission = $this->input->post('lb_commission');
        $vehiclename=implode(",",$vehiclename);
        if(!empty($_FILES['imageone']['name'])){
          $config['upload_path'] = './assets/images/spare';
          $config['allowed_types'] = 'jpg|jpeg|png|gif';
          $config['file_name'] = $_FILES['imageone']['name'];
          
          //Load upload library and initialize configuration
          $this->load->library('upload',$config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('imageone')){
              $uploadData = $this->upload->data();
              $imageone = $uploadData['file_name'];
          }else{
              $imageone = '';
          }
      }else{
          $imageone = '';
      }
      if(!empty($_FILES['imagetwo']['name'])){
        $config['upload_path'] = './assets/images/spare';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['file_name'] = $_FILES['imagetwo']['name'];
        
        //Load upload library and initialize configuration
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('imagetwo')){
            $uploadData = $this->upload->data();
            $imagetwo = $uploadData['file_name'];
        }else{
            $imagetwo = '';
        }
      }else{
        $imagetwo = '';
      }
      
      if(!empty($_FILES['imagethree']['name'])){
        $config['upload_path'] = './assets/images/spare';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['file_name'] = $_FILES['imagethree']['name'];
        
        //Load upload library and initialize configuration
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('imagethree')){
            $uploadData = $this->upload->data();
            $imagethree = $uploadData['file_name'];
        }else{
            $imagethree = '';
        }
      }else{
        $imagethree = '';
      }
      
      if(!empty($_FILES['imagefour']['name'])){
        $config['upload_path'] = './assets/images/spare';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['file_name'] = $_FILES['imagefour']['name'];
        
        //Load upload library and initialize configuration
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('imagefour')){
            $uploadData = $this->upload->data();
            $imagefour = $uploadData['file_name'];
        }else{
            $imagefour = '';
        }
      }else{
        $imagefour = '';
      }


        $result = $this->SpareModel->add($space_title,$spare_desc,$space_hsn,$tech_hours,$status,$imageone,$imagetwo,$imagethree,$imagefour,$vehiclename,$sparecategory,$veh_cost,$lb_rate,$lb_commission,$margin);

if ($result == 'true') {
          echo "<script>alert('Spare Added Successfully');
            window.location.href='../Spare';
            </script>";
          }else{
            redirect("admin/Spare");
        }
      }else{
        redirect("admin/Access");
      }  
    }
public function create()
{
  if($this->session->userdata('usersid') != null)
  {
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue))
    { 
      $spare =  json_decode($columnvalue->permission_spare); 
    }
    if((!empty($spare) && $spare->add == "true") || $this->session->userdata('user_role') == "admin")
    {
      $title['title']="Add Spare ";
      $data['sparecat'] = $this->SpareModel->sparecat();
      $this->load->view('admin/layouts/header',$title);
      $this->load->view('admin/layouts/navbar');
      $this->load->view('admin/spare/create',$data);
      $this->load->view('admin/layouts/footer');
    }else
    {
      redirect("admin/Access");
    }  
  }else
  {
    redirect('Logout');
  }
}

   function update(){
             
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $spare =  json_decode($columnvalue->permission_spare); }

if((!empty($spare) && $spare->update == "true") || $this->session->userdata('user_role') == "admin"){
    $sp_id = $this->input->post('sp_id');
    $sparecategory = $this->input->post('sparecategory');
    $space_hsn = $this->input->post('space_hsn');
        $tech_hours = $this->input->post('tech_hours');
        $spare_desc = $this->input->post('spare_desc');
        $space_title = $this->input->post('space_title');
        $status = $this->input->post('status');
        $vehiclename = $this->input->post('vehiclename');
        $margin = $this->input->post('margin');
        $veh_cost = $this->input->post('veh_cost');
        $lb_rate = $this->input->post('lb_rate');
        $lb_commission = $this->input->post('lb_commission');
        $vehiclename=implode(",",$vehiclename);
        
$vehiclename=implode(",",$vehiclename);
        if(!empty($_FILES['imageone']['name'])){
          $config['upload_path'] = './assets/images/spare';
          $config['allowed_types'] = 'jpg|jpeg|png|gif';
          $config['file_name'] = $_FILES['imageone']['name'];
          
          //Load upload library and initialize configuration
          $this->load->library('upload',$config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('imageone')){
              $uploadData = $this->upload->data();
              $imageone = $uploadData['file_name'];
          }else{
              $imageone = '';
          }
      }else{
        $imageone = $this->input->post('hiddenimageone');
      }
      if(!empty($_FILES['imagetwo']['name'])){
        $config['upload_path'] = './assets/images/spare';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['file_name'] = $_FILES['imagetwo']['name'];
        
        //Load upload library and initialize configuration
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('imagetwo')){
            $uploadData = $this->upload->data();
            $imagetwo = $uploadData['file_name'];
        }else{
            $imagetwo = '';
        }
      }else{
        $imagetwo = $this->input->post('hiddenimagetwo');
      }
      
      if(!empty($_FILES['imagethree']['name'])){
        $config['upload_path'] = './assets/images/spare';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['file_name'] = $_FILES['imagethree']['name'];
        
        //Load upload library and initialize configuration
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('imagethree')){
            $uploadData = $this->upload->data();
            $imagethree = $uploadData['file_name'];
        }else{
            $imagethree = '';
        }
      }else{
        $imagethree = $this->input->post('hiddenimagethree');
      }
      
      if(!empty($_FILES['imagefour']['name'])){
        $config['upload_path'] = './assets/images/spare';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['file_name'] = $_FILES['imagefour']['name'];
        
        //Load upload library and initialize configuration
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('imagefour')){
            $uploadData = $this->upload->data();
            $imagefour = $uploadData['file_name'];
        }else{
            $imagefour = '';
        }
      }else{
        $imagefour = $this->input->post('hiddenimagefour');
      }
$result = $this->SpareModel->update($space_title,$spare_desc,$space_hsn,$tech_hours,$status,$imageone,$imagetwo,$imagethree,$imagefour,$vehiclename,$sp_id,$sparecategory,$veh_cost,$lb_rate,$lb_commission,$margin);

        if ($result == 1) {
          echo "<script>alert('Spare Updated Successfully');
             window.location.href='../Spare';
            </script>";
          }else{
        redirect("admin/Spare");
        }
      }else{
        redirect("admin/Access");
      }  
    }
function loaddata(){
   
      $id = $this->input->post("id");
      $data['sparecat'] = $this->SpareModel->sparecat();
      $data['spare'] = $this->SpareModel->loaddata($id);
     
      $this->load->view('admin/spare/update',$data);
      
    }

    function loaddataview(){
   
      $id = $this->input->post("id");
      $data['sparecat'] = $this->SpareModel->sparecat();
      $data['spare'] = $this->SpareModel->loaddata1($id);
     
      $this->load->view('admin/spare/view',$data);
      
    }

    function namecheckexist()
    {
        $username = $_POST['key'];
          $exists = $this->SpareModel->namecheckexist1($username);
      
          $count = count($exists);
          
      
          if (empty($count)) {
              echo 1;
          } else {
              echo 2;
          }
      }

    function exportcsv(){
     // file name 
    $filename = 'spare'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
     // get data 
    $usersData = $this->SpareModel->getUserDetails();
    // file creation 
    $file = fopen('php://output','w');
    $header = array("Id","Spare Category Name","Spare Title","Description","Spare hsn Code","Vehicle Id","Image One","Image Two","Image Three","Image Four","Status","Created Date","Modified Date","Margin"); 
    fputcsv($file, $header);
    foreach ($usersData as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
    }
}

    