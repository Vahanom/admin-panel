<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Completed extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/CompletedModel');
            $this->load->helper('form'); 
	  }

     public function index()
     {
      if($this->session->userdata('usersid') != null){

    
        foreach($this->session->userdata('permission') as $columnvalue){
        }
       
        if(!empty($columnvalue)){ $emplyee =  json_decode($columnvalue->permission_employee); }


         if((!empty($emplyee) && $emplyee->view == "true") || $this->session->userdata('user_role') == "admin"){
     
          $title['title']="All Completed";
          $this->load->view('admin/layouts/header',$title);
          $this->load->view('admin/layouts/navbar');
          $data['completed'] = $this->CompletedModel->view();
          $this->load->view('admin/Completed/allcompleted',$data);
          $this->load->view('admin/layouts/footer');
        }else{
          redirect("admin/Access");
        }
    
        }else{
             redirect('Logout');
        }
    }

    function searchvalue(){

     $filterstatus  = $this->input->post('filterstatus'); 
     if($filterstatus === 'all'){
       $title['title']="All Complete";
       $data['completed']= $this->CompletedModel->getstatusWhereLikeall($filterstatus); 
              $this->load->view('admin/layouts/header',$title);
              $this->load->view('admin/layouts/navbar');
              $this->load->view('admin/Completed/allcompleted',$data);
              $this->load->view('admin/layouts/footer');
     }else{
     $data['completed']= $this->CompletedModel->getstatusWhereLike($filterstatus); 
            if($data){ 
              $title['title']="All Completed";
              $this->load->view('admin/layouts/header',$title);
              $this->load->view('admin/layouts/navbar');
              $this->load->view('admin/Completed/allcompleted',$data);
              $this->load->view('admin/layouts/footer');
            }else{
               echo "filter data cant be show on view";
            }
     }
    }


    function loaddataview(){
      foreach($this->session->userdata('permission') as $columnvalue){
      }
     
         if(!empty($columnvalue)){ $emplyee =  json_decode($columnvalue->permission_employee); }
  
  
            if((!empty($emplyee) && $emplyee->view == "true") || $this->session->userdata('user_role') == "admin"){
             $id = $this->input->post("id");
              $type_servicespare = $this->input->post("type_servicespare");
             $order_details_id = $this->input->post("order_details_id");
             $completed = $this->CompletedModel->loaddata1($id);
              $data1 = $this->CompletedModel->loaddata2($order_details_id);
             $data2 = $this->CompletedModel->loaddata3($id,$type_servicespare);
     
             $this->load->view('admin/Completed/view',['completed'=>$completed,'data1'=>$data1,'data2'=>$data2]);
           }else{
              redirect("admin/Access");
        }
      
    }

 }

    