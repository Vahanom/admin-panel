<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  
  class Order extends CI_Controller {
    
    function __construct()
    {
            parent::__construct();        
            $this->load->library('session');
            $this->load->model('admin/OrderModel');
            $this->load->model('admin/CompletedModel');
            $this->load->helper('form'); 
    }

     public function index()
     {
      if($this->session->userdata('usersid') != null){

    
        foreach($this->session->userdata('permission') as $columnvalue){
        }
       
        if(!empty($columnvalue)){ $emplyee =  json_decode($columnvalue->permission_employee); }


         if((!empty($emplyee) && $emplyee->view == "true") || $this->session->userdata('user_role') == "admin"){
     
          $title['title']="All Order";
          $this->load->view('admin/layouts/header',$title);
          $this->load->view('admin/layouts/navbar');
          $order_value = $this->uri->segment(4); 
          $data['order'] = $this->OrderModel->view($order_value);
          $this->load->view('admin/order/allorders',$data);
          $this->load->view('admin/layouts/footer');
        }else{
          redirect("admin/Access");
        }
    
        }else{
             redirect('Logout');
        }
    }

    function searchvalue($status){
     $orderstatus  = $this->input->post('orderstatus'); 
     if($orderstatus){
     if($orderstatus === 'all'){
       return redirect('admin/Order/searchvalue/all');

     }else{
     $data['order']= $this->OrderModel->getstatusWhereLike($orderstatus); 
     // print_r($data);exit;
            if($data){ 
             $title['title']="All Order";
          $this->load->view('admin/layouts/header',$title);
          $this->load->view('admin/layouts/navbar');
          $this->load->view('admin/order/allorders',$data);
          $this->load->view('admin/layouts/footer');   // return redirect('admin/Order/index/'.$orderstatus);
            }else{
               echo "filter data cant be show on view";
            }
     }
     }
     if(!empty($status)){
      if($status === 'all')
        $status = "";
      if($status=="spares%20dispatched")
        $status="spares dispatched";

      if($status=="technician%20arrived")
        $status="technician arrived";
     $data['order']= $this->OrderModel->getstatusWhereLike($status); 
     // print_r($data);exit;
            if($data){ 
             $title['title']="All Order";
          $this->load->view('admin/layouts/header',$title);
          $this->load->view('admin/layouts/navbar');
          $this->load->view('admin/order/allorders',$data);
          $this->load->view('admin/layouts/footer');   // return redirect('admin/Order/index/'.$orderstatus);
            }else{
               echo "filter data cant be show on view";
            }
      
     }
    }
    function searchvalue1(){
     $orderstatus  = $this->input->post('orderstatus'); 
     if($orderstatus){
     if($orderstatus === 'all'){
       return redirect('admin/Order/searchvalue/all');

     }
     if($orderstatus === 'Pending'){
       return redirect('admin/Order/searchvalue/Pending');

     }
     if($orderstatus === 'Parking'){
       return redirect('admin/Order/searchvalue/Parking');

     }
     if($orderstatus === 'Completed'){
       return redirect('admin/Order/searchvalue/Completed');

     }
     }
    }

    function loaddataview(){
      foreach($this->session->userdata('permission') as $columnvalue){
      }
     
         if(!empty($columnvalue)){ $emplyee =  json_decode($columnvalue->permission_employee); }
            if((!empty($emplyee) && $emplyee->view == "true") || $this->session->userdata('user_role') == "admin"){
             $id = $this->input->post("id");
             $type_servicespare = $this->input->post("type_servicespare");
             $order_details_id = $this->input->post("order_details_id");
             $order = $this->OrderModel->loaddata1($id);
             $data1 = $this->OrderModel->loaddata2($id);
             $data2 = $this->OrderModel->loaddata3($id,$type_servicespare);
             $data4 = $this->OrderModel->loaddata4($id);
             $answer = $this->OrderModel->loaddata5($id);
            //  print_r($data1);exit;
             $this->load->view('admin/order/view',['order'=>$order,'data1'=>$data1,'data2'=>$data2,'data4'=>$data4,'answer'=>$answer]);
           }else{
              redirect("admin/Access");
        }
      
    }

   //Parking modal..
    function getparkingModel() {
        $id = $this->input->post("id"); 
        $data['parking'] = $this->OrderModel->parkingmodel($id);
        $this->load->view('admin/order/parkingmodalview',$data);

    }

    function Updatemodel(){
      $userid=$this->session->userdata('usersid');
      $radioid = $this->input->post("roleSubjectRadio");
      if($radioid === "ongoing"){
      $id=$this->input->post("order_id");
      $result=$this->OrderModel->updatestatus($radioid,$id);
      if($result){
         echo "<script>alert('Order Status Updated Successfully');
            window.location.href = 'index';
            </script>";
      }
      else{
        echo "data not added";
      }
    }elseif($radioid === "manually"){
         // to get the user id from user table
         $val ='parking';
         $techicianid= $this->input->post("techician");
         $id=$this->input->post("order_id");
         $result=$this->OrderModel->updateTechnicalid($radioid,$id,$techicianid,$userid);
         if($result){
            echo "<script>alert('technical id Updated Successfully');
                      window.location.href = 'index/parking';
                 </script>";
           //$this->load->view('admin/order/allorders');
               // redirect('admin/Order/index/'.$val);
          }
      else{
        echo "data not added";
      }
      }
    }

   function search()
    {
        $q = $this->input->get('q');
        $this->load->model('OrderModel');
        echo json_encode($this->OrderModel->getdropdown($q));
    }

//Pending..
    function getpendingModel() {
        $id = $this->input->post("id"); //print_r($id);
        $data['completed'] = $this->OrderModel->pendingmodel($id);
        $this->load->view('admin/order/pendingmodalview',$data);

    }

    function UpdatePendingmodel(){
      $radioid = $this->input->post("roleSubjectRadio");
      if($radioid === "parking"){
      $id=$this->input->post("order_id");
      $result=$this->OrderModel->updatependingstatus($radioid,$id);
      if($result){
         echo "<script>alert('Order Status Updated Successfully');
            window.location.href = 'index/pending';
            </script>";
      }
      else{
        echo "data not added";
      }
    }elseif($radioid === "manually"){
         // to get the user id from user table
         $techicianid= $this->input->post("techician");
         $id=$this->input->post("order_id");
         $result=$this->OrderModel->updatependingTechnicalid($radioid,$id,$techicianid);
         if($result){
            echo "<script>alert('technical id Updated Successfully');
            window.location.href = 'index/pending';
            </script>";
          }
      else{
        echo "data not added";
      }
      }
    }

    function pendingsearch()
    {
        $q = $this->input->get('q');
        $this->load->model('OrderModel');
        echo json_encode($this->OrderModel->getpeningdropdown($q));
    }

       function exportcsv($data){
     // file name 
    $filename = 'order_'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
     // get data 
     if($data=="spares%20dispatched")
     $data = "spares dispatched";
     if($data=="technician%20arrived")
     $data = "technician arrived";
     if($data=="all")
        $usersData = $this->OrderModel->getUserDetails1();
    else
        $usersData = $this->OrderModel->getUserDetails($data);
    // file creation 
    $file = fopen('php://output','w');
    $header = array("Order Type","Order Date","Order Address","Order Pincode","Technician Accept Status","Supplier Accept Status","Order Status"); 
    fputcsv($file, $header);
    foreach ($usersData as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
    }

 }

    