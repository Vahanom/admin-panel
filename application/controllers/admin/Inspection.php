<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Inspection extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/InspectionModel');
            $this->load->helper('form'); 
	  }

     public function index()
     {
   
      if($this->session->userdata('usersid') != null){
        foreach($this->session->userdata('permission') as $columnvalue){
        }
        if(!empty($columnvalue)){ $inspection =  json_decode($columnvalue->permission_inspection); }

        
if((!empty($inspection) && $inspection->view == "true") || $this->session->userdata('user_role') == "admin"){
        $title['title']="All Inspection";
        $data['inspection'] = $this->InspectionModel->view();
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        $this->load->view('admin/inspection/allinspection',$data);
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }
        }else{
             redirect('Logout');
        }
    }

    function searchvalue(){

     $filterstatus  = $this->input->post('filterstatus'); 
     if($filterstatus === 'all'){
       return redirect('admin/Inspection');
     }else{
     $data['inspection']= $this->InspectionModel->getstatusWhereLike($filterstatus); 
            if($data){ 
              $title['title']="All Inspection";
              $this->load->view('admin/layouts/header',$title);
              $this->load->view('admin/layouts/navbar');
              $this->load->view('admin/inspection/allinspection',$data);
              $this->load->view('admin/layouts/footer');
            }else{
               echo "filter data cant be show on view";
            }
     }
    }

    function add(){
      foreach($this->session->userdata('permission') as $columnvalue){
      }
      if(!empty($columnvalue)){ $insurance =  json_decode($columnvalue->permission_insurance); }
if((!empty($insurance) && $insurance->add == "true") || $this->session->userdata('user_role') == "admin"){
        $title = $this->input->post('title');
        $description = $this->input->post('description');
        $type = $this->input->post('type');
    $gst_percentage = $this->input->post('gst_per');
        $veh_type = $this->input->post('veh_type');
        $status = $this->input->post('status');
        $pre_pur = $this->input->post('pre_pur');
        $price = $this->input->post('price');
        $selling_price = $this->input->post('selling_price');
        
        if(!empty($_FILES['imageone']['name'])){
         
          $config['upload_path'] = './assets/images/inspection';
          $config['allowed_types'] = 'jpg|jpeg|png|gif';
          $config['file_name'] = $_FILES['imageone']['name'];
          $this->load->library('upload',$config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('imageone')){
              $uploadData = $this->upload->data();
              $imageone = $uploadData['file_name'];
          }else{
           
              $imageone = '';
            
          }
        }else{
          $imageone = '';
        }
$result = $this->InspectionModel->add($title,$description,$imageone,$type,$veh_type,$price,$status,$gst_percentage,$pre_pur,$selling_price);
//  die;
        if ($result == 'true') {
          echo "<script>alert('Inspection Added Successfully');
            window.location.href='../Inspection';
            </script>";
          }else{
            redirect("admin/Inspection");
        }
      }else{
        redirect("admin/Access");
      }
    }

 

    public function create()
    {
      if($this->session->userdata('usersid') != null){
        foreach($this->session->userdata('permission') as $columnvalue){
        }
        if(!empty($columnvalue)){ $insurance =  json_decode($columnvalue->permission_insurance); }
  
        
  if((!empty($insurance) && $insurance->add == "true") || $this->session->userdata('user_role') == "admin"){
        $title['title']="Add Inspection";
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        $this->load->view('admin/inspection/create');
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }  }else{
             redirect('Logout');
        }
   }

   function update(){
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $insurance =  json_decode($columnvalue->permission_insurance); }

  if((!empty($insurance) && $insurance->update == "true") || $this->session->userdata('user_role') == "admin"){
    $title = $this->input->post('title');
    $id = $this->input->post('id');
    $description = $this->input->post('description');
    $type = $this->input->post('type');
    $status = $this->input->post('status');
    $gst_percentage = $this->input->post('gst_per');
        $pre_pur = $this->input->post('pre_pur');
        $veh_type = $this->input->post('veh_type');
    $price = $this->input->post('price');
    $selling_price = $this->input->post('selling_price');

    if(!empty($_FILES['imageone']['name'])){
      $config['upload_path'] = './assets/images/inspection';
      $config['allowed_types'] = 'jpg|jpeg|png|gif';
      $config['file_name'] = $_FILES['imageone']['name'];
      
      //Load upload library and initialize configuration
      $this->load->library('upload',$config);
      $this->upload->initialize($config);
      if($this->upload->do_upload('imageone')){
          $uploadData = $this->upload->data();
          $imageone = $uploadData['file_name'];
      }else{
          $imageone = '';
      }
  }else{
    $imageone = $this->input->post('hiddenimageone');
  }
$result = $this->InspectionModel->update($title,$id,$description,$imageone,$type,$veh_type,$status,$price,$gst_percentage,$pre_pur,$selling_price);
        if ($result == 1) {
          echo "<script>alert('Inspection Updated Successfully');
             window.location.href='../Inspection';
            </script>";
          }else{
        redirect("admin/Inspection");
        }
      }else{
        redirect("admin/Access");
      }
    }



    function loaddata(){
   
      $id = $this->input->post("id");
      $data['inspection'] = $this->InspectionModel->loaddata($id);
      $this->load->view('admin/inspection/update',$data);
      
    }

    function loaddataview(){
   
      $id = $this->input->post("id");
      $data['inspection'] = $this->InspectionModel->loaddata1($id);
     
      $this->load->view('admin/inspection/view',$data);
      
    }
    
     function exportcsv(){
     // file name 
    $filename = 'inspection_'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
     // get data 
    $usersData = $this->InspectionModel->getUserDetails();
    // file creation 
    $file = fopen('php://output','w');
    $header = array("Id","title","description","type","price","selling_price","status","created","modified","expanded"); 
    fputcsv($file, $header);
    foreach ($usersData as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
    }
    
    }

    