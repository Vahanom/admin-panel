<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Parking extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/ParkingModel');
            $this->load->helper('form'); 
	  }

     public function index()
     {
      if($this->session->userdata('usersid') != null){

    
        foreach($this->session->userdata('permission') as $columnvalue){
        }
       
        if(!empty($columnvalue)){ $emplyee =  json_decode($columnvalue->permission_employee); }


         if((!empty($emplyee) && $emplyee->view == "true") || $this->session->userdata('user_role') == "admin"){
     
          $title['title']="All Completed";
          $this->load->view('admin/layouts/header',$title);
          $this->load->view('admin/layouts/navbar');
          $data['parking'] = $this->ParkingModel->view();
          $this->load->view('admin/Parking/allparking',$data);
          $this->load->view('admin/layouts/footer');
        }else{
          redirect("admin/Access");
        }
    
        }else{
             redirect('Logout');
        }
    }

    function searchvalue(){

     $filterstatus  = $this->input->post('filterstatus'); 
     if($filterstatus === 'all'){
       $title['title']="All Parking";
       $data['parking']= $this->ParkingModel->getstatusWhereLikeall($filterstatus); 
              $this->load->view('admin/layouts/header',$title);
              $this->load->view('admin/layouts/navbar');
              $this->load->view('admin/Parking/allparking',$data);
              $this->load->view('admin/layouts/footer');
     }else{
     $data['parking']= $this->ParkingModel->getstatusWhereLike($filterstatus); 
            if($data){ 
              $title['title']="All Employee";
              $this->load->view('admin/layouts/header',$title);
              $this->load->view('admin/layouts/navbar');
              $this->load->view('admin/Parking/allparking',$data);
              $this->load->view('admin/layouts/footer');
            }else{
               echo "filter data cant be show on view";
            }
     }
    }

    function loaddataview(){
      foreach($this->session->userdata('permission') as $columnvalue){
      }
     
         if(!empty($columnvalue)){ $emplyee =  json_decode($columnvalue->permission_employee); }
  
  
            if((!empty($emplyee) && $emplyee->view == "true") || $this->session->userdata('user_role') == "admin"){
             $id = $this->input->post("id");
              $type_servicespare = $this->input->post("type_servicespare");
             $order_details_id = $this->input->post("order_details_id");
             $parking = $this->ParkingModel->loaddata1($id);
             $data1 = $this->ParkingModel->loaddata4($order_details_id);
             $data2 = $this->ParkingModel->loaddata5($id,$type_servicespare);
     
             $this->load->view('admin/Parking/view',['parking'=>$parking,'data1'=>$data1,'data2'=>$data2]);
           }else{
              redirect("admin/Access");
        }
      
    }


    //working code..
    function geteditModel() {
        $id = $this->input->post("id");
        $data['parking'] = $this->ParkingModel->loaddata2($id);
        $this->load->view('admin/parking/modalview',$data);

    }

    function Updatemodel(){
      $userid=$this->session->userdata('usersid');
      $radioid = $this->input->post("roleSubjectRadio");
      if($radioid === "ongoing"){
      $id=$this->input->post("order_id");
      $result=$this->ParkingModel->updatestatus($radioid,$id);
      if($result){
         echo "<script>alert('Order Status Updated Successfully');
            window.location.href = 'index';
            </script>";
      }
      else{
        echo "data not added";
      }
    }elseif($radioid === "manually"){
         // to get the user id from user table
         $techicianid= $this->input->post("techician");
         $id=$this->input->post("order_id");
         $result=$this->ParkingModel->updateTechnicalid($radioid,$id,$techicianid,$userid);
         if($result){
            echo "<script>alert('technical id Updated Successfully');
            window.location.href = 'index';
            </script>";
          }
      else{
        echo "data not added";
      }
      }
    }

    function search()
    {
        $q = $this->input->get('q');
        $this->load->model('ParkingModel');
        echo json_encode($this->ParkingModel->getdropdown($q));
    }

 }

    