<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Tool extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/ToolModel');
            $this->load->helper('form'); 
	  }

     public function index()
     {
      if($this->session->userdata('usersid') != null){
        
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $tool =  json_decode($columnvalue->permission_tool); }

if((!empty($tool) && $tool->view == "true") || $this->session->userdata('user_role') == "admin"){
        $title['title']="All Tool";
        $data['tool'] = $this->ToolModel->view();
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        $this->load->view('admin/tool/alltool',$data);
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }
        }else{
             redirect('Logout');
        }
    }

    function searchvalue(){

     $filterstatus  = $this->input->post('filterstatus'); 
     if($filterstatus === 'all'){
       return redirect('admin/Tool');
     }else{
     $data['tool']= $this->ToolModel->getstatusWhereLike($filterstatus); 
            if($data){ 
              $title['title']="All Tool";
              $this->load->view('admin/layouts/header',$title);
              $this->load->view('admin/layouts/navbar');
              $this->load->view('admin/tool/alltool',$data);
              $this->load->view('admin/layouts/footer');
            }else{
               echo "filter data cant be show on view";
            }
     }
    }

    function add(){
      
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $tool =  json_decode($columnvalue->permission_tool); }

if((!empty($tool) && $tool->add == "true") || $this->session->userdata('user_role') == "admin"){
        $service_title = $this->input->post('service_title');
        $service_desc = $this->input->post('service_desc');
       
        $status = $this->input->post('status');
        
        if(!empty($_FILES['imageone']['name'])){
          $config['upload_path'] = './assets/images/tool';
          $config['allowed_types'] = 'jpg|jpeg|png|gif';
          $config['file_name'] = $_FILES['imageone']['name'];
          
          //Load upload library and initialize configuration
          $this->load->library('upload',$config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('imageone')){
              $uploadData = $this->upload->data();
              $imageone = $uploadData['file_name'];
          }else{
              $imageone = '';
          }
      }else{
          $imageone = '';
      }

      if(!empty($_FILES['imagetwo']['name'])){
        $config['upload_path'] = './assets/images/tool';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['file_name'] = $_FILES['imagetwo']['name'];
        
        //Load upload library and initialize configuration
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('imagetwo')){
            $uploadData = $this->upload->data();
            $imagetwo = $uploadData['file_name'];
        }else{
            $imagetwo = '';
        }
      }else{
        $imagetwo = '';
      }

        
     
        
 $result = $this->ToolModel->add($service_title,$service_desc,$status,$imageone,$imagetwo);
//  die;
        if ($result == 'true') {
          echo "<script>alert('Tool Added Successfully');
            window.location.href='../Tool';
            </script>";
          }else{
            redirect("admin/Tool");
        }
      }else{
        redirect("admin/Access");
      }
    }

 

    public function create()
    {
      if($this->session->userdata('usersid') != null){
        
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $tool =  json_decode($columnvalue->permission_tool); }

if((!empty($tool) && $tool->add == "true") || $this->session->userdata('user_role') == "admin"){
        $title['title']="Add Tool";
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        $this->load->view('admin/tool/create');
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }
        }else{
             redirect('Logout');
        }
   }

   function update(){
     
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $tool =  json_decode($columnvalue->permission_tool); }

if((!empty($tool) && $tool->update == "true") || $this->session->userdata('user_role') == "admin"){
    $tool_id = $this->input->post('tool_id');
    $service_title = $this->input->post('service_title');
    $service_desc = $this->input->post('service_desc');
   
    $status = $this->input->post('status');

    if(!empty($_FILES['imageone']['name'])){
      $config['upload_path'] = './assets/images/tool';
      $config['allowed_types'] = 'jpg|jpeg|png|gif';
      $config['file_name'] = $_FILES['imageone']['name'];
      
      //Load upload library and initialize configuration
      $this->load->library('upload',$config);
      $this->upload->initialize($config);
      if($this->upload->do_upload('imageone')){
          $uploadData = $this->upload->data();
          $imageone = $uploadData['file_name'];
      }else{
          $imageone = '';
      }
  }else{
    $imageone = $this->input->post('hiddenimageone');
  }

  if(!empty($_FILES['imagetwo']['name'])){
    $config['upload_path'] = './assets/images/tool';
    $config['allowed_types'] = 'jpg|jpeg|png|gif';
    $config['file_name'] = $_FILES['imagetwo']['name'];
    
    //Load upload library and initialize configuration
    $this->load->library('upload',$config);
    $this->upload->initialize($config);
    if($this->upload->do_upload('imagetwo')){
        $uploadData = $this->upload->data();
        $imagetwo = $uploadData['file_name'];
    }else{
        $imagetwo = '';
    }
  }else{
    $imagetwo = $this->input->post('hiddenimagetwo');
  }
$result = $this->ToolModel->update($service_title,$service_desc,$status,$imageone,$imagetwo,$tool_id);

        if ($result == 1) {
          echo "<script>alert('Tool Updated Successfully');
            window.location.href='../Tool';
            </script>";
          }else{
        redirect("admin/Tool");
        }
      }else{
        redirect("admin/Access");
      }
    }



    function loaddata(){
   
      $id = $this->input->post("id");
      $data['tool'] = $this->ToolModel->loaddata($id);
      $this->load->view('admin/tool/update',$data);
      
    }

    function loaddataview(){
   
      $id = $this->input->post("id");
      $data['tool'] = $this->ToolModel->loaddata1($id);
     
      $this->load->view('admin/tool/view',$data);
      
    }

   function exportcsv(){
     // file name 
    $filename = 'tool'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
     // get data 
    $usersData = $this->ToolModel->getUserDetails();
    // file creation 
    $file = fopen('php://output','w');
    $header = array("Id","Tool Title","Tool Description","Tool Status","Tool Created Date","Tool Modified Date"); 
    fputcsv($file, $header);
    foreach ($usersData as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
    }
    
    }

    