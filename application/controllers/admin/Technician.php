<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Technician extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/TechnicianModel');
            $this->load->helper('form'); 
	  }

     public function index()
     {
      if($this->session->userdata('usersid') != null){
        
  
    if(!empty($columnvalue)){ $technician =  json_decode($columnvalue->permission_technician); }

if((!empty($technician) && $technician->view == "true") || $this->session->userdata('user_role') == "admin"){

        $title['title']="All Technicians";
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        
        $data['technician'] = $this->TechnicianModel->view();
        $this->load->view('admin/technician/alltechnician',$data);
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }  
        }else{
             redirect('Logout');
        }
    }


    function searchvalue(){

     $filterstatus  = $this->input->post('filterstatus'); 
     if($filterstatus === 'all'){
       return redirect('admin/Technician');
     }else{
     $data['technician']= $this->TechnicianModel->getstatusWhereLike($filterstatus); 
            if($data){ 
              $title['title']="All Technician";
              $this->load->view('admin/layouts/header',$title);
              $this->load->view('admin/layouts/navbar');
              $this->load->view('admin/technician/alltechnician',$data);
              $this->load->view('admin/layouts/footer');
            }else{
               echo "filter data cant be show on view";
            }
     }
    }

    function add(){
           
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $technician =  json_decode($columnvalue->permission_technician); }

if((!empty($technician) && $technician->add == "true") || $this->session->userdata('user_role') == "admin"){
        // print_r($this->input->post()) ;exit;
        
        $vehiclename = $this->input->post('vehiclename');
        $vehiclename=implode(",",$vehiclename);
        
        $expertise = $this->input->post('expertise');
        $expertise=implode(",",$expertise);

        $technician_name = $this->input->post('technician_name');
        $technician_email = $this->input->post('technician_email');
        $technician_aadhar = $this->input->post('technician_aadhar');
        $technician_pan = $this->input->post('technician_pan');
        $technician_address = $this->input->post('technician_address');
        $technician_pincode = $this->input->post('technician_pincode');
        $technician_phonenumber = $this->input->post('technician_phonenumber');
        
        $technician_alternatephonenumber = $this->input->post('technician_alternatephonenumber');
        $latlong = $this->input->post('latlong');
        $technician_bankacc = $this->input->post('technician_bankacc');

        $ifscnumber = $this->input->post('ifscnumber');
        $gstnumber = $this->input->post('gstnumber');
        $status = $this->input->post('status');
        $technician_type = $this->input->post('technician_type');
        $working_type = $this->input->post('working_type');
        $technician_order_distance = $this->input->post('technician_order_distance');
        $weekly_available_hours = $this->input->post('weekly_available_hours');
        if(isset($_POST['technician_certificate']))
         {
             $technician_certificate = array();
               foreach($_POST['technician_certificate'] as $val)
                {
                   $technician_certificate[] = $val;
                }
             $technician_certificate = implode(',', $technician_certificate); }
             $data =$technician_certificate; 
        if(!empty($_FILES['imageone']['name'])){
          $config['upload_path'] = './assets/images/technician';
          $config['allowed_types'] = 'jpg|jpeg|png|gif';
          $config['file_name'] = $_FILES['imageone']['name'];
          
          //Load upload library and initialize configuration
          $this->load->library('upload',$config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('imageone')){
              $uploadData = $this->upload->data();
              $imageone = $uploadData['file_name'];
          }else{
              $imageone = '';
          }
      }else{
          $imageone = '';
      }
      if(!empty($_FILES['imagetwo']['name'])){
        $config['upload_path'] = './assets/images/technician';
        $config['allowed_types'] = 'pdf|doc|docx';
         $config['file_name'] = $_FILES['imagetwo']['name'];
        
        //Load upload library and initialize configuration
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('imagetwo')){
            $uploadData = $this->upload->data();
            $imagetwo = $uploadData['file_name'];
        }else{
            $imagetwo = '';
        }
    }else{
        $imagetwo = '';
    }
    // var_dump($imagetwo);die;
    $long = $this->input->post('long');
 $result = $this->TechnicianModel->add($technician_name,$technician_email,$technician_aadhar,$technician_pan,$technician_address,$technician_pincode,$technician_phonenumber,$latlong,$technician_bankacc,$ifscnumber,$gstnumber,$technician_type,$vehiclename,$expertise,$status,$imageone,$imagetwo,$technician_alternatephonenumber,$long,$working_type,$technician_order_distance,$data,$weekly_available_hours);
//  die;
        if ($result == 'true') {
          echo "<script>alert('Technician Added Successfully');
           window.location.href='../Technician';
            </script>";
          }else{
            redirect("admin/Technician");
        }
      }else{
        redirect("admin/Access");
      }
    }

 

    public function create()
    {
      if($this->session->userdata('usersid') != null){
        
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $technician =  json_decode($columnvalue->permission_technician); }

if((!empty($technician) && $technician->add == "true") || $this->session->userdata('user_role') == "admin"){
        $title['title']="Add Technician";
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        $this->load->view('admin/technician/create');
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }
        }else{
             redirect('Logout');
        }
   }

   function update(){
     
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $technician =  json_decode($columnvalue->permission_technician); }

if((!empty($technician) && $technician->update == "true") || $this->session->userdata('user_role') == "admin"){
    $puser_id = $this->input->post('puser_id');
      $vehiclename = $this->input->post('vehiclename');
        $vehiclename=implode(",",$vehiclename);
        $expertise = $this->input->post('expertise');
        $expertise=implode(",",$expertise);
    
    $technician_name = $this->input->post('technician_name');
    $technician_type = $this->input->post('technician_type');
        $technician_email = $this->input->post('technician_email');
        $technician_aadhar = $this->input->post('technician_aadhar');
        $technician_pan = $this->input->post('technician_pan');
        $technician_address = $this->input->post('technician_address');
        $technician_pincode = $this->input->post('technician_pincode');
        $technician_alternatephonenumber = $this->input->post('technician_alternatephonenumber');
        $technician_phonenumber = $this->input->post('technician_phonenumber');
        $latlong = $this->input->post('latlong');
        $technician_bankacc = $this->input->post('technician_bankacc');
        $long = $this->input->post('long');
        $ifscnumber = $this->input->post('ifscnumber');
        $gstnumber = $this->input->post('gstnumber');
        $status = $this->input->post('status');
        $working_type = $this->input->post('working_type');
        $technician_order_distance = $this->input->post('technician_order_distance');
         $weekly_available_hours = $this->input->post('weekly_available_hours');
          if(isset($_POST['technician_certificate']))
         {
             $technician_certificate = array();
               foreach($_POST['technician_certificate'] as $val)
                {
                   $technician_certificate[] = $val;
                }
             $technician_certificate = implode(',', $technician_certificate); }
             $data =$technician_certificate; //print_r($data); die;

        if(!empty($_FILES['imageone']['name'])){
          $config['upload_path'] = './assets/images/technician';
          $config['allowed_types'] = 'jpg|jpeg|png|gif';
          $config['file_name'] = $_FILES['imageone']['name'];
          
          //Load upload library and initialize configuration
          $this->load->library('upload',$config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('imageone')){
              $uploadData = $this->upload->data();
              $imageone = $uploadData['file_name'];
          }else{
              $imageone = '';
          }
      }else{
        $imageone = $this->input->post('hiddenimageone');
      }
      if(!empty($_FILES['imagetwo']['name'])){
        $config['upload_path'] = './assets/images/technician';
        $config['allowed_types'] = 'pdf|doc|docx';
         $config['file_name'] = $_FILES['imagetwo']['name'];
        
        //Load upload library and initialize configuration
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('imagetwo')){
            $uploadData = $this->upload->data();
            $imagetwo = $uploadData['file_name'];
        }else{
            $imagetwo = '';
        }
    }else{
      $imagetwo = $this->input->post('hiddenimagetwo');
    }
   
$result = $this->TechnicianModel->update($technician_name,$technician_email,$technician_aadhar,$technician_pan,$technician_address,$technician_pincode,$technician_phonenumber,$latlong,$technician_bankacc,$ifscnumber,$gstnumber,$technician_type,$expertise,$vehiclename,$status,$imageone,$imagetwo,$puser_id,$technician_alternatephonenumber,$long,$working_type,$technician_order_distance,$weekly_available_hours,$data);

        if ($result == 1) {
          echo "<script>alert('Technician Updated Successfully');
            window.location.href='../Technician';
            </script>";
          }else{
        redirect("admin/Technician");
        }
      }else{
        redirect("admin/Access");
      }
    }



    function loaddata(){
   
      $id = $this->input->post("id");
      
      $data['technician'] = $this->TechnicianModel->loaddata($id);
      // print_r(implode(",", pieces)$data['technician'][0]->expertise);exit;
       $exp =explode(",",$data['technician'][0]->expertise);
    //   print_r($data['technician']);exit;
         $data['exp'] = $exp;                        
     $veh_make =explode(",",$data['technician'][0]->puser_veh_id);
    //   print_r($data['technician']);exit;
         $data['veh_make'] = $veh_make;                        
      $this->load->view('admin/technician/update',$data);
      
    }

    function loaddataview(){
   
      $id = $this->input->post("id");
      
      $data['technician'] = $this->TechnicianModel->loaddata1($id);
     
      $this->load->view('admin/technician/view',$data);
      
    }

    function phonenocheckexist(){
      $username = $_POST['key'];
        $exists = $this->TechnicianModel->phonenocheckexist($username);
    
        $count = count($exists);
        
    
        if (empty($count)) {
            echo 1;
        } else {
            echo 2;
        }
    }
    
    function emailcheckexist(){
        $username = $_POST['key'];
          $exists = $this->TechnicianModel->emailcheckexist($username);
      
          $count = count($exists);
          
      
          if (empty($count)) {
              echo 1;
          } else {
              echo 2;
          }
      }

      function tool(){
        //SELECT tool_title,tool_thumbnail,tbl_puser_tools.p_usertool_status FROM `tbl_tools` LEFT join tbl_puser_tools on tbl_puser_tools.p_usertool_id=tbl_tools.tool_id where technician_id='23'
         
        if($this->session->userdata('usersid') != null){
        
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $technician =  json_decode($columnvalue->permission_technician); }

if((!empty($technician) && $technician->view == "true") || $this->session->userdata('user_role') == "admin"){

        $title['title']="All Technicians";
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        // technican id from puser_tbl..
        $id = $this->uri->segment(4);
        $dataa = $this->TechnicianModel->loadtechnician($id); 
        $data = $this->TechnicianModel->tool($id);// echo "<pre>"; print_r($data); echo "</pre>";die; 
        $this->load->view('admin/technician/alltool',['dataa' =>$dataa,'data'=>$data]);
       // $data['technician'] = $this->TechnicianModel->tool($id);
       // $this->load->view('admin/technician/alltool',$data,$dataa);
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }  
        }else{
             redirect('Logout');
        }

      }

      function getModel(){
         $id = $this->input->post("id");
        $data = $this->TechnicianModel->loaddata2($id);
        $data1 = $this->TechnicianModel->loaddata3();
        $this->load->view('admin/technician/modalview',['data'=>$data,'data1'=>$data1]);
      }

      function Updatemodel(){
        $technician_id =$this->input->post("technician_id");
           if(isset($_POST['tool'])){
             foreach($_POST['tool'] as $tar_value){
                $tooloption=$tar_value;
                $result=$this->TechnicianModel->updatemodeldata($tooloption,$technician_id);
              }
            }

             if ($result == 'true') {
                         echo "<script>alert('Technician Tool Added Successfully');
                               window.location.href='tool/$technician_id';
                                </script>";
                                //redirect('admin/Technician/tool/'.$technician_id);
                 }else{
                          redirect("admin/Technician");
                 }
      }


      function deletedata(){
        $technician_id =$this->input->get("technician_id");
         $id = $this->uri->segment(4);
         $result=$this->TechnicianModel->deletetechnicaltool($id);
          if ($result == 'true') {
                         echo "<script>alert('Technician Added Successfully');
                                window.location.href='../tool/$technician_id';
                                </script>";
                               // redirect('admin/Technician/tool/'.$technician_id);
                 }else{
                          redirect("admin/Technician");
                 }
      }

      // services
      function service(){
         if($this->session->userdata('usersid') != null){
           foreach($this->session->userdata('permission') as $columnvalue){
            }
         if(!empty($columnvalue)){ $technician =  json_decode($columnvalue->permission_technician); }
           if((!empty($technician) && $technician->view == "true") || $this->session->userdata('user_role') == "admin"){
                  $title['title']="All Technicians";
                  $this->load->view('admin/layouts/header',$title);
                  $this->load->view('admin/layouts/navbar');
                  // technican id from puser_tbl..
                  $id = $this->uri->segment(4);
                  $dataa = $this->TechnicianModel->loadtechnician($id); 
                  $data = $this->TechnicianModel->service($id);// echo "<pre>"; print_r($data); echo "</pre>";die; 
                  $this->load->view('admin/technician/allservice',['dataa' =>$dataa,'data'=>$data]);
                  $this->load->view('admin/layouts/footer');
            }else{
                  redirect("admin/Access");
            }  
          }else{
                  redirect('Logout');
          }
      }

      function getModelservice(){
          $id = $this->input->post("id");
          $data = $this->TechnicianModel->loaddata2($id);
          $data1 = $this->TechnicianModel->loaddata4();
        $this->load->view('admin/technician/servicemodalview',['data'=>$data,'data1'=>$data1]);
      }

      function Updateservicemodel(){
        $technician_id =$this->input->post("technician_id"); 
           if(isset($_POST['service'])){
             foreach($_POST['service'] as $tar_value){
                $serviceoption=$tar_value;  
                $result=$this->TechnicianModel->updateservicemodeldata($serviceoption,$technician_id);

              }
            }

             if ($result == 'true') {
                         echo "<script>alert('Technician Service Added Successfully!!');
                         window.location.href='service/$technician_id';
                                </script>";
                                //redirect('admin/Technician/service/'.$technician_id);
                 }else{
                          redirect("admin/Technician");
                 }

      }

      function exportcsv2(){
       $id = $this->uri->segment(4);
        $filename = 'Technician_service_'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
     // get data 
    $usersData = $this->TechnicianModel->getUserDetails2($id);
    // file creation 
    $file = fopen('php://output','w');
    $header = array("Service Id","Service Title","Service Thumbnail","Service Created Date","Service Status"); 
    fputcsv($file, $header);
    foreach ($usersData as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
     
    }

      function deleteservicedata(){
        $technician_id =$this->input->get("technician_id");
         $id = $this->uri->segment(4); 
         $result=$this->TechnicianModel->deletetechnicalservice($id);
          if ($result == 'true') {
                         echo "<script>alert('Service Deleted Successfully');
                               window.location.href='../service/$technician_id';
                                </script>";
                                //redirect('admin/Technician/service/'.$technician_id);
                 }else{
                          redirect("admin/Technician");
                 }
      }

       function exportcsv(){
     // file name 
    $filename = 'Technician'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
     // get data 
    $usersData = $this->TechnicianModel->getUserDetails();
    // file creation 
    $file = fopen('php://output','w');
    $header = array("User Id","Technician Name","Technician Address","Technician Pincode","Technician Phone Number","Technician Email","Technician Aadhar Number","Technician Pan Number"," GST Number","Bank Number","Technician IFSC","Technician Availability Status","Technician Status","User Type"); 
    fputcsv($file, $header);
    foreach ($usersData as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
    }
    function vehiclelist()
    {
        $this->load->database();
        if(!empty($this->input->get("q")))
        {
          $q = $this->input->get("q");
            //$sql = "select vehb_id,vehb_make,vehb_model,vehb_trim from tbl_vehicle  where vehb_make LIKE '%$q%' and "vehb_status"='Active' ";
          $sql = "select vehb_id,vehb_make,vehb_model,vehb_trim from tbl_vehicle  where vehb_make LIKE '%$q%'";
            // echo $sql;die;
          $query = $this->db->query($sql);
          $json = $query->result();
        }else
        {
            //$sql = "select vehb_id,vehb_make,vehb_model,vehb_trim from tbl_vehicle where "vehb_status"='Active'";
          $sql = "select vehb_id,vehb_make,vehb_model,vehb_trim from tbl_vehicle ";
            // echo $sql;die;
          $query = $this->db->query($sql);
          $json = $query->result(); 
        }
        // print_r($json);exit;
      echo json_encode($json);
    }

    function exportcsv1(){
       $id = $this->uri->segment(4);
        $filename = 'Technician_tool_'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
     // get data 
    $usersData = $this->TechnicianModel->getUserDetails1($id);
    // file creation 
    $file = fopen('php://output','w');
    $header = array("Tool Id","Tool Title","Tool Thumbnail","Tool Status","Tool Created Date"); 
    fputcsv($file, $header);
    foreach ($usersData as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
      
    }
    
    
 }

    