<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class UserPollution extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/UserPollutionModel');
            $this->load->helper('form'); 
	  }

     public function index()
     {
   
      if($this->session->userdata('usersid') != null){
        foreach($this->session->userdata('permission') as $columnvalue){
        }
        if(!empty($columnvalue)){ $insurance =  json_decode($columnvalue->permission_insurance); }
if((!empty($insurance) && $insurance->view == "true") || $this->session->userdata('user_role') == "admin"){
        $title['title']="All Insurance";
        $data['userpollution'] = $this->UserPollutionModel->view();
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        $this->load->view('admin/userpollution/alluserpollution',$data);
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }
        }else{
             redirect('Logout');
        }
    }

    function loaddataview(){
   
      $id = $this->input->post("id");
      $muser_id = $this->input->post("muser_id");
      $pol_id = $this->input->post("pol_id");
      $data= $this->UserPollutionModel->loaddata1($id,$muser_id);
      $data2 = $this->UserPollutionModel->loaddata2($id,$pol_id);
     $this->load->view('admin/userpollution/view',['data'=>$data,'data2'=>$data2]);
      
    }

    function searchvalue(){

     $filterstatus  = $this->input->post('filterstatus'); 
     if($filterstatus === 'all'){
       return redirect('admin/UserPollution');
     }else{
     $data['userpollution']= $this->UserPollutionModel->getstatusWhereLike($filterstatus); 
            if($data){ 
              $title['title']="All User Pollution";
              $this->load->view('admin/layouts/header',$title);
              $this->load->view('admin/layouts/navbar');
              $this->load->view('admin/userpollution/alluserpollution',$data);
              $this->load->view('admin/layouts/footer');
            }else{
               echo "filter data cant be show on view";
            }
     }
    }

   function exportcsv(){
     // file name 
    $filename = 'userpollution_'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
     // get data 
    $usersData = $this->UserPollutionModel->getUserDetails();
    // file creation 
    $file = fopen('php://output','w');
    $header = array("ID","Pollution Id","User Name","","Remark","Status","Created Date"); 
    fputcsv($file, $header);
    foreach ($usersData as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
    }
    
    }

    