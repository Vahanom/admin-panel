<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class AllOrders extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/AllOrdersModel');
            $this->load->helper('form'); 
	  }

     public function index()
     {
      if($this->session->userdata('usersid') != null){

    
        foreach($this->session->userdata('permission') as $columnvalue){
        }
       
        if(!empty($columnvalue)){ $emplyee =  json_decode($columnvalue->permission_employee); }


         if((!empty($emplyee) && $emplyee->view == "true") || $this->session->userdata('user_role') == "admin"){
     
          $title['title']="All Orders";
          $this->load->view('admin/layouts/header',$title);
          $this->load->view('admin/layouts/navbar');
          $data['allorders'] = $this->AllOrdersModel->view();
          $this->load->view('admin/allOrders/allorders',$data);
          $this->load->view('admin/layouts/footer');
        }else{
          redirect("admin/Access");
        }
    
        }else{
             redirect('Logout');
        }
    }

    function loaddataview(){
      foreach($this->session->userdata('permission') as $columnvalue){
      }
     
         if(!empty($columnvalue)){ $emplyee =  json_decode($columnvalue->permission_employee); }
  
  
            if((!empty($emplyee) && $emplyee->view == "true") || $this->session->userdata('user_role') == "admin"){
             $id = $this->input->post("id");
             // print_r($id);exit;
             $type_servicespare = $this->input->post("type_servicespare");
             $allorders = $this->AllOrdersModel->loaddata1($id);
             $data1 = $this->AllOrdersModel->loaddata2($id);
             $data2 = $this->AllOrdersModel->loaddata3($id,$type_servicespare);
             $this->load->view('admin/allOrders/view',['allorders'=>$allorders,'data1'=>$data1,'data2'=>$data2]);
           }else{
              redirect("admin/Access");
        }
      
    }

    function searchvalue(){
     $orderstatus  = $this->input->post('orderstatus'); 
     if($orderstatus === 'all'){
       return redirect('admin/AllOrders');
     }else{
     $data['allorders']= $this->AllOrdersModel->getstatusWhereLike($orderstatus); 
            if($data){ 
              $title['title']="All Orders";
              $this->load->view('admin/layouts/header',$title);
              $this->load->view('admin/layouts/navbar');
              $this->load->view('admin/allOrders/allorders',$data);
              $this->load->view('admin/layouts/footer');
            }else{
               echo "filter data cant be show on view";
            }
     }
    }

    function exportcsv(){
     // file name 
    $filename = 'order_'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
     // get data 
    $usersData = $this->AllOrdersModel->getUserDetails();
    // file creation 
    $file = fopen('php://output','w');
    $header = array("Order Type","Order Date","Order Address","Order Pincode","Technician Accept Status","Supplier Accept Status","Order Status"); 
    fputcsv($file, $header);
    foreach ($usersData as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
    }

 }

    