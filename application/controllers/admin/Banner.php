<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Banner extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/BannerModel');
            $this->load->helper('form'); 
	  }

     public function index()
     {
      if($this->session->userdata('usersid') != null){
        foreach($this->session->userdata('permission') as $columnvalue){
        }
        if(!empty($columnvalue)){ $banner =  json_decode($columnvalue->permission_banner); }
  
if((!empty($banner) && $banner->view == "true") || $this->session->userdata('user_role') == "admin"){

        $title['title']="All Banner/Pop-up";
        $data['banner'] = $this->BannerModel->view();
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        $this->load->view('admin/banner/allbanner',$data);
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }
        }else{
             redirect('Logout');
        }
    }

    function searchvalue(){

     $filterstatus  = $this->input->post('filterstatus'); 
     if($filterstatus === 'all'){
       return redirect('admin/Banner');
     }else{
     $data['banner']= $this->BannerModel->getstatusWhereLike($filterstatus); 
            if($data){ 
              $title['title']="All Banner";
              $this->load->view('admin/layouts/header',$title);
              $this->load->view('admin/layouts/navbar');
              $this->load->view('admin/banner/allbanner',$data);
              $this->load->view('admin/layouts/footer');
            }else{
               echo "filter data cant be show on view";
            }
     }
    }

    function add(){
      foreach($this->session->userdata('permission') as $columnvalue){
      }
      if(!empty($columnvalue)){ $banner =  json_decode($columnvalue->permission_banner); }

if((!empty($banner) && $banner->add == "true") || $this->session->userdata('user_role') == "admin"){

        $bannertitle = $this->input->post('bannertitle');
        $banner_desc = $this->input->post('banner_desc');
        $bannertype = $this->input->post('bannertype');
       
        $status = $this->input->post('status');
        
        if(!empty($_FILES['imageone']['name'])){
         
          $config['upload_path'] = './assets/images/banner';
          $config['allowed_types'] = 'jpg|jpeg|png|gif';
          $config['file_name'] = $_FILES['imageone']['name'];
          
          //Load upload library and initialize configuration
          $this->load->library('upload',$config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('imageone')){
              $uploadData = $this->upload->data();
              $imageone = $uploadData['file_name'];
          }else{
           
              $imageone = '';
            
          }
      }else{
          $imageone = '';
      }

      if(!empty($_FILES['imagetwo']['name'])){
        $config['upload_path'] = './assets/images/banner';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['file_name'] = $_FILES['imagetwo']['name'];
        
        //Load upload library and initialize configuration
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('imagetwo')){
            $uploadData = $this->upload->data();
            $imagetwo = $uploadData['file_name'];
        }else{
            $imagetwo = '';
        }
      }else{
        $imagetwo = '';
      }

        
     
       
 $result = $this->BannerModel->add($bannertitle,$banner_desc,$bannertype,$status,$imageone,$imagetwo);
//  die;
        if ($result == 'true') {
          echo "<script>alert('Banner/Pop-up Added Successfully');
            window.location.href = 'index';
            </script>";
          }else{
            redirect("admin/Banner");
        }
      }else{
        redirect("admin/Access");
      }
    }

 

    public function create()
    {
      if($this->session->userdata('usersid') != null){
        foreach($this->session->userdata('permission') as $columnvalue){
        }
        if(!empty($columnvalue)){ $banner =  json_decode($columnvalue->permission_banner); }
  
if((!empty($banner) && $banner->add == "true") || $this->session->userdata('user_role') == "admin"){

        $title['title']="Add Banner/Pop-up";
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        $this->load->view('admin/banner/create');
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      } }else{
             redirect('Logout');
        }
   }

   function update(){
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $banner =  json_decode($columnvalue->permission_banner); }

if((!empty($banner) && $banner->update == "true") || $this->session->userdata('user_role') == "admin"){

    $ban_id = $this->input->post('ban_id');
    $bannertitle = $this->input->post('bannertitle');
    $banner_desc = $this->input->post('banner_desc');
    $bannertype = $this->input->post('bannertype');
   
    $status = $this->input->post('status');

    if(!empty($_FILES['imageone']['name'])){
      $config['upload_path'] = './assets/images/banner';
      $config['allowed_types'] = 'jpg|jpeg|png|gif';
      $config['file_name'] = $_FILES['imageone']['name'];
      
      //Load upload library and initialize configuration
      $this->load->library('upload',$config);
      $this->upload->initialize($config);
      if($this->upload->do_upload('imageone')){
          $uploadData = $this->upload->data();
          $imageone = $uploadData['file_name'];
      }else{
          $imageone = '';
      }
  }else{
    $imageone = $this->input->post('hiddenimageone');
  }

  if(!empty($_FILES['imagetwo']['name'])){
    $config['upload_path'] = './assets/images/banner';
    $config['allowed_types'] = 'jpg|jpeg|png|gif';
    $config['file_name'] = $_FILES['imagetwo']['name'];
    
    //Load upload library and initialize configuration
    $this->load->library('upload',$config);
    $this->upload->initialize($config);
    if($this->upload->do_upload('imagetwo')){
        $uploadData = $this->upload->data();
        $imagetwo = $uploadData['file_name'];
    }else{
        $imagetwo = '';
    }
  }else{
    $imagetwo = $this->input->post('hiddenimagetwo');
  }
$result = $this->BannerModel->update($bannertitle,$banner_desc,$bannertype,$status,$imageone,$imagetwo,$ban_id);

        if ($result == 1) {
          echo "<script>alert('Banner/Pop-up Updated Successfully');
            window.location.href = 'index';
            </script>";
          }else{
        redirect("admin/Banner");
        }
      }else{
        redirect("admin/Access");
      }
    }



    function loaddata(){
   
      $id = $this->input->post("id");
      $data['banner'] = $this->BannerModel->loaddata($id);
      $this->load->view('admin/banner/update',$data);
      
    }

    function loaddataview(){
   
      $id = $this->input->post("id");
      $data['banner'] = $this->BannerModel->loaddata1($id);
     
      $this->load->view('admin/banner/view',$data);
      
    }

     function exportcsv(){
     // file name 
    $filename = 'banner_'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
     // get data 
    $usersData = $this->BannerModel->getUserDetails();
    // file creation 
    $file = fopen('php://output','w');
    $header = array("ban_id","ban_title","ban_description","ban_type","ban_status"); 
    fputcsv($file, $header);
    foreach ($usersData as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
    }
    
    }

    