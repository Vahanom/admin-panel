<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Sparecat extends CI_Controller {
    
    function __construct()
    {
		      	parent::__construct();				
            $this->load->library('session');
            $this->load->model('admin/SparecatModel');
            $this->load->helper('form'); 
	  }

     public function index()
     {
      if($this->session->userdata('usersid') != null){
        
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $sparecategory =  json_decode($columnvalue->permission_sparecategory); }

if((!empty($sparecategory) && $sparecategory->view == "true") || $this->session->userdata('user_role') == "admin"){
        $title['title']="All Spare Category";
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        
        $data['sparecat'] = $this->SparecatModel->view();
        $this->load->view('admin/sparecat/allsparecat',$data);
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }  
        }else{
             redirect('Logout');
        }
    }

    function searchvalue(){

     $filterstatus  = $this->input->post('filterstatus'); 
     if($filterstatus === 'all'){
       return redirect('admin/Sparecat');
     }else{
     $data['sparecat']= $this->SparecatModel->getstatusWhereLike($filterstatus); 
            if($data){ 
              $title['title']="All Spare Category";
              $this->load->view('admin/layouts/header',$title);
              $this->load->view('admin/layouts/navbar');
              $this->load->view('admin/sparecat/allsparecat',$data);
              $this->load->view('admin/layouts/footer');
            }else{
               echo "filter data cant be show on view";
            }
     }
    }

    function add(){
      
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $sparecategory =  json_decode($columnvalue->permission_sparecategory); }

if((!empty($sparecategory) && $sparecategory->add == "true") || $this->session->userdata('user_role') == "admin"){
        $space_categorytitle = $this->input->post('space_categorytitle');
        $spare_desc = $this->input->post('spare_desc');
        $type_vehicle = $this->input->post('type_vehicle');
        $status = $this->input->post('status');

        
     
        
 $result = $this->SparecatModel->add($space_categorytitle,$spare_desc,$type_vehicle,$status);
//  die;
        if ($result == 'true') {
          echo "<script>alert('Space Category Added Successfully');
            window.location.href='../Sparecat';
            </script>";
          }else{
            redirect("admin/Vehicle");
        }
      }else{
        redirect("admin/Access");
      }  
    }

 

    public function create()
    {
      if($this->session->userdata('usersid') != null){
        
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $sparecategory =  json_decode($columnvalue->permission_sparecategory); }

if((!empty($sparecategory) && $sparecategory->add == "true") || $this->session->userdata('user_role') == "admin"){
        $title['title']="Add Spare Category";
        $this->load->view('admin/layouts/header',$title);
        $this->load->view('admin/layouts/navbar');
        $this->load->view('admin/sparecat/create');
        $this->load->view('admin/layouts/footer');
      }else{
        redirect("admin/Access");
      }  
        }else{
             redirect('Logout');
        }
   }

   function update(){
     
    foreach($this->session->userdata('permission') as $columnvalue){
    }
    if(!empty($columnvalue)){ $sparecategory =  json_decode($columnvalue->permission_sparecategory); }

if((!empty($sparecategory) && $sparecategory->update == "true") || $this->session->userdata('user_role') == "admin"){
    $spcat_id = $this->input->post('spcat_id');
    $space_categorytitle = $this->input->post('space_categorytitle');
        $spare_desc = $this->input->post('spare_desc');
        $type_vehicle = $this->input->post('type_vehicle');
        $status = $this->input->post('status');

        
    
   
$result = $this->SparecatModel->update($space_categorytitle,$spare_desc,$type_vehicle,$status,$spcat_id);

        if ($result == 1) {
          echo "<script>alert('Spare Category Updated Successfully');
            window.location.href='../Sparecat';
            </script>";
          }else{
        redirect("admin/Sparecat");
        }
      }else{
        redirect("admin/Access");
      }  
    }

    function namecheckexist()
    {
        $username = $_POST['key'];
          $exists = $this->SparecatModel->namecheckexist1($username);
      
          $count = count($exists);
          
      
          if (empty($count)) {
              echo 1;
          } else {
              echo 2;
          }
      }


    function loaddata(){
   
      $id = $this->input->post("id");
      
      $data['vehicle'] = $this->SparecatModel->loaddata($id);
     
      $this->load->view('admin/sparecat/update',$data);
      
    }

    function loaddataview(){
   
      $id = $this->input->post("id");
      
      $data['vehicle'] = $this->SparecatModel->loaddata1($id);
     
      $this->load->view('admin/sparecat/view',$data);
      
    }

    function exportcsv(){
     // file name 
    $filename = 'spare_category'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
     // get data 
    $usersData = $this->SparecatModel->getUserDetails();
    // file creation 
    $file = fopen('php://output','w');
    $header = array("Id","Spare Category Title","Spare Category Description ","Spare Vehicle Type","Spare Category Status"); 
    fputcsv($file, $header);
    foreach ($usersData as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
    }
   
    
    }

    