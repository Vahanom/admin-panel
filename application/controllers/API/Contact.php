<?php
   
   require APPPATH . '/libraries/REST_Controller.php';
   use Restserver\Libraries\REST_Controller;
     
class Contact extends REST_Controller {
    //Sprint02C
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
     $this->load->model('API_Model/ContactModelAPI');
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get()
	{   
        $data = $this->ContactModelAPI->get_contact();
          if($data == 0){
               $this->response(array('success_code' => 401,'msg' => 'Something went wrong'));
          }else{
           $this->response(array('success_code' =>200,'msg' => 'success','data' => $data));
       }
	}
 public function index_post()
    {
        $name = $this->input->get('name'); 
        $email = $this->input->get('email');
        $contact_number = $this->input->get('contact_number');
        $message = $this->input->get('message');

        $data = $this->ContactModelAPI->add_contact($name,$email,$contact_number,$message);

        if($data == 0){
         $this->response(array('success_code' => 200,'msg' => 'success','data' => $data));
          }else{
           $this->response(array('success_code' =>200,'msg' => 'success','data' => $data));
       }
    } 

   
    	
}