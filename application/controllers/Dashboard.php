<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class Dashboard extends CI_Controller {

    function __construct() { 
        parent::__construct(); 
        $this->load->library('session'); 
        $this->load->helper('form'); 
        $this->load->model('admin/DashboardModel');
     }

     public function index()
     {
        if($this->session->userdata('usersid') != null){
            $title['title']="Dashboard";
               $this->load->view('admin/layouts/header',$title);
            $this->load->view('admin/layouts/navbar');
             //$this->load->view('modalresetpassword');
            $this->load->view('admin/dashboard',$data);
            $this->load->view('admin/layouts/footer');
            }else{
                 return redirect('Logout');
            }
     }

      public function pie_chart_js() { echo "sobiya"; die;
   
      $query =  $this->db->query("SELECT * FROM `tbl_notifications`"); 
 
      $record = $query->result();
      $data = [];
 
      foreach($record as $row) {
            $data['label'][] = $row->day_name;
            $data['data'][] = (int) $row->count;
      }
      $data['chart_data'] = json_encode($data);
      $this->load->view('admin/dashboard',$data);
    }

}