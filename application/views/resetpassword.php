<!DOCTYPE html>
<html>
<head>
<title>Enter OTP</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</head>
<body>

	<div class="container">
		<div class="error"></div>
		<form>
  <div class="form-group row">
    <label for="colFormLabel" class="col-sm-2 col-form-label">Password</label>
    <div class="col-sm-10">
      <input type="password" name="password" class="form-control" id="password" >
    </div>
  </div>
  <div class="form-group row">
    <label for="colFormLabel" class="col-sm-2 col-form-label">Confirm Password</label>
    <div class="col-sm-10">
      <input type="password" name='cpassword' class="form-control" id="cpassword" >
    </div>
  </div>
    
    <div class="col-auto">
      <div class="form-check mb-2">
        <input class="form-check-input" type="checkbox" id="autoSizingCheck">
        <label class="form-check-label" for="autoSizingCheck">
          Remember me
        </label>
      </div>
   
    <div class="col-auto">
      <button type="button" onclick="sendOTP();" class="btn btn-primary mb-2">Submit</button>
    </div>
  </div>
</form>
	</div>

	<script src="jquery-3.2.1.min.js" type="text/javascript"></script>
	<script src="verification.js"></script>



	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
function sendOTP(){
	var password=document.getElementById('password').value;
	var cpassword=document.getElementById('cpassword').value;

	if(password=='')
	{
		alert('Please Enter password');
				return false;
	}
	if(cpassword==''){
		alert('Please Enter Confirm password');
				return false;
	}
	if(password != cpassword)
	{
		alert('Please Enter Correct Pasword');
				return false;
	}
 $.ajax({
        url: '<?php echo site_url('login/updatepassword'); ?>',
        type: 'POST',
        data: {
            key: password
        },
        dataType: 'json',
        success: function(data) {
		
			if(data==2){
				alert('Password Updated Successfully');
				window.location.href="<?php echo base_url(); ?>login";
				
			}
        }
    });
}
</script>

</body>

</html>