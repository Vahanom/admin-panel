
					<!--begin::Content-->
					<div class="d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Subheader-->
					
						<!--end::Subheader-->
						<!--begin::Entry-->
						<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
							<div class="container">
								<div class="row">
									<div class="col-lg-12">
										<!--begin::Card-->
										<div class="card card-custom gutter-b example example-compact">
											
											<?php foreach($inspection as $row){} 
								// 			print_r($row);exit;
											?>
											<!--begin::Form-->
											
											<form class="form" method="post" id="resetform" action="<?php echo  base_url('admin/Inspection/update'); ?>" enctype="multipart/form-data" >
											<div class="card-body">
													<div class="form-group row">
														<div class="col-lg-12">
														<label>Inspection Title:</label>
														<input type="text" required  value="<?php echo $row->title; ?>" name="title" class="form-control" id="title" placeholder="Enter Inspection Title" />
                                                         <input type="hidden" required value="<?php echo $row->id; ?>" name="id"  class="form-control" id="id" placeholder="Enter English Title" />
                                                        
														</div></div>
														<div class="form-group row">
														<div class="col-lg-12">
														<label>Description:</label>
															<input type="text" required  value="<?php echo $row->description; ?>" name="description" class="form-control" id="description" placeholder="Enter Inspection Description" />
                                                        
                                                        </div>
                                                    </div>
													
													<div class="form-group row">
                                                       
													<div class="col-lg-12">
													<label>Thumbnail image:</label>
                                                            	<a target="_blank" href="<?php echo base_url() ?>/assets/images/inspection/<?php echo $row->thumbnail; ?>"><img style="width: 70px; height: 70px" src="<?php 
																if($row->thumbnail==""){
																	echo base_url('/assets/images/no-images/no-product.png'); 
																	}else{
																		echo base_url() ?>/assets/images/inspection/<?php echo  $row->thumbnail; 
																	} ?>"></a>
															    <input type="file"  accept="image/x-png,image/jpeg"  name="imageone" class="form-control" id="imageone" />
															 <input type="hidden" pattern=".*\S+.*"  name="hiddenimageone" value="<?php echo $row->thumbnail ?>"  class="form-control" />
													    
														</div>
														</div>

														<div class="form-group row">
                                                       
													   <div class="col-lg-12">
													   <label>Type:</label>
													   <select id="type" required class="form-control" name="type">
															<option value="<?php echo $row->type; ?>"><?php echo $row->type; ?></option>
															<?php  
															if($row->type=="Basic"){
																?>
																<option value="Comprehensive">Comprehensive</option>
																<?php
															}else{
																?>
																<option value="Basic">Basic</option>
																<?php
															} ?>
															</select>
															 </div>
															
												   </div>

												
												   <div class="form-group row">
                                                       
													   <div class="col-lg-12">
													   <label>Status:</label>
															<select class="form-control" name="status" required>
															<option value="<?php echo $row->status; ?>"><?php echo $row->status; ?></option>
															<?php  if($row->status!="Active"){
																?>
																<option value="Active">Active</option>
																<?php
															}else{
																?>
																<option value="Inactive">Inactive</option>
																<?php
															} ?>
															</select>
															 </div>
															
												   </div>
												   

												    <div class="form-group row">
														<div class="col-lg-12">
														<label>Price:</label>
															<input type="text" required  value="<?php echo $row->price; ?>" name="price" class="form-control" id="price" placeholder="Enter Price" />
                                                        
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
														<div class="col-lg-12">
														<label>Selling Price:</label>
															<input type="text" required  value="<?php echo $row->selling_price; ?>" name="selling_price" class="form-control" id="selling_price" placeholder="Enter Selling Price" />
                                                        
                                                        </div>
                                                    </div>
												   <div class="form-group row">
														<div class="col-lg-12">
													   <label>Inspection Type:</label>
														   <select id="pre_pur" required class="form-control" name="pre_pur">
															<option value="">Select Type</option>
															<option value="0" <?php if($row->expanded="0") echo "selected"; ?>>Inspection</option>
															<option value="1" <?php if($row->expanded="1") echo "selected"; ?>>Pre-Purchase</option>
															</select>
													 </div>
													</div>
												   <div class="form-group row">
														<div class="col-lg-12">
														   <label>Vehicle Type:</label>
														   <select id="veh_type" required class="form-control" name="veh_type">
															<option value="">Select Type</option>
															<option value="Bus" <?php if($row->vehicle_type="Bus") echo "selected"; ?>>Bus</option>
															<option value="Four-wheeler" <?php if($row->vehicle_type="Four-wheeler") echo "selected"; ?>>Four-wheeler</option>
															<option value="Truck" <?php if($row->vehicle_type="Truck") echo "selected"; ?>>Truck</option>
															<option value="Two-wheeler" <?php if($row->vehicle_type="Two-wheeler") echo "selected"; ?>>Two-wheeler</option>
															</select>
													</div>
													</div>
												   <div class="form-group row">
														<div class="col-lg-12">
															<label>GST Percentage:</label>
															<input type="text" required name="gst_per" class="form-control" value="<?php echo $row->gst_percentage; ?>" id="gst_per" placeholder="Enter Gst Percentage" />
                                                           
                                                        </div>
                                                    </div>
												   
												  
												</div>
												
											
												<div class="card-footer">
													<div class="row">
														<div class="col-lg-12">
															<button type="submit" class="btn btn-primary mr-2">Save</button>
															</div>
													</div>
												</div>
											</form>
											<!--end::Form-->
										</div>
										<!--end::Card-->
									</div>
								</div>
							</div>
							<!--end::Container-->
						</div>
						<!--end::Entry-->
					</div>
                    <!--end::Content-->
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                    
					<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>  
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
            <script>
            	document.getElementById("title").addEventListener('keydown', function (e) {
  if (this.value.length === 0 && e.which === 32) e.preventDefault();
  if (this.value.length === 0 && e.which === 48) e.preventDefault();
  //special character
  if (e.which === 56 || e.which === 53 || e.which === 51 || e.which === 49 || e.which === 50 || e.which === 52 || e.which === 48 ) e.preventDefault();
});
            </script>       