
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Subheader-->
					
						<!--end::Subheader-->
						<!--begin::Entry-->
						<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
							<div class="container">
								<div class="row">
									<div class="col-lg-12">
										<!--begin::Card-->
										<div class="card card-custom gutter-b example example-compact">
											
											<?php foreach($inspection as $row){} ?>
											<!--begin::Form-->
											
											<form class="form" method="post" id="resetform" action="<?php echo  base_url('admin/Insurance/update'); ?>" enctype="multipart/form-data" >
											<div class="card-body">
													<div class="form-group row">
														<div class="col-lg-12">
														<label>Inspection Title:</label>
														<input type="text" disabled required  value="<?php echo $row->title; ?>" name="service_title" class="form-control" id="title" placeholder="Enter Inspection Title" />
                                                         <input type="hidden" required value="<?php echo $row->id; ?>" name="id"  class="form-control" id="id" placeholder="Enter English Title" />
                                                        
														</div></div>
														<div class="form-group row">
														<div class="col-lg-12">
														<label>Description:</label>
															<input type="text" disabled required  value="<?php echo $row->description; ?>" name="description" class="form-control" id="description" placeholder="Enter Insurance Description" />
                                                        
                                                        </div>
                                                    </div>
													
													<div class="form-group row">
                                                       
													<div class="col-lg-12">
													<label>Thumbnail image:</label>
                                                            	<a target="_blank" href="<?php echo base_url() ?>/assets/images/inspection/<?php echo $row->thumbnail; ?>"><img style="width: 70px; height: 70px" src="<?php 
																if($row->thumbnail==""){
																	echo base_url('/assets/images/no-images/no-product.png'); 
																	}else{
																		echo base_url() ?>/assets/images/inspection/<?php echo  $row->thumbnail; 
																	} ?>" onerror="this.onerror=null;this.src='<?php echo base_url('/assets/images/noimage.png'); ?>';"></a>
															    <!-- <input type="file"  accept="image/x-png,image/jpeg"  name="imageone" class="form-control" id="imageone" /> -->
															 <input type="hidden" pattern=".*\S+.*"  name="hiddenimageone" value="<?php echo $row->thumbnail ?>"  class="form-control" />
													    
														</div>
														</div>

														 <div class="form-group row">
                                                       
													   <div class="col-lg-12">
													   <label>Type:</label>
													   <select id="type" disabled required class="form-control" name="type">
															<option value="<?php echo $row->type; ?>"><?php echo $row->type; ?></option>
															<?php  
															if($row->type=="Basic"){
																?>
																<option value="Comprehensive">Comprehensive</option>
																<?php
															}else{
																?>
																<option value="Basic">Basic</option>
																<?php
															} ?>
															</select>
															 </div>
															
												   </div>

												   <div class="form-group row">
                                                       
													   <div class="col-lg-12">
													   <label>Status:</label>
															<select disabled class="form-control" name="status" required>
															<option value="<?php echo $row->status; ?>"><?php echo $row->status; ?></option>
															<?php  if($row->status!="Active"){
																?>
																<option value="Active">Active</option>
																<?php
															}else{
																?>
																<option value="Inactive">Inactive</option>
																<?php
															} ?>
															</select>
															 </div>
															
												   </div>
												  

												    <div class="form-group row">
														<div class="col-lg-12">
														<label>Price:</label>
															<input type="text" disabled required  value="<?php echo $row->price; ?>" name="price" class="form-control" id="price" placeholder="Enter Price" />
                                                        
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
														<div class="col-lg-12">
														<label>Selling Price:</label>
															<input type="text" disabled required  value="<?php echo $row->selling_price; ?>" name="selling_price" class="form-control" id="selling_price" placeholder="Enter Selling Price" />
                                                        
                                                        </div>
                                                    </div>
												   
												  
												</div>
												
											
												<div class="card-footer">
													<div class="row">
														<div class="col-lg-12">
															<!-- <button type="submit" class="btn btn-primary mr-2">Save</button> -->
															</div>
													</div>
												</div>
											</form>
											<!--end::Form-->
										</div>
										<!--end::Card-->
									</div>
								</div>
							</div>
							<!--end::Container-->
						</div>
						<!--end::Entry-->
					</div>
                    <!--end::Content-->
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                    
					<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>  
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
