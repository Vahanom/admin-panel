
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Subheader-->
					
						<!--end::Subheader-->
						<!--begin::Entry-->
						<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
							<div class="container">
								<div class="row">
									<div class="col-lg-12">
										<!--begin::Card-->
										<div class="card card-custom gutter-b example example-compact">
											
											<?php foreach($servicecat as $row){} ?>
											<!--begin::Form-->
											
											<form class="form" method="post" id="resetform" action="<?php echo  base_url('admin/Servicecat/update'); ?>" enctype="multipart/form-data" >
											<div class="card-body">
													<div class="form-group row">
														<div class="col-lg-12">
														<label>Service Category Title:</label>
															<input type="text" required name="service_title" value="<?php echo $row->ser_cat_title;?>" class="form-control" id="service_title" placeholder="Enter Service Category Title" />
                                                         <input type="hidden" required name="ser_catid" value="<?php echo $row->ser_catid;?>" class="form-control" id="ser_catid" placeholder="Enter English Title" />
                                                        
														</div></div>
														<div class="form-group row">
														<div class="col-lg-12">
														<label>Service Category Description:</label>
															<input type="text" required name="service_desc" value="<?php echo $row->ser_cat_description;?>"  class="form-control" id="service_desc" placeholder="Enter Service Category Description" />
                                                         
                                                        </div>
                                                    </div>
													<div class="form-group row">
														<div class="col-lg-12">
														<label>Service Category Vehicle Type:</label>
															<select id="type_vehicle" required class="form-control" name="type_vehicle">
															<option  value="<?php echo $row->vehicle_type;?>" ><?php echo $row->vehicle_type;?></option>
															<option value="Four-wheeler">Four-wheeler</option>
															<option value="Two-wheeler">Two-wheeler</option>
															<option value="Bus">Bus</option>
															<option value="Truck">Truck</option>
															</select>
                                                        </div>
                                                    </div>
												
													<div class="form-group row">
                                                       
													<div class="col-lg-12">
															<label>Service Category Thumbnail image:</label>
                                                            	<a target="_blank" href="<?php echo base_url() ?>/assets/images/service/<?php echo $row->ser_thumbnail; ?>"><img style="width: 70px; height: 70px" src="<?php 
																if($row->ser_thumbnail==""){
																	echo base_url('/assets/images/no-images/no-product.png'); 
																	}else{
																		echo base_url() ?>/assets/images/service/<?php echo  $row->ser_thumbnail; 
																	} ?>"></a>
															    <input type="file"  accept="image/x-png,image/jpeg"  name="imageone" class="form-control" id="imageone" />
															 <input type="hidden" pattern=".*\S+.*"  name="hiddenimageone" value="<?php echo $row->ser_thumbnail ?>"  class="form-control" />
													    
														</div>
														</div>

														

													<div class="form-group row">
                                                       
													   <div class="col-lg-12">
															   <label>Service Category Img:</label>
																   <a target="_blank" href="<?php echo base_url() ?>/assets/images/service/<?php echo $row->ser_image; ?>"><img style="width: 70px; height: 70px" src="<?php 
																   if($row->ser_image==""){
																	   echo base_url('/assets/images/no-images/no-product.png'); 
																	   }else{
																		   echo base_url() ?>/assets/images/service/<?php echo  $row->ser_image; 
																	   } ?>"></a>
																   <input type="file"  accept="image/x-png,image/jpeg"  name="imagetwo" class="form-control" id="imagetwo" />
																<input type="hidden" pattern=".*\S+.*"  name="hiddenimagetwo" value="<?php echo $row->ser_image ?>"  class="form-control" />
														   
														   </div>
														   </div>
														 
												   <div class="form-group row">
                                                       
													   <div class="col-lg-12">
														   <label>Select Status:</label>
															<select class="form-control" name="status" required>
															<option value="<?php echo $row->ser_category_status; ?>"><?php echo $row->ser_category_status; ?></option>
															<?php  if($row->ser_category_status!="Active"){
																?>
																<option value="Active">Active</option>
																<?php
															}else{
																?>
																<option value="Inactive">Inactive</option>
																<?php
															} ?>
															</select>
															 </div>
															
												   </div>

												   <div class="form-group row">
                                                       
													   <div class="col-lg-12">
													   <label>Service Category:</label>
													   <select id="servicecategory" required class="form-control" name="servicecategory">
													
													   <option  value="<?php echo $row->ser_category_type;?>" ><?php echo $row->ser_category_type;?></option>
															<option value="Quick">Quick</option>
															<option value="Standard">Standard</option>
															</select>
															 </div>
															
												   </div>
												  
												</div>
												
											
												<div class="card-footer">
													<div class="row">
														<div class="col-lg-12">
															<button type="submit" class="btn btn-primary mr-2">Save</button>
															</div>
													</div>
												</div>
											</form>
											<!--end::Form-->
										</div>
										<!--end::Card-->
									</div>
								</div>
							</div>
							<!--end::Container-->
						</div>
						<!--end::Entry-->
					</div>
                    <!--end::Content-->
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                    
					<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>  
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
                    <script>
						$(document).ready(function(){
						
						$('.vehiclename').select2({
                        placeholder: '--- Select Vehicle ---',
                        ajax: {
                       url: websiteurl + 'Vehicle/vehiclelist',
                        dataType: 'json',
                        delay: 250,
                        processResults: function (data) {
							console.log(data);
                        return {
                            results: $.map(data, function(obj)
                        {
							return { id: obj.vehb_id, text: obj.vehb_make+'/'+obj.vehb_model+'/'+obj.vehb_trim };
                         
                        })
                        };
                        },
                        cache: true
                        }
                        });
					});

                    
					$(".alphaonly").keydown(function(event){
        var inputValue = event.which;
        // console.log(inputValue);
        if (!(inputValue >= 65 && inputValue <= 123)
                            && (inputValue != 32 && inputValue != 0)
                            && ( inputValue != 8)
                            && (inputValue != 9)){
                                event.preventDefault(); 
                        }
    });
					 function validatepannumber(){
                        var pannumber = document.getElementById("technician_pan").value;
                        if(pannumber==""){
                            return true;
                        }else{
                            var regpan = /^([A-Z]){5}([0-9]){4}([A-Z]){1}?$/;
                            if(regpan.test(pannumber) == false)
                                {
									alert("Not a valid Pan Number");
	   document.getElementById("technician_pan").value='';
	   return false;
                                }else{
                                return true;
                                }
                            }
                        }
				function duplicatecheck(){
	var value=document.getElementById("technician_phonenumber").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Phone Number");
	   document.getElementById("technician_phonenumber").value='';
	   return false;
     }


 $.ajax({
	url: websiteurl + 'Technician/phonenocheckexist',
        type: 'POST',
        data: {
            key: value
        },
        dataType: 'json',
        success: function(data) {
		
			if(data==2){
				alert('Phone Number is Already exists');
				document.getElementById("technician_phonenumber").value='';
			}
        }
    });
}

function duplicatecheckemail(){
	var value=document.getElementById("technician_email").value;
	
 $.ajax({
	url: websiteurl + 'Technician/emailcheckexist',
        type: 'POST',
        data: {
            key: value
        },
        dataType: 'json',
        success: function(data) {
		
			if(data==2){
				alert('Email is Already exists');
				document.getElementById("technician_email").value='';
			}
        }
    });
}

function adharcheck(){
	var value=document.getElementById("technician_aadhar").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{6})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Adhar Number");
	   document.getElementById("technician_aadhar").value='';
	   return false;
     }
}

function pincodevalidation(){
	var value=document.getElementById("technician_pincode").value;
	
	var phoneno = /^\(?([1-9]{1})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{2})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Pincode Number");
	   document.getElementById("technician_pincode").value='';
	   return false;
     }
}
function validategstnumber(){
                            var cgstnumber = document.getElementById("gstnumber").value;
                            if(cgstnumber==""){
                            return true;
                        }else{
                            var regpan = /\d{2}[A-Z]{5}\d{4}[A-Z]{1}[A-Z\d]{1}[Z]{1}[A-Z\d]{1}/;
                            if(regpan.test(cgstnumber) == false)
                            {
								alert("Not a valid GST Number");
	   document.getElementById("gstnumber").value='';
                                   
                                    return false;
                                }else{
                                return true;
                                }
                            }
                        }
$("#service_title, #service_desc").on("keypress", function(e) {
    if (e.which === 32 && !this.value.length)
        e.preventDefault();
});
document.getElementById("service_title").addEventListener('keydown', function (e) {
  if (this.value.length === 0 && e.which === 32) e.preventDefault();
  if (this.value.length === 0 && e.which === 48) e.preventDefault();
  //special character
  if (e.which === 56 || e.which === 53 || e.which === 51 || e.which === 49 || e.which === 50 || e.which === 52 || e.which === 48 ) e.preventDefault();
});
						</script>