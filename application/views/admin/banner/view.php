
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Subheader-->
					
						<!--end::Subheader-->
						<!--begin::Entry-->
						<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
							<div class="container">
								<div class="row">
									<div class="col-lg-12">
										<!--begin::Card-->
										<div class="card card-custom gutter-b example example-compact">
											
											<?php foreach($banner as $row){} ?>
											<!--begin::Form-->
											
											<form class="form" method="post" id="resetform" action="<?php echo  base_url('admin/Banner/update'); ?>" enctype="multipart/form-data" >
											<div class="card-body">
													<div class="form-group row">
														<div class="col-lg-12">
														<label>Banner/Popup Title:</label>
															<input type="text" disabled required value="<?php echo $row->ban_title; ?>"  name="bannertitle" class="form-control" id="bannertitle" placeholder="Enter Title" />
                                                           <input type="hidden" required value="<?php echo $row->ban_id; ?>" name="ban_id"  class="form-control" id="ban_id" placeholder="Enter English Title" />
                                                        
														</div></div>
														<div class="form-group row">
														<div class="col-lg-12">
														<label>Enter Description:</label>
															<input type="text" disabled required value="<?php echo $row->ban_description; ?>"  name="banner_desc" class="form-control" id="banner_desc" placeholder="Enter Description" />
                                                        
                                                        </div>
                                                    </div>
													
																			<div class="form-group row">
                                                       
													<div class="col-lg-12">
													<label>Upload Thumbnail:</label>
                                                            	<a target="_blank" href="<?php echo base_url() ?>/assets/images/banner/<?php echo $row->ban_thumb_img; ?>"><img style="width: 70px; height: 70px" src="<?php 
																if($row->ban_thumb_img==""){
																	echo base_url('/assets/images/no-images/no-product.png'); 
																	}else{
																		echo base_url() ?>/assets/images/banner/<?php echo  $row->ban_thumb_img; 
																	} ?>" onerror="this.onerror=null;this.src='<?php echo base_url('/assets/images/noimage.png'); ?>';"></a>
															    <!-- <input type="file"  accept="image/x-png,image/jpeg"  name="imageone" class="form-control" id="imageone" /> -->
															 <input type="hidden" pattern=".*\S+.*"  name="hiddenimageone" value="<?php echo $row->ban_thumb_img ?>"  class="form-control" />
													    
														</div>
														</div>

														

													<div class="form-group row">
                                                       
													   <div class="col-lg-12">
													 
													   <label>Upload Image:</label>
																   <a target="_blank" href="<?php echo base_url() ?>/assets/images/banner/<?php echo $row->ban_img; ?>"><img style="width: 70px; height: 70px" src="<?php 
																   if($row->ban_img==""){
																	   echo base_url('/assets/images/no-images/no-product.png'); 
																	   }else{
																		   echo base_url() ?>/assets/images/banner/<?php echo  $row->ban_img; 
																	   } ?>" onerror="this.onerror=null;this.src='<?php echo base_url('/assets/images/noimage.png'); ?>';"></a>
																   <!-- <input type="file"  accept="image/x-png,image/jpeg"  name="imagetwo" class="form-control" id="imagetwo" /> -->
																<input type="hidden" pattern=".*\S+.*"  name="hiddenimagetwo" value="<?php echo $row->ban_img ?>"  class="form-control" />
														   
														   </div>
														   </div>
														   
												   
												   <div class="form-group row">
                                                       
													   <div class="col-lg-12">
													   <label>Status:</label>
															<select class="form-control" disabled name="status" required>
															<option value="<?php echo $row->ban_status; ?>"><?php echo $row->ban_status; ?></option>
															<?php  if($row->ban_status!="Active"){
																?>
																<option value="Active">Active</option>
																<?php
															}else{
																?>
																<option value="Inactive">Inactive</option>
																<?php
															} ?>
															</select>
															 </div>
															
												   </div>
												   <div class="form-group row">
                                                       
													   <div class="col-lg-12">
														   <label>Select Type:</label>
														   <select id="bannertype" disabled required class="form-control" name="bannertype">
															<option value="<?php echo $row->ban_type; ?>"><?php echo $row->ban_type; ?></option>
															<?php  if($row->ban_type=="Banner"){
																?>
																<option value="Pop-up">Pop-up</option>
																<?php
															}else{
																?>
																<option value="Banner">Banner</option>
																<?php
															} ?>
															</select>
															 </div>
															
												   </div>
												   
												  
												</div>
												
											
												<div class="card-footer">
													<div class="row">
														<div class="col-lg-12">
															</div>
													</div>
												</div>
											</form>
											<!--end::Form-->
										</div>
										<!--end::Card-->
									</div>
								</div>
							</div>
							<!--end::Container-->
						</div>
						<!--end::Entry-->
					</div>
                    <!--end::Content-->
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                    
					<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>  
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
                    <script>
						$(document).ready(function(){
						
						$('.vehiclename').select2({
                        placeholder: '--- Select Vehicle ---',
                        ajax: {
                       url: websiteurl + 'Vehicle/vehiclelist',
                        dataType: 'json',
                        delay: 250,
                        processResults: function (data) {
							console.log(data);
                        return {
                            results: $.map(data, function(obj)
                        {
							return { id: obj.vehb_id, text: obj.vehb_make+'/'+obj.vehb_model+'/'+obj.vehb_trim };
                         
                        })
                        };
                        },
                        cache: true
                        }
                        });
					});

                    
					$(".alphaonly").keydown(function(event){
        var inputValue = event.which;
        // console.log(inputValue);
        if (!(inputValue >= 65 && inputValue <= 123)
                            && (inputValue != 32 && inputValue != 0)
                            && ( inputValue != 8)
                            && (inputValue != 9)){
                                event.preventDefault(); 
                        }
    });
					 function validatepannumber(){
                        var pannumber = document.getElementById("technician_pan").value;
                        if(pannumber==""){
                            return true;
                        }else{
                            var regpan = /^([A-Z]){5}([0-9]){4}([A-Z]){1}?$/;
                            if(regpan.test(pannumber) == false)
                                {
									alert("Not a valid Pan Number");
	   document.getElementById("technician_pan").value='';
	   return false;
                                }else{
                                return true;
                                }
                            }
                        }
				function duplicatecheck(){
	var value=document.getElementById("technician_phonenumber").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Phone Number");
	   document.getElementById("technician_phonenumber").value='';
	   return false;
     }


 $.ajax({
	url: websiteurl + 'Technician/phonenocheckexist',
        type: 'POST',
        data: {
            key: value
        },
        dataType: 'json',
        success: function(data) {
		
			if(data==2){
				alert('Phone Number is Already exists');
				document.getElementById("technician_phonenumber").value='';
			}
        }
    });
}

function duplicatecheckemail(){
	var value=document.getElementById("technician_email").value;
	
 $.ajax({
	url: websiteurl + 'Technician/emailcheckexist',
        type: 'POST',
        data: {
            key: value
        },
        dataType: 'json',
        success: function(data) {
		
			if(data==2){
				alert('Email is Already exists');
				document.getElementById("technician_email").value='';
			}
        }
    });
}

function adharcheck(){
	var value=document.getElementById("technician_aadhar").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{6})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Adhar Number");
	   document.getElementById("technician_aadhar").value='';
	   return false;
     }
}

function pincodevalidation(){
	var value=document.getElementById("technician_pincode").value;
	
	var phoneno = /^\(?([1-9]{1})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{2})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Pincode Number");
	   document.getElementById("technician_pincode").value='';
	   return false;
     }
}
function validategstnumber(){
                            var cgstnumber = document.getElementById("gstnumber").value;
                            if(cgstnumber==""){
                            return true;
                        }else{
                            var regpan = /\d{2}[A-Z]{5}\d{4}[A-Z]{1}[A-Z\d]{1}[Z]{1}[A-Z\d]{1}/;
                            if(regpan.test(cgstnumber) == false)
                            {
								alert("Not a valid GST Number");
	   document.getElementById("gstnumber").value='';
                                   
                                    return false;
                                }else{
                                return true;
                                }
                            }
                        }

						</script>