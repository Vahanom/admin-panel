	<!--begin::Footer-->

    <div class="footer bg-white py-4 d-flex flex-lg-column" id="kt_footer">
																	<!--begin::Container-->
	<div class="container d-flex flex-column flex-md-row align-items-center justify-content-between">
																		<!--begin::Copyright-->
                                                                        <div class="text-dark order-2 order-md-1">
    <span class="text-muted font-weight-bold mr-2"><?php echo date('Y'); ?></span>
	<a href="#" target="_blank" class="text-dark-75 text-hover-primary">Vahanom</a>
	</div>
																		<!--end::Copyright-->
																		<!--begin::Nav-->
	<!-- <div class="nav nav-dark order-1 order-md-2">
	<a href="http://keenthemes.com/metronic" target="_blank" class="nav-link pr-3 pl-0">About</a>
	<a href="http://keenthemes.com/metronic" target="_blank" class="nav-link px-3">Team</a>
	<a href="http://keenthemes.com/metronic" target="_blank" class="nav-link pl-3 pr-0">Contact</a>
	</div> -->
																		<!--end::Nav-->
</div>
																	<!--end::Container-->
</div>

<script>var HOST_URL = "https://keenthemes.com/metronic/tools/preview";</script>
													<!--begin::Global Config(global config for global JS scripts)-->
													<script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#6993FF", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#F3F6F9", "dark": "#212121" }, "light": { "white": "#ffffff", "primary": "#E1E9FF", "secondary": "#ECF0F3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#212121", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#ECF0F3", "gray-300": "#E5EAEE", "gray-400": "#D6D6E0", "gray-500": "#B5B5C3", "gray-600": "#80808F", "gray-700": "#464E5F", "gray-800": "#1B283F", "gray-900": "#212121" } }, "font-family": "Poppins" };</script>
													<!--end::Global Config-->
													<!--begin::Global Theme Bundle(used by all pages)-->
													<script src="<?php echo base_url(); ?>assets/plugins/global/plugins.bundle.js?v=7.0.4"></script>
													<script src="<?php echo base_url(); ?>assets/plugins/custom/prismjs/prismjs.bundle.js?v=7.0.4"></script>
													<script src="<?php echo base_url(); ?>assets/js/scripts.bundle.js?v=7.0.4"></script>
													<!--end::Global Theme Bundle-->
													<!--begin::Page Vendors(used by this page)-->
													<script src="<?php echo base_url(); ?>assets/plugins/custom/fullcalendar/fullcalendar.bundle.js?v=7.0.4"></script>
													<!--end::Page Vendors-->
													<!--begin::Page Scripts(used by this page)-->
													<script src="<?php echo base_url(); ?>assets/js/pages/widgets.js?v=7.0.4"></script>
                          <!--end::Page Scripts-->
                          <script src="<?php echo base_url('assets/js/pages/crud/file-upload/dropzonejs.js?v=7.0.4'); ?>"></script>
												</body>
<!--end::Body-->
</html>
	<div class="modal fade" id="thankyouModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Unlock The Screen!</h4>
            </div>
            <div class="modal-body">
            <div id="captacha"></div>              
            </div>    
        </div>
    </div>
</div>

<script>
    //  var signup_email = $('#signUp').val();
    //  var signup_password = $('#signUpPassword').val() ;                  
    //             $.ajax({
    //                 type: "POST",
    //                 url: "<?php echo site_url('form/insert'); ?>",
    //                 data: { signup_email: signup_email, signup_password: signup_password },
    //                 dataType: "html",
    //                 success: function(data) {
    //                     if(data) {
    //                         $('#thankyouModal').modal('show');
    //                     }
    //                 }, error: function() {
    //                     alert("ERROR!");
    //                 }
    //             });

</script>

</body>
<script>

var timeoutID;
 
function setup() {
	
    this.addEventListener("mousemove", resetTimer, false);
    this.addEventListener("mousedown", resetTimer, false);
    this.addEventListener("keypress", resetTimer, false);
    this.addEventListener("DOMMouseScroll", resetTimer, false);
    this.addEventListener("mousewheel", resetTimer, false);
    this.addEventListener("touchmove", resetTimer, false);
    this.addEventListener("MSPointerMove", resetTimer, false);

    startTimer();
}
setup();
 
function startTimer() {
    
    // wait 2 seconds before calling goInactive
    // timeoutID = window.setTimeout(goInactive, 15 * 60 * 1000 );
    timeoutID = window.setTimeout(goInactive, 15 * 60 * 1000);
}
function resetTimer(e) {
    window.clearTimeout(timeoutID);
    goActive();
}
 
function goInactive() {
	window.location.href='<?php echo base_url('admin/logout') ?>';

    }

 
 
function goActive() {
    // do something
         
    startTimer();
}


// document.addEventListener('contextmenu', event => event.preventDefault());
// $(document).keydown(function(e){
//     if(e.which === 123){
//        return false;
//     }
// });

function myFunction() {
  return "Write something clever here...";
}
</script>

<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script>
	$(function() {
		$('#example').DataTable({
      'paging': $('#example tbody tr').length>10,
      "fnDrawCallback": function (oSettings) {
        var pgr = $(oSettings.nTableWrapper).find('.dataTables_paginate')
        if (oSettings._iDisplayLength > oSettings.fnRecordsDisplay()) {
          pgr.hide();
        } else {
          pgr.show()
        }
      },
      'searching':$('#example tbody tr').length>10,
      'lengthChange': false,
      "columnDefs": [
          { "orderable": false, "targets": [-1] }
        ],
      'info': $('#example tbody tr').length>=1,
      'autoWidth': false
    });
    
          
	})
</script>
<!--end::Footer-->
<script>
	// var websiteurl = "http://localhost/uneeed-admin/admin/";
	// var websiteurl = "https://qset.co.in/uneeed-admin/admin/";
    var websiteurl = "<?php echo base_url('admin/') ?>";

</script>

<script src="https://cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script>
	CKEDITOR.replace( 'englishck',
	{
		toolbar :
		[
			{ name: 'basicstyles', items : [ 'Bold','Italic' ] },
			{ name: 'paragraph', items : [ 'NumberedList','BulletedList' ] },
			{ name: 'tools', items : [ 'Maximize','-','About' ] }
		]
	});
    CKEDITOR.replace( 'marathick',
	{
		toolbar :
		[
			{ name: 'basicstyles', items : [ 'Bold','Italic' ] },
			{ name: 'paragraph', items : [ 'NumberedList','BulletedList' ] },
			{ name: 'tools', items : [ 'Maximize','-','About' ] }
		]
	});
    CKEDITOR.replace( 'kanadack',
	{
		toolbar :
		[
			{ name: 'basicstyles', items : [ 'Bold','Italic' ] },
			{ name: 'paragraph', items : [ 'NumberedList','BulletedList' ] },
			{ name: 'tools', items : [ 'Maximize','-','About' ] }
		]
	});
    CKEDITOR.replace( 'hindihck',
	{
		toolbar :
		[
			{ name: 'basicstyles', items : [ 'Bold','Italic' ] },
			{ name: 'paragraph', items : [ 'NumberedList','BulletedList' ] },
			{ name: 'tools', items : [ 'Maximize','-','About' ] }
		]
	});
</script>