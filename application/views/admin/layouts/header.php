
<!DOCTYPE html>
<html lang="en">
<head>
	<base href="">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo $title; ?></title>
	<meta name="description" content="Updates and statistics" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" />
	<link href="<?php echo base_url(); ?>assets/plugins/custom/fullcalendar/fullcalendar.bundle.css?v=7.0.4" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/plugins/global/plugins.bundle.css?v=7.0.4" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/plugins/custom/prismjs/prismjs.bundle.css?v=7.0.4" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/css/style.bundle.css?v=7.0.4" rel="stylesheet" type="text/css" />
	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/media/logos/logo.png" />
	<script src="https://www.google.com/recaptcha/api.js"></script>

</head>