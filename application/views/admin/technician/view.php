
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Subheader-->
					
						<!--end::Subheader-->
						<!--begin::Entry-->
						<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
							<div class="container">
								<div class="row">
									<div class="col-lg-12">
										<!--begin::Card-->
										<div class="card card-custom gutter-b example example-compact">
											
											<?php foreach($technician as $row){} ?>
											<!--begin::Form-->
											
											<form class="form" method="post" id="resetform" action="<?php echo  base_url('admin/Technician/update'); ?>" enctype="multipart/form-data" >
											<div class="card-body">
													<div class="form-group row">
														<div class="col-lg-12">
														<label>Technician Name:</label>
															<input type="text" disabled required name="technician_name" value="<?php echo $row->puser_name;?>" class="form-control" id="technician_name" placeholder="Enter Name" />
                                                         <input type="hidden" required name="puser_id" value="<?php echo $row->puser_id;?>" class="form-control" id="puser_id" placeholder="Enter English Title" />
                                                        
														</div></div>
														<div class="form-group row">
														<div class="col-lg-12">
														<label>Technician Email:</label>
															<input type="email" disabled required name="technician_email" value="<?php echo $row->puser_email;?>" class="form-control" onchange="duplicatecheckemail();" id="technician_email" placeholder="Enter Email" />
                                                        
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
														<div class="col-lg-12">
														<label>Adhar Number:</label>
															<input type="text" disabled required name="technician_aadhar" value="<?php echo $row->puser_adhar_number;?>" onchange="adharcheck();" class="form-control" id="technician_aadhar" placeholder="Enter Adhar Number" />
                                                        
														</div>
														</div>
														<div class="form-group row">
														<div class="col-lg-12">
														<label>Pan Number:</label>
															<input type="text" disabled required name="technician_pan" value="<?php echo $row->puser_pan_number;?>" class="form-control" onchange="validatepannumber();" id="technician_pan" placeholder="Enter Pan Number" />
                                                        
                                                        </div>
                                                    </div>
													<div class="form-group row">
														<div class="col-lg-12">
														<label>Address:</label>
															<textarea cols="80" disabled id="technician_address" class="form-control" value="<?php echo $row->puser_address;?>" name="technician_address" rows="" placeholder="Enter Address"><?php echo $row->puser_address;?></textarea>
                                                         </div>
														</div>
														<div class="form-group row">
														<div class="col-lg-12">
														<label>Pincode:</label>
															<input type="number" disabled required name="technician_pincode" value="<?php echo $row->puser_pincode;?>" class="form-control" onchange="pincodevalidation();" id="technician_pincode" placeholder="Enter Pincode" />
                                                        
														</div>
                                                    </div>
													<div class="form-group row">
														<div class="col-lg-12">
														<label>Contact Number:</label>
												   <input type="number" disabled class="form-control" onchange="duplicatecheck()" value="<?php echo $row->puser_phno;?>" required name="technician_phonenumber" id="technician_phonenumber"  placeholder="Enter Phone Number" />
													   
														 </div>
														</div>
														<div class="form-group row">
														<div class="col-lg-12">
														<label>Alternate Contact Number:</label>
												   <input type="number" disabled class="form-control" onchange="duplicatecheckalter()" value="<?php echo $row->technician_alternatephonenumber;?>"  name="technician_alternatephonenumber" id="technician_alternatephonenumber"  placeholder="Enter Phone Number" />
													
														 </div>
														</div>
														<div class="form-group row">
														<div class="col-lg-12">
														<label>Latitude :</label>
												   <input type="text" disabled class="form-control" required name="latlong" value="<?php echo $row->puser_lat_long;?>" id="latlong"  placeholder="Enter lat long" />
													
														 </div>
														</div>
														<div class="form-group row">
														<div class="col-lg-12">
														<label>Longitude:</label>
												   <input type="number" disabled class="form-control"  value="<?php echo $row->puser_long;?>" required name="long" id="long"  placeholder="Enter long" />
														
														 </div>
														</div>		
														<div class="form-group row">
														<div class="col-lg-12">
														<label>Bank Account Number:</label>
												   <input type="number" disabled class="form-control" min="9999999999" value="<?php echo $row->puser_bankacc_number;?>" max="999999999999999999999999999999" required name="technician_bankacc" id="technician_bankacc"  placeholder="Enter Bank Account Number" />
														 </div>
														</div>

														<div class="form-group row">
														<div class="col-lg-12">
														<label>IFSC number:</label>
												   <input type="text" disabled class="form-control alphaonly" maxlength="11" value="<?php echo $row->puser_bankacc_ifsc;?>" required name="ifscnumber" id="ifscnumber"  placeholder="Enter IFSC Number" />
													 </div>
														</div>

														<div class="form-group row">
														<div class="col-lg-12">
														<label>GST Number:</label>
												   <input type="text"  disabled pattern=".*\S+.*" onchange="validategstnumber();" value="<?php echo $row->puser_GST_number;?>" class="form-control" required name="gstnumber" id="gstnumber"  placeholder="Enter GST Number" />
														  </div>
														</div>
														<div class="form-group row">
														<div class="col-lg-12">
														<label>Technician Type:</label>
												   <input type="text"  disabled value="<?php echo $row->puser_technician_type;?>" class="form-control" name="technician_type" id="technician_type"/>
														  </div>
														</div>
														<div class="form-group row">
														<div class="col-lg-12">
														<label>Expertise:</label>
												   <input type="text"  value="<?php echo $row->expertise;?>" class="form-control"  disabled/>
														  </div>
														</div>
														<div class="form-group row">
														<div class="col-lg-12">
														<label>Select Vehicle:</label>
                                                            <select  multiple required class="form-control vehiclename" disabled>
															<?php foreach ($technician as $row1) {?>
																<option value="<?php echo $row1->vehb_make ?>" selected ><?php echo $row1->vehb_make;  ?></option>
															<?php }?>
															</select>
                                                        </div>
                                                    </div>
												   
														<div class="form-group row">
														<div class="col-lg-12">
															<label>Profile Image:</label>
                                                            	<a target="_blank" href="<?php echo base_url() ?>/assets/images/technician/<?php echo $row->puser_profile_image; ?>"><img style="width: 70px; height: 70px" src="<?php 
																if($row->puser_profile_image==""){
																	echo base_url('/assets/images/no-images/no-product.png'); 
																	}else{
																		echo base_url() ?>/assets/images/technician/<?php echo  $row->puser_profile_image; 
																	} ?>" onerror="this.onerror=null;this.src='<?php echo base_url('/assets/images/noimage.png'); ?>';"></a>
															    <!-- <input type="file"  accept="image/x-png,image/jpeg"  name="imageone" class="form-control" id="imageone" /> -->
															 <input type="hidden" pattern=".*\S+.*"  name="hiddenimageone" value="<?php echo $row->puser_profile_image ?>"  class="form-control" />
													    
														</div>
														</div>

														<div class="form-group row">
														<div class="col-lg-12">
											<label >Upload CV</label>
												<div class="row">
											<div class="col-lg-12">
											<?php if($row->puser_document_url==""){  ?>
		 <p> There is No CV Uploaded Yet!.</p>
											   <?php  } else{
                                                   $abc=$row->puser_document_url;
                                                   ?>
												<a  class="form-control" href="<?php echo base_url(); ?>assets/images/technician/<?php echo $abc;?>" target="_blank">Uploaded CV</a>
		<?php } 
		
		
		?>	
											</div>
											<div class="col-lg-12">
											<!-- <input type="file"   accept=".pdf,.doc,.docx"  name="imagetwo" class="form-control" id="imagetwo" /> -->
											<input type="hidden" id="hiddenimagetwo"  accept=".pdf,.doc,.docx"  name="hiddenimagetwo" value="<?php echo $row->puser_document_url; ?>"  class="form-control" />
												
											</div>
										</div>
													</div>
													</div>

												   <div class="form-group row">
                                                       
													   <div class="col-lg-12">
														   <label>Select Status:</label>
															<select class="form-control" disabled name="status" required>
															<option value="<?php echo $row->puser_account_status; ?>"><?php echo $row->puser_account_status; ?></option>
															<?php  if($row->puser_account_status!="Active"){
																?>
																<option value="Active">Active</option>
																<?php
															}else{
																?>
																<option value="Inactive">Inactive</option>
																<?php
															} ?>
															</select>
															 </div>
															
												   </div>

												   <div class="form-group row">
                                                       
													   <div class="col-lg-12">
														   <label>I want to work with:</label>
															<select class="form-control" disabled name="working_type" required>
															<option value="<?php echo $row->working_type; ?>"><?php echo $row->working_type; ?></option>
															<?php  if($row->working_type!="Active"){
																?>
																<option value="Active">Active</option>
																<?php
															}else{
																?>
																<option value="Inactive">Inactive</option>
																<?php
															} ?>
															</select>
															 </div>

														
															
												   </div>
												   <div class="form-group row"> 
													   <div class="col-lg-12">
															<label>Distance:</label>
															<input type="text" disabled value="<?php echo $row->technician_order_distance; ?>" name="technician_order_distance" class="form-control" id="technician_order_distance" placeholder="Enter Order Distance" />
                                                        
														</div>		
												   </div>

												    <div class="form-group row"> 
													   <div class="col-lg-12">
															<label>How Many Hours per week:</label>
															<input type="text" disabled name="weekly_available_hours" class="form-control" id="weekly_available_hours" value="<?php echo $row->weekly_available_hours; ?>" placeholder="Enter Order Distance" />
                                                        
														</div>		
												   </div>
												  
												</div>

												<!--<div class="form-group row">
												   <div class="col-lg-12" style="background-color: black;">
												   	<?php
                                                         $R="ASE A1";
                                                         $t=$row->technician_certificate; //echo strpos($t,$R); 
                                                          if(strpos($t, $R) !== false){ ?>
	                                                           <input type="checkbox" class="form-control" checked disabled><?php echo $R; ?>
	                                                      <?php
                                                           } else{
                                                                echo "Word Not Found!";
                                                          }?>
												    </div>		
												   </div>
												   </div>-->


												   <div class="form-group row">
												   <div class="col-lg-12">	
												   <input type="checkbox" <?php $R="ASEA1";
                                                         $t=$row->technician_certificate; if(strpos($t, $R) !== false){echo "checked"; }?> disabled id="Sunday">

												   <label>ASR A1</label>
												   </div>
												   </div>

												   <div class="form-group row">
												   <div class="col-lg-12">	
												   <input type="checkbox" <?php  $R="ASEA2";
                                                         $t=$row->technician_certificate; if(strpos($t, $R) !== false){echo "checked"; }?> disabled id="Sunday">

												   <label>ASR A2</label>
												   </div>
												   </div>

												   <div class="form-group row">
												   <div class="col-lg-12">	
												   <input type="checkbox" <?php  $R="ASEA3";
                                                         $t=$row->technician_certificate; if(strpos($t, $R) !== false){echo "checked"; }?> disabled id="Sunday">

												   <label>ASR A3</label>
												   </div>
												   </div>

												   <div class="form-group row">
												   <div class="col-lg-12">	
												   <input type="checkbox" <?php  $R="ASEA4";
                                                         $t=$row->technician_certificate; if(strpos($t, $R) !== false){echo "checked"; }?> disabled id="Sunday">

												   <label>ASR A4</label>
												   </div>
												   </div>

												   <div class="form-group row">
												   <div class="col-lg-12">	
												   <input type="checkbox" <?php  $R="ASEA5";
                                                         $t=$row->technician_certificate; if(strpos($t, $R) !== false){echo "checked"; }?> disabled id="Sunday">

												   <label>ASR A5</label>
												   </div>
												   </div>

												   <div class="form-group row">
												   <div class="col-lg-12">	
												   <input type="checkbox" <?php  $R="ASEA6";
                                                         $t=$row->technician_certificate; if(strpos($t, $R) !== false){echo "checked"; }?> disabled id="Sunday">

												   <label>ASR A6</label>
												   </div>
												   </div>

												   <div class="form-group row">
												   <div class="col-lg-12">	
												   <input type="checkbox" <?php  $R="ASEA7";
                                                         $t=$row->technician_certificate; if(strpos($t, $R) !== false){echo "checked"; }?> disabled id="Sunday">

												   <label>ASR A7</label>
												   </div>
												   </div>

												   <div class="form-group row">
												   <div class="col-lg-12">	
												   <input type="checkbox" <?php  $R="ASEA8";
                                                         $t=$row->technician_certificate; if(strpos($t, $R) !== false){echo "checked"; }?> disabled id="Sunday">

												   <label>ASR A8</label>
												   </div>
												   </div>

												   <div class="form-group row">
												   <div class="col-lg-12">	
												   <input type="checkbox" <?php  $R="ASEA9";
                                                         $t=$row->technician_certificate; if(strpos($t, $R) !== false){echo "checked"; }?> disabled id="Sunday">

												   <label>ASR A9</label>
												   </div>
												   </div>

												   <div class="form-group row">
												   <div class="col-lg-12">	
												   <input type="checkbox" <?php  $R="ASEL1";
                                                         $t=$row->technician_certificate; if(strpos($t, $R) !== false){echo "checked"; }?> disabled id="Sunday">

												   <label>ASE L1</label>
												   </div>
												   </div>

												   <div class="form-group row">
												   <div class="col-lg-12">	
												   <input type="checkbox" <?php  $R="Epa609";
                                                         $t=$row->technician_certificate; if(strpos($t, $R) !== false){echo "checked"; }?> disabled id="Sunday">

												   <label>Epa609</label>
												   </div>
												   </div>

												   <div class="form-group row">
												   <div class="col-lg-12">	
												   <input type="checkbox" <?php  $R="AirBag";
                                                         $t=$row->technician_certificate; if(strpos($t, $R) !== false){echo "checked"; }?> disabled id="Sunday">

												   <label>Air Bag</label>
												   </div>
												   </div>
												
												   <div class="form-group row">
												   <div class="col-lg-12">	
												   <input type="checkbox" <?php  $R="MACSAC";
                                                         $t=$row->technician_certificate; if(strpos($t, $R) !== false){echo "checked"; }?> disabled id="Sunday">

												   <label>MACS A/C</label>
												   </div>
												   </div>


												   <div class="form-group row">
												   <div class="col-lg-12">	
												   <input type="checkbox" <?php  $R="SmogLicensed";
                                                         $t=$row->technician_certificate; if(strpos($t, $R) !== false){echo "checked"; }?> disabled id="Sunday">

												   <label>Smog Licensed</label>
												   </div>
												   </div>

												   <div class="form-group row">
												   <div class="col-lg-12">	
												   <input type="checkbox" <?php  $R="BrakeandLampCertified";
                                                         $t=$row->technician_certificate; if(strpos($t, $R) !== false){echo "checked"; }?> disabled id="Sunday">

												   <label>Brake and Lamp Certified</label>
												   </div>
												   </div>
											
												<div class="card-footer">
													<div class="row">
														<div class="col-lg-12">
															<!-- <button type="submit" class="btn btn-primary mr-2">Save</button> -->
															</div>
													</div>
												</div>
											</form>
											<!--end::Form-->
										</div>
										<!--end::Card-->
									</div>
								</div>
							</div>
							<!--end::Container-->
						</div>
						<!--end::Entry-->
					</div>
                    <!--end::Content-->
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                    
					<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>  
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
					<script src="https://cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
					<script>
						$(document).ready(function(){
						
						$('.vehiclename').select2({
                        placeholder: '--- Select Vehicle ---',
                        ajax: {
                       url: websiteurl + 'Vehicle/vehiclelist',
                        dataType: 'json',
                        delay: 250,
                        processResults: function (data) {
							console.log(data);
                        return {
                            results: $.map(data, function(obj)
                        {
							return { id: obj.vehb_make, text: obj.vehb_make };
                         
                        })
                        };
                        },
                        cache: true
                        }
                        });
					});
						$(".alphaonly").keydown(function(event){
        var inputValue = event.which;
        // console.log(inputValue);
        if (!(inputValue >= 65 && inputValue <= 123)
                            && (inputValue != 32 && inputValue != 0)
                            && ( inputValue != 8)
                            && (inputValue != 9)){
                                event.preventDefault(); 
                        }
    });
					 function validatepannumber(){
                        var pannumber = document.getElementById("technician_pan").value;
                        if(pannumber==""){
                            return true;
                        }else{
                            var regpan = /^([A-Z]){5}([0-9]){4}([A-Z]){1}?$/;
                            if(regpan.test(pannumber) == false)
                                {
									alert("Not a valid Pan Number");
	   document.getElementById("technician_pan").value='';
	   return false;
                                }else{
                                return true;
                                }
                            }
                        }
				function duplicatecheck(){
	var value=document.getElementById("supllier_phonenumber").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Phone Number");
	   document.getElementById("supllier_phonenumber").value='';
	   return false;
     }
}



function adharcheck(){
	var value=document.getElementById("technician_aadhar").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{6})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Adhar Number");
	   document.getElementById("technician_aadhar").value='';
	   return false;
     }
}

function pincodevalidation(){
	var value=document.getElementById("technician_pincode").value;
	
	var phoneno = /^\(?([1-9]{1})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{2})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Pincode Number");
	   document.getElementById("technician_pincode").value='';
	   return false;
     }
}
function validategstnumber(){
                            var cgstnumber = document.getElementById("gstnumber").value;
                            if(cgstnumber==""){
                            return true;
                        }else{
                            var regpan = /\d{2}[A-Z]{5}\d{4}[A-Z]{1}[A-Z\d]{1}[Z]{1}[A-Z\d]{1}/;
                            if(regpan.test(cgstnumber) == false)
                            {
								alert("Not a valid GST Number");
	   document.getElementById("gstnumber").value='';
                                   
                                    return false;
                                }else{
                                return true;
                                }
                            }
                        }

						</script>