
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Subheader-->
						<div class="subheader py-2 py-lg-4 subheader-transparent" id="kt_subheader">
							<div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
								<!--begin::Info-->
								<div class="d-flex align-items-center flex-wrap mr-1">
									<!--begin::Heading-->
									<div class="d-flex flex-column">
										<!--begin::Title-->
										<h2 class="text-white font-weight-bold my-2 mr-5">Add Technician</h2>
										<!--end::Title-->
										<!--begin::Breadcrumb-->
										<div class="d-flex align-items-center font-weight-bold my-2">
											<!--begin::Item-->
											<a href="#" class="opacity-75 hover-opacity-100">
												<i class="flaticon2-shelter text-white icon-1x"></i>
											</a>
											
											<!--end::Item-->
											<!--begin::Item-->
											<span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
											<a href="<?php echo base_url('admin/Dashboard') ?>" class="text-white text-hover-white opacity-75 hover-opacity-100">Dashboard</a>
											<!--end::Item-->
											<!--begin::Item-->
											<span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
											<a href="<?php echo base_url('admin/Technician/index') ?>" class="text-white text-hover-white opacity-75 hover-opacity-100">Technician</a>
											<!--end::Item-->
											<!--begin::Item-->
											<!--end::Item-->
											<!--begin::Item-->
											<span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
											<a href="" class="text-white text-hover-white opacity-75 hover-opacity-100">Add Technician</a>
											<!--end::Item-->
										</div>
										<!--end::Breadcrumb-->
									</div>

									<!--end::Heading-->
								</div>
								<!--end::Info-->
							</div>
						</div>
						<!--end::Subheader-->
						<!--begin::Entry--> 
						<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
							<div class="container">
								<div class="row">
									<div class="col-lg-12">
										<!--begin::Card-->
										<div class="card card-custom gutter-b example example-compact">
											<div class="card-header">
												<h3 class="card-title">Add Technician</h3>
											</div>
											<!--begin::Form-->
											<form class="form" method="post" action="<?php echo  base_url('admin/Technician/add'); ?>" enctype="multipart/form-data" >
												<div class="card-body">
													<div class="form-group row">
														<div class="col-lg-6">
															<label>Technician Name:</label>
															<input type="text" required name="technician_name" class="form-control" id="technician_name" placeholder="Enter Technician Name" />
                                                        </div>
														<div class="col-lg-6">
															<label>Technician Email:</label>
															<input type="email" required name="technician_email" class="form-control" onchange="duplicatecheckemail();" id="technician_email" placeholder="Enter Technician Email" />
                                                        
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
														<div class="col-lg-6">
															<label>Adhar Number:</label>
															<input type="text" required name="technician_aadhar" onchange="adharcheck();" class="form-control" id="technician_aadhar" placeholder="Enter Technician Adhar Number" />
                                                      	</div>
														<div class="col-lg-6">
															<label>Pan Number:</label>
															<input type="text" required name="technician_pan" class="form-control" onchange="validatepannumber();" id="technician_pan" placeholder="Enter Technician Pan Number" />
                                                        
                                                        </div>
                                                    </div>
													<div class="form-group row">
														<div class="col-lg-6">
															<label>Address:</label>
															<textarea cols="80" id="technician_address" class="form-control"  name="technician_address" rows="" placeholder="Enter Technician Address"></textarea>
                                                        </div>

														<div class="col-lg-6">
															<label>Pincode:</label>
															<input type="number" required name="technician_pincode" class="form-control" onchange="pincodevalidation();" id="technician_pincode" placeholder="Enter Technician Pincode" />
                                                        </div>
                                                    </div>
												
													<div class="form-group row">
												   		<div class="col-lg-6">
												   			<label>Contact Number:</label>
											   				<input type="number" class="form-control" onchange="duplicatecheck()" required name="technician_phonenumber" id="technician_phonenumber"  placeholder="Enter Technician Phone Number" />
													    </div>
														<div class="col-lg-6">
															<label>Alternate Contact Number:</label>
												   			<input type="number" class="form-control" onchange="duplicatecheckalter()" required name="technician_alternatephonenumber" id="technician_alternatephonenumber"  placeholder="Enter Technician Phone Number" />
														</div>
													</div>
												   	<div class="form-group row">
												  		<div class="col-lg-6">
												   			<label>Bank Account Number:</label>
												   			<input type="number" class="form-control" min="9999999999" max="999999999999999999999999999999" required name="technician_bankacc" id="technician_bankacc"  placeholder="Enter Technician Bank Account Number" />
													    </div>
														<div class="col-lg-6">
												   			<label>IFSC number:</label>
												   			<input type="text" class="form-control" onkeyup="alphanumeric(this.value)"  maxlength="11" required name="ifscnumber" id="ifscnumber"  placeholder="Enter Technician IFSC Number" />
													   	</div>
													</div>
												   	<div class="form-group row">
												   		<div class="col-lg-6">
												   			<label>Profile Picture:</label>
												   			<input type="file"  accept="image/x-png,image/jpeg" required name="imageone" class="form-control" id="imageone" />
                                                     	</div>
														<div class="col-lg-6">
												   			<label>Upload CV:</label>
												   			<input type="file"   accept=".pdf,.doc,.docx" required name="imagetwo" class="form-control" id="imagetwo" />
                                                   	 	</div>
													</div>
													<div class="form-group row">
												   		<div class="col-lg-6">
												   			<label>GST Number:</label>
												   			<input type="text"  pattern=".*\S+.*" onchange="validategstnumber();" class="form-control" required name="gstnumber" id="gstnumber"  placeholder="Enter Technician GST Number" />
														</div>
											 			<div class="col-lg-6">
														   <label>Technician Type:</label>
														   <select id="technician_type" required class="form-control" name="technician_type">
															<option value="">Select Type</option>
															<option value="Type 1">Type 1</option>
															<option value="Type 2">Type 2</option> 
															<option value="Type 3">Type 3</option>
															</select>
														</div>
													</div>
												   	<div class="form-group row">
												    	<!-- <div class="col-lg-6">
												  			<label>Longitude:</label>
												   			<input type="number" class="form-control" required name="long" id="long"  placeholder="Enter longitude" />
													   	</div> -->
													   	<!-- <div class="col-lg-6">
														   <label>I want to work:</label>
														   <select id="working_type" required class="form-control" name="working_type">
															<option value="">Select Status</option>
															<option value="Part Time">Part Time</option>
															<option value="Full Time">Full Time</option>
															</select>
														</div>	
														 -->
												    </div>
													<div class="form-group row">
												    	<div class="col-lg-6">
															<label>Distance:</label>
															<input type="text" required name="technician_order_distance" class="form-control" id="technician_order_distance" placeholder="Enter Order Distance" />
                                                        </div>
                                                        <div class="col-lg-6">
															<label>How Many Hours per week:</label>
															<input type="text" required name="weekly_available_hours" class="form-control" id="weekly_available_hours" placeholder="Enter Weekly available hours" />
                                                        </div>
                                                    </div>
													<div class="form-group row">
												  		<div class="col-lg-6">
															<label>Select Expertise:</label>
														   <select id="expertise" multiple class="form-control expertise" name="expertise[]" onchange="vehselect();">
															<option value="">Select Expertise</option>
															<option value="2 WHEELER">2 WHEELER</option>
															<option value="4 WHEELER">4 WHEELER</option>
															<option value="BUS">BUS</option>
															<option value="Truck">TRUCK</option>
															</select>
														</div>
														<!-- <div class="col-lg-6">
															<label>Latitude:</label>
															<input type="number" class="form-control" required name="latlong" id="latlong"  placeholder="Enter latitude" />
													   	</div> -->
														<div class="col-lg-6">
															<label>Select Make Vehicle:</label>
                                                            <select name="vehiclename[]" id="vehiclename" multiple required class="form-control vehiclename"></select>
                                                    	</div>	
														
												   	</div>
													<div class="form-group row">
												  		<div class="col-lg-6">
														   <label>Select Status:</label>
														   <select id="status" required class="form-control" name="status">
															<option value="">Select Status</option>
															<option value="Active">Active</option>
															<option value="Inactive">Inactive</option>
															</select>
														</div>
														<!-- <div class="col-lg-6">
															<label>Latitude:</label>
															<input type="number" class="form-control" required name="latlong" id="latlong"  placeholder="Enter latitude" />
													   	</div> -->
														<div class="col-lg-6">
														   <label>I want to work:</label>
														   <select id="working_type" required class="form-control" name="working_type">
															<option value="">Select Status</option>
															<option value="Part Time">Part Time</option>
															<option value="Full Time">Full Time</option>
															</select>
														</div>	
														
												   	</div>
													<div class="form-group row">
												        <div class="col-lg-12">
													    	<label>Certifications I have obtained:</label></br>
															<input type="checkbox" name="technician_certificate[]" value="ASEA1">ASE A1</br>
	                                                        <input type="checkbox" name="technician_certificate[]" value="ASEA2">ASE A2</br>
	                                                        <input type="checkbox" name="technician_certificate[]" value="ASEA3">ASE A3</br>
	                                                        <input type="checkbox" name="technician_certificate[]" value="ASEA4">ASE A4</br>
	                                                        <input type="checkbox" name="technician_certificate[]" value="ASEA5">ASE A5</br>
	                                                        <input type="checkbox" name="technician_certificate[]" value="ASEA6">ASE A6</br>
	                                                        <input type="checkbox" name="technician_certificate[]" value="ASEA7">ASE A7</br>
	                                                        <input type="checkbox" name="technician_certificate[]" value="ASEA8">ASE A8</br>
	                                                        <input type="checkbox" name="technician_certificate[]" value="ASEA9">ASE A9</br>
	                                                        <input type="checkbox" name="technician_certificate[]" value="ASEL1">ASE L1</br>
	                                                        <input type="checkbox" name="technician_certificate[]" value="Epa609">Epa609</br>
	                                                        <input type="checkbox" name="technician_certificate[]" value="AirBag">Air Bag</br>
	                                                        <input type="checkbox" name="technician_certificate[]" value="MACSAC">MACS A/C</br>
	                                                        <input type="checkbox" name="technician_certificate[]" value="SmogLicensed">Smog Licensed</br>
	                                                        <input type="checkbox" name="technician_certificate[]" value="BrakeandLampCertified">Brake and Lamp Certified</br>
														</div>	
														
													</div>
										   		</div>
												<div class="card-footer">
													<div class="row">
														<div class="col-lg-6">
															<button type="submit" class="btn btn-primary mr-2">Save</button>
															<button type="reset" class="btn btn-secondary">Reset</button>
														</div>
													</div>
												</div>
											</form>
											<!--end::Form-->
										</div>
										<!--end::Card-->
									</div>
								</div>
							</div>
							<!--end::Container-->
						</div>
						<!--end::Entry-->
					</div>
					<!-- <div class="col-lg-6">
															<label>Select Offer:</label>
															<select name="offer_select" id="" class="form-control offer_select"></select>
													</div> -->
                    <!--end::Content-->
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                    
					<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>  
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
                    <script>
					$(".alphaonly").keydown(function(event){
        var inputValue = event.which;
        // console.log(inputValue);
        if (!(inputValue >= 65 && inputValue <= 123)
                            && (inputValue != 32 && inputValue != 0)
                            && ( inputValue != 8)
                            && (inputValue != 9)){
                                event.preventDefault(); 
                        }
    });
					 function validatepannumber(){
                        var pannumber = document.getElementById("technician_pan").value;
                        if(pannumber==""){
                            return true;
                        }else{
                            var regpan = /^([A-Z]){5}([0-9]){4}([A-Z]){1}?$/;
                            if(regpan.test(pannumber) == false)
                                {
									alert("Not a valid Pan Number");
	   document.getElementById("technician_pan").value='';
	   return false;
                                }else{
                                return true;
                                }
                            }
                        }
						function alphanumeric(inputtxt)
{ 
	// alert(inputtxt);
var letters = /^[0-9a-zA-Z]+$/;

if(inputtxt.match(letters))
{
// alert('Your registration number have accepted : you can try another');
// document.form1.text1.focus();
return true;
}
else
{
  alert('Please input alphanumeric characters only');
  document.getElementById("ifscnumber").value='';
return false;
}
}
				function duplicatecheck(){
	var value=document.getElementById("technician_phonenumber").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Phone Number");
	   document.getElementById("technician_phonenumber").value='';
	   return false;
     }


 $.ajax({
	url: websiteurl + 'Technician/phonenocheckexist',
        type: 'POST',
        data: {
            key: value
        },
        dataType: 'json',
        success: function(data) {
		
			if(data==2){
				alert('Phone Number is Already exists');
				document.getElementById("technician_phonenumber").value='';
			}
        }
    });
}
function duplicatecheckalter(){
	var value=document.getElementById("technician_alternatephonenumber").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Phone Number");
	   document.getElementById("technician_alternatephonenumber").value='';
	   return false;
     }


}
function duplicatecheckemail(){
	var value=document.getElementById("technician_email").value;
	
 $.ajax({
	url: websiteurl + 'Technician/emailcheckexist',
        type: 'POST',
        data: {
            key: value
        },
        dataType: 'json',
        success: function(data) {
		
			if(data==2){
				alert('Email is Already exists');
				document.getElementById("technician_email").value='';
			}
        }
    });
}

function adharcheck(){
	var value=document.getElementById("technician_aadhar").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{6})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Adhar Number");
	   document.getElementById("technician_aadhar").value='';
	   return false;
     }
}

function pincodevalidation(){
	var value=document.getElementById("technician_pincode").value;
	
	var phoneno = /^\(?([1-9]{1})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{2})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Pincode Number");
	   document.getElementById("technician_pincode").value='';
	   return false;
     }
}
function validategstnumber(){
                            var cgstnumber = document.getElementById("gstnumber").value;
                            if(cgstnumber==""){
                            return true;
                        }else{
                            var regpan = /\d{2}[A-Z]{5}\d{4}[A-Z]{1}[A-Z\d]{1}[Z]{1}[A-Z\d]{1}/;
                            if(regpan.test(cgstnumber) == false)
                            {
								alert("Not a valid GST Number");
	   document.getElementById("gstnumber").value='';
                                   
                                    return false;
                                }else{
                                return true;
                                }
                            }
                        }
document.getElementById("technician_name").addEventListener('keydown', function (e) {
  if (this.value.length === 0 && e.which === 32) e.preventDefault();
  if (this.value.length === 0 && e.which === 48) e.preventDefault();
  //special character
  if (e.which === 56 || e.which === 53 || e.which === 51 || e.which === 49 || e.which === 50 || e.which === 52 || e.which === 48 ) e.preventDefault();
});

		
	$(document).ready(function(){
						

		//Multi select with json value from ajax
		$('.vehiclename').select2({
        placeholder: '--- Select Vehicle ---',
        ajax: {
       	url: websiteurl + 'Technician/vehiclelist',
        dataType: 'json',
       	delay: 250,
        processResults: function (data) {
			console.log(data);
        return {
            results: $.map(data, function(obj)
        {
			return { id: obj.vehb_make, text: obj.vehb_make };
         
        })
        };
        },
        cache: true
        }
        });

        //Multi Select With Hard Code Values
        $('.expertise').select2({
        placeholder: '--- Select Expertise ---',
        
        });
	});
</script>