
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Subheader-->
					
						<!--end::Subheader-->
						<!--begin::Entry-->
						<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
							<div class="container">
								<div class="row">
									<div class="col-lg-12">
										<!--begin::Card-->
										<div class="card card-custom gutter-b example example-compact">
											
											<?php foreach($technician as $row){} ?>
											<!--begin::Form-->
											
											<form class="form" method="post" id="resetform" action="<?php echo  base_url('admin/Technician/update'); ?>" enctype="multipart/form-data" >
											<div class="card-body">
													<div class="form-group row">
														<div class="col-lg-12">
														<label>Technician Name:</label>
															<input type="text" required name="technician_name" value="<?php echo $row->puser_name;?>" class="form-control" id="technician_name" placeholder="Enter Technician Name" />
                                                         <input type="hidden" required name="puser_id" value="<?php echo $row->puser_id;?>" class="form-control" id="puser_id" placeholder="Enter English Title" />
                                                        
														</div></div>
														<div class="form-group row">
														<div class="col-lg-12">
														<label>Technician Email:</label>
															<input type="email" required name="technician_email" value="<?php echo $row->puser_email;?>" class="form-control" onchange="duplicatecheckemail();" id="technician_email" placeholder="Enter Technician Email" />
                                                        
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
														<div class="col-lg-12">
														<label>Adhar Number:</label>
															<input type="text" required name="technician_aadhar" value="<?php echo $row->puser_adhar_number;?>" onchange="adharcheck();" class="form-control" id="technician_aadhar" placeholder="Enter Technician Adhar Number" />
                                                        
														</div>
														</div>
														<div class="form-group row">
														<div class="col-lg-12">
														<label>Pan Number:</label>
															<input type="text" required name="technician_pan" value="<?php echo $row->puser_pan_number;?>" class="form-control" onchange="validatepannumber();" id="technician_pan" placeholder="Enter Technician Pan Number" />
                                                        
                                                        </div>
                                                    </div>
													<div class="form-group row">
														<div class="col-lg-12">
														<label>Address:</label>
															<textarea cols="80" id="technician_address" class="form-control" value="<?php echo $row->puser_address;?>" name="technician_address" rows="" placeholder="Enter Technician Address"><?php echo $row->puser_address;?></textarea>
                                                         </div>
														</div>
														<div class="form-group row">
														<div class="col-lg-12">
														<label>Pincode:</label>
															<input type="number" required name="technician_pincode" value="<?php echo $row->puser_pincode;?>" class="form-control" onchange="pincodevalidation();" id="technician_pincode" placeholder="Enter Technician Pincode" />
                                                        
														</div>
                                                    </div>
													<div class="form-group row">
														<div class="col-lg-12">
														<label>Contact Number:</label>
												   <input type="number" class="form-control" onchange="duplicatecheck()" value="<?php echo $row->puser_phno;?>" required name="technician_phonenumber" id="technician_phonenumber"  placeholder="Enter Technician Phone Number" />
													   
														 </div>
														</div>
														<div class="form-group row">
														<div class="col-lg-12">
														<label>Alternate Contact Number:</label>
												   <input type="number" class="form-control" onchange="duplicatecheckalter()" value="<?php echo $row->technician_alternatephonenumber;?>"  name="technician_alternatephonenumber" id="technician_alternatephonenumber"  placeholder="Enter Technician Phone Number" />
													
														 </div>
														</div>
														<!-- <div class="form-group row">
															<div class="col-lg-12">
																<label>Latitude:</label>
												   				<input type="text" class="form-control" required name="latlong" value="<?php echo $row->puser_lat_long;?>" id="latlong"  placeholder="Enter Technician lat long" />
															</div>
														</div>
														<div class="form-group row">
															<div class="col-lg-12">
																<label>Longitude:</label>
												   				<input type="number" class="form-control"  value="<?php echo $row->puser_long;?>" required name="long" id="long"  placeholder="Enter Technician long" />
															</div>
														</div> -->
															
														<div class="form-group row">
														<div class="col-lg-12">
														<label>Bank Account Number:</label>
												   <input type="number" class="form-control" min="9999999999" value="<?php echo $row->puser_bankacc_number;?>" max="999999999999999999999999999999" required name="technician_bankacc" id="technician_bankacc"  placeholder="Enter Technician Bank Account Number" />
														 </div>
														</div>

														<div class="form-group row">
														<div class="col-lg-12">
														<label>IFSC number:</label>
												   <input type="text" class="form-control alphaonly" maxlength="11" value="<?php echo $row->puser_bankacc_ifsc;?>" required name="ifscnumber" id="ifscnumber"  placeholder="Enter Technician IFSC Number" />
													 </div>
														</div>

														<div class="form-group row">
														<div class="col-lg-12">
														<label>GST Number:</label>
												   <input type="text"  pattern=".*\S+.*" onchange="validategstnumber();" value="<?php echo $row->puser_GST_number;?>" class="form-control" required name="gstnumber" id="gstnumber"  placeholder="Enter Technician GST Number" />
														  </div>
														</div>

														<div class="form-group row">
														<div class="col-lg-12">
															<label>Profile Image:</label>
                                                            	<a target="_blank" href="<?php echo base_url() ?>/assets/images/technician/<?php echo $row->puser_profile_image; ?>"><img style="width: 70px; height: 70px" src="<?php 
																if($row->puser_profile_image==""){
																	echo base_url('/assets/images/noimage.png'); 
																	}else{
																		echo base_url() ?>/assets/images/technician/<?php echo  $row->puser_profile_image; 
																	} ?>"></a>
															    <input type="file"  accept="image/x-png,image/jpeg"  name="imageone" class="form-control" id="imageone" />
															 <input type="hidden" pattern=".*\S+.*"  name="hiddenimageone" value="<?php echo $row->puser_profile_image ?>"  class="form-control" />
													    
														</div>
														</div>

														<div class="form-group row">
														<div class="col-lg-12">
											<label >Upload CV</label>
												<div class="row">
											<div class="col-lg-12">
											<?php if($row->puser_document_url==""){  ?>
												<p> There is No CV Uploaded Yet!.</p>
											   <?php  } else{
                                                   $abc=$row->puser_document_url;
                                                   ?>
												<a  class="form-control" href="<?php echo base_url(); ?>assets/images/technician/<?php echo $abc;?>" target="_blank">Uploaded CV</a>
		<?php } 
		
		
		?>	
											</div>
											<div class="col-lg-12">
											<input type="file"   accept=".pdf,.doc,.docx"  name="imagetwo" class="form-control" id="imagetwo" />
											<input type="hidden" id="hiddenimagetwo"  accept=".pdf,.doc,.docx"  name="hiddenimagetwo" value="<?php echo $row->puser_document_url; ?>"  class="form-control" />
												
											</div>
										</div>
													</div>
													</div>

												   <div class="form-group row">
                                                       
													   <div class="col-lg-12">
														   <label>I want to work with:</label>
															<select class="form-control" name="working_type" required>
															<option value="<?php echo $row->working_type; ?>"><?php echo $row->working_type; ?></option>
															<?php  if($row->working_type!="Part Time"){
																?>
																<option value="Part Time">Part Time</option>
																<?php
															}else{
																?>
																<option value="Full Time">Full Time</option>
																<?php
															} ?>
															</select>
															 </div>
															
												   </div>
												   <div class="form-group row">
                                                        <div class="col-lg-12">
														   <label>Technician Type:</label>
															<select class="form-control" name="technician_type" required>
																<option value="">Select Type</option>
																<option value="Type 1" <?php if($row->puser_technician_type=="Type 1") echo "selected";?>>Type 1</option>
																<option value="Type 2" <?php if($row->puser_technician_type=="Type 2") echo "selected";?>>Type 2</option>
																<option value="Type 3" <?php if($row->puser_technician_type=="Type 3") echo "selected";?>>Type 3</option>
															</select>
														</div>	
												   </div>
												   	<div class="form-group row">
                                                        <div class="col-lg-12">
														   <label>Select Expertise:</label>
															<select class="form-control expertise" name="expertise[]" multiple required>
															<?php foreach ($exp as $row1) {?>
																<option value="<?php echo $row1;?>" selected><?php echo $row1;?></option>
															<?php }?>
																<option value="2 WHEELER" >2 WHEELER</option>
																<option value="4 WHEELER" >4 WHEELER</option>
																<option value="BUS" >BUS</option>
																<option value="TRUCK" >TRUCK</option>
															</select>
														</div>	
												   </div>
												   <div class="form-group row">
														<div class="col-lg-12">
														<label>Select Maked Vehicle:</label>
                                                            <select name="vehiclename[]" id="vehiclename" multiple required class="form-control vehiclename">
															<?php 
												// 			print_r($veh_make);exit;
															foreach ($veh_make as $row1) {?>
																<option value="<?php echo $row1; ?>" selected ><?php echo $row1;  ?></option>
															<?php }?>
															</select>
                                                        </div>
                                                    </div>
												    <div class="form-group row">
                                                        <div class="col-lg-12">
														   <label>Select Status:</label>
															<select class="form-control" name="status" required>
																<option value="">Select Status</option>
																<option value="Active" <?php if($row->puser_account_status=="Active") echo "selected";?>>Active</option>
																<option value="Inactive" <?php if($row->puser_account_status=="Inactive") echo "selected";?>>Inactive</option>
															</select>
														</div>	
												   </div>

												   <div class="form-group row"> 
													   <div class="col-lg-12">
															<label>Distance:</label>
															<input type="text" required value="<?php echo $row->technician_order_distance; ?>" name="technician_order_distance" class="form-control" id="technician_order_distance" placeholder="Enter Technician Order Distance" />
                                                        
														</div>		
												   </div>

												   <div class="form-group row"> 
													   <div class="col-lg-12">
															<label>How Many Hours per week:</label>
															<input type="text" required value="<?php echo $row->weekly_available_hours; ?>" name="weekly_available_hours" class="form-control" id="weekly_available_hours" placeholder="Enter Technician Order Distance" />
                                                        
														</div>		
												   </div>

												   <!--<div class="form-group row">
												    	<div class="col-lg-12">
												    		<label>Certifications I have obtained:</label></br>
														  <input type="checkbox" name="technician_certificate[]" value="ASE A1">ASE A1</br>
                                                          <input type="checkbox" name="technician_certificate[]" value="ASE A2">ASE A2</br>
                                                          <input type="checkbox" name="technician_certificate[]" value="ASE A3">ASE A3</br>
                                                          <input type="checkbox" name="technician_certificate[]" value="ASE A4">ASE A4</br>
                                                          <input type="checkbox" name="technician_certificate[]" value="ASE A5">ASE A5</br>
                                                          <input type="checkbox" name="technician_certificate[]" value="ASE A6">ASE A6</br>
                                                          <input type="checkbox" name="technician_certificate[]" value="ASE A7">ASE A7</br>
                                                          <input type="checkbox" name="technician_certificate[]" value="ASE A8">ASE A8</br>
                                                          <input type="checkbox" name="technician_certificate[]" value="ASE A9">ASE A9</br>
                                                          <input type="checkbox" name="technician_certificate[]" value="ASE L1">ASE L1</br>
                                                          <input type="checkbox" name="technician_certificate[]" value="Epa609">Epa609</br>
                                                          <input type="checkbox" name="technician_certificate[]" value="Air Bag">Air Bag</br>
                                                          <input type="checkbox" name="technician_certificate[]" value="MACS A/C">MACS A/C</br>
                                                          <input type="checkbox" name="technician_certificate[]" value="Smog Licensed">Smog Licensed</br>
                                                          <input type="checkbox" name="technician_certificate[]" value="Brake and Lamp Certified">Brake and Lamp Certified</br>
														</div>	</div>-->


												<div class="form-group row">
												   <div class="col-lg-12">
												   <input type="checkbox" name="technician_certificate[]" <?php  $R="ASEA1";
                                                         $t=$row->technician_certificate; if(strpos($t, $R) !== false){echo "checked"; }?> value=<?php echo "ASEA1"; ?>>
                                                              <label>ASE A1</label> 
												   </div>
												   <div class="col-lg-12">
												   <input type="checkbox" <?php  $R="ASEA2";
                                                         $t=$row->technician_certificate; if(strpos($t, $R) !== false){echo "checked"; }?> name="technician_certificate[]" value=<?php echo "ASEA2"; ?>>
                                                              <label>ASE A2</label> 
												   </div>
												   <div class="col-lg-12">
												   <input type="checkbox" <?php  $R="ASEA3";
                                                         $t=$row->technician_certificate; if(strpos($t, $R) !== false){echo "checked"; }?> name="technician_certificate[]" value=<?php echo "ASEA3"; ?>>
                                                              <label>ASE A3</label> 
												   </div>

												   <div class="col-lg-12">
												   <input type="checkbox" <?php  $R="ASEA4";
                                                         $t=$row->technician_certificate; if(strpos($t, $R) !== false){echo "checked"; }?> name="technician_certificate[]" value=<?php echo "ASEA4"; ?>>
                                                              <label>ASE A4</label> 
												   </div>

												   <div class="col-lg-12">
												   <input type="checkbox" <?php  $R="ASE A5";
                                                         $t=$row->technician_certificate; if(strpos($t, $R) !== false){echo "checked"; }?> name="technician_certificate[]" value=<?php echo "ASEA5"; ?>>
                                                              <label>ASE A5</label> 
												   </div>

												   <div class="col-lg-12">
												   <input type="checkbox" <?php  $R="ASEA6";
                                                         $t=$row->technician_certificate; if(strpos($t, $R) !== false){echo "checked"; }?> name="technician_certificate[]" value=<?php echo "ASEA6"; ?>>
                                                              <label>ASE A6</label> 
												   </div>

												   <div class="col-lg-12">
												   <input type="checkbox" <?php  $R="ASEA7";
                                                         $t=$row->technician_certificate; if(strpos($t, $R) !== false){echo "checked"; }?> name="technician_certificate[]" value=<?php echo "ASEA7"; ?>>
                                                              <label>ASE A7</label> 
												   </div>
												   <div class="col-lg-12">
												   <input type="checkbox" <?php  $R="ASEA8";
                                                         $t=$row->technician_certificate; if(strpos($t, $R) !== false){echo "checked"; }?> name="technician_certificate[]" value=<?php echo "ASEA8"; ?>>
                                                              <label>ASE A8</label> 
												   </div>

												   <div class="col-lg-12">
												   <input type="checkbox" <?php  $R="ASEA9";
                                                         $t=$row->technician_certificate; if(strpos($t, $R) !== false){echo "checked"; }?> name="technician_certificate[]" value=<?php echo "ASEA9"; ?>>
                                                              <label>ASE A9</label> 
												   </div>

												   <div class="col-lg-12">
												   <input type="checkbox" <?php  $R="ASEL1";
                                                         $t=$row->technician_certificate; if(strpos($t, $R) !== false){echo "checked"; }?> name="technician_certificate[]" value=<?php echo "ASEL1"; ?>>
                                                              <label>ASE L1</label> 
												   </div>

												   <div class="col-lg-12">
												   <input type="checkbox" <?php  $R="Epa609";
                                                         $t=$row->technician_certificate; if(strpos($t, $R) !== false){echo "checked"; }?> name="technician_certificate[]" value=<?php echo "Epa609"; ?>>
                                                              <label>Epa609</label> 
												   </div>

												   <div class="col-lg-12">
												   <input type="checkbox" <?php  $R="AirBag";
                                                         $t=$row->technician_certificate; if(strpos($t, $R) !== false){echo "checked"; }?> name="technician_certificate[]" value=<?php echo "AirBag"; ?>>
                                                              <label>Air Bag</label> 
												   </div>

												   <div class="col-lg-12">
												   <input type="checkbox" <?php  $R="MACSAC";
                                                         $t=$row->technician_certificate; if(strpos($t, $R) !== false){echo "checked"; }?> name="technician_certificate[]" value=<?php echo "MACSAC"; ?>>
                                                              <label>MACS A/C</label> 
												   </div>

												   <div class="col-lg-12">
												   <input type="checkbox" <?php  $R="SmogLicensed";
                                                         $t=$row->technician_certificate; if(strpos($t, $R) !== false){echo "checked"; }?> name="technician_certificate[]" value=<?php echo "SmogLicensed"; ?>>
                                                              <label>Smog Licensed</label> 
												   </div>

												   <div class="col-lg-12">
												   <input type="checkbox" <?php  $R="BrakeandLampCertified";
                                                         $t=$row->technician_certificate; if(strpos($t, $R) !== false){echo "checked"; }?> name="technician_certificate[]" value=<?php echo "BrakeandLampCertified"; ?>>
                                                              <label>Brake and Lamp Certified</label> 
												   </div>
												</div> 
												
											
												<div class="card-footer">
													<div class="row">
														<div class="col-lg-12">
															<button type="submit" class="btn btn-primary mr-2">Save</button>
															</div>
													</div>
												</div>
											</form>
											<!--end::Form-->
										</div>
										<!--end::Card-->
									</div>
								</div>
							</div>
							<!--end::Container-->
						</div>
						<!--end::Entry-->
					</div>
                    <!--end::Content-->
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                    
					<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>  
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
					<script src="https://cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
					<script>

						$(document).ready(function(){
						
						$('.vehiclename').select2({
                        placeholder: '--- Select Vehicle ---',
                        ajax: {
                       url: websiteurl + 'Technician/vehiclelist',
                        dataType: 'json',
                        delay: 250,
                        processResults: function (data) {
							console.log(data);
                        return {
                            results: $.map(data, function(obj)
                        {
							return { id: obj.vehb_make, text: obj.vehb_make };
                         
                        })
                        };
                        },
                        cache: true
                        }
                        });
                        $('.expertise').select2({
        placeholder: '--- Select Expertise ---',
        
        });
					});
						$(".alphaonly").keydown(function(event){
        var inputValue = event.which;
        // console.log(inputValue);
        if (!(inputValue >= 65 && inputValue <= 123)
                            && (inputValue != 32 && inputValue != 0)
                            && ( inputValue != 8)
                            && (inputValue != 9)){
                                event.preventDefault(); 
                        }
    });
					 function validatepannumber(){
                        var pannumber = document.getElementById("technician_pan").value;
                        if(pannumber==""){
                            return true;
                        }else{
                            var regpan = /^([A-Z]){5}([0-9]){4}([A-Z]){1}?$/;
                            if(regpan.test(pannumber) == false)
                                {
									alert("Not a valid Pan Number");
	   document.getElementById("technician_pan").value='';
	   return false;
                                }else{
                                return true;
                                }
                            }
                        }
				function duplicatecheck(){
	var value=document.getElementById("supllier_phonenumber").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Phone Number");
	   document.getElementById("supllier_phonenumber").value='';
	   return false;
     }
}



function adharcheck(){
	var value=document.getElementById("technician_aadhar").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{6})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Adhar Number");
	   document.getElementById("technician_aadhar").value='';
	   return false;
     }
}

function pincodevalidation(){
	var value=document.getElementById("technician_pincode").value;
	
	var phoneno = /^\(?([1-9]{1})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{2})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Pincode Number");
	   document.getElementById("technician_pincode").value='';
	   return false;
     }
}
function validategstnumber(){
                            var cgstnumber = document.getElementById("gstnumber").value;
                            if(cgstnumber==""){
                            return true;
                        }else{
                            var regpan = /\d{2}[A-Z]{5}\d{4}[A-Z]{1}[A-Z\d]{1}[Z]{1}[A-Z\d]{1}/;
                            if(regpan.test(cgstnumber) == false)
                            {
								alert("Not a valid GST Number");
	   document.getElementById("gstnumber").value='';
                                   
                                    return false;
                                }else{
                                return true;
                                }
                            }
                        }
						function duplicatecheckalter(){
	var value=document.getElementById("technician_alternatephonenumber").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Phone Number");
	   document.getElementById("technician_alternatephonenumber").value='';
	   return false;
     }


}

$("#technician_name, #technician_address").on("keypress", function(e) {
    if (e.which === 32 && !this.value.length)
        e.preventDefault();
    if (e.which === 48 && !this.value.length)
        e.preventDefault();
    
});
						</script>