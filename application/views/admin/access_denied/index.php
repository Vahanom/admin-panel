<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>403 forbidden (CSS hover)</title>
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/access_style.css">
</head>
<body>
<!-- partial:index.partial.html -->
<div class="scene">
  <div class="overlay"></div>
  <div class="overlay"></div>
  <div class="overlay"></div>
  <div class="overlay"></div>
  <span class="bg-403">403</span>
  <div class="text">
    <span class="hero-text"></span>
    <span class="msg">can't let <span>you</span> in.</span>
    <span class="support">
    <a href="javascript:history.back()">Go Back</a>

    </span>
  </div>
  <div class="lock"></div>
</div>
<!-- partial -->
  
</body>
</html>