
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Subheader-->
						<div class="subheader py-2 py-lg-4 subheader-transparent" id="kt_subheader">
							<div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
								<!--begin::Info-->
								<div class="d-flex align-items-center flex-wrap mr-1">
									<!--begin::Heading-->
									<div class="d-flex flex-column">
										<!--begin::Title-->
										<h2 class="text-white font-weight-bold my-2 mr-5">Add Coupon</h2>
										<!--end::Title-->
										<!--begin::Breadcrumb-->
										<div class="d-flex align-items-center font-weight-bold my-2">
											<!--begin::Item-->
											<a href="#" class="opacity-75 hover-opacity-100">
												<i class="flaticon2-shelter text-white icon-1x"></i>
											</a>
											
											<!--end::Item-->
											<!--begin::Item-->
											<span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
											<a href="<?php echo base_url('admin/Dashboard') ?>" class="text-white text-hover-white opacity-75 hover-opacity-100">Dashboard</a>
											<!--end::Item-->
											<!--begin::Item-->
											<span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
											<a href="<?php echo base_url('admin/Coupon/index') ?>" class="text-white text-hover-white opacity-75 hover-opacity-100">Coupon</a>
											<!--end::Item-->
											<!--begin::Item-->
											<!--end::Item-->
											<!--begin::Item-->
											<span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
											<a href="" class="text-white text-hover-white opacity-75 hover-opacity-100">Add Coupon</a>
											<!--end::Item-->
										</div>
										<!--end::Breadcrumb-->
									</div>

									<!--end::Heading-->
								</div>
								<!--end::Info-->
							</div>
						</div>
						<!--end::Subheader-->
						<!--begin::Entry-->
						<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
							<div class="container">
								<div class="row">
									<div class="col-lg-12">
										<!--begin::Card-->
										<div class="card card-custom gutter-b example example-compact">
											<div class="card-header">
												<h3 class="card-title">Add Coupon</h3>
											</div>
											<!--begin::Form-->
											<form class="form" method="post" action="<?php echo  base_url('admin/Coupon/add'); ?>" enctype="multipart/form-data" >
												<div class="card-body">
													<div class="form-group row">
														<div class="col-lg-6">
															<label>Coupon Title:</label>
															<input type="text" required name="Coupon_title" class="form-control" id="Coupon_title" placeholder="Enter Coupon Title" />
                                                        
														</div>
														<div class="col-lg-6">
															<label>Coupon Description:</label>
															<input type="text" required name="Coupon_desc" class="form-control" id="Coupon_desc" placeholder="Enter Coupon Description" />
                                                        
                                                        </div>
                                                    </div>
													
												   <div class="form-group row">
												   <div class="col-lg-6">
														   <label>Select Coupon Type:</label>
														   <select id="coupontype" required onchange="dispaydiv(this.value);" class="form-control" name="coupontype">
															<option value="">Select Coupon Type</option>
															<option value="Flat">Flat</option>
															<option value="Percentage">Percentage</option>
															</select>
															 </div>
															 <div class="col-lg-6" id="Flat" style="display:none;">
														   <label>Flat Amount:</label>
														   <input type="number" required name="Coupon_flat" class="form-control" id="Coupon_flat" placeholder="Enter Coupon Flat Amount" />
                                                        
															 </div>
															 <div class="col-lg-6" id="Percentage"  style="display:none;">
														   <label>Percentage:</label>
														   <input type="number" required name="Coupon_per" class="form-control" id="Coupon_per" placeholder="Enter Coupon Percentage" />
                                                        
															 </div>
												   </div>
												   <div class="form-group row">
                                                        
                                                        <div class="col-lg-6">
															<label>Start Date:</label>
															<input type="date" min='<?php echo date("Y-m-d");?>' onchange="checkdate();"  id="start_date" required name="start_date" class="form-control" placeholder="Enter Start Date" />
														  </div>

														  <div class="col-lg-6">
															<label>End Date:</label>
															<input type="date" min='<?php echo date("Y-m-d");?>' onchange="checkdate();"   id="end_date" required name="end_date" class="form-control" placeholder="Enter End Date" />
														  </div>
                                                    </div>
													<div class="form-group row">
                                                       
													<div class="col-lg-6">
															<label>Maximum Discount:</label>
															<input type="number"  step=".01" id="maximumbillamount" required name="maximumbillamount" class="form-control" placeholder="Enter Maximum Discount Bill Amount" />
														  </div>
														  <div class="col-lg-6">
															<label>Max User Count:</label>
                                                        	<input type="number" id="maxuserlimit" required name="maxuserlimit" class="form-control" placeholder="Enter Max User Limit" />
														 </div>
														
                                                    </div>
													<div class="form-group row">
													<div class="col-lg-6">
															<label>Max Use Per User:</label>
															<input type="number" id="maxuseperuser" required name="maxuseperuser" class="form-control" placeholder="Enter Max Use Per User" />
														  </div>
                                                       
														  <div class="col-lg-6">
															<label>Min Billing Amount:</label>
                                                        	<input type="number" step=".01" id="minimumbillamount" required name="minimumbillamount" class="form-control" placeholder="Enter Minimum Bill Amount" />
														 </div>
                                                    </div>
											
												   <div class="form-group row">
															<div class="col-lg-6">
															<label>Coupon Code:</label>
                                                        	<input type="text" id="couponcode" required name="couponcode" class="form-control" placeholder="Enter Coupon Code" onblur="duplicatecheckname();"/>
														 </div>
															 <div class="col-lg-6">
														   <label>Coupon Status:</label>
														   <select id="status" required class="form-control" name="status">
															<option value="">Select Status</option>
															<option value="Active">Active</option>
															<option value="Inactive">Inactive</option>
															</select>
															 </div>
															 
															 
															
												   </div>
												</div>
												<div class="card-footer">
													<div class="row">
														<div class="col-lg-6">
															<button type="submit" class="btn btn-primary mr-2">Save</button>
															<button type="reset" class="btn btn-secondary">Reset</button>
														</div>
													</div>
												</div>
											</form>
											<!--end::Form-->
										</div>
										<!--end::Card-->
									</div>
								</div>
							</div>
							<!--end::Container-->
						</div>
						<!--end::Entry-->
					</div>
					<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Password Reset</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="response">
      	
      </div>
  
    
    </div>
  </div>
</div>
					<!-- <div class="col-lg-6">
															<label>Select Offer:</label>
															<select name="offer_select" id="" class="form-control offer_select"></select>
													</div> -->
                    <!--end::Content-->
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                    
					<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>  
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
                    <script type="text/javascript">
                    	//duplicate check Coupon Code With Same text
						function duplicatecheckname()
						{
							var value=document.getElementById("couponcode").value;
							
						 	$.ajax(
						 	{
								url: websiteurl + 'Coupon/namecheckexist',
						        type: 'POST',
						        data: 
						        {
						            key: value
						        },
						        dataType: 'json',
						        success: function(data) {
								
									if(data==2){
										alert('Coupon Code is Already exists');
										document.getElementById("couponcode").value='';
									}
						        }
						    });
						}
   function showEditForm(id){
            $.ajax({
                type: "POST", 
				url: websiteurl + 'Reset/geteditModel',
				data: {
				"id": id
				},
                success: function(response) {
                  
                 $("#response").html(response);
                   $("#exampleModalCenter").modal('show');
                  
                }
            });
}


</script>
					
                    <script>
					
					function checkdate()
					{
						var start_date=document.getElementById("start_date").value;
						var end_date = document.getElementById("end_date");

						end_date.setAttribute("min", start_date);

					}	

					function dispaydiv(value)
					{
						if(value=='Flat'){
							document.getElementById(value).style.display = "block";
							document.getElementById('Percentage').style.display = "none";
							document.getElementById('Coupon_per').required = false;
						}else{
							document.getElementById(value).style.display = "block";
							document.getElementById('Flat').style.display = "none";
							document.getElementById('Coupon_flat').required = false;

						}

					}	
document.getElementById("Coupon_title").addEventListener('keydown', function (e) { 
  if (this.value.length === 0 && e.which === 32) e.preventDefault();
  if (this.value.length === 0 && e.which === 48) e.preventDefault();
  //special character
  if (e.which === 56 || e.which === 53 || e.which === 51 || e.which === 49 || e.which === 50 || e.which === 52 || e.which === 48 ) e.preventDefault();
});
						</script>