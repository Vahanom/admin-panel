
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Subheader-->
					
						<!--end::Subheader-->
						<!--begin::Entry-->
						<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
							<div class="container">
								<div class="row">
									<div class="col-lg-12">
										<!--begin::Card-->
										<div class="card card-custom gutter-b example example-compact">
											
											<?php foreach($coupon as $row){} ?>
											<!--begin::Form-->
											
											<form class="form" method="post" id="resetform" action="<?php echo  base_url('admin/Coupon/update'); ?>" enctype="multipart/form-data" >
											<div class="card-body">
													<div class="form-group row">
														<div class="col-lg-12">
														<label>Coupon Title:</label>
															<input type="text" disabled required name="Coupon_title" value="<?php echo $row->coupon_title; ?>" class="form-control" id="Coupon_title" placeholder="Enter Coupon Title" />
                                                        <input type="hidden" required value="<?php echo $row->cpn_id; ?>" name="cpn_id"  class="form-control" id="cpn_id" placeholder="Enter English Title" />
                                                        
														</div></div>
														<div class="form-group row">
														<div class="col-lg-12">
														<label>Coupon Description:</label>
															<input type="text" disabled required name="Coupon_desc"  value="<?php echo $row->cpn_description; ?>" class="form-control" id="Coupon_desc" placeholder="Enter Coupon Description" />
                                                        
                                                        </div>
                                                    </div>
													
																			<div class="form-group row">
                                                       
													<div class="col-lg-12">
													<label>Select Coupon Type:</label>
														   <select id="coupontype" disabled required onchange="dispaydiv(this.value);" class="form-control" name="coupontype">
										
															<option value="<?php echo $row->cpn_type; ?>"><?php echo $row->cpn_type; ?></option>
															<?php  if($row->cpn_type=="Flat"){
																?>
																<option value="Percentage">Percentage</option>
																<?php
															}else{
																?>
																<option value="Flat">Flat</option>
																<?php
															} ?>
															</select>
														</div>
														</div>

														<div class="form-group row" id="Flat">
                                                       
													   <div class="col-lg-12" >
													  <label>Flat Amount:</label>
														   <input type="number" disabled value="<?php echo $row->cpn_flat_amount; ?>" required name="Coupon_flat" class="form-control" id="Coupon_flat" placeholder="Enter Coupon Flat Amount" />
                                                        
														   </div>
														   </div>
														   <div class="form-group row"  id="Percentage">
                                                       
													   <div class="col-lg-12">
													   <label>Percentage:</label>
														   <input type="number" disabled required value="<?php echo $row->cpn_percentage; ?>" name="Coupon_per" class="form-control" id="Coupon_per" placeholder="Enter Coupon Percentage" />
                                                        
														   </div>
														   </div>
														   <div class="form-group row">
                                                       
													   <div class="col-lg-12">
													   <label>Start Date:</label>
															<input type="date" disabled min='<?php echo date("Y-m-d");?>' value="<?php echo $row->cpn_startdate; ?>" onchange="checkdate();"  id="start_date" required name="start_date" class="form-control" placeholder="Enter Start Date" />
														 
														   </div>
														   </div>
														   <div class="form-group row">
                                                       
													   <div class="col-lg-12">
													   <label>End Date:</label>
															<input type="date" disabled min='<?php echo date("Y-m-d");?>' value="<?php echo $row->cpn_enddate; ?>" onchange="checkdate();"   id="end_date" required name="end_date" class="form-control" placeholder="Enter End Date" />
														 
														   </div>
														   </div>
														   <div class="form-group row">
                                                       
													   <div class="col-lg-12">
													   <label>Maximum Discount:</label>
															<input type="number" disabled step=".01" id="maximumbillamount" value="<?php echo $row->cpn_max_discount; ?>" required name="maximumbillamount" class="form-control" placeholder="Enter Maximum Discount Bill Amount" />
														
														   </div>
														   </div>   
														   <div class="form-group row">
                                                       
													   <div class="col-lg-12">
													   <label>Max User Count:</label>
                                                        	<input type="number" disabled id="maxuserlimit" required value="<?php echo $row->cpn_max_users; ?>" name="maxuserlimit" class="form-control" placeholder="Enter Max User Limit" />
														 
														   </div>
														   </div>   
														    <div class="form-group row">
                                                       
													   <div class="col-lg-12">
													   <label>Max Use Per User:</label>
															<input type="number" disabled id="maxuseperuser" value="<?php echo $row->cpn_maxperusers; ?>" required name="maxuseperuser" class="form-control" placeholder="Enter Max Use Per User" />
														 
														   </div>
														   </div> 

														       <div class="form-group row">
                                                       
													   <div class="col-lg-12">
													   <label>Min Billing Amount:</label>
                                                        	<input type="number" disabled step=".01" id="minimumbillamount" value="<?php echo $row->cpn_min_bill_amt; ?>" required name="minimumbillamount" class="form-control" placeholder="Enter Minimum Bill Amount" />
														 
														   </div>
														   </div>     
												   <div class="form-group row">
                                                       
													   <div class="col-lg-12">
													   <label>Coupon Status:</label>
															<select class="form-control" disabled name="status" required>
															<option value="<?php echo $row->cpn_status; ?>"><?php echo $row->cpn_status; ?></option>
															<?php  if($row->cpn_status!="Active"){
																?>
																<option value="Active">Active</option>
																<?php
															}else{
																?>
																<option value="Inactive">Inactive</option>
																<?php
															} ?>
															</select>
															 </div>
															
												   </div>

												   
												  
												   <div class="form-group row">
                                                       
													   <div class="col-lg-12">
													   <label>Coupon Code:</label>
                                                        	<input type="text" disabled id="couponcode" value="<?php echo $row->cpn_code; ?>" required name="couponcode" class="form-control" placeholder="Enter Coupon Code" />
														
														   </div>
														   </div> 
												</div>
												
											
												<div class="card-footer">
													<div class="row">
														<div class="col-lg-12">
															<!-- <button type="submit" class="btn btn-primary mr-2">Save</button> -->
															</div>
													</div>
												</div>
											</form>
											<!--end::Form-->
										</div>
										<!--end::Card-->
									</div>
								</div>
							</div>
							<!--end::Container-->
						</div>
						<!--end::Entry-->
					</div>
                    <!--end::Content-->
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                    
					<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>  
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
                    <script>
				$(document).ready(function(){
					value=document.getElementById('coupontype').value;
	dispaydiv(value);
});
function dispaydiv(value)
					{
						if(value=='Flat'){
							document.getElementById(value).style.display = "block";
							document.getElementById('Percentage').style.display = "none";
							document.getElementById('Coupon_per').required = false;
						}else{
							document.getElementById(value).style.display = "block";
							document.getElementById('Flat').style.display = "none";
							document.getElementById('Coupon_flat').required = false;

						}

					}	
					function checkdate()
					{
						var start_date=document.getElementById("start_date").value;
						var end_date = document.getElementById("end_date");

						end_date.setAttribute("min", start_date);

					}	
						</script>