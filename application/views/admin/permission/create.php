
<style>
.regular-checkbox{
		display: inline-block;
		border-radius: 50%;
		width: 38px;
		height: 38px;
		border: 1px solid red;
	}
	.regular-checkbox input{
		opacity: 0;
		position: absolute;
	}
	.regular-checkbox small{
		width: 100%;
		height: 100%;
		float: left;
	}
	.regular-checkbox input:checked ~ small:after{
		content: '\2714';
		height: 38px; 
		width: 38px;
		color: green;
		font-size: 26px;
		text-align: center;
		float: left;
	}
</style>


					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Subheader-->
						<div class="subheader py-2 py-lg-4 subheader-transparent" id="kt_subheader">
							<div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
								<!--begin::Info-->
								<div class="d-flex align-items-center flex-wrap mr-1">
									<!--begin::Heading-->
									<div class="d-flex flex-column">
										<!--begin::Title-->
										<h2 class="text-white font-weight-bold my-2 mr-5">Add Permission</h2>
										<!--end::Title-->
										<!--begin::Breadcrumb-->
										<div class="d-flex align-items-center font-weight-bold my-2">
											<!--begin::Item-->
											<a href="#" class="opacity-75 hover-opacity-100">
												<i class="flaticon2-shelter text-white icon-1x"></i>
											</a>
											
											<!--end::Item-->
											<!--begin::Item-->
											<span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
											<a href="<?php echo base_url('admin/Dashboard') ?>" class="text-white text-hover-white opacity-75 hover-opacity-100">Dashboard</a>
											<!--end::Item-->
											<!--begin::Item-->
											<span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
											<a href="<?php echo base_url('admin/Employees/index') ?>" class="text-white text-hover-white opacity-75 hover-opacity-100">Employees</a>
											<!--end::Item-->
											<!--begin::Item-->
											<!--end::Item-->
											<!--begin::Item-->
											<span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
											<a href="" class="text-white text-hover-white opacity-75 hover-opacity-100">Add Permission</a>
											<!--end::Item-->
										</div>
										<!--end::Breadcrumb-->
									</div>

									<!--end::Heading-->
								</div>
								<!--end::Info-->
							</div>
						</div>
						<!--end::Subheader-->
						<!--begin::Entry-->
						<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
							<div class="container">
								<div class="row">
									<div class="col-lg-12">
										<!--begin::Card-->
										<div class="card card-custom gutter-b example example-compact">
											
											<!--begin::Form-->
											<div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
	<div class="card">
		<div class="card-header">
			<h4>Add Permission</h4>
    </div>
    <div class="card-header">
	
			<h4>Employee Name : <?php
		
						foreach($empdetails as $row){
						}
							
									echo $row->webuser_name;
									
									?></h4>
		</div>
		<div class="card-body">
		<div class="row">
									<table class="table table-bordered">
		<thead>
      <tr>
        <th></th>
        <th class="text-center">SideBar</th>
        <th class="text-center">Add</th>
        <th class="text-center">View</th>
        <th class="text-center">Update</th>
        <th class="text-center">Check All</th>
        <th class="text-center">Action</th>
      </tr>
    </thead>

	<tbody>
	<tr id="ecase123">
  <?php foreach($defaultpermisions as $columnvalue){
						}
	 if(!empty($columnvalue)){ $emplyee =  json_decode($columnvalue->permission_employee); }?>
        <td>Employee</td>
        <td class="text-center"> 

        <label class="regular-checkbox">
        <input type="checkbox" class="ecase" id="esidebar" <?php if(!empty($emplyee) && $emplyee->sidebar == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
         
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="ecase" id = "eadd"  <?php if(!empty($emplyee) && $emplyee->add == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
          <small></small>
</label>
        
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="ecase" id = "eview"  <?php if(!empty($emplyee) && $emplyee->view == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
          
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="ecase" id = "eupdate" <?php if(!empty($emplyee) && $emplyee->update == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
		 
        </td>
        
		<input type="hidden" value="permission_employee" id="ecolumnname">
    <input type="hidden" value="<?php  echo $row->webuser_id;  ?>" id="emp_id">
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" id="eselectall" <?php if(!empty($emplyee) && $emplyee->update == "true" && $emplyee->view == "true"&& $emplyee->add == "true"&& $emplyee->sidebar == "true"
        ){?> checked<?php }?>/>
          <span class="checkmark"></span>
    <small></small>
</label>
	
        </td>
		<td class="text-center"> 
		<button type="button" id="savebanner" class="btn btn-md" onclick="savepermission('e')">Save</button>
        </td>
      </tr>
      <tr id="bcase123">
  <?php if(!empty($columnvalue)){ $supplier =  json_decode($columnvalue->permission_supplier); }?>
        <td>Branch</td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="bcase" id="bsidebar" <?php if(!empty($supplier) && $supplier->sidebar == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
       
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="bcase" id = "badd"  <?php if(!empty($supplier) && $supplier->add == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
         
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="bcase" id = "bview"  <?php if(!empty($supplier) && $supplier->view == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
         
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="bcase" id = "bupdate" <?php if(!empty($supplier) && $supplier->update == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
		  
        </td>
		<input type="hidden" value="permission_supplier" id="bcolumnname">
    <input type="hidden" value="<?php echo $row->webuser_id ; ?>" id="emp_id">
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" id="bselectall" <?php if(!empty($supplier) && $supplier->update == "true" && $supplier->view == "true"&& $supplier->add == "true"&& $supplier->sidebar == "true"
        ){?> checked<?php }?>/>
          <span class="checkmark"></span>
    <small></small>
</label>
		
        </td>
		<td class="text-center"> 
		<button type="button" id="savebanner" class="btn btn-md" onclick="savepermission('b')">Save</button>
        </td>
      </tr>
      <tr id="ccase123">
    <?php if(!empty($columnvalue)){ $technician =  json_decode($columnvalue->permission_technician); } ?>
        <td>Technician</td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="ccase" id="csidebar" <?php if(!empty($technician) && $technician->sidebar == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
       
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="ccase" id = "cadd"  <?php if(!empty($technician) && $technician->add == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
        
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="ccase" id = "cview"  <?php if(!empty($technician) && $technician->view == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
         
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
    	  <input type="checkbox" class="ccase" id = "cupdate" <?php if(!empty($technician) && $technician->update == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
	
        </td>
		<input type="hidden" value="permission_technician" id="ccolumnname">
    <input type="hidden" value="<?php echo $row->webuser_id ; ?>" id="emp_id">
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" id="cselectall"  <?php if(!empty($technician) && $technician->update == "true" && $technician->view == "true"&& $technician->add == "true"&& $technician->sidebar == "true"
        ){?> checked<?php }?>/>
          <span class="checkmark"></span>
    <small></small>
</label>
	
        </td>
		<td class="text-center"> 
		<button type="button" id="savebanner" class="btn btn-md" onclick="savepermission('c')">Save</button>
        </td>
      </tr>
      <tr id="vcase123">
    <?php if(!empty($columnvalue)){ $vehicle =  json_decode($columnvalue->permission_vehicle	); }  ?>
        <td>Vehicle</td>
        <td class="text-center">
        <label class="regular-checkbox">
        <input type="checkbox" class="vcase" id="vsidebar" <?php if(!empty($vehicle) && $vehicle->sidebar == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label> 
         
        </td>
        <td class="text-center">
        <label class="regular-checkbox">
        <input type="checkbox" class="vcase" id = "vadd"  <?php if(!empty($vehicle) && $vehicle->add == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label> 
         
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="vcase" id = "vview"  <?php if(!empty($vehicle) && $vehicle->view == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
        
        </td>
        <td class="text-center">
        <label class="regular-checkbox">
        <input type="checkbox" class="vcase" id = "vupdate" <?php if(!empty($vehicle) && $vehicle->update == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label> 
		  
        </td>
		<input type="hidden" value="permission_vehicle" id="vcolumnname">
    <input type="hidden" value="<?php echo $row->webuser_id ; ?>" id="emp_id">
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" id="vselectall"  <?php if(!empty($vehicle) && $vehicle->update == "true" && $vehicle->view == "true"&& $vehicle->add == "true"&& $vehicle->sidebar == "true"
        ){?> checked<?php }?>/>
          <span class="checkmark"></span>
    <small></small>
</label>
	
        </td>
		<td class="text-center"> 
		<button type="button" id="savebanner" class="btn btn-md" onclick="savepermission('v')">Save</button>
        </td>
      </tr>
	    <tr id="lcase123">
    <?php if(!empty($columnvalue)){ $sparecategory =  json_decode($columnvalue->permission_sparecategory); }  ?>
        <td>Spare Category</td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="lcase" id="lsidebar" <?php if(!empty($sparecategory) && $sparecategory->sidebar == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
        
        </td>
        <td class="text-center">
        <label class="regular-checkbox">
        <input type="checkbox" class="lcase" id = "ladd"  <?php if(!empty($sparecategory) && $sparecategory->add == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
          
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="lcase" id = "lview"  <?php if(!empty($sparecategory) && $sparecategory->view == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
         
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="lcase" id = "lupdate" <?php if(!empty($sparecategory) && $sparecategory->update == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
        <small></small>
        </label>
		    </td>
		<input type="hidden" value="permission_sparecategory" id="lcolumnname">
    <input type="hidden" value="<?php echo $row->webuser_id ; ?>" id="emp_id">
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" id="lselectall" <?php if(!empty($sparecategory) && $sparecategory->update == "true" && $sparecategory->view == "true"&& $sparecategory->add == "true"&& $sparecategory->sidebar == "true"
        ){?> checked<?php }?>/>
          <span class="checkmark"></span>
    <small></small>
</label>
		
        </td>
		<td class="text-center"> 
		<button type="button" id="savebanner" class="btn btn-md" onclick="savepermission('l')">Save</button>
        </td>
      </tr>
      <tr id="veccase123">
    <?php if(!empty($columnvalue)){ $spare =  json_decode($columnvalue->permission_spare); }  ?>
        <td>Spare</td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="veccase" id="vecsidebar" <?php if(!empty($spare) && $spare->sidebar == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
      </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="veccase" id = "vecadd"  <?php if(!empty($spare) && $spare->add == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="veccase" id = "vecview"  <?php if(!empty($spare) && $spare->view == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
         
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="veccase" id = "vecupdate" <?php if(!empty($spare) && $spare->update == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
		 
        </td>
		<input type="hidden" value="permission_spare" id="veccolumnname">
    <input type="hidden" value="<?php echo $row->webuser_id ; ?>" id="emp_id">
        <td class="text-center"> 
        <label class="regular-checkbox">
 <input type="checkbox" id="vecselectall" <?php if(!empty($spare) && $spare->update == "true" && $spare->view == "true"&& $spare->add == "true"&& $spare->sidebar == "true"
        ){?> checked<?php }?>/>
          <span class="checkmark"></span>
    <small></small>
</label>
        </td>
		<td class="text-center"> 
		<button type="button" id="savebanner" class="btn btn-md" onclick="savepermission('vec')">Save</button>
        </td>
      </tr>
      <tr id="dcase123">
    <?php if(!empty($columnvalue)){ $servicecat =  json_decode($columnvalue->permission_servicecat); } ?>
        <td>Service category</td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="dcase" id="dsidebar" <?php if(!empty($servicecat) && $servicecat->sidebar == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="dcase" id = "dadd"  <?php if(!empty($servicecat) && $servicecat->add == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
         
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="dcase" id = "dview"  <?php if(!empty($servicecat) && $servicecat->view == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
        
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="dcase" id = "dupdate" <?php if(!empty($servicecat) && $servicecat->update == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
		 
        </td>
		<input type="hidden" value="permission_servicecat" id="dcolumnname">
    <input type="hidden" value="<?php echo $row->webuser_id ; ?>" id="emp_id">
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" id="dselectall" <?php if(!empty($servicecat) && $servicecat->update == "true" && $servicecat->view == "true"&& $servicecat->add == "true"&& $servicecat->sidebar == "true"
        ){?> checked<?php }?>/>
          <span class="checkmark"></span>
    <small></small>
</label>
		
        </td>
		<td class="text-center"> 
		<button type="button" id="savebanner" class="btn btn-md" onclick="savepermission('d')">Save</button>
        </td>
      </tr>
      <tr id="ocase123">
    <?php if(!empty($columnvalue)){ $service =  json_decode($columnvalue->permission_service); } ?>
        <td>Service</td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="ocase" id="osidebar" <?php if(!empty($service) && $service->sidebar == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
         
        </td>
        <td class="text-center">
        <label class="regular-checkbox">
        <input type="checkbox" class="ocase" id = "oadd"  <?php if(!empty($service) && $service->add == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label> 
       
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="ocase" id = "oview"  <?php if(!empty($service) && $service->view == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
        
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="ocase" id = "oupdate" <?php if(!empty($service) && $service->update == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
		 
        </td>
		<input type="hidden" value="permission_service" id="ocolumnname">
    <input type="hidden" value="<?php echo $row->webuser_id ; ?>" id="emp_id">
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" id="oselectall"  <?php if(!empty($service) && $service->update == "true" && $service->view == "true"&& $service->add == "true"&& $service->sidebar == "true"
        ){?> checked<?php }?>/>
          <span class="checkmark"></span>
    <small></small>
</label>
	
        </td>
		<td class="text-center"> 
		<button type="button" id="savebanner" class="btn btn-md" onclick="savepermission('o')">Save</button>
        </td>
      </tr>
      <tr id="gcase123">
    <?php if(!empty($columnvalue)){ $tool =  json_decode($columnvalue->permission_tool);  } ?>
        <td>Tool</td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="gcase" id="gsidebar" <?php if(!empty($tool) && $tool->sidebar == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
         
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="gcase" id = "gadd"  <?php if(!empty($tool) && $tool->add == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
         
        </td>
        <td class="text-center">
        <label class="regular-checkbox">
   
        <input type="checkbox" class="gcase" id = "gview"  <?php if(!empty($tool) && $tool->view == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label> 
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="gcase" id = "gupdate" <?php if(!empty($tool) && $tool->update == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
		  
        </td>
		<input type="hidden" value="permission_tool" id="gcolumnname">
    <input type="hidden" value="<?php echo $row->webuser_id ; ?>" id="emp_id">
        <td class="text-center">
        <label class="regular-checkbox">
        <input type="checkbox" id="gselectall"  <?php if(!empty($tool) && $tool->update == "true" && $tool->view == "true"&& $tool->add == "true"&& $tool->sidebar == "true"
        ){?> checked<?php }?>/>
          <span class="checkmark"></span>
    <small></small>
</label> 
		
        </td>
		<td class="text-center"> 
		<button type="button" id="savebanner" class="btn btn-md" onclick="savepermission('g')">Save</button>
        </td>
      </tr>
	 
      <tr id="vhrcase123">
    <?php if(!empty($columnvalue)){ $pollution =  json_decode($columnvalue->permission_pollution); } ?>
        <td>Pollution</td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="vhrcase" id="vhrsidebar"  <?php if(!empty($pollution) && $pollution->sidebar == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
          
        </td>
        <td class="text-center">  <label class="regular-checkbox">
        <input type="checkbox" class="vhrcase" id = "vhradd"   <?php if(!empty($pollution) && $pollution->add == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
          
        </td>
        <td class="text-center"> 
          <label class="regular-checkbox">
          <input type="checkbox" class="vhrcase" id = "vhrview"   <?php if(!empty($pollution) && $pollution->view == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label> 
        
        </td>
        <td class="text-center">
        <label class="regular-checkbox">
        <input type="checkbox" class="vhrcase" id = "vhrupdate"  <?php if(!empty($pollution) && $pollution->update == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label> 
	
        </td>
		<input type="hidden" value="permission_pollution" id="vhrcolumnname">
    <input type="hidden" value="<?php echo $row->webuser_id ; ?>" id="emp_id">
        <td class="text-center"> 
        <label class="regular-checkbox">
    
		<input type="checkbox" id="vhrselectall" <?php if(!empty($pollution) && $pollution->update == "true" && $pollution->view == "true"&& $pollution->add == "true"&& $pollution->sidebar == "true"
        ){?> checked<?php }?>/>
          <span class="checkmark"></span>
    <small></small>
</label>
        </td>
		<td class="text-center"> 
		<button type="button" id="savebanner" class="btn btn-md" onclick="savepermission('vhr')">Save</button>
        </td>
      </tr>
      <tr id="podcase123">
    <?php if(!empty($columnvalue)){ $insurance =  json_decode($columnvalue->permission_insurance); }
    
    ?>
        <td>Insurance</td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="podcase" id="podsidebar" <?php if(!empty($insurance) && $insurance->sidebar == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
          
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="podcase" id = "podadd"  <?php if(!empty($insurance) && $insurance->add == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
         
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="podcase" id = "podview"  <?php if(!empty($insurance) && $insurance->view == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
         
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="podcase" id = "podupdate" <?php if(!empty($insurance) && $insurance->update == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
		 
        </td>
		<input type="hidden" value="permission_insurance" id="podcolumnname">
    <input type="hidden" value="<?php echo $row->webuser_id ; ?>" id="emp_id">
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" id="podselectall" <?php if(!empty($insurance) && $insurance->update == "true" && $insurance->view == "true"&& $insurance->add == "true"&& $insurance->sidebar == "true"
        ){?> checked<?php }?>/>
          <span class="checkmark"></span>
    <small></small>
</label>
		
        </td>
		<td class="text-center"> 
		<button type="button" id="savebanner" class="btn btn-md" onclick="savepermission('pod')">Save</button>
        </td>
      </tr>
	  <tr id="frccase123">
    <?php if(!empty($columnvalue)){ $coupon =  json_decode($columnvalue->permission_coupon); }  ?>
        <td>Coupon</td>
        <td class="text-center">
        <label class="regular-checkbox">
        <input type="checkbox" class="frccase" id="frcsidebar" <?php if(!empty($coupon) && $coupon->sidebar == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label> 
         
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="frccase" id = "frcadd"  <?php if(!empty($coupon) && $coupon->add == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
        
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="frccase" id = "frcview"  <?php if(!empty($coupon) && $coupon->view == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
         
        </td>
        <td class="text-center">
        <label class="regular-checkbox">
        <input type="checkbox" class="frccase" id = "frcupdate" <?php if(!empty($coupon) && $coupon->update == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label> 
		 
        </td>
		<input type="hidden" value="permission_coupon" id="frccolumnname">
		<input type="hidden" value="<?php echo $row->webuser_id ; ?>" id="emp_id">
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" id="frcselectall" <?php if(!empty($coupon) && $coupon->update == "true" && $coupon->view == "true"&& $coupon->add == "true"&& $coupon->sidebar == "true"
        ){?> checked<?php }?>/>
          <span class="checkmark"></span>
    <small></small>
</label>
	
        </td>
		<td class="text-center"> 
		<button type="submit" id="savebanner" class="btn btn-md" onclick="savepermission('frc')">Save</button>
        </td>
      </tr>
	  <tr id="billcase123">
    <?php if(!empty($columnvalue)){ $permission =  json_decode($columnvalue->permission_permission); }  ?>
        <td>Permission</td>
        <td class="text-center">
        <label class="regular-checkbox">
        <input type="checkbox" class="billcase" id="billsidebar" <?php if(!empty($permission) && $permission->sidebar == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label> 
         
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="billcase" id = "billadd"  <?php if(!empty($permission) && $permission->add == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
         
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="billcase" id = "billview"  <?php if(!empty($permission) && $permission->view == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
         
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="billcase" id = "billupdate" <?php if(!empty($permission) && $permission->update == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
		
        </td>
		<input type="hidden" value="permission_permission" id="billcolumnname">
		<input type="hidden" value="<?php echo $row->webuser_id ; ?>" id="emp_id">
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" id="billselectall" <?php if(!empty($permission) && $permission->update == "true" && $permission->view == "true"&& $permission->add == "true"&& $permission->sidebar == "true"
        ){?> checked<?php }?>/>
          <span class="checkmark"></span>
    <small></small>
</label>
		
        </td>
		<td class="text-center"> 
		<button type="submit" id="savebanner" class="btn btn-md" onclick="savepermission('bill')">Save</button>
        </td>
      </tr>
      <tr id="toolcase123">
    <?php if(!empty($columnvalue)){ $banner =  json_decode($columnvalue->permission_banner); }  ?>
        <td>Banner/Pop-up</td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="toolcase" id="toolsidebar" <?php if(!empty($banner) && $banner->sidebar == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
       
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="toolcase" id = "tooladd"  <?php if(!empty($banner) && $banner->add == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
         
        </td>
        <td class="text-center">
        <label class="regular-checkbox">
        <input type="checkbox" class="toolcase" id = "toolview"  <?php if(!empty($banner) && $banner->view == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label> 
          
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="toolcase" id = "toolupdate" <?php if(!empty($banner) && $banner->update == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
	
        </td>
		<input type="hidden" value="permission_banner" id="toolcolumnname">
		<input type="hidden" value="<?php echo $row->webuser_id ; ?>" id="emp_id">
        <td class="text-center"> 
        <label class="regular-checkbox">
   
		<input type="checkbox" id="toolselectall" <?php if(!empty($banner) && $banner->update == "true" && $banner->view == "true"&& $banner->add == "true"&& $banner->sidebar == "true"
        ){?> checked<?php }?>/>
          <span class="checkmark"></span>
    <small></small>
</label>
        </td>
		<td class="text-center"> 
		<button type="submit" id="savebanner" class="btn btn-md" onclick="savepermission('tool')">Save</button>
        </td>
      </tr>
      <!-- notifcation -->

      <tr id="notcase123">
    <?php if(!empty($columnvalue)){ $notification =  json_decode($columnvalue->permission_notification); }
    
    ?>
        <td>Notification</td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="notcase" id="notsidebar" <?php if(!empty($notification) && $notification->sidebar == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
          
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="notcase" id = "notadd"  <?php if(!empty($notification) && $notification->add == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
         
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="notcase" id = "notview"  <?php if(!empty($notification) && $notification->view == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
         
        </td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="notcase" id = "notupdate" <?php if(!empty($notification) && $notification->update == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
         
        </td>
        <input type="hidden" value="permission_notification" id="notcolumnname">
    <input type="hidden" value="<?php echo $row->webuser_id ; ?>" id="emp_id">
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" id="notselectall" <?php if(!empty($notification) && $notification->update == "true" && $notification->view == "true"&& $notification->add == "true"&& $notification->sidebar == "true"
        ){?> checked<?php }?>/>
          <span class="checkmark"></span>
    <small></small>
</label>
        
        </td>
        <td class="text-center"> 
        <button type="button" id="savebanner" class="btn btn-md" onclick="savepermission('not')">Save</button>
        </td>
      </tr>
      <!-- about us -->
      <tr id="aboutcase123">
    <?php if(!empty($columnvalue)){ $aboutus =  json_decode($columnvalue->permission_aboutus); } ?>
        <td>About us</td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="aboutcase" id="aboutsidebar"  <?php if(!empty($aboutus) && $aboutus->sidebar == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
          
        </td>
        <td class="text-center">  <label class="regular-checkbox">
        <input type="checkbox" class="aboutcase" id = "aboutadd"   <?php if(!empty($aboutus) && $aboutus->add == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
          
        </td>
        <td class="text-center"> 
          <label class="regular-checkbox">
          <input type="checkbox" class="aboutcase" id = "aboutview"   <?php if(!empty($aboutus) && $aboutus->view == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label> 
        
        </td>
        <td class="text-center">
        <label class="regular-checkbox">
        <input type="checkbox" class="aboutcase" id = "aboutupdate"  <?php if(!empty($aboutus) && $aboutus->update == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label> 
    
        </td>
        <input type="hidden" value="permission_aboutus" id="aboutcolumnname">
    <input type="hidden" value="<?php echo $row->webuser_id ; ?>" id="emp_id">
        <td class="text-center"> 
        <label class="regular-checkbox">
    
        <input type="checkbox" id="aboutselectall" <?php if(!empty($aboutus) && $aboutus->update == "true" && $aboutus->view == "true"&& $aboutus->add == "true"&& $aboutus->sidebar == "true"
        ){?> checked<?php }?>/>
          <span class="checkmark"></span>
    <small></small>
</label>
        </td>
        <td class="text-center"> 
        <button type="button" id="savebanner" class="btn btn-md" onclick="savepermission('about')">Save</button>
        </td>
      </tr>
      <!-- Terms-->
      
      <tr id="termscase123">
    <?php if(!empty($columnvalue)){ $terms =  json_decode($columnvalue->permission_terms); } ?>
        <td>Terms & Conditions</td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="termscase" id="termssidebar"  <?php if(!empty($terms) && $terms->sidebar == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
          
        </td>
        <td class="text-center">  <label class="regular-checkbox">
        <input type="checkbox" class="termscase" id = "termsadd"   <?php if(!empty($terms) && $terms->add == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
          
        </td>
        <td class="text-center"> 
          <label class="regular-checkbox">
          <input type="checkbox" class="termscase" id = "termsview"   <?php if(!empty($terms) && $terms->view == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label> 
        
        </td>
        <td class="text-center">
        <label class="regular-checkbox">
        <input type="checkbox" class="termscase" id = "termsupdate"  <?php if(!empty($terms) && $terms->update == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label> 
    
        </td>
        <input type="hidden" value="permission_terms" id="termscolumnname">
    <input type="hidden" value="<?php echo $row->webuser_id ; ?>" id="emp_id">
        <td class="text-center"> 
        <label class="regular-checkbox">
    
        <input type="checkbox" id="termsselectall" <?php if(!empty($terms) && $terms->update == "true" && $terms->view == "true"&& $terms->add == "true"&& $terms->sidebar == "true"
        ){?> checked<?php }?>/>
          <span class="checkmark"></span>
    <small></small>
</label>
        </td>
        <td class="text-center"> 
        <button type="button" id="savebanner" class="btn btn-md" onclick="savepermission('terms')">Save</button>
        </td>
      </tr>

       <!-- support-->
      
      <tr id="supportcase123">
    <?php if(!empty($columnvalue)){ $support =  json_decode($columnvalue->permission_support); } ?>
        <td>Support</td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="supportcase" id="supportsidebar"  <?php if(!empty($support) && $support->sidebar == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
          
        </td>
        <td class="text-center">  <label class="regular-checkbox">
        <input type="checkbox" class="supportcase" id = "supportadd"   <?php if(!empty($support) && $support->add == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
          
        </td>
        <td class="text-center"> 
          <label class="regular-checkbox">
          <input type="checkbox" class="supportcase" id = "supportview"   <?php if(!empty($support) && $support->view == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label> 
        
        </td>
        <td class="text-center">
        <label class="regular-checkbox">
        <input type="checkbox" class="supportcase" id = "supportupdate"  <?php if(!empty($support) && $support->update == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label> 
    
        </td>
        <input type="hidden" value="permission_support" id="supportcolumnname">
    <input type="hidden" value="<?php echo $row->webuser_id ; ?>" id="emp_id">
        <td class="text-center"> 
        <label class="regular-checkbox">
    
        <input type="checkbox" id="supportselectall" <?php if(!empty($support) && $support->update == "true" && $support->view == "true"&& $support->add == "true"&& $support->sidebar == "true"
        ){?> checked<?php }?>/>
          <span class="checkmark"></span>
    <small></small>
</label>
        </td>
        <td class="text-center"> 
        <button type="button" id="savebanner" class="btn btn-md" onclick="savepermission('support')">Save</button>
        </td>
      </tr>

<!-- web technician -->
 <tr id="webtechniciancase123">
    <?php if(!empty($columnvalue)){ $webtechnician =  json_decode($columnvalue->permission_webtechnician); } ?>
        <td>Web Technician</td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="webtechniciancase" id="webtechniciansidebar"  <?php if(!empty($webtechnician) && $webtechnician->sidebar == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
          
        </td>
        <td class="text-center">  <label class="regular-checkbox">
        <input type="checkbox" class="webtechniciancase" id = "webtechnicianadd"   <?php if(!empty($webtechnician) && $webtechnician->add == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
          
        </td>
        <td class="text-center"> 
          <label class="regular-checkbox">
          <input type="checkbox" class="webtechniciancase" id = "webtechnicianview"   <?php if(!empty($webtechnician) && $webtechnician->view == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label> 
        
        </td>
        <td class="text-center">
        <label class="regular-checkbox">
        <input type="checkbox" class="webtechniciancase" id = "webtechnicianupdate"  <?php if(!empty($webtechnician) && $webtechnician->update == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label> 
    
        </td>
        <input type="hidden" value="permission_webtechnician" id="webtechniciancolumnname">
    <input type="hidden" value="<?php echo $row->webuser_id ; ?>" id="emp_id">
        <td class="text-center"> 
        <label class="regular-checkbox">
    
        <input type="checkbox" id="webtechnicianselectall" <?php if(!empty($webtechnician) && $webtechnician->update == "true" && $webtechnician->view == "true"&& $webtechnician->add == "true"&& $webtechnician->sidebar == "true"
        ){?> checked<?php }?>/>
          <span class="checkmark"></span>
    <small></small>
</label>
        </td>
        <td class="text-center"> 
        <button type="button" id="savebanner" class="btn btn-md" onclick="savepermission('webtechnician')">Save</button>
        </td>
      </tr>

 <!-- inspection-->
      
      <tr id="inspectioncase123">
    <?php if(!empty($columnvalue)){ $inspection =  json_decode($columnvalue->permission_inspection); } ?>
        <td>Inspection</td>
        <td class="text-center"> 
        <label class="regular-checkbox">
        <input type="checkbox" class="inspectioncase" id="inspectionsidebar"  <?php if(!empty($inspection) && $inspection->sidebar == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
          
        </td>
        <td class="text-center">  <label class="regular-checkbox">
        <input type="checkbox" class="inspectioncase" id = "inspectionadd"   <?php if(!empty($inspection) && $inspection->add == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label>
          
        </td>
        <td class="text-center"> 
          <label class="regular-checkbox">
          <input type="checkbox" class="inspectioncase" id = "inspectionview"   <?php if(!empty($inspection) && $inspection->view == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label> 
        
        </td>
        <td class="text-center">
        <label class="regular-checkbox">
        <input type="checkbox" class="inspectioncase" id = "inspectionupdate"  <?php if(!empty($inspection) && $inspection->update == "true"){?> checked<?php }?>>
          <span class="checkmark"></span>
    <small></small>
</label> 
    
        </td>
        <input type="hidden" value="permission_inspection" id="inspectioncolumnname">
    <input type="hidden" value="<?php echo $row->webuser_id ; ?>" id="emp_id">
        <td class="text-center"> 
        <label class="regular-checkbox">
    
        <input type="checkbox" id="inspectionselectall" <?php if(!empty($inspection) && $inspection->update == "true" && $inspection->view == "true"&& $terms->add == "true"&& $inspection->sidebar == "true"
        ){?> checked<?php }?>/>
          <span class="checkmark"></span>
    <small></small>
</label>
        </td>
        <td class="text-center"> 
        <button type="button" id="savebanner" class="btn btn-md" onclick="savepermission('inspection')">Save</button>
        </td>
      </tr>
	  </tbody>
	</table>
	</div>
		</div>
	</div>
</div>
											<!--end::Form-->
										</div>
										<!--end::Card-->
									</div>
								</div>
							</div>
							<!--end::Container-->
						</div>
						<!--end::Entry-->
					</div>
					<!-- <div class="col-lg-6">
															<label>Select Offer:</label>
															<select name="offer_select" id="" class="form-control offer_select"></select>
													</div> -->
                    <!--end::Content-->
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                    
					<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>  
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
                    <script>
					function savepermission(name) {
		
		var columnname = document.getElementById(name+"columnname").value;
		var emp_id = document.getElementById("emp_id").value;
		var sidebar = document.getElementById(name+"sidebar").checked;
		var add = document.getElementById(name+"add").checked;
		var update = document.getElementById(name+"update").checked;
		var view = document.getElementById(name+"view").checked;
		// alert(name);
		
		$.ajax({
				method: 'post',
				url: websiteurl + 'Permission/permissionadd',
				data: {
					"columnname": columnname,
					"sidebar": sidebar,
					"add": add,
					"view": view,
					"update": update,
					"emp_id": emp_id,
				},
				async: true,
				success: function(data){
			
					alert("Permission Saved Successfully");
						window.location.reload();
					
				// 
				},
				error: function(data){
					
				},
			});
				
   
    }
			$(function(){

// add multiple select / deselect functionality
$("#eselectall").click(function () {
	$('#ecase123').css({"background-color":"lightblue"});
	  $('.ecase').prop('checked', this.checked);
});

$(".ecase").click(function(){
	if($(".ecase").length == $(".ecase:checked").length) {
		$("#eselectall").prop('checked', true);
		$('#ecase123').css({"background-color":"lightblue"});

	} else {
		// $("#eselectall").removeAttr("checked");
		$('#eselectall').prop('checked', false);
		$('#ecase123').css({"background-color":"lightblue"});
	}

});
});
$("#bselectall").click(function () {
	$('#bcase123').css({"background-color":"lightblue"});

	  $('.bcase').prop('checked', this.checked);
});

$(".bcase").click(function(){
	
	if($(".bcase").length == $(".bcase:checked").length) {
		
		$('#bcase123').css({"background-color":"lightblue"});
		$("#bselectall").prop('checked', true);
	} else {
		$('#bcase123').css({"background-color":"lightblue"});
		// $("#bselectall").removeAttr("checked");
		$('#bselectall').prop('checked', false);
	}

});

$(function(){

// add multiple select / deselect functionality
$("#cselectall").click(function () {
	$('#ccase123').css({"background-color":"lightblue"});
	  $('.ccase').prop('checked', this.checked);
});

$(".ccase").click(function(){
	if($(".ccase").length == $(".ccase:checked").length) {
		$("#cselectall").prop('checked', true);
		
	$('#ccase123').css({"background-color":"lightblue"});
	} else {
		$('#ccase123').css({"background-color":"lightblue"});
		$('#cselectall').prop('checked', false);
		// $("#cselectall").removeAttr("checked");
	}

});
});

$(function(){

// add multiple select / deselect functionality
$("#vselectall").click(function () {
	$('#vcase123').css({"background-color":"lightblue"});
	  $('.vcase').prop('checked', this.checked);
});

$(".vcase").click(function(){

	if($(".vcase").length == $(".vcase:checked").length) {
	$('#vcase123').css({"background-color":"lightblue"});
		$("#vselectall").prop('checked', true);
	} else {
		$('#vcase123').css({"background-color":"lightblue"});
		// $("#vselectall").removeAttr("checked");
		$('#vselectall').prop('checked', false);
	}

});
});

$(function(){

// add multiple select / deselect functionality
$("#lselectall").click(function () {
	$('#lcase123').css({"background-color":"lightblue"});
	  $('.lcase').prop('checked', this.checked);
});

$(".lcase").click(function(){
	if($(".lcase").length == $(".lcase:checked").length) {
		$("#lselectall").prop('checked', true);
		
	$('#lcase123').css({"background-color":"lightblue"});
	} else {
		$('#lcase123').css({"background-color":"lightblue"});
		// $("#lselectall").removeAttr("checked");
		$('#lselectall').prop('checked', false);
	}

});
});

$(function(){

// add multiple select / deselect functionality
$("#vecselectall").click(function () {
	$('#veccase123').css({"background-color":"lightblue"});
	  $('.veccase').prop('checked', this.checked);
});

$(".veccase").click(function(){
	
	if($(".veccase").length == $(".veccase:checked").length) {
		$('#veccase123').css({"background-color":"lightblue"});
		$("#vecselectall").prop('checked', true);
	} else {
		$('#veccase123').css({"background-color":"lightblue"});
		// $("#vecselectall").removeAttr("checked");
		$('#vecselectall').prop('checked', false);
	}

});
});

$(function(){

// add multiple select / deselect functionality
$("#duepayselectall").click(function () {
	$('#duepay123').css({"background-color":"lightblue"});
	  $('.duepay').prop('checked', this.checked);
});

$(".duepay").click(function(){

	if($(".duepay").length == $(".duepay:checked").length) {
		$('#duepay123').css({"background-color":"lightblue"});
		$("#duepayselectall").prop('checked', true);
	} else {
		$('#duepay123').css({"background-color":"lightblue"});
		// $("#duepayselectall").removeAttr("checked");
		$('#duepayselectall').prop('checked', false);
	}

});
});


$(function(){

// add multiple select / deselect functionality
$("#dselectall").click(function () {
	$('#dcase123').css({"background-color":"lightblue"});
	  $('.dcase').prop('checked', this.checked);
});

$(".dcase").click(function(){

	if($(".dcase").length == $(".dcase:checked").length) {
		$('#dcase123').css({"background-color":"lightblue"});
		$("#dselectall").prop('checked', true);
	} else {
		$('#dcase123').css({"background-color":"lightblue"});
		// $("#dselectall").removeAttr("checked");
		$('#dselectall').prop('checked', false);
	}

});
});
$(function(){

// add multiple select / deselect functionality
$("#oselectall").click(function () {
	$('#ocase123').css({"background-color":"lightblue"});
	  $('.ocase').prop('checked', this.checked);
});

$(".ocase").click(function(){
	if($(".ocase").length == $(".ocase:checked").length) {
		
	$('#ocase123').css({"background-color":"lightblue"});
		$("#oselectall").prop('checked', true);
	} else {
		$('#ocase123').css({"background-color":"lightblue"});
		// $("#oselectall").removeAttr("checked");
		$('#oselectall').prop('checked', false);
	}

});
});
$(function(){

// add multiple select / deselect functionality
$("#gselectall").click(function () {
	$('#gcase123').css({"background-color":"lightblue"});
	  $('.gcase').prop('checked', this.checked);
});

$(".gcase").click(function(){
	if($(".gcase").length == $(".gcase:checked").length) {
		
	$('#gcase123').css({"background-color":"lightblue"});
		$("#gselectall").prop('checked', true);
	} else {
		$('#gcase123').css({"background-color":"lightblue"});
		// $("#gselectall").removeAttr("checked");
		$('#gselectall').prop('checked', false);
	}

});
});
// support
$(function(){

// add multiple select / deselect functionality
$("#supportselectall").click(function () {
    
    $('#supportcase123').css({"background-color":"lightblue"});
      $('.supportcase').prop('checked', this.checked);
});

$(".supportcase").click(function(){
    if($(".supportcase").length == $(".supportcase:checked").length) {
        
    $('#supportcase123').css({"background-color":"lightblue"});
        $("#supportselectall").prop('checked', true);
    } else {
        $('#supportcase123').css({"background-color":"lightblue"});
        // $("#vhrselectall").removeAttr("checked");
        $('#supportselectall').prop('checked', false);
    }

});
});
// Terms
$(function(){

// add multiple select / deselect functionality
$("#termsselectall").click(function () {
   
    $('#termscase123').css({"background-color":"lightblue"});
      $('.termscase').prop('checked', this.checked);
});

$(".termscase").click(function(){
    if($(".termscase").length == $(".termscase:checked").length) {
        
    $('#termscase123').css({"background-color":"lightblue"});
        $("#termsselectall").prop('checked', true);
    } else {
        $('#termscase123').css({"background-color":"lightblue"});
        // $("#vhrselectall").removeAttr("checked");
        $('#termsselectall').prop('checked', false);
    }

});
});

// inspection
$(function(){

// add multiple select / deselect functionality
$("#inspectionselectall").click(function () {
   
    $('#inspectioncase123').css({"background-color":"lightblue"});
      $('.inspectioncase').prop('checked', this.checked);
});

$(".inspectioncase").click(function(){
    if($(".inspectioncase").length == $(".inspectioncase:checked").length) {
        
    $('#inspectioncase123').css({"background-color":"lightblue"});
        $("#inspectionselectall").prop('checked', true);
    } else {
        $('#inspectioncase123').css({"background-color":"lightblue"});
        // $("#vhrselectall").removeAttr("checked");
        $('#inspectionselectall').prop('checked', false);
    }

});
});
//web technician..

// Terms
$(function(){

// add multiple select / deselect functionality
$("#webtechnicianselectall").click(function () {
   
    $('#webtechniciancase123').css({"background-color":"lightblue"});
      $('.webtechniciancase').prop('checked', this.checked);
});

$(".webtechniciancase").click(function(){ 
    if($(".webtechniciancase").length == $(".webtechniciancase:checked").length) {
        
    $('#webtechniciancase123').css({"background-color":"lightblue"});
        $("#webtechnicianselectall").prop('checked', true);
    } else {
        $('#webtechniciancase123').css({"background-color":"lightblue"});
        // $("#vhrselectall").removeAttr("checked");
        $('#webtechnicianselectall').prop('checked', false);
    }

});
});
//about us
$(function(){

// add multiple select / deselect functionality
$("#aboutselectall").click(function () {
    //alert("sobiya about us");
    $('#aboutcase123').css({"background-color":"lightblue"});
      $('.aboutcase').prop('checked', this.checked);
});

$(".aboutcase").click(function(){
    if($(".aboutcase").length == $(".aboutcase:checked").length) {
        
    $('#aboutcase123').css({"background-color":"lightblue"});
        $("#aboutselectall").prop('checked', true);
    } else {
        $('#aboutcase123').css({"background-color":"lightblue"});
        // $("#vhrselectall").removeAttr("checked");
        $('#aboutselectall').prop('checked', false);
    }

});
});
$(function(){

// add multiple select / deselect functionality
$("#vhrselectall").click(function () {
	$('#vhrcase123').css({"background-color":"lightblue"});
	  $('.vhrcase').prop('checked', this.checked);
});

$(".vhrcase").click(function(){
	if($(".vhrcase").length == $(".vhrcase:checked").length) {
		
	$('#vhrcase123').css({"background-color":"lightblue"});
		$("#vhrselectall").prop('checked', true);
	} else {
		$('#vhrcase123').css({"background-color":"lightblue"});
		// $("#vhrselectall").removeAttr("checked");
		$('#vhrselectall').prop('checked', false);
	}

});
});

$(function(){

// add multiple select / deselect functionality
$("#podselectall").click(function () {
	$('#podcase123').css({"background-color":"lightblue"});
	  $('.podcase').prop('checked', this.checked);
});

$(".podcase").click(function(){
	
	if($(".podcase").length == $(".podcase:checked").length) {
		$('#podcase123').css({"background-color":"lightblue"});
		$("#podselectall").prop('checked', true);
	} else {
		$('#podcase123').css({"background-color":"lightblue"});
		// $("#podselectall").removeAttr("checked");
		$('#podselectall').prop('checked', false);
	}

});
});




$(function(){

// add multiple select / deselect functionality
$("#frcselectall").click(function () {
	$('#frccase123').css({"background-color":"lightblue"});
	  $('.frccase').prop('checked', this.checked);
});

$(".frccase").click(function(){

	if($(".frccase").length == $(".frccase:checked").length) {
		$('#frccase123').css({"background-color":"lightblue"});
		$("#frcselectall").prop('checked', true);
	} else {
		$('#frccase123').css({"background-color":"lightblue"});
		// $("#frcselectall").removeAttr("checked");
		$('#frcselectall').prop('checked', false);
	}

});
});


$(function(){

// add multiple select / deselect functionality
$("#billselectall").click(function () {
	$('#billcase123').css({"background-color":"lightblue"});
	  $('.billcase').prop('checked', this.checked);
});

$(".billcase").click(function(){

	if($(".billcase").length == $(".billcase:checked").length) {
		$('#billcase123').css({"background-color":"lightblue"});
		$("#billselectall").prop('checked', true);
	} else {
		$('#billcase123').css({"background-color":"lightblue"});
		// $("#billselectall").removeAttr("checked");
		$('#billselectall').prop('checked', false);
	}

});
});


$(function(){

// add multiple select / deselect functionality
$("#toolselectall").click(function () {
	$('#toolcase123').css({"background-color":"lightblue"});
	  $('.toolcase').prop('checked', this.checked);
});

$(".toolcase").click(function(){

	if($(".toolcase").length == $(".toolcase:checked").length) {
		$('#toolcase123').css({"background-color":"lightblue"});
		$("#toolselectall").prop('checked', true);
	} else {
		$('#toolcase123').css({"background-color":"lightblue"});
		// $("#toolselectall").removeAttr("checked");
		$('#toolselectall').prop('checked', false);
	}

});
});


$(function(){

// add multiple select / deselect functionality
$("#numselectall").click(function () {
	$('#numcase123').css({"background-color":"lightblue"});
	  $('.numcase').prop('checked', this.checked);
});

$(".numcase").click(function(){

	if($(".numcase").length == $(".numcase:checked").length) {
		$('#numcase123').css({"background-color":"lightblue"});
		$("#numselectall").prop('checked', true);
	} else {
		$('#numcase123').css({"background-color":"lightblue"});
		// $("#numselectall").removeAttr("checked");
		$('#numselectall').prop('checked', false);
	}

});
});

$(function(){

// add multiple select / deselect functionality
$("#repselectall").click(function () {
	$('#repcase123').css({"background-color":"lightblue"});
	  $('.repcase').prop('checked', this.checked);
});

$(".repcase").click(function(){

	if($(".repcase").length == $(".repcase:checked").length) {
		$('#repcase123').css({"background-color":"lightblue"});
		$("#repselectall").prop('checked', true);
	} else {
		$('#repcase123').css({"background-color":"lightblue"});
		// $("#repselectall").removeAttr("checked");
		$('#repselectall').prop('checked', false);
	}

});
});

$(function(){

// add multiple select / deselect functionality
$("#actiselectall").click(function () {
	$('#acticase123').css({"background-color":"lightblue"});
	  $('.acticase').prop('checked', this.checked);
});

$(".acticase").click(function(){

	if($(".acticase").length == $(".acticase:checked").length) {
		$('#acticase123').css({"background-color":"lightblue"});
		$("#actiselectall").prop('checked', true);
	} else {
		$('#acticase123').css({"background-color":"lightblue"});
		// $("#actiselectall").removeAttr("checked");
		$('#actiselectall').prop('checked', false);
	}

});
});

$(function(){

// add multiple select / deselect functionality
$("#assselectall").click(function () {
	$('#asscase123').css({"background-color":"lightblue"});
	  $('.asscase').prop('checked', this.checked);
});

$(".asscase").click(function(){

	if($(".asscase").length == $(".asscase:checked").length) {
		$('#asscase123').css({"background-color":"lightblue"});
		$("#assselectall").prop('checked', true);
	} else {
		$('#asscase123').css({"background-color":"lightblue"});
		// $("#assselectall").removeAttr("checked");
		$('#assselectall').prop('checked', false);
	}

});
});

$(function(){

// add multiple select / deselect functionality
$("#notselectall").click(function () {
    $('#notcase123').css({"background-color":"lightblue"});
      $('.asscase').prop('checked', this.checked);
});

$(".asscase").click(function(){

    if($(".asscase").length == $(".asscase:checked").length) {
        $('#notcase123').css({"background-color":"lightblue"});
        $("#notselectall").prop('checked', true);
    } else {
        $('#notcase123').css({"background-color":"lightblue"});
        // $("#assselectall").removeAttr("checked");
        $('#notselectall').prop('checked', false);
    }

});
});

						</script>