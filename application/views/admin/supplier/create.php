
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Subheader-->
						<div class="subheader py-2 py-lg-4 subheader-transparent" id="kt_subheader">
							<div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
								<!--begin::Info-->
								<div class="d-flex align-items-center flex-wrap mr-1">
									<!--begin::Heading-->
									<div class="d-flex flex-column">
										<!--begin::Title-->
										<h2 class="text-white font-weight-bold my-2 mr-5">Add Supplier</h2>
										<!--end::Title-->
										<!--begin::Breadcrumb-->
										<div class="d-flex align-items-center font-weight-bold my-2">
											<!--begin::Item-->
											<a href="#" class="opacity-75 hover-opacity-100">
												<i class="flaticon2-shelter text-white icon-1x"></i>
											</a>
											
											<!--end::Item-->
											<!--begin::Item-->
											<span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
											<a href="<?php echo base_url('admin/Dashboard') ?>" class="text-white text-hover-white opacity-75 hover-opacity-100">Dashboard</a>
											<!--end::Item-->
											<!--begin::Item-->
											<span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
											<a href="<?php echo base_url('admin/Supplier/index') ?>" class="text-white text-hover-white opacity-75 hover-opacity-100">Supplier</a>
											<!--end::Item-->
											<!--begin::Item-->
											<!--end::Item-->
											<!--begin::Item-->
											<span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
											<a href="" class="text-white text-hover-white opacity-75 hover-opacity-100">Add Supplier</a>
											<!--end::Item-->
										</div>
										<!--end::Breadcrumb-->
									</div>

									<!--end::Heading-->
								</div>
								<!--end::Info-->
							</div>
						</div>
						<!--end::Subheader-->
						<!--begin::Entry-->
						<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
							<div class="container">
								<div class="row">
									<div class="col-lg-12">
										<!--begin::Card-->
										<div class="card card-custom gutter-b example example-compact">
											<div class="card-header">
												<h3 class="card-title">Add Supplier</h3>
											</div>
											<!--begin::Form-->
											<form class="form" method="post" action="<?php echo  base_url('admin/Supplier/add'); ?>" enctype="multipart/form-data" onsubmit="return (check());" id="formvalidate">
												<div class="card-body">
													<div class="form-group row">
														<div class="col-lg-6">
															<label>Supplier Name:</label>
															<input type="text" required name="supplier_name" class="form-control" id="supplier_name" placeholder="Enter Supplier Name" />
                                                        
														</div>
														<div class="col-lg-6">
															<label>Supplier Email:</label>
															<input type="email" required name="supplier_email" class="form-control" onchange="duplicatecheckemail();" id="supplier_email" placeholder="Enter Supplier Email" />
                                                        
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
														<div class="col-lg-6">
															<label>Adhar Number:</label>
															<input type="text" required name="supplier_aadhar" onchange="adharcheck();" class="form-control" id="supplier_aadhar" placeholder="Enter Supplier Adhar Number" />
                                                        
														</div>
														<div class="col-lg-6">
															<label>Pan Number:</label>
															<input type="text" required name="supplier_pan" class="form-control" onchange="validatepannumber();" id="supplier_pan" placeholder="Enter Supplier Pan Number" />
                                                        
                                                        </div>
                                                    </div>
													<div class="form-group row">
														<div class="col-lg-6">
															<label>Address:</label>
															<textarea cols="80" id="supplier_address" class="form-control"  name="supplier_address" rows="" placeholder="Enter Supplier Address"></textarea>
                                                        </div>

														<div class="col-lg-6">
														<label>Pincode:</label>
															<input type="number" required name="supplier_pincode" class="form-control" onchange="pincodevalidation();" id="supplier_pincode" placeholder="Enter Supplier Pincode" />
                                                        
															</div>
                                                    </div>
												
													<div class="form-group row">
												   <div class="col-lg-6">
												   <label>Contact Number:</label>
												   <input type="number" class="form-control" onchange="duplicatecheck()" required name="supllier_phonenumber" id="supllier_phonenumber"  placeholder="Enter Supplier Phone Number" />
														   
															 </div>
															  <div class="col-lg-6">
												   <label>ID Proof:</label>
												   <input type="file" accept="image/x-png,image/jpeg" required name="imagetwo" class="form-control" id="imagetwo" />
                                                         
															 </div>
															 
															
												   </div>
												   <div class="form-group row">
												   <div class="col-lg-6">
												   <label>Bank Account Number:</label>
												   <input type="number" class="form-control" min="9999999999" max="999999999999999999999999999999" required name="supllier_bankacc" id="supllier_bankacc"  placeholder="Enter Supplier Bank Account Number" />
														   
															 </div>
															 <div class="col-lg-6">
												   <label>IFSC number:</label>
												   <input type="text" onkeyup="alphanumeric(this.value)" class="form-control" maxlength="11" required name="ifscnumber" id="ifscnumber"  placeholder="Enter Supplier IFSC Number" />
														   
															 </div>
															
												   </div>
												   <div class="form-group row">
												   <div class="col-lg-6">
												   <label>Profile Picture:</label>
												   <input type="file" accept="image/x-png,image/jpeg" required name="imageone" class="form-control" id="imageone" />
                                                         
															 </div>
															 <div class="col-lg-6">
												   <label>Company Registration Number:</label>
												   <input type="text" class="form-control" maxlength="30"  minlength="10"  required name="companyregnumber" id="companyregnumber"  placeholder="Enter Supplier Company Registration Number" />
														   
															 </div>
															
												   </div>

												   <div class="form-group row">
												   <div class="col-lg-6">
												   <label>GST Number:</label>
												   <input type="text"  pattern=".*\S+.*" onchange="validategstnumber();" class="form-control" required name="gstnumber" id="gstnumber"  placeholder="Enter Supplier GST Number" />
														   
															 </div>
															
												 
													   <div class="col-lg-6">
														   <label>Select Status:</label>
														   <select id="status" required class="form-control" name="status">
															<option value="">Select Status</option>
															<option value="Active">Active</option>
															<option value="Inactive">Inactive</option>
															</select>
															 </div>
															
												   </div>
													<!-- <div class="form-group row">
														<div class="col-lg-6">
												   			<label>Latitude:</label>
												   			<input type="number" class="form-control" required name="latlong" id="latlong"  placeholder="Enter Latitude" />
													   	</div>
														<div class="col-lg-6">
												   			<label>Longitude:</label>
												   			<input type="number" class="form-control" required name="long" id="long"  placeholder="Enter Longitude" />
														</div>	
												   </div> -->
												   
												   <div class="form-group row">
												   <div class="col-lg-12">
												   <input type="checkbox" onchange="displaytime('monday','monday1');" id="monday" >

												   <label>Monday:</label>
												   <div  id="monday1" style="display:none">
												   <div class="row">
												   <div class="col-lg-6">
												   <input type="time"   class="form-control"  name="mondaystarttime" id="mondaystarttime"  placeholder="Enter GST Number" />
														   
															 </div>
															
												 
													   <div class="col-lg-6">
														   <input type="time" class="form-control"  name="mondayendtime" id="mondayendtime"  placeholder="Enter GST Number" />
														  
															 </div>
															 </div>
															 </div>
															
												   </div>
												   </div>
												   <div class="form-group row">
												   <div class="col-lg-12">
												   <input type="checkbox" onclick="displaytime('Tuesday','Tuesday1')" id="Tuesday" >

												   <label>Tuesday:</label>
												   <div  id="Tuesday1" style="display:none">
												   <div class="row" >
												   <div class="col-lg-6">
												   <input type="time"   class="form-control"  name="Tuesdaystarttime" id="Tuesdaystarttime"  placeholder="Enter GST Number" />
														   
															 </div>
															
												 
													   <div class="col-lg-6">
														   <input type="time" class="form-control"  name="Tuesdayendtime" id="Tuesdayendtime"  placeholder="Enter GST Number" />
														  
															 </div>
															 </div>
															</div>
												   </div>
												   </div>
												   <div class="form-group row">
												   <div class="col-lg-12">
												   <input type="checkbox" onclick="displaytime('Wednesday','Wednesday1')" id="Wednesday" >

												   <label>Wednesday:</label>
												   <div  id="Wednesday1" style="display:none">
												   <div class="row" >
												   <div class="col-lg-6">
												   <input type="time"   class="form-control"  name="Wednesdaystarttime" id="Wednesdaystarttime"  placeholder="Enter GST Number" />
														   
															 </div>
															
												 
													   <div class="col-lg-6">
														   <input type="time" class="form-control"  name="Wednesdayendtime" id="Wednesdayendtime"  placeholder="Enter GST Number" />
														  
															 </div>
															 </div> </div>
															
												   </div>
												   </div>
												   <div class="form-group row">
												   <div class="col-lg-12">
												   <input type="checkbox"  onclick="displaytime('Thursday','Thursday1')" id="Thursday">

												   <label>Thursday:</label>
												   <div  id="Thursday1" style="display:none">
												   <div class="row">
												   <div class="col-lg-6">
												   <input type="time"   class="form-control"  name="Thursdaystarttime" id="Thursdaystarttime"  placeholder="Enter GST Number" />
														   
															 </div>
															
												 
													   <div class="col-lg-6">
														   <input type="time" class="form-control"  name="Thursdayendtime" id="Thursdayendtime"  placeholder="Enter GST Number" />
														  
															 </div>
															 </div>
															 </div>		
												   </div>
												   </div>
												   <div class="form-group row">
												   <div class="col-lg-12">
												   <input type="checkbox" onclick="displaytime('Friday','Friday1')" id="Friday">

												   <label>Friday:</label>
												   <div  id="Friday1" style="display:none">
												   <div class="row">
												   <div class="col-lg-6">
												   <input type="time"   class="form-control"  name="Fridaystarttime" id="Fridaystarttime"  placeholder="Enter GST Number" />
														   
															 </div>
															
												 
													   <div class="col-lg-6">
														   <input type="time" class="form-control"  name="Fridayendtime" id="Fridayendtime"  placeholder="Enter GST Number" />
														  
															 </div>
															 </div>
															 </div>
												   </div>
												   </div>
												   <div class="form-group row">
												   <div class="col-lg-12">
												   <input type="checkbox" onclick="displaytime('Saturday','Saturday1')" id="Saturday" >

												   <label>Saturday:</label>
												   <div  id="Saturday1" style="display:none">
												   <div class="row" >
												   <div class="col-lg-6">
												   <input type="time"   class="form-control"  name="Saturdaystarttime" id="Saturdaystarttime"  placeholder="Enter GST Number" />
														   
															 </div>
															
												 
													   <div class="col-lg-6">
														   <input type="time" class="form-control"  name="Saturdayendtime" id="Saturdayendtime"  placeholder="Enter GST Number" />
														  
															 </div>
															 </div>
															 </div>
															
												   </div>
												   </div>
												   <div class="form-group row">
												   <div class="col-lg-12">
												   <input type="checkbox" onclick="displaytime('Sunday','Sunday1')" id="Sunday">

												   <label>Sunday:</label>
												   <div  id="Sunday1" style="display:none">
												   <div class="row" >
												   <div class="col-lg-6">
												   <input type="time"   class="form-control"  name="Sundaystarttime" id="Sundaystarttime"  placeholder="Enter GST Number" />
														   
															 </div>
															
												 
													   <div class="col-lg-6">
														   <input type="time" class="form-control"  name="Sundayendtime" id="Sundayendtime"  placeholder="Enter GST Number" />
														  
															 </div>
															 </div>
															 </div>
												   </div>
												   </div>
												   
												   
												</div>
												<div class="card-footer">
													<div class="row">
														<div class="col-lg-6">
															<button type="submit"  class="btn btn-primary mr-2">Save</button>
															<button type="reset" class="btn btn-secondary">Reset</button>
														</div>
													</div>
												</div>
											</form>
											<!--end::Form-->
										</div>
										<!--end::Card-->
									</div>
								</div>
							</div>
							<!--end::Container-->
						</div>
						<!--end::Entry-->
					</div>
					<!-- <div class="col-lg-6">
															<label>Select Offer:</label>
															<select name="offer_select" id="" class="form-control offer_select"></select>
													</div> -->
                    <!--end::Content-->
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                    
					<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>  
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
                    <script>
					
					function check(){
						// alert('sssss');
	// duplicatecheck();
	validatepannumber();
	// duplicatecheckemail();	
	adharcheck();	
	pincodevalidation();	
	validategstnumber();
	//   validatetime();	
}
					$(".alphaonly").keydown(function(event){
        var inputValue = event.which;
        // console.log(inputValue);
        if (!(inputValue >= 65 && inputValue <= 123)
                            && (inputValue != 32 && inputValue != 0)
                            && ( inputValue != 8)
                            && (inputValue != 9)){
                                event.preventDefault(); 
                        }
    });
					 function validatepannumber(){
                        var pannumber = document.getElementById("supplier_pan").value;
                        if(pannumber==""){
                            return true;
                        }else{
                            var regpan = /^([A-Z]){5}([0-9]){4}([A-Z]){1}?$/;
                            if(regpan.test(pannumber) == false)
                                {
									alert("Not a valid Pan Number");
	   document.getElementById("supplier_pan").value='';
	   return false;
                                }else{
                                return true;
                                }
                            }
                        }
						
				function duplicatecheck(){
	var value=document.getElementById("supllier_phonenumber").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Phone Number");
	   document.getElementById("supllier_phonenumber").value='';
	   return false;
     }


 $.ajax({
	url: websiteurl + 'Supplier/phonenocheckexist',
        type: 'POST',
        data: {
            key: value
        },
        dataType: 'json',
        success: function(data) {
		
			if(data==2){
				alert('Phone Number is Already exists');
				document.getElementById("supllier_phonenumber").value='';
				return false;
			}else{
				return true;
			}
        }
    });
}

function duplicatecheckemail(){
	var value=document.getElementById("supplier_email").value;
	
 $.ajax({
	url: websiteurl + 'Supplier/emailcheckexist',
        type: 'POST',
        data: {
            key: value
        },
        dataType: 'json',
        success: function(data) {
		
			if(data==2){
				alert('Email is Already exists');
				document.getElementById("supplier_email").value='';
				return false;
			}else{
				return true;
			}
        }
    });
}

function adharcheck(){
	var value=document.getElementById("supplier_aadhar").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{6})$/;
  if(value.match(phoneno))
     {
	    return true;
	 }
   else
     {
	   alert("Not a valid Adhar Number");
	   document.getElementById("supplier_aadhar").value='';
	   return false;
     }
}

function pincodevalidation(){
	var value=document.getElementById("supplier_pincode").value;
	
	var phoneno = /^\(?([2-9]{1})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{2})$/;
  if(value.match(phoneno))
     {
		return true;
	 }
   else
     {
	   alert("Not a valid Pincode Number");
	   document.getElementById("supplier_pincode").value='';
	   return false;
     }
}
function validategstnumber(){
	// console.log(return !!(validatetime() & duplicatecheck() & duplicatecheckemail() & validategstnumber()));
                            var cgstnumber = document.getElementById("gstnumber").value;
                            if(cgstnumber==""){
                            return true;
                        }else{
                            var regpan = /\d{2}[A-Z]{5}\d{4}[A-Z]{1}[A-Z\d]{1}[Z]{1}[A-Z\d]{1}/;
                            if(regpan.test(cgstnumber) == false)
                            {
								alert("Not a valid GST Number");
	   document.getElementById("gstnumber").value='';
                                   
                                    return false;
                                }else{
                                return true;
                                }
                            }
                        }
						function displaytime(value,value1){
	var checkBox = document.getElementById(value);
// alert(''+value+'starttime');
	if (checkBox.checked == true){
		document.getElementById(value1).style.display = "block";
		document.getElementById(''+value+'starttime').setAttribute("required","");
		document.getElementById(''+value+'endtime').setAttribute("required","");
		// document.getElementById(''+value+'starttime').setAttribute("required ", "");
  } else {
	document.getElementById(''+value+'starttime').required = false;

	document.getElementById(''+value+'endtime').required = false;
	document.getElementById(value1).style.display = "none";

  }

}

function validatetime(){
	var monday1 = document.getElementById("monday1").style.display;
	var Tuesday1 = document.getElementById("Tuesday1").style.display;
	var Wednesday1 = document.getElementById("Wednesday1").style.display;
	var Thursday1 = document.getElementById("Thursday1").style.display;
	var Friday1 = document.getElementById("Friday1").style.display;
	var Saturday1 = document.getElementById("Saturday1").style.display;
	var Sunday1 = document.getElementById("Sunday1").style.display;

    if(monday1=='block'){
		var mstarttime = document.getElementById("mondaystarttime").value;
		var mendtime = document.getElementById("mondayendtime").value;
if(mstarttime>=mendtime){
	alert('Monday Start Time and End Time should be Greater');
	document.getElementById("mondaystarttime").value='';
	document.getElementById("mondayendtime").value='';
	return false;
}

    }else{
		return true;
	}


	if(Tuesday1=='block'){
		var Tuesdaystarttime = document.getElementById("Tuesdaystarttime").value;
		var Tuesdayendtime = document.getElementById("Tuesdayendtime").value;
if(Tuesdaystarttime>=Tuesdayendtime){
	alert('Tuesday Start Time and End Time should be Greater');
	return false;
}

    }else{
		return true;
	}
	if(Wednesday1=='block'){
		var Wednesdaystarttime = document.getElementById("Wednesdaystarttime").value;
		var Wednesdayendtime = document.getElementById("Wednesdayendtime").value;
if(Wednesdaystarttime>=Wednesdayendtime){
	alert('Wednesday Start Time and End Time should be Greater');
	return false;
}

    }else{
		return true;
	}
	if(Thursday1=='block'){
		var Thursdaystarttime = document.getElementById("Thursdaystarttime").value;
		var Thursdayendtime = document.getElementById("Thursdayendtime").value;
if(Thursdaystarttime>=Thursdayendtime){
	alert('Thursday Start Time and End Time should be Greater');
	return false;
}

    }else{
		return true;
	}
	if(Friday1=='block'){
		var Fridaystarttime = document.getElementById("Fridaystarttime").value;
		var Fridayendtime = document.getElementById("Fridayendtime").value;
if(Fridaystarttime>=Fridayendtime){
	alert('Friday Start Time and End Time should be Greater');
	return false;
}

    }else{
		return true;
	}
	if(Saturday1=='block'){
		var Saturdaystarttime = document.getElementById("Saturdaystarttime").value;
		var Saturdayendtime = document.getElementById("Saturdayendtime").value;
if(Saturdaystarttime>=Saturdayendtime){
	alert('Saturday Start Time and End Time should be Greater');
	return false;
}

    }else{
		return true;
	}

	if(Sunday1=='block'){
		var Sundaystarttime = document.getElementById("Sundaystarttime").value;
		var Sundayendtime = document.getElementById("Sundayendtime").value;
if(Sundaystarttime>=Sundayendtime){
	alert('Sunday Start Time and End Time should be Greater');
	return false;
}

    }else{
		return true;
	}

}
function alphanumeric(inputtxt)
{ 
	// alert(inputtxt);
var letters = /^[0-9a-zA-Z]+$/;

if(inputtxt.match(letters))
{
// alert('Your registration number have accepted : you can try another');
// document.form1.text1.focus();
return true;
}
else
{
  alert('Please input alphanumeric characters only');
  document.getElementById("ifscnumber").value='';
return false;
}
}

document.getElementById("supplier_name").addEventListener('keydown', function (e) {
  if (this.value.length === 0 && e.which === 32) e.preventDefault();
  if (this.value.length === 0 && e.which === 48) e.preventDefault();
  //special character
  if (e.which === 56 || e.which === 53 || e.which === 51 || e.which === 49 || e.which === 50 || e.which === 52 || e.which === 48 ) e.preventDefault();
});
						</script>