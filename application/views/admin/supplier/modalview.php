<form class="form" method="post" action="<?php echo  base_url('admin/Supplier/Updatemodel'); ?>" enctype="multipart/form-data" >
	<?php //print_r($spare_category); 
 //echo "...supplier data"; print_r($data); print_r($data[0]->spare_id);
  //echo "...spare data"; print_r($data);
  ?>
<div class="modal-body">
	      <input type="hidden" required name="spare_id" value="<?php echo $data[0]->puser_id;?>" class="form-control" id="spare_id" placeholder="Enter English Title" />
       <div class="form-group">
          <label for="cars">Select a Spare Category:</label>
          <select name="spare_cat" id="spare_cat" class="form-control" required>
              <option value="">Select Spare Category</option>
             <?php foreach($spare_category as $row): ?>
              <option value="<?php echo $row->spcat_id; ?>"><?php echo $row->spcat_title; ?></option>
            <?php endforeach; ?>
          </select>

          <label for="cars">Select a Spare:</label>
          <select name="spare" id="spare" class="form-control" required>
              <option value="">Select Spare</option>
          </select>
      </div>   

      <div class="form-group">
           <label>MRP:</label>  
           <input type="text" name="p_spare_mrp" id="p_spare_mrp" class="form-control" required placeholder="MRP">
           <p class="nameError"></p>
       </div>   

        <div class="form-group">
           <label>Selling price:</label>  
           <input type="text" name="p_spare_selling_price" id="p_spare_selling_price" class="form-control" required placeholder="Sellng Price" oninput="add_number()">
           <p class="nameError"></p>
       </div>   

        <div class="form-group">
           <label>GST %:</label>  
           <input type="text" name="p_spare_gst_percentage" id="p_spare_gst_percentage" class="form-control" required placeholder="GST" oninput="add_number()">
           <p class="nameError"></p>
       </div>   

        <div class="form-group">
           <label>GST Amount (RS):</label>  
           <!--<input type="text" disabled name="gst_amount" id="gst_amount" class="form-control" placeholder="MRP">-->
           <!--<input id='gst_amount' readonly="" type="text" name="gst_amount" class="form-control" value="<?php $gst_amount= "<p id='gst_amount'> </p>"; echo $gst_amount;?>" />-->
           <input id='gst_amount' readonly="" type="text" name="gst_amount" class="form-control" required value="<?php $gst_amount= " "; echo $gst_amount;?>" />
           <p class="nameError"></p>
       </div>   

        <div class="form-group">
           <label>Exclude GST Price:</label>  
          <!-- <input type="text" disabled name="gstspprice" id="gstspprice" class="form-control" placeholder="MRP ">-->
          <!-- <input id='gstspprice' readonly="" type="text" name="gstspprice" class="form-control" value="<?php $gstspprice= "<p id='gstspprice'> </p>"; echo $gstspprice;?>" />-->
           <input id='gstspprice' readonly="" type="text" name="gstspprice" class="form-control" required value="<?php $gstspprice= " "; echo $gstspprice;?>" />
           <p class="nameError"></p>
       </div> 

        <input type="hidden" required name="supplier_id" value="<?php echo $data[0]->puser_id;?>" class="form-control" id="supplier_id" placeholder="Enter English Title" />  
    
</div>

<div class="modal-footer">
  <button type="submit" class="btn btn-primary">Save</button>
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
    
</div>
</form>
<div>
 
  
  </div>
<script>
$(document).ready(function(){
 $('#spare_cat').change(function(){
  var spare_cat = $('#spare_cat').val(); //alert(spare_cat);
  if(spare_cat != '')
  {
   $.ajax({
    url:websiteurl + 'Supplier/loadSpareCategory',
    method:"POST",
    data:{spare_cat:spare_cat},
    success:function(data)
    {
     $('#spare').html(data);
     //$('#city').html('<option value="">Select City</option>');
    }
   });
  }
  else
  {
   $('#spare').html('<option value="">Select Spare</option>');
   //$('#city').html('<option value="">Select City</option>');
  }
 });
});

// Calculation of GST
    var p_spare_mrp = document.getElementById("p_spare_mrp");
    var p_spare_selling_price = document.getElementById("p_spare_selling_price");
    var p_spare_gst_percentage = document.getElementById("p_spare_gst_percentage");
    
       function add_number() {
          var MRP = parseFloat(p_spare_mrp.value);
          if (isNaN(MRP)) MRP = 0;
          
          var selling_price = parseFloat(p_spare_selling_price.value);
          if (isNaN(selling_price)) selling_price = 0;

           var gst_percentage = parseFloat(p_spare_gst_percentage.value);
          if (isNaN(gst_percentage)) gst_percentage = 0;

          var gst_amount = (selling_price*gst_percentage)/100; //MRP + selling_price + gst_price;
          document.getElementById("gst_amount").value = gst_amount;

          //gst price result
          var gstspprice = selling_price - gst_amount;
          document.getElementById("gstspprice").value = gstspprice;
       }      
</script>