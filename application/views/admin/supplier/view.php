
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Subheader-->
					
						<!--end::Subheader-->
						<!--begin::Entry-->
						<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
							<div class="container">
								<div class="row">
									<div class="col-lg-12">
										<!--begin::Card-->
										<div class="card card-custom gutter-b example example-compact">
											
											<?php foreach($supplier as $row){} ?>
											<!--begin::Form-->
											
											<form class="form" method="post" id="resetform" action="<?php echo  base_url('admin/Supplier/update'); ?>" enctype="multipart/form-data" >
											<div class="card-body">
													<div class="form-group row">
														<div class="col-lg-12">
														<label>Supplier Name:</label>
															<input type="text" disabled required name="supplier_name" value="<?php echo $row->puser_name;?>" class="form-control" id="supplier_name" placeholder="Enter Supplier Name" />
                                                          <input type="hidden" required name="puser_id" value="<?php echo $row->puser_id;?>" class="form-control" id="puser_id" placeholder="Enter English Title" />
                                                        
														</div></div>
														<div class="form-group row">
														<div class="col-lg-12">
														<label>Supplier Email:</label>
															<input type="email" disabled required name="supplier_email" value="<?php echo $row->puser_email;?>" class="form-control" onchange="duplicatecheckemail();" id="supplier_email" placeholder="Enter Supplier Email" />
                                                        
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
														<div class="col-lg-12">
														<label>Adhar Number:</label>
															<input type="text" disabled required name="supplier_aadhar" value="<?php echo $row->puser_adhar_number;?>" onchange="adharcheck();" class="form-control" id="supplier_aadhar" placeholder="Enter supplier Adhar Number" />
                                                        
														</div>
														</div>
														<div class="form-group row">
														<div class="col-lg-12">
														<label>Pan Number:</label>
															<input type="text" disabled required name="supplier_pan" value="<?php echo $row->puser_pan_number;?>" class="form-control" onchange="validatepannumber();" id="supplier_pan" placeholder="Enter Supplier Pan Number" />
                                                        
                                                        </div>
                                                    </div>
													<div class="form-group row">
														<div class="col-lg-12">
														<label>Address:</label>
															<textarea cols="80" disabled id="supplier_address" value="<?php echo $row->puser_address;?>" class="form-control"  name="supplier_address" rows="" placeholder="Enter Supplier Address"><?php echo $row->puser_address;?></textarea>
                                                          </div>
														</div>
														<div class="form-group row">
														<div class="col-lg-12">
														<label>Pincode:</label>
															<input type="number" disabled required name="supplier_pincode" value="<?php echo $row->puser_pincode;?>" class="form-control" onchange="pincodevalidation();" id="supplier_pincode" placeholder="Enter Supplier Pincode" />
                                                        
														</div>
                                                    </div>
													<div class="form-group row">
														<div class="col-lg-12">
														<label>Contact Number:</label>
												   <input type="number" disabled class="form-control" value="<?php echo $row->puser_phno;?>" onchange="duplicatecheck()" required name="supllier_phonenumber" id="supllier_phonenumber"  placeholder="Enter Supplier Phone Number" />
														   
														 </div>
														</div>
														
														<div class="form-group row">
														<div class="col-lg-12">
														<label>Bank Account Number:</label>
												   <input type="number"  disabled class="form-control" value="<?php echo $row->puser_bankacc_number;?>" min="9999999999" max="999999999999999999999999999999" required name="supllier_bankacc" id="supllier_bankacc"  placeholder="Enter Supplier Bank Account Number" />
													 </div>
														</div>

														<div class="form-group row">
														<div class="col-lg-12">
														<label>IFSC number:</label>
												   <input type="text" disabled class="form-control alphaonly" value="<?php echo $row->puser_bankacc_ifsc;?>" maxlength="11" required name="ifscnumber" id="ifscnumber"  placeholder="Enter Supplier IFSC Number" />
													 </div>
														</div>

														<div class="form-group row">
														<div class="col-lg-12">
														<label>Company Registration Number:</label>
												   <input type="text"  disabled class="form-control" maxlength="30" value="<?php echo $row->puser_company_reg_number;?>" minlength="10"  required name="companyregnumber" id="companyregnumber"  placeholder="Enter Supplier Company Registration Number" />
														    </div>
														</div>
														<div class="form-group row">
														<div class="col-lg-12">
														<label>GST Number:</label>
												   <input type="text" disabled pattern=".*\S+.*" value="<?php echo $row->puser_GST_number;?>" onchange="validategstnumber();" class="form-control" required name="gstnumber" id="gstnumber"  placeholder="Enter GST Number" />
														  </div>
														</div>

														<div class="form-group row">
														<div class="col-lg-12">
															<label>Profile Image:</label>
                                                            	<a target="_blank" href="<?php echo base_url() ?>/assets/images/supplier/<?php echo $row->puser_profile_image; ?>"><img style="width: 70px; height: 70px" src="<?php 
																if($row->puser_profile_image==""){
																	echo base_url('/assets/images/no-images/no-product.png'); 
																	}else{
																		echo base_url() ?>/assets/images/supplier/<?php echo  $row->puser_profile_image; 
																	} ?>" onerror="this.onerror=null;this.src='<?php echo base_url('/assets/images/noimage.png'); ?>';"></a>
															    <!-- <input type="file"  accept="image/x-png,image/jpeg"  name="imageone" class="form-control" id="imageone" /> -->
															 <input type="hidden" pattern=".*\S+.*"  name="hiddenimageone" value="<?php echo $row->puser_profile_image ?>"  class="form-control" />
													    
														</div>
														</div>

														<div class="form-group row">
														<div class="col-lg-12">
															<label>ID Proof:</label>
                                                            	<a target="_blank" href="<?php echo base_url() ?>/assets/images/supplier/<?php echo $row->idproof; ?>"><img style="width: 70px; height: 70px" src="<?php 
																if($row->idproof==""){
																	echo base_url('/assets/images/no-images/no-product.png'); 
																	}else{
																		echo base_url() ?>/assets/images/supplier/<?php echo  $row->idproof; 
																	} ?>" onerror="this.onerror=null;this.src='<?php echo base_url('/assets/images/noimage.png'); ?>';"></a>
															    <!-- <input type="file"  accept="image/x-png,image/jpeg"  name="imagetwo" class="form-control" id="imagetwo" /> -->
															 <input type="hidden" pattern=".*\S+.*"  name="hiddenimagetwo" value="<?php echo $row->idproof ?>"  class="form-control" />
													    
														</div>
														</div>

												   <div class="form-group row">
                                                       
													   <div class="col-lg-12">
														   <label>Select Status:</label>
															<select disabled class="form-control" name="status" required>
															<option value="<?php echo $row->puser_account_status; ?>"><?php echo $row->puser_account_status; ?></option>
															<?php  if($row->puser_account_status!="Active"){
																?>
																<option value="Active">Active</option>
																<?php
															}else{
																?>
																<option value="Inactive">Inactive</option>
																<?php
															} ?>
															</select>
															 </div>
															
												   </div>
												   <div class="form-group row">
														<div class="col-lg-12">
														<label>Latitude:</label>
												   <input type="text" disabled class="form-control" value="<?php echo $row->puser_lat_long;?>" required name="latlong" id="latlong"  placeholder="Enter lat long" />
														   
														 </div>
														</div>	
												   <div class="form-group row">
                                                       
													   <div class="col-lg-12">
													   <label>Longitude:</label>
												   <input type="number" disabled  value="<?php echo $row->puser_long; ?>"class="form-control" required name="long" id="long"  placeholder="Enter long" />
														   
															 </div>
															
												   </div>

												   <div class="form-group row">
												   <div class="col-lg-12">
												   <input type="checkbox" <?php if($row->puserw_mo_open!=''){echo "checked"; }?> disabled onchange="displaytime('monday','monday1');" id="monday" >

												   <label>Monday:</label>
												   <div  id="monday1" style="display:none">
												   <div class="row">
												   <div class="col-lg-6">
												   <input type="time"   class="form-control" disabled name="mondaystarttime" id="mondaystarttime" value="<?php echo $row->puserw_mo_open; ?>"  placeholder="Enter GST Number" />
														   
															 </div>
															
												 
													   <div class="col-lg-6">
														   <input type="time" class="form-control" disabled name="mondayendtime" id="mondayendtime" value="<?php echo $row->puserw_mo_close; ?>"    placeholder="Enter GST Number" />
														  
															 </div>
															 </div>
															 </div>
															
												   </div>
												   </div>
												   <div class="form-group row">
												   <div class="col-lg-12">
												   <input type="checkbox"  <?php if($row->puserw_tu_open!=''){echo "checked"; }?> disabled  onclick="displaytime('Tuesday','Tuesday1')" id="Tuesday" >

												   <label>Tuesday:</label>
												   <div  id="Tuesday1" style="display:none">
												   <div class="row" >
												   <div class="col-lg-6">
												   <input type="time"   class="form-control" disabled name="Tuesdaystarttime" value="<?php echo $row->puserw_tu_open; ?>"   id="Tuesdaystarttime"  placeholder="Enter GST Number" />
														   
															 </div>
															
												 
													   <div class="col-lg-6">
														   <input type="time" class="form-control" disabled name="Tuesdayendtime" value="<?php echo $row->puserw_tu_close; ?>"   id="Tuesdayendtime"  placeholder="Enter GST Number" />
														  
															 </div>
															 </div>
															</div>
												   </div>
												   </div>
												   <div class="form-group row">
												   <div class="col-lg-12">
												   <input type="checkbox"  <?php if($row->puserw_we_open!=''){echo "checked"; }?> disabled onclick="displaytime('Wednesday','Wednesday1')" id="Wednesday" >

												   <label>Wednesday:</label>
												   <div  id="Wednesday1" style="display:none">
												   <div class="row" >
												   <div class="col-lg-6">
												   <input type="time"   class="form-control" disabled name="Wednesdaystarttime" value="<?php echo $row->puserw_we_open; ?>"   id="Wednesdaystarttime"  placeholder="Enter GST Number" />
														   
															 </div>
															
												 
													   <div class="col-lg-6">
														   <input type="time" class="form-control" disabled name="Wednesdayendtime" value="<?php echo $row->puserw_we_close; ?>"   id="Wednesdayendtime"  placeholder="Enter GST Number" />
														  
															 </div>
															 </div> </div>
															
												   </div>
												   </div>
												   <div class="form-group row">
												   <div class="col-lg-12">
												   <input type="checkbox"  <?php if($row->puserw_th_open!=''){echo "checked"; }?> disabled  onclick="displaytime('Thursday','Thursday1')" id="Thursday">

												   <label>Thursday:</label>
												   <div  id="Thursday1" style="display:none">
												   <div class="row">
												   <div class="col-lg-6">
												   <input type="time"   class="form-control" disabled name="Thursdaystarttime" value="<?php echo $row->puserw_th_open; ?>"   id="Thursdaystarttime"  placeholder="Enter GST Number" />
														   
															 </div>
															
												 
													   <div class="col-lg-6">
														   <input type="time" class="form-control" disabled name="Thursdayendtime" value="<?php echo $row->puserw_th_close; ?>"   id="Thursdayendtime"  placeholder="Enter GST Number" />
														  
															 </div>
															 </div>
															 </div>		
												   </div>
												   </div>
												   <div class="form-group row">
												   <div class="col-lg-12">
												   <input type="checkbox"  <?php if($row->puserw_fr_open!=''){echo "checked"; }?> disabled onclick="displaytime('Friday','Friday1')" id="Friday">

												   <label>Friday:</label>
												   <div  id="Friday1" style="display:none">
												   <div class="row">
												   <div class="col-lg-6">
												   <input type="time"   class="form-control" disabled name="Fridaystarttime" value="<?php echo $row->puserw_fr_open; ?>"   id="Fridaystarttime"  placeholder="Enter GST Number" />
														   
															 </div>
															
												 
													   <div class="col-lg-6">
														   <input type="time" class="form-control" disabled name="Fridayendtime" value="<?php echo $row->puserw_fr_close; ?>"   id="Fridayendtime"  placeholder="Enter GST Number" />
														  
															 </div>
															 </div>
															 </div>
												   </div>
												   </div>
												   <div class="form-group row">
												   <div class="col-lg-12">
												   <input type="checkbox"  <?php if($row->puserw_sa_open!=''){echo "checked"; }?> disabled onclick="displaytime('Saturday','Saturday1')" id="Saturday" >

												   <label>Saturday:</label>
												   <div  id="Saturday1" style="display:none">
												   <div class="row" >
												   <div class="col-lg-6">
												   <input type="time"   class="form-control" disabled name="Saturdaystarttime" value="<?php echo $row->puserw_sa_open; ?>"   id="Saturdaystarttime"  placeholder="Enter GST Number" />
														   
															 </div>
															
												 
													   <div class="col-lg-6">
														   <input type="time" class="form-control" disabled name="Saturdayendtime" value="<?php echo $row->puserw_sa_close; ?>"   id="Saturdayendtime"  placeholder="Enter GST Number" />
														  
															 </div>
															 </div>
															 </div>
															
												   </div>
												   </div>
												   <div class="form-group row">
												   <div class="col-lg-12">
												   <input type="checkbox"  <?php if($row->puserw_su_open!=''){echo "checked"; }?> disabled onclick="displaytime('Sunday','Sunday1')" id="Sunday">

												   <label>Sunday:</label>
												   <div  id="Sunday1" style="display:none">
												   <div class="row" >
												   <div class="col-lg-6">
												   <input type="time" disabled  class="form-control"  name="Sundaystarttime" value="<?php echo $row->puserw_su_open; ?>"   id="Sundaystarttime"  placeholder="Enter GST Number" />
														   
															 </div>
															
												 
													   <div class="col-lg-6">
														   <input type="time" disabled class="form-control"  name="Sundayendtime" value="<?php echo $row->puserw_su_close; ?>"   id="Sundayendtime"  placeholder="Enter GST Number" />
														  
															 </div>
															 </div>
															 </div>
												   </div>
												   </div>
												  
												</div>
												
											
												<div class="card-footer">
													<div class="row">
														<div class="col-lg-12">
															<!-- <button type="submit" class="btn btn-primary mr-2">Save</button> -->
															</div>
													</div>
												</div>
											</form>
											<!--end::Form-->
										</div>
										<!--end::Card-->
									</div>
								</div>
							</div>
							<!--end::Container-->
						</div>
						<!--end::Entry-->
					</div>
                    <!--end::Content-->
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                    
					<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>  
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
					<script src="https://cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
					<script>
						$(".alphaonly").keydown(function(event){
        var inputValue = event.which;
        // console.log(inputValue);
        if (!(inputValue >= 65 && inputValue <= 123)
                            && (inputValue != 32 && inputValue != 0)
                            && ( inputValue != 8)
                            && (inputValue != 9)){
                                event.preventDefault(); 
                        }
    });
					 function validatepannumber(){
                        var pannumber = document.getElementById("supplier_pan").value;
                        if(pannumber==""){
                            return true;
                        }else{
                            var regpan = /^([A-Z]){5}([0-9]){4}([A-Z]){1}?$/;
                            if(regpan.test(pannumber) == false)
                                {
									alert("Not a valid Pan Number");
	   document.getElementById("supplier_pan").value='';
	   return false;
                                }else{
                                return true;
                                }
                            }
                        }
				function duplicatecheck(){
	var value=document.getElementById("supllier_phonenumber").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Phone Number");
	   document.getElementById("supllier_phonenumber").value='';
	   return false;
     }
}



function adharcheck(){
	var value=document.getElementById("supplier_aadhar").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{6})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Adhar Number");
	   document.getElementById("supplier_aadhar").value='';
	   return false;
     }
}

function pincodevalidation(){
	var value=document.getElementById("supplier_pincode").value;
	
	var phoneno = /^\(?([1-9]{1})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{2})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Pincode Number");
	   document.getElementById("supplier_pincode").value='';
	   return false;
     }
}
function validategstnumber(){
                            var cgstnumber = document.getElementById("gstnumber").value;
                            if(cgstnumber==""){
                            return true;
                        }else{
                            var regpan = /\d{2}[A-Z]{5}\d{4}[A-Z]{1}[A-Z\d]{1}[Z]{1}[A-Z\d]{1}/;
                            if(regpan.test(cgstnumber) == false)
                            {
								alert("Not a valid GST Number");
	   document.getElementById("gstnumber").value='';
                                   
                                    return false;
                                }else{
                                return true;
                                }
                            }
                        }

function displaytime(value,value1){
	var checkBox = document.getElementById(value);
// alert(''+value+'starttime');
	if (checkBox.checked == true){
		document.getElementById(value1).style.display = "block";
		document.getElementById(''+value+'starttime').setAttribute("required","");
		document.getElementById(''+value+'endtime').setAttribute("required","");
		// document.getElementById(''+value+'starttime').setAttribute("required ", "");
  } else {
	document.getElementById(''+value+'starttime').value = '';

document.getElementById(''+value+'endtime').value = '';
	document.getElementById(''+value+'starttime').required = false;

	document.getElementById(''+value+'endtime').required = false;
	document.getElementById(value1).style.display = "none";

  }

}

$(document).ready(function(){
	displaytime('monday','monday1');
	displaytime('Tuesday','Tuesday1');
	displaytime('Wednesday','Wednesday1');
	displaytime('Thursday','Thursday1');
	displaytime('Friday','Friday1');
	displaytime('Saturday','Saturday1');
	displaytime('Sunday','Sunday1');
});
						</script>