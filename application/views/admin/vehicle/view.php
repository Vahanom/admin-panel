
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Subheader-->
					
						<!--end::Subheader-->
						<!--begin::Entry-->
						<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
							<div class="container">
								<div class="row">
									<div class="col-lg-12">
										<!--begin::Card-->
										<div class="card card-custom gutter-b example example-compact">
											
											<?php foreach($vehicle as $row){} ?>
											<!--begin::Form-->
											
											<form class="form" method="post" id="resetform" action="<?php echo  base_url('admin/Vehicle/update'); ?>" enctype="multipart/form-data" >
											<div class="card-body">
													<div class="form-group row">
														<div class="col-lg-12">
														<label>Enter Vehicle Name:</label>
															<input type="text" disabled required name="make_name" value="<?php echo $row->vehb_make ?>" class="form-control" id="make_name" placeholder="Enter Vehicle Make Name" />
                                                        <input type="hidden" required name="vehb_id" value="<?php echo $row->vehb_id;?>" class="form-control" id="vehb_id" placeholder="Enter English Title" />
                                                        
														</div></div>
														<div class="form-group row">
														<div class="col-lg-12">
														<label>Enter Model Name:</label>
															<input type="text" disabled required name="model_name" value="<?php echo $row->vehb_model ?>" class="form-control" id="model_name" placeholder="Enter Vehicle Model Name" />
                                                        
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
														<div class="col-lg-12">
														<label>Enter Trim:</label>
															<input type="text" disabled required name="trim_vehicle" value="<?php echo $row->vehb_trim ?>" class="form-control" id="trim_vehicle" placeholder="Enter Vehicle Trim" />
                                                        
														</div>
														</div>
														<div class="form-group row">
														<div class="col-lg-12">
														<label>Fuel Type:</label>
															<select id="fuel" disabled required class="form-control" name="fuel">
															<option value="<?php echo $row->vehb_fuel; ?>"><?php echo $row->vehb_fuel; ?></option>
															<?php  if($row->vehb_fuel!="Petrol"){
																?>
																<option value="Diesel">Diesel</option>
															<?php } else{ ?>
															
																<option value="Petrol">Petrol</option>
															<?php } ?>
															</select>
                                                        </div>
                                                    </div>
												

														<div class="form-group row">
														<div class="col-lg-12">
															<label>Vehicle Image:</label>
                                                            	<a target="_blank" href="<?php echo base_url() ?>/assets/images/vehicle/<?php echo $row->vehb_image; ?>"><img style="width: 70px; height: 70px" src="<?php 
																if($row->vehb_image==""){
																	echo base_url('/assets/images/no-images/no-product.png'); 
																	}else{
																		echo base_url() ?>/assets/images/vehicle/<?php echo  $row->vehb_image; 
																	} ?>" onerror="this.onerror=null;this.src='<?php echo base_url('/assets/images/noimage.png'); ?>';"></a>
															    <!-- <input type="file"  accept="image/x-png,image/jpeg"  name="imageone" class="form-control" id="imageone" /> -->
															 <input type="hidden" pattern=".*\S+.*"  name="hiddenimageone" value="<?php echo $row->vehb_image ?>"  class="form-control" />
													    
														</div>
														</div>

														

												   <div class="form-group row">
                                                       
													   <div class="col-lg-12">
														   <label>Select Status:</label>
															<select disabled class="form-control" name="status" required>
															<option value="<?php echo $row->vehb_status; ?>"><?php echo $row->vehb_status; ?></option>
															<?php  if($row->vehb_status!="Active"){
																?>
																<option value="Active">Active</option>
																<?php
															}else{
																?>
																<option value="Inactive">Inactive</option>
																<?php
															} ?>
															</select>
															 </div>
															
												   </div>
												   <div class="form-group row">
                                                       
													   <div class="col-lg-12">
													   <label>Select Type:</label>
												   <select id="type_vehicle" disabled required class="form-control" name="type_vehicle">
												   <option value="<?php echo $row->vehb_type; ?>"><?php echo $row->vehb_type; ?></option>

															<option value="Four-wheeler">Four-wheeler</option>
															<option value="Two-wheeler">Two-wheeler</option>
															<option value="Bus">Bus</option>
															<option value="Truck">Truck</option>
															<option value="CNG">CNG</option>
															<option value="Electric Vehicle">Electric Vehicle</option>
															</select>
															 </div>
															
												   </div>
												  
												</div>
												
											
												<div class="card-footer">
													<div class="row">
														<div class="col-lg-12">
															<!-- <button type="submit" class="btn btn-primary mr-2">Save</button> -->
															</div>
													</div>
												</div>
											</form>
											<!--end::Form-->
										</div>
										<!--end::Card-->
									</div>
								</div>
							</div>
							<!--end::Container-->
						</div>
						<!--end::Entry-->
					</div>
                    <!--end::Content-->
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                    
					<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>  
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
					<script src="https://cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
					<script>
						$(".alphaonly").keydown(function(event){
        var inputValue = event.which;
        // console.log(inputValue);
        if (!(inputValue >= 65 && inputValue <= 123)
                            && (inputValue != 32 && inputValue != 0)
                            && ( inputValue != 8)
                            && (inputValue != 9)){
                                event.preventDefault(); 
                        }
    });
					 function validatepannumber(){
                        var pannumber = document.getElementById("technician_pan").value;
                        if(pannumber==""){
                            return true;
                        }else{
                            var regpan = /^([A-Z]){5}([0-9]){4}([A-Z]){1}?$/;
                            if(regpan.test(pannumber) == false)
                                {
									alert("Not a valid Pan Number");
	   document.getElementById("technician_pan").value='';
	   return false;
                                }else{
                                return true;
                                }
                            }
                        }
				function duplicatecheck(){
	var value=document.getElementById("supllier_phonenumber").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Phone Number");
	   document.getElementById("supllier_phonenumber").value='';
	   return false;
     }
}



function adharcheck(){
	var value=document.getElementById("technician_aadhar").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{6})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Adhar Number");
	   document.getElementById("technician_aadhar").value='';
	   return false;
     }
}

function pincodevalidation(){
	var value=document.getElementById("technician_pincode").value;
	
	var phoneno = /^\(?([1-9]{1})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{2})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Pincode Number");
	   document.getElementById("technician_pincode").value='';
	   return false;
     }
}
function validategstnumber(){
                            var cgstnumber = document.getElementById("gstnumber").value;
                            if(cgstnumber==""){
                            return true;
                        }else{
                            var regpan = /\d{2}[A-Z]{5}\d{4}[A-Z]{1}[A-Z\d]{1}[Z]{1}[A-Z\d]{1}/;
                            if(regpan.test(cgstnumber) == false)
                            {
								alert("Not a valid GST Number");
	   document.getElementById("gstnumber").value='';
                                   
                                    return false;
                                }else{
                                return true;
                                }
                            }
                        }

						</script>