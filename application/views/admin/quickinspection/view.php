
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Subheader-->
					
						<!--end::Subheader-->
						<!--begin::Entry-->
						<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
							<div class="container">
								<div class="row">
									<div class="col-lg-12">
										<!--begin::Card-->
										<div class="card card-custom gutter-b example example-compact">
											
											
											<!--begin::Form-->
											
											<form class="form" method="post" id="resetform" action="<?php echo  base_url('admin/Insurance/update'); ?>" enctype="multipart/form-data" >
											<div class="card-body">
												<?php foreach($data as $row): ?>
													<div class="form-group row">
														<div class="col-lg-12">
														<label>User Name:</label>
														<input type="text" disabled required  value="<?php echo $row->muser_name; ?>" name="muser_name" class="form-control" id="muser_name" placeholder="Enter User Name" />
                                                        
                                                        
														</div></div>
														<div class="form-group row">
														<div class="col-lg-12">
														<label>Remark:</label>
															<input type="text" disabled required  value="<?php echo $row->remark; ?>" name="remark" class="form-control" id="remark" placeholder="Enter Remark" />
                                                        
                                                        </div>
                                                    </div>
													
												   <div class="form-group row">
                                                       
													   <div class="col-lg-12">
													   <label>Status:</label>
															<select disabled class="form-control" name="status" required>
															<option value="<?php echo $row->status; ?>"><?php echo $row->status; ?></option>
															<?php  if($row->status!="Active"){
																?>
																<option value="Active">Active</option>
																<?php
															}else{
																?>
																<option value="Inactive">Inactive</option>
																<?php
															} ?>
															</select>
															 </div>
															
												   </div>
												<?php endforeach;?>
												  <?php foreach ($data2 as $row2): ?>
												    <div class="form-group row">
														<div class="col-lg-12">
														<label>Vehicle Type:</label>
															<input type="text" disabled required  value="<?php echo $row2->vehb_type; ?>" name="vehb_type" class="form-control" id="vehb_type" placeholder="Enter Pollution Description" />
                                                        
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
														<div class="col-lg-12">
														<label>Vehicle Make:</label>
															<input type="text" disabled required  value="<?php echo $row2->vehb_make; ?>" name="vehb_make" class="form-control" id="vehb_make" placeholder="Enter Pollution Description" />
                                                        
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
														<div class="col-lg-12">
														<label>Vehicle Model:</label>
															<input type="text" disabled required  value="<?php echo $row2->vehb_model; ?>" name="vehb_model" class="form-control" id="vehb_model" placeholder="Enter Pollution Description" />
                                                        
                                                        </div>
                                                    </div>


<div class="form-group row">
														<div class="col-lg-12">
														<label>Vehicle Trim:</label>
															<input type="text" disabled required  value="<?php echo $row2->vehb_trim; ?>" name="vehb_trim" class="form-control" id="vehb_trim" placeholder="Enter Pollution Description" />
                                                        
                                                        </div>
                                                    </div>

                                                     <div class="form-group row">
														<div class="col-lg-12">
														<label>Vehicle Fuel:</label>
															<input type="text" disabled required  value="<?php echo $row2->vehb_fuel; ?>" name="vehb_fuel" class="form-control" id="vehb_fuel" placeholder="Enter Pollution Description" />
                                                        
                                                        </div>
                                                    </div>


                                                    <div class="form-group row">
                                                       
													   <div class="col-lg-12">
													   <label>Image:</label>
																   <a target="_blank" href="<?php echo base_url() ?>/assets/images/insurance/<?php echo $row2->vehb_image; ?>"><img style="width: 70px; height: 70px" src="<?php 
																   if($row2->vehb_image==""){
																	   echo base_url('/assets/images/no-images/no-product.png'); 
																	   }else{
																		   echo base_url() ?>/assets/images/insurance/<?php echo $row2->vehb_image; 
																	   } ?>" onerror="this.onerror=null;this.src='<?php echo base_url('/assets/images/noimage.png'); ?>';"></a>
																   <!-- <input type="file"  accept="image/x-png,image/jpeg"  name="imagetwo" class="form-control" id="imagetwo" /> -->
																<input type="hidden" pattern=".*\S+.*"  name="hiddenimagetwo" value="<?php echo $row2->vehb_image ?>"  class="form-control" />
														   
														   </div>
														   </div>

														   

                                                  
												   <?php endforeach; ?>
												  
												</div>
												
											
												<div class="card-footer">
													<div class="row">
														<div class="col-lg-12">
															<!-- <button type="submit" class="btn btn-primary mr-2">Save</button> -->
															</div>
													</div>
												</div>
											</form>
											<!--end::Form-->
										</div>
										<!--end::Card-->
									</div>
								</div>
							</div>
							<!--end::Container-->
						</div>
						<!--end::Entry-->
					</div>
                    <!--end::Content-->
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                    
					<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>  
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
                    <script>
						$(document).ready(function(){
						
						$('.vehiclename').select2({
                        placeholder: '--- Select Vehicle ---',
                        ajax: {
                       url: websiteurl + 'Vehicle/vehiclelist',
                        dataType: 'json',
                        delay: 250,
                        processResults: function (data) {
							console.log(data);
                        return {
                            results: $.map(data, function(obj)
                        {
							return { id: obj.vehb_id, text: obj.vehb_make+'/'+obj.vehb_model+'/'+obj.vehb_trim };
                         
                        })
                        };
                        },
                        cache: true
                        }
                        });
					});

                    
					$(".alphaonly").keydown(function(event){
        var inputValue = event.which;
        // console.log(inputValue);
        if (!(inputValue >= 65 && inputValue <= 123)
                            && (inputValue != 32 && inputValue != 0)
                            && ( inputValue != 8)
                            && (inputValue != 9)){
                                event.preventDefault(); 
                        }
    });
					 function validatepannumber(){
                        var pannumber = document.getElementById("technician_pan").value;
                        if(pannumber==""){
                            return true;
                        }else{
                            var regpan = /^([A-Z]){5}([0-9]){4}([A-Z]){1}?$/;
                            if(regpan.test(pannumber) == false)
                                {
									alert("Not a valid Pan Number");
	   document.getElementById("technician_pan").value='';
	   return false;
                                }else{
                                return true;
                                }
                            }
                        }
				function duplicatecheck(){
	var value=document.getElementById("technician_phonenumber").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Phone Number");
	   document.getElementById("technician_phonenumber").value='';
	   return false;
     }


 $.ajax({
	url: websiteurl + 'Technician/phonenocheckexist',
        type: 'POST',
        data: {
            key: value
        },
        dataType: 'json',
        success: function(data) {
		
			if(data==2){
				alert('Phone Number is Already exists');
				document.getElementById("technician_phonenumber").value='';
			}
        }
    });
}

function duplicatecheckemail(){
	var value=document.getElementById("technician_email").value;
	
 $.ajax({
	url: websiteurl + 'Technician/emailcheckexist',
        type: 'POST',
        data: {
            key: value
        },
        dataType: 'json',
        success: function(data) {
		
			if(data==2){
				alert('Email is Already exists');
				document.getElementById("technician_email").value='';
			}
        }
    });
}

function adharcheck(){
	var value=document.getElementById("technician_aadhar").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{6})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Adhar Number");
	   document.getElementById("technician_aadhar").value='';
	   return false;
     }
}

function pincodevalidation(){
	var value=document.getElementById("technician_pincode").value;
	
	var phoneno = /^\(?([1-9]{1})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{2})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Pincode Number");
	   document.getElementById("technician_pincode").value='';
	   return false;
     }
}
function validategstnumber(){
                            var cgstnumber = document.getElementById("gstnumber").value;
                            if(cgstnumber==""){
                            return true;
                        }else{
                            var regpan = /\d{2}[A-Z]{5}\d{4}[A-Z]{1}[A-Z\d]{1}[Z]{1}[A-Z\d]{1}/;
                            if(regpan.test(cgstnumber) == false)
                            {
								alert("Not a valid GST Number");
	   document.getElementById("gstnumber").value='';
                                   
                                    return false;
                                }else{
                                return true;
                                }
                            }
                        }

						</script>