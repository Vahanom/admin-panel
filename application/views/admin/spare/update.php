
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Subheader-->
					
						<!--end::Subheader-->
						<!--begin::Entry-->
						<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
							<div class="container">
								<div class="row">
									<div class="col-lg-12">
										<!--begin::Card-->
										<div class="card card-custom gutter-b example example-compact">
											
											<?php foreach($spare as $row){} ?>
											<!--begin::Form-->
											
											<form class="form" method="post" id="resetform" action="<?php echo  base_url('admin/Spare/update'); ?>" enctype="multipart/form-data" >
											<div class="card-body">

												     <div class="form-group row">
                                                       
													   <div class="col-lg-12">
													   <label>Select Spare Category:</label>
														   <select id="sparecategory" required class="form-control" name="sparecategory">
													
															<?php foreach($sparecat as $sparecat_rec) {
																    
																	if($row->sp_category_id==$sparecat_rec->spcat_id){?>
																	
															<option selected value="<?php echo $sparecat_rec->spcat_id; ?>"><?php echo $sparecat_rec->spcat_title; ?></option>


															<?php } else { ?>
																<option value="<?php echo $sparecat_rec->spcat_id; ?>"><?php echo $sparecat_rec->spcat_title; ?></option>

																<?php } }?>
															</select>
															 </div>
															
												   </div>

												   <div class="form-group row">
														<div class="col-lg-12">
														<label>Margin:</label>
															<input type="number" required name="margin" value="<?php echo $row->margin;?>" class="form-control" id="margin" placeholder="Enter Spare Margin" />
                                                            
                                                        </div>
                                                    </div>

													<div class="form-group row">
														<div class="col-lg-12">
														<label>Enter Spare Title:</label>
															<input type="text" required name="space_title" value="<?php echo $row->sp_title;?>"  class="form-control" id="space_title" placeholder="Enter Spare title" />
                                                             <input type="hidden" required name="sp_id" value="<?php echo $row->sp_id;?>" class="form-control" id="sp_id" placeholder="Enter English Title" />
                                                        
														</div></div>
														<div class="form-group row">
														<div class="col-lg-12">
														<label>Spare description:</label>
															<input type="text" required name="spare_desc" value="<?php echo $row->sp_description;?>" class="form-control" id="spare_desc" placeholder="Enter Spare description" />
                                                            
                                                        </div>
                                                    </div>
													<div class="form-group row">
														<div class="col-lg-12">
														<label>Spare HSN:</label>
															<input type="text" required name="space_hsn" value="<?php echo $row->sp_hsn_code;?>" class="form-control" id="space_hsn" placeholder="Enter Spare HSN" />
                                                        
                                                        </div>
                                                    </div>
													<div class="form-group row">
														<div class="col-lg-12">
														<label>Spares Time Required:</label>
															<input type="number" required name="tech_hours" value="<?php echo $row->tech_hours;?>" class="form-control" id="tech_hours" placeholder="Enter Hours" />
                                                        
                                                        </div>
                                                    </div>
													<div class="form-group row">
														<div class="col-lg-12">
														<label>Labour Rate:</label>
															<input type="number" required name="lb_rate" value="<?php echo $row->labour_rate;?>" class="form-control" id="lb_rate" placeholder="Enter Rate" onblur="cal_cost()"/>
                                                        
                                                        </div>
                                                    </div>
													<div class="form-group row">
														<div class="col-lg-12">
														<label>Labour Commission:</label>
															<input type="number" required name="lb_commission" value="<?php echo $row->vh_labour_commission;?>" class="form-control" id="lb_commission" placeholder="Enter Commission" onblur="cal_cost()"/>
                                                        
                                                        </div>
                                                    </div>
													<div class="form-group row">
														<div class="col-lg-12">
														<label>Cost:</label>
															<input type="number" required name="veh_cost" value="<?php echo $row->vh_cost;?>" class="form-control" id="vh_cost" placeholder="Enter Cost" readonly/>
                                                        
                                                        </div>
                                                    </div>
													<div class="form-group row">
														<div class="col-lg-12">
														<label>Select Vehicle:</label>
                                                            <select name="vehiclename[]" id="vehiclename" multiple required class="form-control vehiclename">
															<?php foreach ($spare as $row1) {?>
																<option value="<?php echo $row1->vehb_id ?>" selected ><?php echo $row1->vehb_make.'/'.$row1->vehb_model.'/'.$row1->vehb_trim;  ?></option>
															<?php }?>
															</select>
                                                        </div>
                                                    </div>
													<div class="form-group row">
                                                    	<div class="col-lg-12">
															<label>Image One:</label>
                                                            	<a target="_blank" href="<?php echo base_url() ?>/assets/images/spare/<?php echo $row->sp_imageone; ?>"><img style="width: 70px; height: 70px" src="<?php 
																if($row->sp_imageone==""){
																	echo base_url('/assets/images/noimage.png'); 
																	}else{
																		echo base_url() ?>/assets/images/spare/<?php echo  $row->sp_imageone; 
																	} ?>"></a>
															    <input type="file"  accept="image/x-png,image/jpeg"  name="imageone" class="form-control" id="imageone" />
															 <input type="hidden" pattern=".*\S+.*"  name="hiddenimageone" value="<?php echo $row->sp_imageone ?>"  class="form-control" />
													    </div>
													</div>

													<!-- <div class="form-group row">
                                                       	<div class="col-lg-12">
															   <label>Image Two:</label>
																   <a target="_blank" href="<?php //echo base_url() ?>/assets/images/spare/<?php //echo $row->sp_imagetwo; ?>">
																   	<img style="width: 70px; height: 70px" src="<?php 
																   //if($row->sp_imagetwo==""){
																	   //echo base_url('/assets/images/no-images/no-product.png'); 
																	 //  }else{
																		//   echo base_url() ?>/assets/images/spare/<?php //echo  $row->sp_imagetwo; 
																	   //} ?>"></a>
																   <input type="file"  accept="image/x-png,image/jpeg"  name="imagetwo" class="form-control" id="imagetwo" />
																<input type="hidden" pattern=".*\S+.*"  name="hiddenimagetwo" value="<?php //echo $row->sp_imagetwo ?>"  class="form-control" />
														   </div>
												   	</div> -->
													<!-- <div class="form-group row">
                                                    	<div class="col-lg-12">
															   <label>Image Three:</label>
																   <a target="_blank" href="<?php //echo base_url() ?>/assets/images/spare/<?php// echo $row->sp_imagethree; ?>"><img style="width: 70px; height: 70px" src="<?php 
																   // if($row->sp_imagethree==""){
																	  //  echo base_url('/assets/images/no-images/no-product.png'); 
																	  //  }else{
																		 //   echo base_url() ?>/assets/images/spare/<?php //echo  $row->sp_imagethree; 
																	  //  } ?>"></a>
																   <input type="file"  accept="image/x-png,image/jpeg"  name="imagethree" class="form-control" id="imagethree" />
																<input type="hidden" pattern=".*\S+.*"  name="hiddenimagethree" value="<?php //echo $row->sp_imagethree ?>"  class="form-control" />
														</div>
													</div>	 -->
													<!-- <div class="form-group row">
                                                    	<div class="col-lg-12">
															   <label>Image Four:</label>
																   <a target="_blank" href="<?php //echo base_url() ?>/assets/images/spare/<?php //echo $row->sp_imagefour; ?>"><img style="width: 70px; height: 70px" src="<?php 
																   // if($row->sp_imagefour==""){
																	  //  echo base_url('/assets/images/no-images/no-product.png'); 
																	  //  }else{
																		 //   echo base_url() ?>/assets/images/spare/<?php //echo  $row->sp_imagefour; 
																	  //  } ?>"></a>
																   <input type="file"  accept="image/x-png,image/jpeg"  name="imagefour" class="form-control" id="imagefour" />
																<input type="hidden" pattern=".*\S+.*"  name="hiddenimagefour" value="<?php //echo $row->sp_imagefour ?>"  class="form-control" />
														</div>
													</div> -->
												   <div class="form-group row">
                                                       
													   <div class="col-lg-12">
														   <label>Select Status:</label>
															<select class="form-control" name="status" required>
															<option value="<?php echo $row->sp_status; ?>"><?php echo $row->sp_status; ?></option>
															<?php  if($row->sp_status!="Active"){
																?>
																<option value="Active">Active</option>
																<?php
															}else{
																?>
																<option value="Inactive">Inactive</option>
																<?php
															} ?>
															</select>
															 </div>
															
												   </div>

												  
												  
												</div>
												
											
												<div class="card-footer">
													<div class="row">
														<div class="col-lg-12">
															<button type="submit" class="btn btn-primary mr-2">Save</button>
															</div>
													</div>
												</div>
											</form>
											<!--end::Form-->
										</div>
										<!--end::Card-->
									</div>
								</div>
							</div>
							<!--end::Container-->
						</div>
						<!--end::Entry-->
					</div>
                    <!--end::Content-->
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                    
					<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>  
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
                    <script>
                    function cal_cost(){
                        var lbcm= $('#lb_commission').val();
                        var rate= $('#lb_rate').val();
                        var cost = parseInt(rate)+parseInt(rate*(lbcm/100));
                        $('#vh_cost').val(cost);
                        }
						$(document).ready(function(){
						
						$('.vehiclename').select2({
                        placeholder: '--- Select Vehicle ---',
                        ajax: {
                       url: websiteurl + 'Vehicle/vehiclelist',
                        dataType: 'json',
                        delay: 250,
                        processResults: function (data) {
							console.log(data);
                        return {
                            results: $.map(data, function(obj)
                        {
							return { id: obj.vehb_id, text: obj.vehb_make+'/'+obj.vehb_model+'/'+obj.vehb_trim };
                         
                        })
                        };
                        },
                        cache: true
                        }
                        });
					});

                    
					$(".alphaonly").keydown(function(event){
        var inputValue = event.which;
        // console.log(inputValue);
        if (!(inputValue >= 65 && inputValue <= 123)
                            && (inputValue != 32 && inputValue != 0)
                            && ( inputValue != 8)
                            && (inputValue != 9)){
                                event.preventDefault(); 
                        }
    });
					 function validatepannumber(){
                        var pannumber = document.getElementById("technician_pan").value;
                        if(pannumber==""){
                            return true;
                        }else{
                            var regpan = /^([A-Z]){5}([0-9]){4}([A-Z]){1}?$/;
                            if(regpan.test(pannumber) == false)
                                {
									alert("Not a valid Pan Number");
	   document.getElementById("technician_pan").value='';
	   return false;
                                }else{
                                return true;
                                }
                            }
                        }
				function duplicatecheck(){
	var value=document.getElementById("technician_phonenumber").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Phone Number");
	   document.getElementById("technician_phonenumber").value='';
	   return false;
     }


 $.ajax({
	url: websiteurl + 'Technician/phonenocheckexist',
        type: 'POST',
        data: {
            key: value
        },
        dataType: 'json',
        success: function(data) {
		
			if(data==2){
				alert('Phone Number is Already exists');
				document.getElementById("technician_phonenumber").value='';
			}
        }
    });
}

function duplicatecheckemail(){
	var value=document.getElementById("technician_email").value;
	
 $.ajax({
	url: websiteurl + 'Technician/emailcheckexist',
        type: 'POST',
        data: {
            key: value
        },
        dataType: 'json',
        success: function(data) {
		
			if(data==2){
				alert('Email is Already exists');
				document.getElementById("technician_email").value='';
			}
        }
    });
}

function adharcheck(){
	var value=document.getElementById("technician_aadhar").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{6})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Adhar Number");
	   document.getElementById("technician_aadhar").value='';
	   return false;
     }
}

function pincodevalidation(){
	var value=document.getElementById("technician_pincode").value;
	
	var phoneno = /^\(?([1-9]{1})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{2})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Pincode Number");
	   document.getElementById("technician_pincode").value='';
	   return false;
     }
}
function validategstnumber(){
                            var cgstnumber = document.getElementById("gstnumber").value;
                            if(cgstnumber==""){
                            return true;
                        }else{
                            var regpan = /\d{2}[A-Z]{5}\d{4}[A-Z]{1}[A-Z\d]{1}[Z]{1}[A-Z\d]{1}/;
                            if(regpan.test(cgstnumber) == false)
                            {
								alert("Not a valid GST Number");
	   document.getElementById("gstnumber").value='';
                                   
                                    return false;
                                }else{
                                return true;
                                }
                            }
                        }

// space should not accept..
document.getElementById("space_title").addEventListener('keydown', function (e) {
  if (this.value.length === 0 && e.which === 32) e.preventDefault();
});
document.getElementById("spare_desc").addEventListener('keydown', function (e) {
  if (this.value.length === 0 && e.which === 32) e.preventDefault();
});
document.getElementById("space_hsn").addEventListener('keydown', function (e) {
  if (this.value.length === 0 && e.which === 32) e.preventDefault();
});


</script>