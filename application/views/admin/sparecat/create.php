
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Subheader-->
						<div class="subheader py-2 py-lg-4 subheader-transparent" id="kt_subheader">
							<div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
								<!--begin::Info-->
								<div class="d-flex align-items-center flex-wrap mr-1">
									<!--begin::Heading-->
									<div class="d-flex flex-column">
										<!--begin::Title-->
										<h2 class="text-white font-weight-bold my-2 mr-5">Add Spare Category</h2>
										<!--end::Title-->
										<!--begin::Breadcrumb-->
										<div class="d-flex align-items-center font-weight-bold my-2">
											<!--begin::Item-->
											<a href="#" class="opacity-75 hover-opacity-100">
												<i class="flaticon2-shelter text-white icon-1x"></i>
											</a>
											
											<!--end::Item-->
											<!--begin::Item-->
											<span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
											<a href="<?php echo base_url('admin/Dashboard') ?>" class="text-white text-hover-white opacity-75 hover-opacity-100">Dashboard</a>
											<!--end::Item-->
											<!--begin::Item-->
											<span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
											<a href="<?php echo base_url('admin/Sparecat/index') ?>" class="text-white text-hover-white opacity-75 hover-opacity-100">Spare Category</a>
											<!--end::Item-->
											<!--begin::Item-->
											<!--end::Item-->
											<!--begin::Item-->
											<span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
											<a href="" class="text-white text-hover-white opacity-75 hover-opacity-100">Add Spare Category</a>
											<!--end::Item-->
										</div>
										<!--end::Breadcrumb-->
									</div>

									<!--end::Heading-->
								</div>
								<!--end::Info-->
							</div>
						</div>
						<!--end::Subheader-->
						<!--begin::Entry-->
						<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
							<div class="container">
								<div class="row">
									<div class="col-lg-12">
										<!--begin::Card-->
										<div class="card card-custom gutter-b example example-compact">
											<div class="card-header">
												<h3 class="card-title">Add Spare Category</h3>
											</div>
											<!--begin::Form-->
											<form class="form" method="post" action="<?php echo  base_url('admin/Sparecat/add'); ?>" enctype="multipart/form-data" >
												<div class="card-body">
													<div class="form-group row">
														<div class="col-lg-6">
															<label>Spare Category Name:</label>
															<input type="text" required name="space_categorytitle" class="form-control" id="space_categorytitle" placeholder="Enter Spare Category Name" onblur="duplicatecheckname();"/>
                                                        
														</div>
														<div class="col-lg-6">
															<label>Spare Category Description:</label>
															<input type="text" required name="spare_desc" class="form-control" id="spare_desc" placeholder="Enter Spare description" />
                                                        
                                                        </div>
                                                    </div>
                                                
												   <div class="form-group row">
												   <div class="col-lg-6">
												   <label>Vehicle Type:</label>
												   <select id="type_vehicle" required class="form-control" name="type_vehicle">
															<option value="">Vehicle Type</option>
															<option value="Four-wheeler">Four-wheeler</option>
															<option value="Two-wheeler">Two-wheeler</option>
															<option value="Bus">Bus</option>
															<option value="Truck">Truck</option>
															</select>
													 </div>
															 <div class="col-lg-6">
														   <label>Select Status:</label>
														   <select id="status" required class="form-control" name="status">
															<option value="">Select Status</option>
															<option value="Active">Active</option>
															<option value="Inactive">Inactive</option>
															</select>
															 </div>
															
												   </div>
												  
												 
												</div>
												<div class="card-footer">
													<div class="row">
														<div class="col-lg-6">
															<button type="submit" class="btn btn-primary mr-2">Save</button>
															<button type="reset" class="btn btn-secondary">Reset</button>
														</div>
													</div>
												</div>
											</form>
											<!--end::Form-->
										</div>
										<!--end::Card-->
									</div>
								</div>
							</div>
							<!--end::Container-->
						</div>
						<!--end::Entry-->
					</div>
					<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Password Reset</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="response">
      	
      </div>
  
    
    </div>
  </div>
</div>

					<!-- <div class="col-lg-6">
															<label>Select Offer:</label>
															<select name="offer_select" id="" class="form-control offer_select"></select>
													</div> -->
                    <!--end::Content-->
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                    
					<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>  
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
                    <script type="text/javascript">
   function showEditForm(id){
            $.ajax({
                type: "POST", 
				url: websiteurl + 'Reset/geteditModel',
				data: {
				"id": id
				},
                success: function(response) {
                  
                 $("#response").html(response);
                   $("#exampleModalCenter").modal('show');
                  
                }
            });
}


</script>
                    <script>
					$(".alphaonly").keydown(function(event){
        var inputValue = event.which;
        // console.log(inputValue);
        if (!(inputValue >= 65 && inputValue <= 123)
                            && (inputValue != 32 && inputValue != 0)
                            && ( inputValue != 8)
                            && (inputValue != 9)){
                                event.preventDefault(); 
                        }
    });
					 function validatepannumber(){
                        var pannumber = document.getElementById("technician_pan").value;
                        if(pannumber==""){
                            return true;
                        }else{
                            var regpan = /^([A-Z]){5}([0-9]){4}([A-Z]){1}?$/;
                            if(regpan.test(pannumber) == false)
                                {
									alert("Not a valid Pan Number");
	   document.getElementById("technician_pan").value='';
	   return false;
                                }else{
                                return true;
                                }
                            }
                        }
				function duplicatecheck(){
	var value=document.getElementById("technician_phonenumber").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Phone Number");
	   document.getElementById("technician_phonenumber").value='';
	   return false;
     }


 $.ajax({
	url: websiteurl + 'Technician/phonenocheckexist',
        type: 'POST',
        data: {
            key: value
        },
        dataType: 'json',
        success: function(data) {
		
			if(data==2){
				alert('Phone Number is Already exists');
				document.getElementById("technician_phonenumber").value='';
			}
        }
    });
}

function duplicatecheckemail(){
	var value=document.getElementById("technician_email").value;
	
 $.ajax({
	url: websiteurl + 'Technician/emailcheckexist',
        type: 'POST',
        data: {
            key: value
        },
        dataType: 'json',
        success: function(data) {
		
			if(data==2){
				alert('Email is Already exists');
				document.getElementById("technician_email").value='';
			}
        }
    });
}
function duplicatecheckname(){
	var value=document.getElementById("space_categorytitle").value;
	
 $.ajax({
	url: websiteurl + 'Sparecat/namecheckexist',
        type: 'POST',
        data: {
            key: value
        },
        dataType: 'json',
        success: function(data) {
		
			if(data==2){
				alert('Spare Category Name is Already exists');
				document.getElementById("space_categorytitle").value='';
			}
        }
    });
}
function adharcheck(){
	var value=document.getElementById("technician_aadhar").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{6})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Adhar Number");
	   document.getElementById("technician_aadhar").value='';
	   return false;
     }
}

function pincodevalidation(){
	var value=document.getElementById("technician_pincode").value;
	
	var phoneno = /^\(?([1-9]{1})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{2})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Pincode Number");
	   document.getElementById("technician_pincode").value='';
	   return false;
     }
}
function validategstnumber(){
                            var cgstnumber = document.getElementById("gstnumber").value;
                            if(cgstnumber==""){
                            return true;
                        }else{
                            var regpan = /\d{2}[A-Z]{5}\d{4}[A-Z]{1}[A-Z\d]{1}[Z]{1}[A-Z\d]{1}/;
                            if(regpan.test(cgstnumber) == false)
                            {
								alert("Not a valid GST Number");
	   document.getElementById("gstnumber").value='';
                                   
                                    return false;
                                }else{
                                return true;
                                }
                            }
                        }
document.getElementById("space_categorytitle").addEventListener('keydown', function (e) {
  if (this.value.length === 0 && e.which === 32) e.preventDefault();
  if (this.value.length === 0 && e.which === 48) e.preventDefault();
  //special character
  if (e.which === 56 || e.which === 53 || e.which === 51 || e.which === 49 || e.which === 50 || e.which === 52 || e.which === 48 ) e.preventDefault();
});
						</script>