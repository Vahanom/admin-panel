                    <!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Subheader-->
						
						<!--end::Subheader-->
						<!--begin::Entry-->
						<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
							<div class="container">
								<div class="row">
									<div class="col-lg-12">
										<!--begin::Card-->
										<div class="card card-custom gutter-b example example-compact">
                                        <?php  foreach($myprofile as $row){} ?>	
											<!--begin::Form-->
											<form class="form" method="post" id="configform" action="<?php echo  base_url('admin/profile/update'); ?>" enctype="multipart/form-data" onsubmit="return validatephone();">
												<div class="card-body">
												
                                                    <div class="form-group row">

                                                    <div class="col-lg-6">
															<label> Name:</label>
                                                            <input type="text" required id="title" pattern=".*\S+.*" name="u_name" class="form-control" placeholder="Enter name" value="<?php echo $row->webuser_name;?>"/>
                                                            <b class="text-danger"><span id="duplicacy_error"></span></b>
                                                           
														</div>
                                                        
                                                        <div class="col-lg-6">
															<label>Email:</label>
                                                            <input type="email" required id="email" pattern=".*\S+.*" name="u_email" class="form-control" placeholder="Enter email" value="<?php echo $row->webuser_email;?>"/>
                                                            <b class="text-danger"><span id="duplicacyemail_error"></span></b>
                                                        </div>
                                                        </div>
                                                    
													<div class="form-group row">
                                                      
                                                        
                                                        <div class="col-lg-6">
															<label>Contact Number:</label>
                                                            <input type="number" required pattern=".*\S+.*" name="u_phno" onchange="validatephone()" onkeyup="removephnoerror()" class="form-control" id="phno" placeholder="Enter contact number" value="<?php echo $row->webuser_phno;?>"/>
                                                            <b class="text-danger"><span id="phonevalidity"></span></b>
                                                        </div>
                                                        <div class="col-lg-6">
															<label>Address:</label>
                                                                <textarea name="u_address" required pattern=".*\S+.*" class="form-control" id="u_address" cols="100" rows="1" value="<?php echo $row->webuser_address;?>" placeholder="Enter Stock Agent address"><?php echo $row->webuser_address;?></textarea>
                                                        </div>

                                                       
                                                    </div>
                                                    
                                                    
												</div>
												<div class="card-footer">
													<div class="row">
														<div class="col-lg-6">
															<button type="submit" class="btn btn-primary mr-2">Save</button>
															<button type="reset" onclick="resetbutton();" class="btn btn-secondary">Reset</button>
														</div>
													</div>
												</div>
											</form>
											<!--end::Form-->
										</div>
										<!--end::Card-->
									</div>
								</div>
							</div>
							<!--end::Container-->
						</div>
						<!--end::Entry-->
                    </div>
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>  
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

                    <script>

function resetbutton(){
$('#email').removeAttr('value');
$('#title').removeAttr('value');
$('#phno').removeAttr('value');
$("#u_address").text(" ");
    }
                        </script>

                    <!--end::Content-->
                    <script>
                        $("#documents").change(function(){
                        var allowedTypes = ['image/jpeg', 'image/png', 'image/jpg'];
                            var file = this.files[0];
                            var fileType = file.type;
                            if(!allowedTypes.includes(fileType)) {
                                jQuery("#chk-error-documents").html('<b class="text-danger">Please choose a valid file (JPG/JPEG/PNG)</b>');
                                $("#documents").val('');
                                return false;
                            } else {
                            jQuery("#chk-error-documents").html('');
                            }
                        });

                        function validatephone() {
                        var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
                            var phonenumber = document.getElementById("phno").value;
                                        if(phonenumber.match(phoneno))
                                        {
                                            document.getElementById("phonevalidity").innerHTML = "";
                                            return true;
                                        }else
                                        {
                                            document.getElementById("phonevalidity").innerHTML = "Invalid contact number";
                                            return false;
                                        }
                        }

                        function validatepannumber(){
                        var pannumber = document.getElementById("pan").value;
                        if(pannumber==""){
                            return true;
                        }else{
                            var regpan = /^([A-Z]){5}([0-9]){4}([A-Z]){1}?$/;
                            if(regpan.test(pannumber) == false)
                                {
                                    document.getElementById("panvalidity").innerHTML = "Invalid pan number";
                                    return false;
                                }else{
                                    document.getElementById("panvalidity").innerHTML = "";
                                return true;
                                }
                            }
                        }

                        function validategstnumber(){
                            var cgstnumber = document.getElementById("gst").value;
                            if(cgstnumber==""){
                            return true;
                        }else{
                            var regpan = /\d{2}[A-Z]{5}\d{4}[A-Z]{1}[A-Z\d]{1}[Z]{1}[A-Z\d]{1}/;
                            if(regpan.test(cgstnumber) == false)
                            {
                                    document.getElementById("gstvalidity").innerHTML = "Invalid gst number";
                                    return false;
                                }else{
                                document.getElementById("gstvalidity").innerHTML = "";
                                return true;
                                }
                            }
                        }

                        function removephnoerror(){
                            document.getElementById("phonevalidity").innerHTML = "";
                        }

                        function removepanerror(){
                            document.getElementById("panvalidity").innerHTML = "";
                        }

                        function removegsterror(){
                            document.getElementById("gstvalidity").innerHTML = "";
                        }

                       function checkduplicate(stockagentName){
                        var seller_uid = document.getElementById('seller_uid').value;
                        var sa_stockroom_id = document.getElementById('sa_stockroom_id').value;
                        $.ajax({
                                type: "POST",
                                url: "<?php echo base_url('admin/Stockagent/checkduplicate'); ?>",
                                data: { 
                                    'stockagentName': stockagentName,
                                    'sellerid': seller_uid,
                                    'sa_stockroom_id': sa_stockroom_id
                                },				
                                success: function(response) {
                                    if(response==1){
                                        document.getElementById('duplicacy_error').innerHTML="Stockagent already exist"
                                        document.getElementById('title').value="";
                                    }else{
                                        document.getElementById('duplicacy_error').innerHTML=""
                                    }
                                }
                            });
                       }

                       function removeduplicacy_error(){
                            document.getElementById('duplicacy_error').innerHTML=""
                        }



                        function checkduplicateemail(email){
                      
                            var seller_uid = document.getElementById('seller_uid').value;
                            var sa_stockroom_id = document.getElementById('sa_stockroom_id').value;
                            $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url('admin/Stockagent/checkduplicateemail'); ?>",
                                    data: { 
                                        'email': email,
                                        'sellerid': seller_uid,
                                        'sa_stockroom_id': sa_stockroom_id
                                    },				
                                    success: function(response) {
                                        if(response==1){
                                            document.getElementById('duplicacyemail_error').innerHTML="Stockagent email already exist"
                                            document.getElementById('email').value="";
                                        }else{
                                            document.getElementById('duplicacyemail_error').innerHTML=""
                                        }
                                    }
                                });
                        
                       }

                       function removeduplicacyemail_error(){
                            document.getElementById('duplicacyemail_error').innerHTML=""
                        }


                        function checkduplicatemobile(phno){
                       
                            var seller_uid = document.getElementById('seller_uid').value;
                            var sa_stockroom_id = document.getElementById('sa_stockroom_id').value;
                            $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url('admin/Stockagent/checkduplicatemobile'); ?>",
                                    data: { 
                                        'phno': phno,
                                        'sellerid': seller_uid,
                                        'sa_stockroom_id': sa_stockroom_id
                                    },				
                                    success: function(response) {
                                        if(response==1){
                                            document.getElementById('phonevalidity').innerHTML="Stockagent contact already exist"
                                            document.getElementById('phno').value="";
                                        }else{
                                            document.getElementById('phonevalidity').innerHTML=""
                                        }
                                    }
                                });
                        
                       }

                       function removeduplicacymobile_error(){
                            document.getElementById('phonevalidity').innerHTML=""
                        }
                    </script>
