
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Subheader-->
						<div class="subheader py-2 py-lg-4 subheader-transparent" id="kt_subheader">
							<div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
								<!--begin::Info-->
								<div class="d-flex align-items-center flex-wrap mr-1">
									<!--begin::Heading-->
									<div class="d-flex flex-column">
										<!--begin::Title-->
										<h2 class="text-white font-weight-bold my-2 mr-5">Add Employee</h2>
										<!--end::Title-->
										<!--begin::Breadcrumb-->
										<div class="d-flex align-items-center font-weight-bold my-2">
											<!--begin::Item-->
											<a href="#" class="opacity-75 hover-opacity-100">
												<i class="flaticon2-shelter text-white icon-1x"></i>
											</a>
											<!--end::Item-->
											<!--begin::Item-->
											<span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
											<a href="<?php echo base_url('admin/Dashboard') ?>" class="text-white text-hover-white opacity-75 hover-opacity-100">Dashboard</a>
											<!--end::Item-->
											<!--begin::Item-->
											<span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
											<a href="<?php echo base_url('admin/Employees/index') ?>" class="text-white text-hover-white opacity-75 hover-opacity-100">Employee</a>
											<!--end::Item-->
											<!--begin::Item-->
											<!--end::Item-->
											<!--begin::Item-->
											<span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
											<a href="" class="text-white text-hover-white opacity-75 hover-opacity-100">Add Employee</a>
											<!--end::Item-->
										</div>
										<!--end::Breadcrumb-->
									</div>

									<!--end::Heading-->
								</div>
								<!--end::Info-->
							</div>
						</div>
						<!--end::Subheader-->
						<!--begin::Entry-->
						<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
							<div class="container">
								<div class="row">
									<div class="col-lg-12">
										<!--begin::Card-->
										<div class="card card-custom gutter-b example example-compact">
											<div class="card-header">
												<h3 class="card-title">Add Employee</h3>
											</div>
											<!--begin::Form-->
											<form class="form" method="post" action="<?php echo  base_url('admin/Employees/add'); ?>" enctype="multipart/form-data" >
												<div class="card-body">
													<div class="form-group row">
														<div class="col-lg-6">
															<label>Employee Name:</label>
															<input type="text" required name="user_name" class="form-control" id="user_name" placeholder="Enter Employee Name" />
                                                        
														</div>
														<div class="col-lg-6">
															<label>Employee Email:</label>
															<input type="email" required name="user_email" class="form-control" onchange="duplicatecheckemail();" id="user_email" placeholder="Enter Employee Email" />
                                                        
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
														<div class="col-lg-6">
															<label>Adhar Number:</label>
															<input type="text" required name="user_aadhar" onchange="adharcheck();" class="form-control" id="user_aadhar" placeholder="Enter Employee Adhar Number" />
                                                        
														</div>
														<div class="col-lg-6">
															<label>Pan Number:</label>
															<input type="text" required name="user_pan" class="form-control" onchange="validatepannumber();" id="user_pan" placeholder="Enter Employee Pan Number" />
                                                        
                                                        </div>
                                                    </div>
													<div class="form-group row">
														<div class="col-lg-6">
															<label>Address:</label>
															<textarea cols="80" id="user_address" class="form-control"  name="user_address" rows="" placeholder="Enter Employee Address"></textarea>
                                                        </div>

														<div class="col-lg-6">
															<label>Blood Group:</label>
															<select id="blood_group" required class="form-control" name="blood_group">
															<option value="">Select Option</option>
															<option value="B+">B+</option>
															<option value="B-">B-</option>
															<option value="O+">O+</option>
															<option value="O-">O-</option>
															<option value="AB+">AB+</option>
															<option value="AB-">AB-</option>
															<option value="A+">A+</option>
															<option value="A-">A-</option>
															</select>
															</div>
                                                    </div>
												
												
												   <div class="form-group row">
												   <div class="col-lg-6">
														   <label>Employee Type:</label>
														   <select id="user_type" required class="form-control" name="user_type">
															<option value="">Employee Type</option>
															<option value="Type One">Type One</option>
															<option value="Type Two">Type Two</option>
															<option value="Type Three">Type Three</option>
															</select>
															 </div>
															
												 
													   <div class="col-lg-6">
														   <label>Select Status:</label>
														   <select id="status" required class="form-control" name="status">
															<option value="">Select Status</option>
															<option value="Active">Active</option>
															<option value="Inactive">Inactive</option>
															</select>
															 </div>
															
												   </div>
												   <div class="form-group row">
												   <div class="col-lg-6">
												   <label>Contact Number:</label>
												   <input type="number" class="form-control" onchange="duplicatecheck()" required name="phonenumber" id="phonenumber"  placeholder="Enter Employee Phone Number" />
														   
															 </div>
													
															
												   </div>
												</div>
												<div class="card-footer">
													<div class="row">
														<div class="col-lg-6">
															<button type="submit" class="btn btn-primary mr-2">Save</button>
															<button type="reset" class="btn btn-secondary">Reset</button>
														</div>
													</div>
												</div>
											</form>
											<!--end::Form-->
										</div>
										<!--end::Card-->
									</div>
								</div>
							</div>
							<!--end::Container-->
						</div>
						<!--end::Entry-->
					</div>
					<!-- <div class="col-lg-6">
															<label>Select Offer:</label>
															<select name="offer_select" id="" class="form-control offer_select"></select>
													</div> -->
                    <!--end::Content-->
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                    
					<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>  
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
                    <script>
					/* function validatepannumber(){
                        var pannumber = document.getElementById("user_pan").value;
                        if(pannumber==""){
                            return true;
                        }else{
                            var regpan = /^([A-Z]){5}([0-9]){4}([A-Z]){1}?$/;
                            if(regpan.test(pannumber) == false)
                                {
									alert("Not a valid Pan Number");
	   document.getElementById("user_pan").value='';
	   return false;
                                }else{
                                return true;
                                }
                            }
                        }*/
// Pan card validation
    function validatepannumber(){
	var pannumber=document.getElementById("user_pan").value;
	
	var regpan = /^([A-Z]){5}([0-9]){4}([A-Z]){1}?$/;
  if(pannumber.match(regpan))
     {
	  
	 }
   else
     {
	   alert("Not a valid pan Number");
	   document.getElementById("user_pan").value='';
	   return true;
     }


 $.ajax({
	url: websiteurl + 'Employees/pancheckexist',
        type: 'POST',
        data: {
            key: pannumber
        },
        dataType: 'json',
        success: function(data) {
		
			if(data==2){
				alert('Pan Number is Already exists');
				document.getElementById("user_pan").value='';
			}
        }
    });
}

//
				function duplicatecheck(){
	var value=document.getElementById("phonenumber").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Phone Number");
	   document.getElementById("phonenumber").value='';
	   return false;
     }


 $.ajax({
	url: websiteurl + 'Employees/phonenocheckexist',
        type: 'POST',
        data: {
            key: value
        },
        dataType: 'json',
        success: function(data) {
		
			if(data==2){
				alert('Phone Number is Already exists');
				document.getElementById("phonenumber").value='';
			}
        }
    });
}

function duplicatecheckemail(){
	var value=document.getElementById("user_email").value;
	
 $.ajax({
	url: websiteurl + 'Employees/emailcheckexist',
        type: 'POST',
        data: {
            key: value
        },
        dataType: 'json',
        success: function(data) {
		
			if(data==2){
				alert('Email is Already exists');
				document.getElementById("user_email").value='';
			}
        }
    });
}
//adhar check..
function adharcheck(){
	var value=document.getElementById("user_aadhar").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{6})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Adhar Number");
	   document.getElementById("user_aadhar").value='';
	   return false;
     }

      $.ajax({
	url: websiteurl + 'Employees/adharcheckexist',
        type: 'POST',
        data: {
            key: value
        },
        dataType: 'json',
        success: function(data) {
		
			if(data==2){
				alert('Adhar Number is Already exists');
				document.getElementById("user_aadhar").value='';
			}
        }
    });
}

document.getElementById("user_name").addEventListener('keydown', function (e) {
  if (this.value.length === 0 && e.which === 32) e.preventDefault();
  if (this.value.length === 0 && e.which === 48) e.preventDefault();
  //special character
  if (e.which === 56 || e.which === 53 || e.which === 51 || e.which === 49 || e.which === 50 || e.which === 52 || e.which === 48) e.preventDefault();
  
});


</script>