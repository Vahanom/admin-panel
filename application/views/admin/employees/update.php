
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Subheader-->
					
						<!--end::Subheader-->
						<!--begin::Entry-->
						<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
							<div class="container">
								<div class="row">
									<div class="col-lg-12">
										<!--begin::Card-->
										<div class="card card-custom gutter-b example example-compact">
											
											<?php foreach($employees as $row){} ?>
											<!--begin::Form-->
											
											<form class="form" method="post" id="resetform" action="<?php echo  base_url('admin/Employees/update'); ?>" enctype="multipart/form-data" >
											<div class="card-body">
													<div class="form-group row">
														<div class="col-lg-12">
														<label>Employee Name:</label>
															<input type="text" required name="user_name" value="<?php echo $row->webuser_name; ?>"  class="form-control" id="user_name" placeholder="Enter Employee Name" />
                                                        <input type="hidden" required name="webuser_id" value="<?php echo $row->webuser_id;?>" class="form-control" id="webuser_id" placeholder="Enter English Title" />
                                                        
														</div></div>
														<div class="form-group row">
														<div class="col-lg-12">
														<label>Employee Email:</label>
															<input type="email" required name="user_email" value="<?php echo $row->webuser_email; ?>"  class="form-control" onchange="duplicatecheckemail();" id="user_email" placeholder="Enter Employee Email" />
                                                        
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
														<div class="col-lg-12">
														<label>Adhar Number:</label>
															<input type="text" required name="user_aadhar" value="<?php echo $row->webuser_adhar_num; ?>"  onchange="adharcheck();" class="form-control" id="user_aadhar" placeholder="Enter Employee Adhar Number" />
                                                        
														</div>
														</div>
														<div class="form-group row">
														<div class="col-lg-12">
														<label>Pan Number:</label>
															<input type="text" required name="user_pan" value="<?php echo $row->webuser_pan_num; ?>" class="form-control" onchange="validatepannumber();" id="user_pan" placeholder="Enter Employee Pan Number" />
                                                        
                                                        </div>
                                                    </div>
													<div class="form-group row">
														<div class="col-lg-12">
														<label>Address:</label>
															<textarea cols="80" id="user_address" value="<?php echo $row->webuser_address; ?>" class="form-control"  name="user_address" rows="" placeholder="Enter Employee Address"><?php echo $row->webuser_address; ?></textarea>
                                                          </div>
														</div>
														<div class="form-group row">
														<div class="col-lg-12">
														
														<label> Blood Group:</label>
															<select class="form-control" name="blood_group" required>
															<option <?=$row->webuser_bloodgroup == 'B+' ? ' selected="selected"' : '';?> value="B+">B+</option>
															<option  <?=$row->webuser_bloodgroup == 'B-' ? ' selected="selected"' : '';?> value="B-">B-</option>
															<option  <?=$row->webuser_bloodgroup == 'O+' ? ' selected="selected"' : '';?> value="O+">O+</option>
															<option  <?=$row->webuser_bloodgroup == 'O-' ? ' selected="selected"' : '';?> value="O-">O-</option>
															<option  <?=$row->webuser_bloodgroup == 'AB+' ? ' selected="selected"' : '';?> value="AB+">AB+</option>
															<option  <?=$row->webuser_bloodgroup == 'AB-' ? ' selected="selected"' : '';?> value="AB-">AB-</option>
															<option  <?=$row->webuser_bloodgroup == 'A+' ? ' selected="selected"' : '';?> value="A+">A+</option>
															<option  <?=$row->webuser_bloodgroup == 'A-' ? ' selected="selected"' : '';?> value="A-">A-</option>
															</select>

														
														</div>
                                                    </div>
													<div class="form-group row">
														<div class="col-lg-12">
														<label>Employee Type:</label>
														   <select id="user_type" required class="form-control" name="user_type">
														   <option value="<?php echo $row->webuser_type; ?>"><?php echo $row->webuser_type; ?></option>
															
															<option value="Type One">Type One</option>
															<option value="Type Two">Type Two</option>
															<option value="Type Three">Type Three</option>
															</select>
														 </div>
														</div>
														
												   <div class="form-group row">
                                                       
													   <div class="col-lg-12">
														   <label>Select Status:</label>
															<select class="form-control" name="status" required>
															<option value="<?php echo $row->webuser_status; ?>"><?php echo $row->webuser_status; ?></option>
															<?php  if($row->webuser_status!="Active"){
																?>
																<option value="Active">Active</option>
																<?php
															}else{
																?>
																<option value="Inactive">Inactive</option>
																<?php
															} ?>
															</select>
															 </div>
															
												   </div>
												   	
												   <div class="form-group row">
                                                       
													   <div class="col-lg-12">
													   <label>Contact Number:</label>
												   <input type="number" class="form-control" value="<?php echo $row->webuser_phno; ?>" onchange="duplicatecheck()" required name="phonenumber" id="phonenumber"  placeholder="Enter Employee Phone Number" />
														  
															 </div>
															
												   </div>
												</div>
												
											
												<div class="card-footer">
													<div class="row">
														<div class="col-lg-12">
															<button type="submit" class="btn btn-primary mr-2">Save</button>
															</div>
													</div>
												</div>
											</form>
											<!--end::Form-->
										</div>
										<!--end::Card-->
									</div>
								</div>
							</div>
							<!--end::Container-->
						</div>
						<!--end::Entry-->
					</div>
                    <!--end::Content-->
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                    
					<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>  
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
					<script src="https://cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
					<script>
					 function validatepannumber(){
                        var pannumber = document.getElementById("user_pan").value;
                        if(pannumber==""){
                            return true;
                        }else{
                            var regpan = /^([A-Z]){5}([0-9]){4}([A-Z]){1}?$/;
                            if(regpan.test(pannumber) == false)
                                {
									alert("Not a valid Pan Number");
	   document.getElementById("user_pan").value='';
	   return false;
                                }else{
                                return true;
                                }
                            }
                        }
function duplicatecheck(){
	var value=document.getElementById("phonenumber").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Phone Number");
	   document.getElementById("phonenumber").value='';
	   return false;
     }

}



function adharcheck(){
	var value=document.getElementById("user_aadhar").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{6})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Adhar Number");
	   document.getElementById("user_aadhar").value='';
	   return false;
     }
}
document.getElementById("user_name").addEventListener('keydown', function (e) {
  if (this.value.length === 0 && e.which === 32) e.preventDefault();
  if (this.value.length === 0 && e.which === 48) e.preventDefault();
});

/*$(function() {

  $('#user_name').keydown(function (e) {
  
    if (e.shiftKey || e.ctrlKey || e.altKey) {
    
      e.preventDefault();
      
    } else {
    
      var key = e.keyCode;
      
      if (!((key == 8) || (key == 32) || (key == 46) ||(key == 57) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
      
        e.preventDefault();
        
      }

    }
    
  });
  
});*/

						</script>