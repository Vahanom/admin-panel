
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Subheader-->
						<div class="subheader py-2 py-lg-4 subheader-transparent" id="kt_subheader">
							<div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
								<!--begin::Info-->
								<div class="d-flex align-items-center flex-wrap mr-1">
									<!--begin::Heading-->
									<div class="d-flex flex-column">
										<!--begin::Title-->
										<h2 class="text-white font-weight-bold my-2 mr-5">Add Questions</h2>
										<!--end::Title-->
										<!--begin::Breadcrumb-->
										<div class="d-flex align-items-center font-weight-bold my-2">
											<!--begin::Item-->
											<a href="#" class="opacity-75 hover-opacity-100">
												<i class="flaticon2-shelter text-white icon-1x"></i>
											</a>
											
											<!--end::Item-->
											<!--begin::Item-->
											<span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
											<a href="<?php echo base_url('admin/Dashboard') ?>" class="text-white text-hover-white opacity-75 hover-opacity-100">Dashboard</a>
											<!--end::Item-->
											<!--begin::Item-->
											<span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
											<a href="<?php echo base_url('admin/Feedque') ?>" class="text-white text-hover-white opacity-75 hover-opacity-100">Feedback Questions</a>
											<!--end::Item-->
											<!--begin::Item-->
											<!--end::Item-->
											<!--begin::Item-->
											<span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
											<a href="" class="text-white text-hover-white opacity-75 hover-opacity-100">Add Feedback Questions</a>
											<!--end::Item-->
										</div>
										<!--end::Breadcrumb-->
									</div>

									<!--end::Heading-->
								</div>
								<!--end::Info-->
							</div>
						</div>
						<!--end::Subheader-->
						<!--begin::Entry-->
						<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
							<div class="container">
								<div class="row">
									<div class="col-lg-12">
										<!--begin::Card-->
										<div class="card card-custom gutter-b example example-compact">
											<div class="card-header">
												<h3 class="card-title">Add Questions</h3>
											</div>
											<!--begin::Form-->
											<form class="form" method="post" action="<?php echo  base_url('admin/Feedque/add'); ?>" enctype="multipart/form-data" >
												<div class="card-body">
													<div class="form-group row">
														<div class="col-lg-4">
															<label>Checkpoint Category:</label>
															<select class="form-control" name="que_category" id="que_category" onchange="sub_cat_menu()" required>
															<?php if(isset($cat)) {
															?>
															<option value="">--Select Checkpoint Category--</option>
															<?php 
																$cnt=1; foreach($cat as $row){
															?>
																<option value="<?php echo $row->cat_id; ?>"><?php echo $row->cat_title; ?></option>
															<?php } 
															?>
															<?php
														}?>
															</select>
                                                        
														</div>
														<div class="col-lg-1 text-center">
															<br><a href="javascript:void(0);" onclick="cat_showEditForm();" class=" btn btn-primary" title="Add Checkpoint Category"><i class="fa fa-plus"></i></a>
														</div>
														<div class="col-lg-1 text-left">
															<br><a href="javascript:void(0);" onclick="cat_showDeleteForm();" class=" btn btn-danger" title="Delete Checkpoint Category"><i class="fa fa-trash"></i></a>
														</div>
														<div class="col-lg-4">
															<label>Checkpoint Sub Category:</label>
															<select name="que_sub_category" class="form-control" id="que_sub_category" required>
															<option value="">---Checkpoint Sub Category---</option>
															</select>
                                                        
														</div>
														<div class="col-lg-1 text-center">
															<br><a href="javascript:void(0);" onclick="sub_cat_showEditForm();" class=" btn btn-primary" title="Add Checkpoint Sub Category"><i class="fa fa-plus"></i></a>
														</div>
														<div class="col-lg-1 text-left">
															<br><a href="javascript:void(0);" onclick="sub_cat_showDeleteForm();" class=" btn btn-danger" title="Delete Checkpoint Sub Category"><i class="fa fa-trash"></i></a>
														</div>
														
														<div class="col-lg-4">
															<label>Status :</label>
															<select required name="status" class="form-control" id="status">
																<option value="Active">Active</option>
																<option value="InActive">Inactive</option>
                                                        	</select>
														</div><div class="col-lg-1 text-center">
															<!--<br><a href="javascript:void(0);" onclick="sub_cat_showEditForm();" class=" btn btn-primary" title="Add Checkpoint Sub Category"><i class="fa fa-plus"></i></a>-->
														</div>
														<div class="col-lg-1 text-left">
															<!--<br><a href="javascript:void(0);" onclick="sub_cat_showDeleteForm();" class=" btn btn-danger" title="Delete Checkpoint Sub Category"><i class="fa fa-trash"></i></a>-->
														</div>
														<div class="col-lg-4">
															<label>Service Type:</label>
															<select required name="ser_type" class="form-control" id="status">
																<option value="Bus">Bus</option>
																<option value="Four Wheeler">Four Wheeler</option>
																<option value="Two Wheeler">Two Wheeler</option>
																<option value="Truck">Truck</option>
                                                        	</select>
														</div>
														<div class="col-lg-2 text-left"></div>
														<div class="col-lg-6 row" id="clone_div">
															<div class="col-lg-9">
																<label>Enter Question:</label>
																<input type="text" required name="ques[]" class="form-control" id="ques" placeholder="Enter Question " />
	                                                        </div>
	                                                        <div class="col-md-3">
																<div class="form-group" id="add_div"><br>
																<a onclick="addPkg()" href="javascript:void(0);" class="btn btn-primary" title="Add Checkpoint Next Question"><i class="fa fa-plus" aria-hidden="true"></i></a>
															   </div>
																<div class="form-group" style="display:none;" id="minus_div">
																<br><a href="javascript:void(0);" class="btn btn-danger" title="Remove Checkpoint Question"><i class="fa fa-minus" aria-hidden="true" ></i></a>
															   </div>
															</div>
														</div>
                                    
															
												   </div>
												 
												</div>
												<div class="card-footer">
													<div class="row">
														<div class="col-lg-6">
															<button type="submit" class="btn btn-primary mr-2">Save</button>
															<button type="reset" class="btn btn-secondary">Reset</button>
														</div>
													</div>
												</div>
											</form>
											<!--end::Form-->
										</div>
										<!--end::Card-->
									</div>
								</div>
							</div>
							<!--end::Container-->
						</div>
						<!--end::Entry-->
					</div>
					<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Password Reset</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="response">
      	
      </div>
  
    
    </div>
  </div>
</div>
					<!-- <div class="col-lg-6">
															<label>Select Offer:</label>
															<select name="offer_select" id="" class="form-control offer_select"></select>
													</div> -->
                    <!--end::Content-->
<!-- category modal view start -->
<div class="modal fade" id="cat_exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="cat_exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="cat_exampleModalCenterTitle">Add Checkpoint Category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="cat_response">
      	
      </div>
  
    
    </div>
  </div>
</div>
<!-- category modal view end -->
<!-- category Delete modal view start -->
<div class="modal fade" id="cat_delexampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="cat_delexampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="cat_delexampleModalCenterTitle">Delete Checkpoint Category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="cat_delresponse">
      	
      </div>
  
    
    </div>
  </div>
</div>
<!-- category Delete modal view end -->
<!-- category Delete modal view start -->
<div class="modal fade" id="sub_cat_delexampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="sub_cat_delexampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="sub_cat_delexampleModalCenterTitle">Delete Checkpoint Sub Category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="sub_cat_delresponse">
      	
      </div>
  
    
    </div>
  </div>
</div>
<!-- category Delete modal view end -->
<!-- Sub category modal view start -->
<div class="modal fade" id="sub_cat_exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="sub_cat_exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="sub_cat_exampleModalCenterTitle">Add Checkpoint Sub Category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="sub_cat_response">
      	
      </div>
  
    
    </div>
  </div>
</div>
<!-- Sub category modal view end -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>  
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script>
	var cloneCount = 1;
	function addPkg(){
    	var count = cloneCount++;
    	$("#clone_div").clone().attr('id', 'clone_div'+ count).insertAfter("#clone_div");
    	$('#clone_div'+ count).find('#add_div').css('display','none');
    	$('#clone_div'+ count).find('#minus_div').css('display','block');
    	$('#clone_div'+ count).find('#minus_div').find('a').attr('onclick','minusPkg(\'clone_div'+ count +'\')');
	}
	function minusPkg(div){
    $('#'+div).remove();
}

 function sub_cat_menu(){
  var que_category = $('#que_category').val(); //alert(que_category);
  if(que_category != '')
  {
   $.ajax({
    url:websiteurl + 'Feedque/loadfeedCategory',
    method:"POST",
    data:{que_category:que_category},
    success:function(data)
    {
     $('#que_sub_category').html(data);
     //$('#city').html('<option value="">Select City</option>');
    }
   });
  }
  else
  {
   $('#que_sub_category').html('<option value="">Select Checkpoint Sub Category</option>');
   //$('#city').html('<option value="">Select City</option>');
  }
 };
function cat_showDeleteForm(){
            $.ajax({
                type: "POST", 
				url: websiteurl + 'Feedque/getCatDeleteModel',
				data: {
				"id": ""
				},
                success: function(response) {
                 $("#cat_delexampleModalCenter").modal('show');
                   
                 $("#cat_delresponse").html(response);
                   
                }
            });
}
function sub_cat_showDeleteForm(){
            $.ajax({
                type: "POST", 
				url: websiteurl + 'Feedque/getsubCatDeleteModel',
				data: {
				"id": ""
				},
                success: function(response) {
                 $("#sub_cat_delexampleModalCenter").modal('show');
                   
                 $("#sub_cat_delresponse").html(response);
                   
                }
            });
}
function cat_showEditForm(){
            $.ajax({
                type: "POST", 
				url: websiteurl + 'Feedque/getCatModel',
				data: {
				"id": ""
				},
                success: function(response) {
                 $("#cat_exampleModalCenter").modal('show');
                   
                 $("#cat_response").html(response);
                   
                }
            });
}
function sub_cat_showEditForm(){
            $.ajax({
                type: "POST", 
				url: websiteurl + 'Feedque/getSubCatModel',
				data: {
				"id": ""
				},
                success: function(response) {
                 $("#sub_cat_exampleModalCenter").modal('show');
                   
                 $("#sub_cat_response").html(response);
                   
                }
            });
}
</script>