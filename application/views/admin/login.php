
<!DOCTYPE html>
<html lang="en">
	<!--begin::Head-->
	<head>
		<meta charset="utf-8" />
		<title>Vahanom | Login </title>
		<meta name="description" content="Login page example" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->
		<!--begin::Page Custom Styles(used by this page)-->
		<!--end::Page Custom Styles-->
		<!--begin::Global Theme Styles(used by all pages)-->
		<?php echo link_tag('assets/css/style.bundle.css?v=7.0.4'); ?>
		<?php echo link_tag('assets/plugins/global/plugins.bundle.css?v=7.0.4'); ?>
		<?php echo link_tag('assets/plugins/custom/prismjs/prismjs.bundle.css?v=7.0.4'); ?>
		<!--<script src="https://www.google.com/recaptcha/api.js"></script>-->
	<!--end::Global Theme Styles-->
		<!--begin::Layout Themes(used by all pages)-->
		<!--end::Layout Themes-->
		
		<?php echo link_tag('assets/media/logos/favicon.ico'); ?>
	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" style="background-image: url(assets/media/bg/bg-10.jpg)" class="quick-panel-right demo-panel-right offcanvas-right header-fixed subheader-enabled page-loading">
		<!--begin::Main-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Login-->
			<div class="login login-2 login-signin-on d-flex flex-column flex-lg-row flex-column-fluid bg-white" id="kt_login">
				<!--begin::Aside-->
				<div class="login-aside order-2 order-lg-1 d-flex flex-row-auto position-relative overflow-hidden">
					<!--begin: Aside Container-->
					<div class="d-flex flex-column-fluid flex-column justify-content-between py-9 px-7 py-lg-13 px-lg-35">
						<!--begin::Logo-->
						<a href="#" class="text-center pt-2">
							<!-- <img src="assets/media/logos/logo.png" class="max-h-75px" alt="" /> -->
							<h1><img style="width:60%;" src="<?php echo base_url();?>assets/media/logos/logo.png" alt="" /></h1>
					
						</a>
						<!--end::Logo-->
						<!--begin::Aside body-->
						<div class="d-flex flex-column-fluid flex-column flex-center">
							<!--begin::Signin-->
							<div class="login-form login-signin py-11">
								<!--begin::Form-->
								

<div id="login">
<p style="color:red; font-size:18px; display:none;" id="validatation" align="center">Username Or Password <br> Is Invalid</p>


							   <form method="post" id="formvalidate" action="<?php echo base_url();?>admin/login/login">
									<!--begin::Title-->
							
									<div class="text-center pb-8">
										<h2 class="font-weight-bolder text-dark font-size-h2 font-size-h1-lg">Sign In</h2>
										<!-- <span class="text-muted font-weight-bold font-size-h4">Or -->
										<!-- <a href="" class="text-primary font-weight-bolder" id="kt_login_signup">Create An Account</a></span> -->
									</div>
									<!--end::Title-->
									<!--begin::Form group-->
									<div class="form-group">
										<label class="font-size-h6 font-weight-bolder text-dark">Username</label>
										
											<?php echo form_input(['name'=>'emailid','id'=>'emailid','class'=>'form-control form-control-solid h-auto py-7 px-6 rounded-lg','autofocus'=>'autofocus','value'=>set_value('emailid')]);?>

											<?php echo form_error('emailid',"<span style='color:red'>","</span>");?>
										</div>
									<!--end::Form group-->
									<!--begin::Form group-->
									
										<div class="form-group">
										<label class="font-size-h6 font-weight-bolder text-dark pt-5">Password</label>										
											<?php echo form_password(['name'=>'password','id'=>'password','class'=>'form-control form-control-solid h-auto py-7 px-6 rounded-lg','autofocus'=>'autofocus','value'=>set_value('password')]);?>
											<?php echo form_error('password',"<div style='color:red'>","</div>");?>
										</div>
										<div class="d-flex justify-content-between mt-n5">
											
											<a href="javascript:;" onclick="return theFunction();" class="text-primary font-size-h6 font-weight-bolder text-hover-primary pt-5" id="kt_login_forgot">Forgot Password ?</a>
										</div>

										
   
    <!--<div class="g-recaptcha" data-sitekey="<?php echo $this->config->item('google_key') ?>"></div> -->
									<!--end::Form group-->
									<!--begin::Action-->
									<div class="text-center pt-2">
										<button id="kt_login_signin_submit" class="btn btn-dark font-weight-bolder font-size-h6 px-8 py-4 my-3">Sign In</button>
									</div>
									
									<!--end::Action-->
								</form>
								</div>
								<!--end::Form-->
							</div>
							<!--end::Signin-->
							<!--begin::Signup-->
							
							<!--end::Signup-->
							<!--begin::Forgot-->
							
							<div id="forgot" style="display:none;">
							<div class="login-form login-forgot pt-11">
								<!--begin::Form-->
								<form method="post"  action="<?php echo base_url();?>admin/login/forget">
									<!--begin::Title--> 
									<div id="forgotpassword">
									<div class="text-center pb-8">
										<h2 class="font-weight-bolder text-dark font-size-h2 font-size-h1-lg">Forgotten Password ?</h2>
										<p class="text-muted font-weight-bold font-size-h4">Enter your Username to reset your password</p>
									</div>
									<?php if ($this->session->flashdata('error1')) { ?>
										<p style="color:red; font-size:18px;" align="center"><?php echo $this->session->flashdata('error1');?></p>
										<?php } ?>
									<!--end::Title-->
									<!--begin::Form group-->
									<div class="form-group">
										<input class="form-control form-control-solid h-auto py-7 px-6 rounded-lg font-size-h6" type="email" placeholder="Username" name="emailforget" id="emailforget" autocomplete="off" />
										<?php echo form_error('emailforget',"<div style='color:red'>","</div>");?>
									</div>
									
									<!--end::Form group-->
									<!--begin::Form group-->
									<div class="form-group d-flex flex-wrap flex-center pb-lg-0 pb-3">
										<button type="button"class="btn btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mx-4" onclick="displayotp();">Submit</button>
									<a href="<?php echo base_url('admin/Login'); ?>"><button type="button"  class="btn btn-light-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mx-4">Cancel</button></a>
									</div>
									</div>
									<!--end::Form group-->
									<div id="enterotp" style="display:none;">
									<div class="text-center pb-8">
										<h2 class="font-weight-bolder text-dark font-size-h2 font-size-h1-lg">Enter Otp</h2>
										<!-- <p class="text-muted font-weight-bold font-size-h4">Your Otp IS <span id="otpdatabase"></span></p> -->
										<input type="text" readonly id="otpdatabase" name="otpdatabase">
									</div>
											<!--end::Title-->
									<!--begin::Form group-->
									<div class="form-group">
										<input class="form-control form-control-solid h-auto py-7 px-6 rounded-lg font-size-h6" type="text" placeholder="Enter OTP" name="otp" id="otp" autocomplete="off" />
										<?php echo form_error('emailforget',"<div style='color:red'>","</div>");?>
									</div>
									
									<!--end::Form group-->
									<!--begin::Form group-->
									<div class="form-group d-flex flex-wrap flex-center pb-lg-0 pb-3">
										<button type="button" class="btn btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mx-4"  onclick="checkotp();">Submit</button>
										<a href="<?php echo base_url('admin/Login'); ?>"><button type="button"  class="btn btn-light-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mx-4">Cancel</button></a>
									</div>
									</div>
<div id="confirmpassword" style="display:none;">

<div class="form-group">
										<label class="font-size-h6 font-weight-bolder text-dark">New Password</label>
										
<?php echo form_password(['name'=>'newpassword','id'=>'newpassword','class'=>'form-control form-control-solid h-auto py-7 px-6 rounded-lg font-size-h6','autofocus'=>'autofocus']);?>

<?php echo form_error('emailid',"<span style='color:red'>","</span>");?>
										</div>
									<!--end::Form group-->
									<!--begin::Form group-->
									<div class="form-group">
										<div class="d-flex justify-content-between mt-n5">
											<label class="font-size-h6 font-weight-bolder text-dark pt-5">Confirm Password</label>
										</div>
										
<?php echo form_password(['name'=>'confirmpasswordfield','id'=>'confirmpasswordfield','class'=>'form-control form-control-solid h-auto py-7 px-6 rounded-lg font-size-h6','autofocus'=>'autofocus']);?>
<?php echo form_error('password',"<div style='color:red'>","</div>");?>
										</div>
										<div class="form-group d-flex flex-wrap flex-center pb-lg-0 pb-3">
										<button type="button"  class="btn btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mx-4"  onclick="updatepassword();">Submit</button>
										<button type="button" class="btn btn-light-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mx-4">Cancel</button>
									</div>
</div>
</div>
			</form>
								<!--end::Form-->
							</div>
							<!--end::Forgot-->
						</div>
						<!--end::Aside body-->
						<!--begin: Aside footer for desktop-->
					
						<!--end: Aside footer for desktop-->
					</div>
					<!--end: Aside Container-->
				</div>
				<!--begin::Aside-->
				<!--begin::Content-->
				<div class="content order-1 order-lg-2 d-flex flex-column w-100 pb-0" style="background-image: url('<?php echo base_url();?>assets/media/logos/vahanomdash.jpg');height: 100%;

/* Center and scale the image nicely */
background-position: center;
background-repeat: no-repeat;
background-size: cover;">
					<!--begin::Title-->
					<!-- <div class="d-flex flex-column justify-content-center text-center pt-lg-40 pt-md-5 pt-sm-5 px-lg-0 pt-5 px-7"> -->
						<!-- <h3 class="display4 font-weight-bolder my-7 text-dark" style="color: #986923;">Amazing Wireframes</h3>
						<p class="font-weight-bolder font-size-h2-md font-size-lg text-dark opacity-70">User Experience &amp; Interface Design, Product Strategy
						<br />Web Application SaaS Solutions</p> -->
					<!-- </div> -->
					<!--end::Title-->
					<!--begin::Image-->
					<!--end::Image-->
				</div>
				<!--end::Content-->
			</div>
			<!--end::Login-->
		</div>
		<!--end::Main-->
		<script>var HOST_URL = "https://keenthemes.com/metronic/tools/preview";</script>
		<!--begin::Global Config(global config for global JS scripts)-->
		<script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#6993FF", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#F3F6F9", "dark": "#212121" }, "light": { "white": "#ffffff", "primary": "#E1E9FF", "secondary": "#ECF0F3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#212121", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#ECF0F3", "gray-300": "#E5EAEE", "gray-400": "#D6D6E0", "gray-500": "#B5B5C3", "gray-600": "#80808F", "gray-700": "#464E5F", "gray-800": "#1B283F", "gray-900": "#212121" } }, "font-family": "Poppins" };</script>
		<!--end::Global Config-->
		<!--begin::Global Theme Bundle(used by all pages)-->
		<script src="<?php echo base_url('assets/plugins/global/plugins.bundle.js?v=7.0.4'); ?>"></script>


		<script src="<?php echo base_url('assets/plugins/custom/prismjs/prismjs.bundle.js?v=7.0.4'); ?>"></script>
		
		<script src="<?php echo base_url('assets/js/scripts.bundle.js?v=7.0.4'); ?>"></script>



		<!--end::Page Scripts-->
	</body>
	<script>
	  
	function theFunction(){
		document.getElementById('forgot').style.display = "block";
				document.getElementById('login').style.display = "none";
	}
        $(document).ready(function (){
            $("#formvalidate").submit(function (e){
                e.preventDefault();
				
  var emailid=document.getElementById('emailid').value;
  var password=document.getElementById('password').value;
  if(emailid == "") {
    
    alert("Please Enter Username");
    return false;
  }

  if(password == "") {
    
    alert("Please Enter Password");
    return false;
  }
//   if(grecaptcha.getResponse() == "") {
    
//     alert("Please Check The Captcha");
//     return false;
//   }
// ,captacharesponse : grecaptcha.getResponse() 

                var url = $(this).attr('action');
                var method = $(this).attr('method');
                var data = $(this).serialize();

				$.ajax({
        /* context: form, //not sure you need that */
        type: method,
        url: url,
        data: data, /* turn the form data into an array */
        dataType: "text",
        success: function(response){  
			// alert(response);
                   if(response == 1)
                    {
						window.location.href='<?php echo base_url('admin/Dashboard') ?>';
                    }
                    else
                    {
						alert('Please Enter Valid Details');
				// 		grecaptcha.reset();


						$('#formvalidate')[0].reset();
						
				document.getElementById('validatation').style.display = "block";
                    } 
        }
    });
               
            });
             
          
             


        });

		function displayotp(){
	var emailforget=document.getElementById('emailforget').value;
	
	if(emailforget=='')
	{
		alert('Please Enter Username');
				return false;
	}
	
 $.ajax({
        url: '<?php echo site_url('admin/login/forget'); ?>',
        type: 'POST',
        data: {
            key: emailforget
        },
        dataType: 'json',
        success: function(data) {
	
			if(data==2){
				alert('Please Enter Valid Username');
				
			}else{
				
				alert('OTP Send To Mail');
				  
				document.getElementById('enterotp').style.display = "block";
				document.getElementById('forgotpassword').style.display = "none";
				document.getElementById('otpdatabase').value = data;
			}
        }
    });
}




function checkotp(){
	var otpdatabase=document.getElementById('otpdatabase').value;
	var otp=document.getElementById('otp').value;
	
	if(emailforget=='')
	{
		alert('Please Enter Username');
				return false;
	}
	else if(otpdatabase ==otp)

{
	alert('OTP Match');
	document.getElementById('enterotp').style.display = "none";
	document.getElementById('confirmpassword').style.display = "block";
	
}
else{
	alert('OTP Does Not Match');
}
}

function updatepassword(){
	var confirmpassword=document.getElementById('confirmpasswordfield').value;
	var newpassword=document.getElementById('newpassword').value;

	if(newpassword=='')
	{
		alert('Please Enter New Password');
				return false;
	}

	if(confirmpassword=='')
	{
		alert('Please Enter Confirm Password');
				return false;
	}	
	if(confirmpassword == newpassword)
	{
	
		$.ajax({
			url: '<?php echo site_url('admin/login/updatepassword'); ?>',
        type: 'POST',
        data: {
            key: newpassword
        },
        dataType: 'json',
        success: function(data) {
	
			if(data==2){
				alert('Password Updated Successfully');
				location.reload();

			}
        }
    });


	}	else{
		alert('confirm password and new password are not matching');
	}

}



function removeerror(){
	document.getElementById('uperror').innerHTML='';
}

        </script>
	<!--end::Body-->
</html>